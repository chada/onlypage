# onlypage  

----  
  
- 后端采用playframework 2.2.3版本框架
- 前端采用angularjs1.0框架
- wechat目录----存放微信公众号接口
- 缓存使用play自带缓存
- 文件存储在阿里云服务器上
- log4j日志，动态配置生成文件

## 开发

----  
 
唯页使用Play Framework开发，需要在开发环境下载安装play，配置play的环境变量（环境变量路径不能有空格）。    

### 软件
- IDEA  
- Java  
- [play 2.2.3](https://downloads.typesafe.com/play/2.2.3/play-2.2.3.zip)  
- jetty  
  
### 步骤
1. 下载：  
    ```
        git clone https://gitee.com/chada/onlypage.git`
    ```  
  
2. 初始化开发（命令提示符进入到项目根目录里面运行以下命令）：  
    ```
        play idea  
    ```
  
3. 运行项目，进行开发（代码的改动会自动热部署，类似angular2的开发过程）：  

    ```
        play debug run  
    ```


### 说明
项目启动默认使用9000端口进行访问，第一次访问会触发class编译。


## 部署  

----  
 
打包生成war，部署到jetty的webapps目录下（修改生成后的war名字为ROOT.war）：  

```
 play war  
```
  
## 异常解决  

----  
 
- `Cannot run program "javac": CreateProcess error=2,`  
    如果出现该异常说明本地命令行无法使用javac命令，出现这种情况有可能是java环境没装好或者java环境变量没有配置，请查阅相关资料java配置环境变量。

- `IDEA识别不了项目，项目里面import的java类找不到`  
    需要在项目的目录下执行play idea，初始化idea的相关信息。
  
## 备注  

----  
 
jetty默认使用8080端口
  