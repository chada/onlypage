package controllers

import sbt._
import play.Project._
import com.github.play2war.plugin._

object ApplicationBuild extends Build {

  val appName = "m_share"
  val appVersion = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    javaCore,
    javaJdbc,
    javaEbean,
    cache,
    //fastJson
    "com.alibaba" % "fastjson" % "1.2.7",
    "mysql" % "mysql-connector-java" % "5.1.18",
    "log4j" % "log4j" % "1.2.17",
    "com.thoughtworks.xstream" % "xstream" % "1.4.8",
    "dom4j" % "dom4j" % "1.6.1",
    //wechat
    "commons-codec" % "commons-codec" % "1.9",
    "com.aliyun.oss" % "aliyun-sdk-oss" % "2.0.2",

    //qq
    "commons-httpclient" % "commons-httpclient" % "3.0.1",
    //wechat withdrawDeposit
    "org.apache.httpcomponents" % "httpclient" % "4.4.1",
    //datetimepicker
    "org.webjars" % "bootstrap-datepicker" % "1.6.1",
    //xml to json
    "org.jsoup" % "jsoup" % "1.7.3"

  )

  val main = play.Project(appName, appVersion, appDependencies)
    .settings(Play2WarPlugin.play2WarSettings: _*)
    .settings(
      Play2WarKeys.servletVersion := "2.5"
    )
}
