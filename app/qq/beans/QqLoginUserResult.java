package qq.beans;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by wangh on 2016/11/22.
 */
public class QqLoginUserResult extends QqResult {
    public String openid;
    public String nickname;
    public int gender;
    public String province;
    public String city;
    public String figureurl_qq_1;
    public String figureurl_qq_2;

    public QqLoginUserResult(){

    }

    public QqLoginUserResult(JSONObject err_msg){
        super(err_msg);
    }

    public static QqLoginUserResult create(JSONObject err_msg){

        return JSON.parseObject(err_msg.toJSONString(), QqLoginUserResult.class);
    }

}
