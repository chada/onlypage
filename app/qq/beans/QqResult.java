package qq.beans;

import com.alibaba.fastjson.JSONObject;
import qq.QqGlobalReturnCode;

/**
 * Created by wangh on 2016/11/22.
 */
public class QqResult {
    public int ret = 0;
    public String msg = "正确返回";
    public String msg_zn = "操作成功";

    public String data;

    private Boolean success = true;

    public Boolean isSuccess(){
        return this.success;
    }

    public QqResult(){

    }

    public QqResult(JSONObject err_msg){
        this.ret = err_msg.getInteger("ret");
        this.data = err_msg.toJSONString();
        if(ret == 0){

        }else{
            this.success = false;
            this.msg = err_msg.getString("msg");
            this.msg_zn = err_msg.getString("msg_zn");
            this.msg_zn = new QqGlobalReturnCode(ret,msg).getMsg();
        }
    }

}
