package qq.beans;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by wangh on 2016/11/22.
 */
public class QqLoginResult extends QqResult{
    public String access_token;         //授权令牌，Access_Token
    public int expires_in;               //该access token的有效期，单位为秒
    public String refresh_token;         //在授权自动续期步骤中，获取新的Access_Token时需要提供的参数

    public QqLoginResult(){

    }
    public QqLoginResult(JSONObject err_msg){
        super(err_msg);
    }
    public static  QqLoginResult create(JSONObject err_msg)  {
            return JSON.parseObject(err_msg.toJSONString(), QqLoginResult.class);
            //json.parseObject(参数1，参数2)，参数1是一个json字符串，参数2是一个类，目的是将json字符串转换成对象

    }
}
