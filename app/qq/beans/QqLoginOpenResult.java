package qq.beans;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by wangh on 2016/11/22.
 */
public class QqLoginOpenResult extends QqResult {
    public String openid;
    public String client_id;

    public QqLoginOpenResult(){

    }
    public QqLoginOpenResult(JSONObject err_msg){
        super(err_msg);
    }

    public static QqLoginOpenResult create(JSONObject err_msg){
        return JSON.parseObject(err_msg.toJSONString(), QqLoginOpenResult.class);

    }
}
