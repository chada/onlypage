package qq;

/**
 * Created by wangh on 2016/11/22.
 */
public class QqGlobalReturnCode {
    public int ret;
    public String msg;

    public QqGlobalReturnCode(){

    }

    public QqGlobalReturnCode(int ret,String msg){
        this.ret = ret;
        this.msg = msg;
    }

    public String getMsg(){
        String msg = "";

        switch (ret){
            case 0:
                msg = "正确返回";
                break;
            case 1002:
                msg = "请先登录";
                break;
            case 100000:
                msg = "缺少参数response_type或response_type非法";
                break;
            case 100001:
                msg = "缺少参数client_id";
                break;
            case 100002:
                msg = "缺少参数client_secret";
                break;
            case 100003:
                msg = "http head中缺少Authorization";
                break;
            case 100004:
                msg = "缺少参数grant_type或grant_type非法";
                break;
            case 100005:
                msg = "缺少参数code";
                break;
            case 100006:
                msg = "缺少refresh token";
                break;
            case 100007:
                msg = "缺少access token";
                break;
            case 100008:
                msg = "该appid不存在";
                break;
            case 100009:
                msg = "client_secret（即appkey）非法";
                break;
            case 100010:
                msg = "回调地址不合法";
                break;
            case 100011:
                msg = "APP不处于上线状态";
                break;
            case 100012:
                msg = "HTTP请求非post方式";
                break;
            case 100013:
                msg = "access token非法";
                break;
            case 100014:
                msg = "access token过期";
                break;
            case 100015:
                msg = "access token废除";
                break;
            case 100016:
                msg = "access token验证失败";
                break;
            case 100017:
                msg = "获取appid失败";
                break;
            case 100018:
                msg = "获取code值失败";
                break;
            case 100019:
                msg = "用code换取access token值失败";
                break;
            case 100020:
                msg = "code被重复使用";
                break;
            case 100021:
                msg = "获取access token值失败";
                break;
            case 100022:
                msg = "获取refresh token值失败";
                break;
            case 100023:
                msg = "获取app具有的权限列表失败";
                break;
            case 100024:
                msg = "获取某OpenID对某appid的权限列表失败";
                break;
            case 100025:
                msg = "获取全量api信息、全量分组信息";
                break;
            case 100026:
                msg = "设置用户对某app授权api列表失败";
                break;
            case 100027:
                msg = "设置用户对某app授权时间失败";
                break;
            case 100028:
                msg = "缺少参数which";
                break;
            case 100029:
                msg = "错误的http请求";
                break;
            case 100030:
                msg = "用户没有对该api进行授权，或用户在腾讯侧删除了该api的权限";
                break;
            case 100031:
                msg = "第三方应用没有对该api操作的权限";
                break;
            case 6900:
                msg = "请求参数格式错误，具体参见返回信息中的msg字段";
                break;
            case 6001:
                msg = "拉取code失败";
                break;
            case 6081:
                msg = "client_id非法";
                break;
            case 6201:
                msg = "系统内部错误";
                break;
            case 6202:
                msg = "系统内部错误";
                break;
            case 6901:
                msg = "client_id暂停使用";
                break;
            case 6902:
                msg = "app信息获取失败";
                break;
            case 6903:
                msg = "获取API授权信息失败";
                break;
            case 6904:
                msg = "执行API授权失败";
                break;
            case 6905:
                msg = "参数redirect_uri无法解析出主域名";
                break;
            case 6906:
                msg = "参数redirect_uri与注册域名不是同一个网站";
                break;
            case 7900:
                msg = "请求参数格式错误，具体参见返回信息中的msg字段";
                break;
            case 7001:
                msg = "换取access token失败";
                break;
            case 7016:
                msg = "app secret长度非法";
                break;
            case 7018:
                msg = "非法的app secret";
                break;
            case 7019:
                msg = "非法的code";
                break;
            case 7020:
                msg = "code已过期";
                break;
            case 7021:
                msg = "code已经被用过";
                break;
            case 7081:
                msg = "client_id非法";
                break;
            case 7201:
                msg = "系统内部错误";
                break;
            case 7202:
                msg = "系统内部错误";
                break;
            case 7901:
                msg = "client_id暂停使用";
                break;
            case 7902:
                msg = "app信息获取失败";
                break;
            case 7905:
                msg = "参数redirect_uri无法解析出主域名";
                break;
            case 7906:
                msg = "参数redirect_uri与注册域名不是同一个网站";
                break;
            case 8900:
                msg = "请求参数格式错误，具体参见返回信息中的msg字段";
                break;
            case 8064:
                msg = "系统内部错误";
                break;
            case 8065:
                msg = "系统内部错误";
                break;
            case 8066:
                msg = "系统内部错误";
                break;
            case 8067:
                msg = "系统内部错误";
                break;
            case 8081:
                msg = "client_id非法";
                break;
            case 8201:
                msg = "系统内部错误";
                break;
            case 8202:
                msg = "系统内部错误";
                break;
            case 8901:
                msg = "client_id暂停使用";
                break;
            case 8902:
                msg = "app信息获取失败";
                break;
            case 8903:
                msg = "获取API授权信息失败";
                break;
            case 8904:
                msg = "执行API授权失败";
                break;
            case 8905:
                msg = "参数redirect_uri无法解析出主域名";
                break;
            case 8906:
                msg = "参数redirect_uri与注册域名不是同一个网站";
                break;
            case 9900:
                msg = "请求参数格式错误，具体参见返回信息中的msg字段";
                break;
            case 9016:
                msg = "access token无效";
                break;
            case 9017:
                msg = "access token已过期";
                break;
            case 9018:
                msg = "access token已废除";
                break;
            case 9094:
                msg = "access token非法";
                break;
            case 9201:
                msg = "系统内部错误";
                break;
            case 9202:
                msg = "系统内部错误";
                break;
            default:
                msg = "未知";
                break;

        }
        return msg;
    }

}
