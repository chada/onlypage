package qq.service;

import qq.beans.QqLoginOpenResult;
import qq.beans.QqLoginResult;
import qq.beans.QqLoginUserResult;
import qq.util.QqLoginUtil;
import util.Common;

import java.io.IOException;

/**
 * Created by wangh on 2016/11/22.
 */
public class QqLoginService {
    //step1.获取Authorization Code
    public static String getUrl(String appid,String redirect_uri,String state){
        return QqLoginUtil.getUrl("code", appid, redirect_uri, state, "get_user_info");
    }

    //step2.通过Authorization Code获取Access Token
    public static QqLoginResult getAccessToken(String code) throws IOException{
        return QqLoginUtil.getAccessToken("authorization_code", Common.getStringConfig("app_ID"),
                Common.getStringConfig("app_KEY"),code,Common.getStringConfig("redirect_URI"));
    }

    //step3.使用Access Token来获取用户的OpenID
    public static QqLoginOpenResult getOpenId(String accessToken) throws IOException {
        return  QqLoginUtil.getOpenID(accessToken);
    }

    //step4.使用Access Token以及OpenID来访问和修改用户数据
    public static QqLoginUserResult getUserInfo(String accessToken,String openid) throws IOException {
        return  QqLoginUtil.getUserInfo(accessToken,Common.getStringConfig("app_ID"),openid);
    }
}
