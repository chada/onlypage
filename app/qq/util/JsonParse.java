package qq.util;

import com.alibaba.fastjson.JSON;

import java.util.Map;

/**
 * Created by wangh on 2016/11/23.
 */
public class JsonParse {
    //自己写的JSON解析方法
    public static String getJson(String source){

        if(source.contains("callback")){
            return source.substring(9,source.length()-3) ;
        }else if(source.contains("{") && !source.contains("&")) {
            Map<String,String> map = JSON.parseObject(source, Map.class);
            for(String key :map.keySet()){
                if(key.equals("gender") && map.get(key).equals("男")){
                    map.put("gender","0");
                }
                if(key.equals("gender") && map.get(key).equals("女")){
                    map.put("gender","1");
                }
            }

            return JSON.toJSONString(map);
        } else{
            StringBuffer buffer = new StringBuffer();
            buffer.append("{");
            String[] arr = source.split("&");
            for(int i = 0; i<arr.length; i++){
                String[] ass = arr[i].split("=");
                for(int j = 0; j<ass.length; j++){
                    if(j == 0){
                        buffer.append("\"").append(ass[j]).append("\"").append(":");
                    }
                    if(j == 1 && i != arr.length-1) {
                        buffer.append("\"").append(ass[j]).append("\"").append(",");
                    }
                    if(j == 1 && i == arr.length-1){
                        buffer.append("\"").append(ass[j]).append("\"");
                    }
                }
            }
            buffer.append("}");
            String jsonText = buffer.toString();
            //  JSONObject text = JSON.parseObject(jsonText,JSONObject.class);

            return jsonText;
        }
    }
}
