package qq.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.utils.URLEncodedUtils;
import qq.beans.QqLoginOpenResult;
import qq.beans.QqLoginResult;
import qq.beans.QqLoginUserResult;

import java.io.IOException;
import java.net.URLEncoder;

/**
 * Created by wangh on 2016/11/22.
 */
public class QqLoginUtil {
    /**
     * 参数               是否必须                含义
     * response_type           必须                 授权类型，此值固定为“code”
     * client_id               必须                 申请QQ登录成功后，分配给应用的appid
     * redirect_uri            必须                 成功授权后的回调地址，必须是注册appid时填写的主域名下的地址，建议设置为网站首页或网站的用户中心。注意需要将url进行URLEncode
     * state                   必须                 client端的状态值。用于第三方应用防止CSRF攻击，成功授权后回调时会原样带回。请务必严格按照流程检查用户与state参数状态的绑定
     * scope                   可选                 请求用户授权时向用户显示的可进行授权的列表,如：scope=get_user_info,list_album,upload_pic,do_like,不传则默认请求对接口get_user_info进行授权
     * 可填写的值是API文档中列出的接口，以及一些动作型的授权（目前仅有：do_like），如果要填写多个接口名称，请用逗号隔开
     */


    public static String getUrl(String response_type, String appid, String redirect_uri, String state, String scope) {
        //打开浏览器，访问如下地址
        return "https://graph.qq.com/oauth2.0/authorize?response_type=" + response_type + "&client_id=" + appid + "&redirect_uri=" + URLEncoder.encode(redirect_uri)
                + "&state=" + state + "&scope=" + scope;
        //返回正确结果http://sjj.onlypage.mobi/api/qq/login?code=4BDDBE5BE8C0AEECEC4887BB203FFEDC&state=state
    }


    public static QqLoginResult getAccessToken(String grant_type, String appid, String secret, String code, String redirect_uri) throws IOException {
        //发送请求到如下地址
        String url = "https://graph.qq.com/oauth2.0/token?grant_type=" + grant_type + "&client_id=" + appid + "&client_secret=" +
                secret + "&code=" + code + "&redirect_uri=" + URLEncoder.encode(redirect_uri);
        JSONObject obj = QqHttpClientUtil.urlGetConnect(url);
        QqLoginResult result = QqLoginResult.create(obj);
        return result;
    }

    public static QqLoginOpenResult getOpenID(String accessToken) throws IOException {
        // 发送请求到如下地址
        String urlToken = "https://graph.qq.com/oauth2.0/me?access_token=" + accessToken;
        JSONObject object = QqHttpClientUtil.urlGetConnect(urlToken);
        QqLoginOpenResult result = QqLoginOpenResult.create(object);
        return result;
    }

    public static QqLoginUserResult getUserInfo(String accessToken, String appid, String openid) throws IOException {
        //发送请求到get_user_info的URL
        String getUserUrl = "https://graph.qq.com/user/get_user_info?access_token=" + accessToken + "&oauth_consumer_key=" + appid + "&openid=" + openid;
        JSONObject object = QqHttpClientUtil.urlGetConnect(getUserUrl);
        QqLoginUserResult result = QqLoginUserResult.create(object);
        return result;
    }
}
