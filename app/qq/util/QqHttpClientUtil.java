package qq.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import util.Common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by wangh on 2016/11/22.
 */
public class QqHttpClientUtil {
    private static final String CHARSET = "UTF-8";

    /**
     * get获取url返回结果，返回类型为JSONObject
     */
    public static JSONObject urlGetConnect(String url) throws IOException {
        HttpClient client = new HttpClient();
        GetMethod get = new GetMethod(url);//根据文档要求用get方法获取url的内容
        client.executeMethod(get);
        InputStream ins = get.getResponseBodyAsStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(ins,CHARSET));
        StringBuffer sbf = new StringBuffer();
        String line = null;
        while((line = reader.readLine())!= null){
            sbf.append(line);
        }
        reader.close();
        JSONObject jsonObject = JSON.parseObject(JsonParse.getJson(sbf.toString()), JSONObject.class);//将json字符串转换成JSONObject（对象）类
        return jsonObject;

    }

    /**
     * 获取url的实际页面内容
     */
    public static String getUrlContent(String url)  {
        HttpClient client = new HttpClient();
        GetMethod get = new GetMethod(url);
        String html = "";
        try {
            int statusCode = client.executeMethod(get);
            if(statusCode !=  HttpStatus.SC_OK){
                html = "error";
            }else {
                //读取内容
                byte[] responseBody = get.getResponseBody();
                //处理内容
                html = new String(responseBody);
            }
        }  catch (IOException e) {
            html = "error";
        }
        return html;
    }
}
