package security;

import com.alibaba.fastjson.JSONObject;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.SimpleResult;

import static play.mvc.Controller.session;


/**
 * Created by Administrator on 2016/3/3.
 */
public class ManagerLoginSecuredAction extends Action.Simple {

    //在类或者方法之前加 @With(ManagerLoginSecuredAction.class)
    @Override
    public F.Promise<SimpleResult> call(Http.Context context) throws Throwable {
        if (session("manager") == null) {
            return F.Promise.promise(new F.Function0<SimpleResult>() {
                @Override
                public SimpleResult apply() throws Throwable {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("success", false);
                    jsonObject.put("data", "您还没有登录");
                    return ok(jsonObject.toJSONString());
                }
            });
        } else {
            return delegate.call(context);
        }
    }


}