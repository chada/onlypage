package beans.user.view;

/**
 * Created by Administrator on 2016/12/6.
 */
public class WithdrawDepositMobileInfo {
    public String wechatName;
    public double coin;

    public WithdrawDepositMobileInfo(String wechatName,int coin){
        this.wechatName = wechatName;
        this.coin = (double)coin/100;
    }
}
