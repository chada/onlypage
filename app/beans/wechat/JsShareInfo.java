package beans.wechat;

import com.sun.jndi.toolkit.url.UrlUtil;
import controllers.util.UrlController;
import models.db1.article.B_Article;
import models.db1.task.B_Task;
import models.db1.user.S_User;
import play.mvc.Http;
import util.Common;
import wechat.other.Js.JsManange;

/**
 * Created by Administrator on 2016/12/12.
 */
public class JsShareInfo {
    public String wechatAppAppid = Common.getStringConfig("wechat.app.appid");
    private String wechatAppAppsecret = Common.getStringConfig("wechat.app.appsecret");

    public String signature;
    public String nonceStr;
    public String timestamp;

    // 分享链接
    public String linkUrl;
    // 分享标题
    public String title;
    // 分享图片
    public String imgUrl;
    // 分享描述
    public String desc;
    //是否启用地址信息
    public boolean enableAddressAcquisition=false;

    public JsShareInfo(B_Task task, S_User user, Http.Request request) throws Exception {
        linkUrl = task.getStartUrl(user);
        title = task.title;
//        title = user.getShowName()+"邀请您参与"+task.title;
        imgUrl = task.coverImg;
        desc = task.remark;
        if(task.strategyInfo.strategy!=null){
            enableAddressAcquisition =task.strategyInfo.strategy.existLocationRuleCondition();
        }
        nonceStr = JsManange.getNonceStr();
        timestamp = JsManange.getTimestamp();
        signature = JsManange.getSignature(wechatAppAppid, wechatAppAppsecret, nonceStr, timestamp, UrlController.serverAddress + request.uri());
    }


    public JsShareInfo(B_Article article, Http.Request request) throws Exception {
        linkUrl = article.getWechatShareLink();
        title = article.title;
        imgUrl = article.coverImg == null ? UrlController.logoMiniImg : article.coverImg;
        desc = article.content.length() > 20 ? article.content.substring(20) : article.content;

        nonceStr = JsManange.getNonceStr();
        timestamp = JsManange.getTimestamp();
        signature = JsManange.getSignature(wechatAppAppid, wechatAppAppsecret, nonceStr, timestamp, UrlController.serverAddress + request.uri());
    }

    public JsShareInfo(Http.Request request)throws Exception {
        nonceStr = JsManange.getNonceStr();
        timestamp = JsManange.getTimestamp();
        signature = JsManange.getSignature(wechatAppAppid, wechatAppAppsecret, nonceStr, timestamp, UrlController.serverAddress + request.uri());
    }
}
