package beans.customization;

import models.db1.B_Customization_Html;

/**
 * Created by whk on 2017/8/10.
 */
public class HtmlViewInfo {
    public String title;
    public String html;

    public HtmlViewInfo(B_Customization_Html customizationHtml) {
        this.title = customizationHtml.title;
        this.html = customizationHtml.html;
    }

}
