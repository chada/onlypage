package beans.util.sms;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import services.LuosimaoService;

/**
 * Created by Administrator on 2016/12/14.
 */
public class SendSmsResult {
    public String msg;
    public String error;

    public static SendSmsResult create(JSONObject data){
       return JSON.parseObject(data.toJSONString(), SendSmsResult.class);
    }

    public Boolean isSuccess() {
        if (error.equals("0")) {
            return true;
        } else {
            return false;
        }
    }


    public static SendSmsResult createTest(){
        SendSmsResult sendSmsResult = new SendSmsResult();
        sendSmsResult.error ="0";
        return sendSmsResult;
    }

    public String getErrorMsg() {
        String msg;
        if (error.equals("-40")) {
            msg = "电话号码不正确";
        } else {
            msg = "发送失败";
        }
        return msg;
    }

}
