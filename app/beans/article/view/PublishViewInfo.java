package beans.article.view;

import models.db1.article.B_Article;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2016/12/1.
 */
public class PublishViewInfo {
    public String title;
    public String content;
    public String editedTime;

    public String useruuid;
    public String username;
    public String avatar;
    public int count;


    public PublishViewInfo(B_Article article) {
        this.title = article.title;
        this.content = article.content;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.editedTime = sdf.format(article.editedTime);

        this.useruuid = article.owner.uuid;
        this.username = article.owner.getShowName();
        this.avatar = article.owner.getShowAvatar();
        this.count = article.getVisitCount();
    }
}
