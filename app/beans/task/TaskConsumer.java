package beans.task;

import beans.task.strategy.StrategyTriggerEvent;
import models.db1.task.B_Task;
import models.db1.task.strategy.B_Task_Strategy_Rule;
import models.db1.user.S_User;
import redis.clients.jedis.Jedis;

/**
 * Created by whk on 2017/11/16.
 */
public class TaskConsumer implements Runnable {
    private StrategyTriggerEvent strategyTriggerEvent;
    private S_User user;
    private String remoteAddress;
    private int taskId;

//    public TaskConsumer() {
//
//    }

    public TaskConsumer(int taskId, StrategyTriggerEvent strategyTriggerEvent, S_User user, String remoteAddress) {
        this.taskId = taskId;
        this.strategyTriggerEvent = strategyTriggerEvent;
        this.user = user;
        this.remoteAddress = remoteAddress;
    }

    @Override
    public void run() {
//        B_Task task = B_Task.find.byId(taskId);
        try {
            B_Task.checkout(taskId,user, strategyTriggerEvent, remoteAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        while (true) {
//            Jedis jedis = new Jedis("localhost");
//            String result = jedis.rpop("task");
//            if (result == null) {
//                try {
//                    Thread.sleep(3000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                String results[] = result.split("_");
//                int userId = Integer.valueOf(results[0]);
//                int taskId = Integer.valueOf(results[1]);
//                String remoteAddress = results[2];
//                String eventTrigger = results[3];
//                int time = Integer.valueOf(results[4]);
//
//                B_Task_Strategy_Rule.TriggerEvent triggerEvent = B_Task_Strategy_Rule.stringConvertTriggerEvent(eventTrigger);
//                B_Task task = B_Task.find.byId(taskId);
//                S_User user = S_User.find.byId(userId);
//
//
//                StrategyTriggerEvent strategyTriggerEvent = new StrategyTriggerEvent(triggerEvent, time);
//                try {
//                    task.checkout(user, strategyTriggerEvent, remoteAddress);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
    }
}
