package beans.task.view;

import controllers.util.UrlController;
import models.db1.task.view.B_Task_View_Tag;

/**
 * Created by Administrator on 2016/12/13.
 */
public class Tag {
    public Boolean isShowTag;
    public Boolean isShowRedPackage;
    public Boolean isShowRedPackageBillBoard;
    public Boolean isShowCreditBillBoard;
    public Boolean isShowUserInput;
    public String withdrawDepositUrl = UrlController.serverAddress+"/api/withdrawDeposit/entrance";

    public Tag(B_Task_View_Tag viewTag) {
        this.isShowTag = viewTag.isShow;
        this.isShowRedPackage = viewTag.isShowRedPackage;
        this.isShowRedPackageBillBoard = viewTag.isShowRedPackageBillBoard;
        this.isShowCreditBillBoard = viewTag.isShowCreditBillBoard;
        this.isShowUserInput = viewTag.isShowUserInput;
    }
}
