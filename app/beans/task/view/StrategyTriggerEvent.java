package beans.task.view;

import com.alibaba.fastjson.JSON;
import models.db1.task.B_Task;

import java.util.List;

/**
 * Created by Administrator on 2016/12/19.
 */
public class StrategyTriggerEvent {
    public int taskId;
    public String title;
    public List<beans.task.strategy.StrategyTriggerEvent> strategyTriggerEventList;

    public StrategyTriggerEvent(B_Task task){
        this.strategyTriggerEventList = task.strategyInfo.strategy.acquireTriggerEventList();
        this.taskId = task.id;
        this.title = task.title;
//        System.out.println(JSON.toJSONString(this.strategyTriggerEventList));
    }
}
