package beans.task.view;

import beans.task.strategy.StrategyTriggerEvent;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import models.db1.article.B_Article;
import models.db1.task.B_Task;
import models.db1.task.promotion.B_Task_Promotion;
import models.db1.user.S_User;

import java.util.List;

/**
 * Created by Administrator on 2016/12/1.
 * 任务手机端展示页面数据对象
 */
public class PublishViewInfo {
    public B_Task.DataType dataType;
    public String title;
    public String data;
    public int taskId;
    public B_Task.State taskState;
    //地推任务是否展示信息填写标志
    public Boolean showPromotionInfoSetLogo=false;

    public beans.article.view.PublishViewInfo articleInfo;
    public Tag tag;

    public beans.task.view.StrategyTriggerEvent strategyTriggerEvent;

    public String preExecutantName;
    public String preExecutantUUID;
    public String executantName;
    public String executantUUID;

    public PublishViewInfo(S_User user,S_User preuser, B_Task task) {
        this.title = task.title;
        this.data = task.data;
        this.taskId = task.id;
        this.taskState=task.state;
        if(task.strategyInfo.strategy!=null){
            this.strategyTriggerEvent = new beans.task.view.StrategyTriggerEvent(task);
        }
        if(task.taskType== B_Task.TaskType.promotion&&(task.promotion!=null)&&(task.promotion.groundPromoter!=null)&&(task.promotion.groundPromoter.id.equals(user.id))){
            this.showPromotionInfoSetLogo=true;
        }

        if(user!=null){
            this.executantUUID=user.uuid;
            this.executantName=user.getShowName();
        }

        if(preuser!=null){
            this.preExecutantUUID=preuser.uuid;
            this.preExecutantName=preuser.getShowName();
        }

        this.dataType = task.dataType;
        if (this.dataType.equals(B_Task.DataType.article)) {
            this.articleInfo = new beans.article.view.PublishViewInfo(task.article);
            task.article.addVisitLog(user);
        }


        this.tag = new Tag(task.viewTag);
    }


}
