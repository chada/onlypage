package beans.task;

import beans.task.strategy.StrategyTriggerEvent;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Log;
import models.db1.task.schedule.B_Task_User_Statistics_Data;
import models.db1.task.strategy.B_Task_Strategy_Execute_Log;
import models.db1.task.strategy.B_Task_Strategy_Rule;
import models.db1.user.S_User;
import models.db1.user.coin.B_User_Coin_Log;
import models.db1.wechat.B_Wechat_Withdraw;
import util.Common;
import wechat.pay.WithDrawDepositManage;
import wechat.pay.beans.withdrawDeposit.WithdrawDepositResult;

/**
 * Created by whk on 2017/11/16.
 */
public class TestConsumer implements Runnable {
    private B_Task_Propagation_Log propagationLog;
    private S_User beneficialUser;
    private B_Task task;
    private String ip;

    public TestConsumer(B_Task_Propagation_Log propagationLog, S_User beneficialUser, B_Task task, String ip) {
        this.propagationLog = propagationLog;
        this.beneficialUser = beneficialUser;
        this.task = task;
        this.ip = ip;
    }

    @Override
    public void run() {
        int coin = 100;
        S_User beneficialUser = propagationLog.executant;

        try {
            beneficialUser.account.addCoin(coin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        B_User_Coin_Log.create_TaskAcquisition(task, beneficialUser, coin);

        //统计表记录数据
        B_Task_User_Statistics_Data.get(beneficialUser, task).addCoin(coin);

        //任务总账户标记增加（已分配金额）
        task.strategyInfo.addHasAllocationCoin(coin);

        //打到用户微信账上(如果超过1元，并且用户具有appOpenid)
        if (beneficialUser.wechatInfo.appOpenid != null) {
            String nonce_str = Common.randomString(32).toUpperCase();
            String partner_trade_no = B_Wechat_Withdraw.getOnePartnerTradeNo();
            String openid = beneficialUser.wechatInfo.appOpenid;
            int amount = coin;
            String spbill_create_ip = ip;
            String desc = task.strategyInfo.autoWithdrawDepositMessage == null ? "唯页红包 " + task.strategyInfo.task.title : task.strategyInfo.autoWithdrawDepositMessage;
            desc = desc.length() > 20 ? desc.substring(20) : desc;

            B_Wechat_Withdraw withdraw = B_Wechat_Withdraw.create(beneficialUser, nonce_str, partner_trade_no, openid, amount, desc);
            WithdrawDepositResult result = WithDrawDepositManage.transfers(nonce_str, partner_trade_no, openid, amount, desc, spbill_create_ip);
            if (result.isSuccess()) {
                try {
                    beneficialUser.account.reduceCoin(amount);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                withdraw.success(result.payment_no, result.payment_time);
                B_User_Coin_Log.create_wehcatWitddrawDeposit(beneficialUser, amount, withdraw);
                B_Task_Strategy_Rule rule = B_Task_Strategy_Rule.find.byId(495);
                B_Task_Strategy_Execute_Log.verifyExistAndCreate(propagationLog.executant, propagationLog.preExecutant, task, rule, coin);
            } else {
                withdraw.fail(result.getErrorMsg());
            }
        }
    }
}
