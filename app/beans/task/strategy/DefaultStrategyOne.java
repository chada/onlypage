package beans.task.strategy;

import models.db1.task.strategy.B_Task_Strategy;
import models.db1.task.strategy.B_Task_Strategy_Rule;

import static controllers.BaseController.currentUser;

/**
 * Created by Administrator on 2016/11/30.
 */
public class DefaultStrategyOne {
    //策略id
    public Integer strategyId;

    //开始节点层数
    public Integer startTriggerNodeLevel;

    //结束节点层数
    public Integer endTriggerNodeLevel;

    //奖励类型
    public B_Task_Strategy_Rule.RewardType rewardType;

    //固定奖励值
    public Integer fixedRewardAmount;

    //随机奖励值-最小值
    public Integer minRewardAmount;

    //随机奖励值-最大值
    public Integer maxRewardAmount;


    /**
     * 默认策略1  创建函数
     * 用户进入分享页面阅读内容时即可获得相应金额或积分奖励
     *
     * @param startTriggerNodeLevel 策略开始节点层数，一般值设为1
     * @param endTriggerNodeLevel   策略结束节点层数，若为无穷大则不填
     * @param rewardType            奖励类型：
     *                              fixedamount-固定金额；
     *                              randomamount-随机金额；
     *                              points-积分（选择积分时账户自动选择为唯页账户）
     * @param fixedRewardAmount     固定金额、积分值
     * @param minRewardAmount       随机金额-最小值
     * @param maxRewardAmount       随机金额-最大值
     * @return 生成的策略对象
     */
    public static B_Task_Strategy createDefaultStrategyOne(Integer startTriggerNodeLevel, Integer endTriggerNodeLevel, B_Task_Strategy_Rule.RewardType rewardType,
                                                           Integer fixedRewardAmount, Integer minRewardAmount, Integer maxRewardAmount) {

        //strategy设置
        B_Task_Strategy strategy = new B_Task_Strategy();
        strategy.title = "默认策略1";
        strategy.strategyType = B_Task_Strategy.StrategyType.defaultStrategyOne;
        strategy.remark = "用户进入分享页面阅读内容时即可获得相应金额或积分奖励";
        strategy.owner = currentUser();
        strategy.save();

        //rule设置
        B_Task_Strategy_Rule taskStrategyRule = new B_Task_Strategy_Rule();
        taskStrategyRule.strategy = strategy;
        taskStrategyRule.triggerEvent = B_Task_Strategy_Rule.TriggerEvent.readtime;
        taskStrategyRule.readTime = 15;
        taskStrategyRule.startTriggerNodeLevel = startTriggerNodeLevel;
        taskStrategyRule.endTriggerNodeLevel = endTriggerNodeLevel;
        taskStrategyRule.rewardUserType = B_Task_Strategy_Rule.RewardUserType.self;
        taskStrategyRule.rewardType = rewardType;
        taskStrategyRule.fixedRewardAmount = fixedRewardAmount;
        taskStrategyRule.minRewardAmount = minRewardAmount;
        taskStrategyRule.maxRewardAmount = maxRewardAmount;
        taskStrategyRule.save();

        return strategy;
    }

    /**
     * 默认策略1  编辑函数
     * <p>
     * 策略iI
     *
     * @param startTriggerNodeLevel 策略开始节点层数，一般值设为1
     * @param endTriggerNodeLevel   策略结束节点层数，若为无穷大则不填
     * @param rewardType            奖励类型：
     *                              fixedamount-固定金额；
     *                              randomamount-随机金额；
     *                              points-积分（选择积分时账户自动选择为唯页账户）
     * @param fixedRewardAmount     固定金额、积分值
     * @param minRewardAmount       随机金额-最小值
     * @param maxRewardAmount       随机金额-最大值
     * @return 生成的策略对象， 返回为null则表明策略不存在或者策略下的规则有异常
     */
    public static B_Task_Strategy editDefaultStrategyOne(B_Task_Strategy strategy, Integer startTriggerNodeLevel, Integer endTriggerNodeLevel, B_Task_Strategy_Rule.RewardType rewardType,
                                                         Integer fixedRewardAmount, Integer minRewardAmount, Integer maxRewardAmount) {
        //获取strategy
        if (strategy == null) {
            return null;
        }

        strategy.title = "测试";
        //获取rule
        if (strategy.strategyRuleList.size() > 1 || strategy.strategyRuleList.size() < 1) {
            return null;
        }
        B_Task_Strategy_Rule taskStrategyRule = strategy.strategyRuleList.get(0);

        //更新rule并保存
        taskStrategyRule.startTriggerNodeLevel = startTriggerNodeLevel;
        taskStrategyRule.endTriggerNodeLevel = endTriggerNodeLevel;
        taskStrategyRule.rewardType = rewardType;
        taskStrategyRule.fixedRewardAmount = fixedRewardAmount;
        taskStrategyRule.minRewardAmount = minRewardAmount;
        taskStrategyRule.maxRewardAmount = maxRewardAmount;
        taskStrategyRule.update();
        return strategy;
    }


    /**
     * 根据策略id返回默认策略所需填写的内容
     *
     * @return 返回为null则表明策略不存在或者策略下的规则有异常
     */
    public static DefaultStrategyOne getDefaultStrategyOneByStragetyId(B_Task_Strategy strategy) {
        //获取strategy
        if (strategy == null) {
            return null;
        }

        //获取rule
        if (strategy.strategyRuleList.size() > 1 || strategy.strategyRuleList.size() < 1) {
            return null;
        }
        B_Task_Strategy_Rule taskStrategyRule = strategy.strategyRuleList.get(0);


        DefaultStrategyOne defaultStrategyOne = new DefaultStrategyOne();
        defaultStrategyOne.strategyId = strategy.id;
        defaultStrategyOne.startTriggerNodeLevel = taskStrategyRule.startTriggerNodeLevel;
        defaultStrategyOne.endTriggerNodeLevel = taskStrategyRule.endTriggerNodeLevel;
        defaultStrategyOne.rewardType = taskStrategyRule.rewardType;
        defaultStrategyOne.fixedRewardAmount = taskStrategyRule.fixedRewardAmount;
        defaultStrategyOne.minRewardAmount = taskStrategyRule.minRewardAmount;
        defaultStrategyOne.maxRewardAmount = taskStrategyRule.maxRewardAmount;
        return defaultStrategyOne;
    }
}
