package beans.task.strategy;

import models.db1.task.strategy.B_Task_Strategy_Rule;

/**
 * Created by Administrator on 2016/12/9.
 */
public class StrategyTriggerEvent {
    public B_Task_Strategy_Rule.TriggerEvent triggerEvent;

    //阅读时间
    public Integer readTime;

    public StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent triggerEvent, Integer readTime){
        this.triggerEvent=triggerEvent;
        this.readTime=readTime;
    }


    public StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent triggerEvent){
        this.triggerEvent=triggerEvent;
    }

    public boolean isSameAs(StrategyTriggerEvent comparedStrategyTriggerEvent){
        if(this.triggerEvent.equals(comparedStrategyTriggerEvent.triggerEvent)){
            if(this.readTime==null&&comparedStrategyTriggerEvent.readTime==null){
                return true;
            }
            if(this.readTime!=null&&comparedStrategyTriggerEvent.readTime!=null&&this.readTime.equals(comparedStrategyTriggerEvent.readTime)){
                return true;
            }
        }
        return false;
    }
}
