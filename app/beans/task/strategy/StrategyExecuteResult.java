package beans.task.strategy;

import models.db1.task.strategy.B_Task_Strategy_Rule;
import models.db1.user.S_User;

/**
 * Created by Administrator on 2016/11/29.
 */
public class StrategyExecuteResult {
    //获益用户
    public S_User beneficialUser;

    //给予利益用户
    public S_User giveBenefitUser;

    //生效规则
    public B_Task_Strategy_Rule effectiveRule;

    //积分数量
    public int point=0;

    //奖励金额
    public int rewardAmount=0;

    //结果
    public Boolean success= false;

    //结果信息
    public String message;

    public StrategyExecuteResult(){

    }

    public StrategyExecuteResult(S_User beneficialUser){
        this.beneficialUser=beneficialUser;
    }
}
