package util;

import org.apache.log4j.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Administrator on 2015/5/21.
 */

public class LoggerUtil {

    public enum LoggerState{wechat,task}
    //可根据xxxx.class类名区分不同配置属性，但此处无用（需在配置文件中进行预先配置，此处为动态配置）
    private static org.apache.log4j.Logger logger = Logger.getLogger(LoggerUtil.class);
    private static final Properties properties = new Properties();
    static {
        try {
            //加载配置文件流，并关闭流
            InputStream log4jPropFileStream = Common.getResource("log4j.properties");
            properties.load(log4jPropFileStream);
            log4jPropFileStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void init(LoggerState state){
        String logName;
        switch (state){
            case wechat:logName = "wechat";break;
            case task:logName = "task";break;
            default:logName = "default";break;
        }
        //修改配置文件中的文件存储路径与文件名
        String logFilePath = Common.getStringConfig("log4j.filePath")+"/"+logName+".log";
        properties.setProperty("log4j.appender.Rolling.File", logFilePath);
        PropertyConfigurator.configure(properties);
    }

    //不同等级log
    public static void info(String str,LoggerState state){
        init(state);
        logger.info(str);
    }
    public static void error(String str,LoggerState state){
        init(state);
        logger.error(str);
    }

    public static void debug(String str,LoggerState state){
        init(state);
        logger.debug(str);
    }

    public static void warn(String str,LoggerState state){
        init(state);
        logger.warn(str);
    }

    public static void trace(String str,LoggerState state){
        init(state);
        logger.trace(str);
    }
}
