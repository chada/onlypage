package util.cache;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import play.cache.Cache;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by Administrator on 2016/3/7.
 */
public class PlayCache extends AbstractCache {
    private Date lastClearTime = null;

    @Override
    public void set(String key, Object val, int seconds) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("created_time", new Date());
        jsonObject.put("value", val);
        Cache.set(key, jsonObject.toJSONString(), seconds);
    }

    private boolean isAvailableTime(Date time) {
        if (time == null) {
            return false;
        }
        if (lastClearTime == null) {
            return true;
        }
        return lastClearTime.before(time);
    }

    @Override
    public String get(String key) {
        String cachedStr = (String) Cache.get(key);
        if (cachedStr == null) {
            return null;
        }
        JSONObject jsonObject = JSON.parseObject(cachedStr);
        if (jsonObject == null) {
            return null;
        }
        Date createdTime = jsonObject.getDate("created_time");
        if (isAvailableTime(createdTime)) {
            return (String) jsonObject.get("value");
        } else {
            remove(key);
            return null;
        }
    }

    @Override
    public Future<String> asyncGet(String key) {
        final String val = get(key);
        return new Future<String>() {
            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                return false;
            }

            @Override
            public boolean isCancelled() {
                return false;
            }

            @Override
            public boolean isDone() {
                return false;
            }

            @Override
            public String get() throws InterruptedException, ExecutionException {
                return val;
            }

            @Override
            public String get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
                return val;
            }
        };
    }

    @Override
    public void clearAll() {
        // 记录最后的clear all时间,如果get记录的时间小于这个时间,则作为无效数据remove
        lastClearTime = new Date();
    }

    @Override
    public void remove(String key) {
        Cache.remove(key);
    }
}