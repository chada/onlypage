package util.cache;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * Created by Administrator on 2016/3/7.
 */
public abstract class AbstractCache {
    public abstract void set(String key, Object val, int seconds);

    public abstract String get(String key);

    public abstract Future<String> asyncGet(String key);

    public String get(String key, Callable<String> fallback) throws Exception {
        String res = get(key);
        if (res == null) {
            res = fallback.call();
        }
        return res;
    }

    public abstract void clearAll();

    public abstract void remove(String key);
}