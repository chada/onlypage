package util.cache;

import play.Logger;
import util.Common;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * Created by Administrator on 2016/3/7.
 */
public class CacheUtil {
    public static int CacheTime = 1800;

    private static AbstractCache cacheInstance = null;

    public static AbstractCache getCacheInstance() {
        if (cacheInstance == null) {
            String cacheClass = Common.getStringConfig("cache.type", "util.cache.PlayCache");
            try {
                Class cls = Thread.currentThread().getContextClassLoader().loadClass(cacheClass);
                cacheInstance = (AbstractCache) cls.newInstance();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                Logger.error(e.getMessage(), e);
            } catch (InstantiationException e) {
                e.printStackTrace();
                Logger.error(e.getMessage(), e);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                Logger.error(e.getMessage(), e);
            }
        }
        return cacheInstance;
    }


    /**
     * 为了避免Key过长,对key进行摘要,md5方法
     */
    public static String digestKey(String key) {
        String md5Str = Common.md5(key);
        char[] resultChars = new char[md5Str.length()];
        for (int i = 0; i < resultChars.length; ++i) {
            char c = md5Str.charAt(i);
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_') {
                resultChars[i] = c;
            } else {
                resultChars[i] = '_';
            }
        }
        return new String(resultChars);
    }

    public static void set(String key, Object value, int seconds) {
        getCacheInstance().set(digestKey(key), value, seconds);
    }


    public static Object get(String key) {
        return getCacheInstance().get(digestKey(key));
    }

    public static Future<String> asyncGet(String key) {
        return getCacheInstance().asyncGet(digestKey(key));
    }

    public static String get(String key, Callable<String> fallback) throws Exception {
        return getCacheInstance().get(digestKey(key), fallback);
    }

    public static void clearAll() {
        getCacheInstance().clearAll();
    }
}
