package util.cache;
import com.alibaba.fastjson.JSON;
import jdk.nashorn.internal.parser.JSONParser;
import models.db1.user.S_User;
import redis.clients.jedis.Jedis;
import util.Common;

/**
 * Created by whk on 2017/9/21.
 */
public class RedisJava {
    public static String redisHost= Common.getStringConfig("redis.host");   //redis主机地址
    public static String redisUserSessionTag= Common.getStringConfig("redisUserSessionTag");//用户登录信息缓存存取标识
    public static Integer redisUserSessionExpireTime= Common.getIntConfig("redisUserSessionExpireTime");//用户登录信息缓存过期时间
    public static void main(String[] args) {
        //连接本地的 Redis 服务
        Jedis jedis = getJedis();
        System.out.println("连接成功");
        //查看服务是否运行
        System.out.println("服务正在运行: "+jedis.ping());


    }

    /**
     * 获取jedis
     */
    public static Jedis getJedis() {
        if (redisHost != null && !"".equals(redisHost)){
            return new Jedis(redisHost);
        }
        return new Jedis("localhost");
    }


    //添加用户信息缓存
    public static void setUserInfo(S_User user){
        Jedis jedis = getJedis();
        jedis.set(redisUserSessionTag+user.uuid, JSON.toJSONString(user));
        jedis.expire(redisUserSessionTag+user.uuid,redisUserSessionExpireTime);
    }


    //获取用户信息缓存
    public static S_User getUserInfoByUUID(String uuid){
        Jedis jedis = getJedis();
        String str = jedis.get(redisUserSessionTag+uuid);
        return  JSON.parseObject(str,S_User.class);
    }

    //删除用户信息缓存
    public static void deleteUserInfoByUUID(String uuid){
        Jedis jedis = getJedis();
        jedis.del(redisUserSessionTag+uuid);
    }

    public static void addTask(String userId,String triggerEvent,String readTime,String address){
        Jedis jedis = getJedis();
        String str = userId+"_"+triggerEvent+"_"+address;
        jedis.lpush("task",userId,triggerEvent,readTime,address);
    }
}
