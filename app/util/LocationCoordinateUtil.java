package util;

/**
 * Created by whk on 2017/7/20.
 */
public class LocationCoordinateUtil {
    private static double EARTH_RADIUS = 6371393;

    private static double radian(double d) {
        return d * Math.PI / 180.0;
    }

    /**
     * 根据两点间经纬度坐标（double值），计算两点间距离，单位为米
     * @param lat1 1号点纬度
     * @param lng1 1号点经度
     * @param lat2 2号点纬度
     * @param lng2 2号点经度
     * @return
     */
    public static Double distanceOfTwoCoordinate(double lat1, double lng1, double lat2, double lng2){
        double latRad1 = radian(lat1);
        double latRad2 = radian(lat2);
        double latRadDifference = latRad1 - latRad2;
        double lngRadDifference = radian(lng1) - radian(lng2);

        double radDistance = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latRadDifference / 2), 2)
                + Math.cos(latRad1) * Math.cos(latRad2) * Math.pow(Math.sin(lngRadDifference / 2), 2)));
        double distance = radDistance*EARTH_RADIUS;
        distance  = Math.round(distance * 10000) / 10000;
        return distance;
    }
}
