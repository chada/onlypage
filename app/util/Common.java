package util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sun.org.apache.xalan.internal.xsltc.trax.DOM2SAX;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.SAXException;
import play.Play;
import play.libs.XPath;
import play.mvc.Http;
import scala.xml.Node;
import scala.xml.parsing.NoBindingFactoryAdapter;

import javax.swing.text.Document;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * Created by Administrator on 2016/2/24.
 */
public class Common {

    public static String encode(String algorithm, String str) {
        if (str == null) {
            return null;
        }
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            messageDigest.update(str.getBytes());
            return getFormattedText(messageDigest.digest());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static final char[] HEX_DIGITS = {'0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    private static String getFormattedText(byte[] bytes) {
        int len = bytes.length;
        StringBuilder buf = new StringBuilder(len * 2);
        // 把密文转换成十六进制的字符串形式
        for (int j = 0; j < len; j++) {
            buf.append(HEX_DIGITS[(bytes[j] >> 4) & 0x0f]);
            buf.append(HEX_DIGITS[bytes[j] & 0x0f]);
        }
        return buf.toString();
    }


    public static String sha1(String s) {
        if (s == null) {
            return null;
        }
        return encode("SHA1", s);
    }

    public static String randomString(int length) {
        String base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    public static String randomStringForNum(int length) {
        String base = "0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }


    public static String md5(String str) {
        try {
            byte[] bytesOfMessage = str.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(bytesOfMessage);
            return new String(thedigest, Charset.forName("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            return str;
        } catch (UnsupportedEncodingException e2) {
            return str;
        }
    }

    public static String md5(String str, int digit) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes("utf-8"));
            //获得密文
            byte b[] = md.digest();

            // 把密文转换成十六进制的字符串形式
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0) i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            if (digit == 16) {
                return buf.toString().substring(8, 24);
            } else if (digit == 32) {
                return buf.toString();
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String md5(File file) throws FileNotFoundException {
        String value = null;
        FileInputStream in = new FileInputStream(file);
        try {
            MappedByteBuffer byteBuffer = in.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, file.length());
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(byteBuffer);
            BigInteger bi = new BigInteger(1, md5.digest());
            value = bi.toString(16);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return value;
    }


    public static String base64encode(String str) {
        try {
            return new String(Base64.encodeBase64(str.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getStringConfig(String key) {
        return Play.application().configuration().getString(key);
    }

    public static String getStringConfig(String key, String defaultValue) {
        return Play.application().configuration().getString(key, defaultValue);
    }

    public static boolean getBooleanConfig(String key) {
        return Play.application().configuration().getBoolean(key);
    }

    public static boolean getBooleanConfig(String key, Boolean defaultValue) {
        return Play.application().configuration().getBoolean(key, defaultValue);
    }

    public static int getIntConfig(String key) {
        return getIntConfig(key, 0);
    }

    public static int getIntConfig(String key, int defaultValue) {
        play.Configuration configuration = Play.application().configuration();
        if (!configuration.keys().contains(key)) {
            return defaultValue;
        }
        return Play.application().configuration().getInt(key);
    }

    //获取play项目中conf文件下的文件流
    public static InputStream getResource(String path) {
        return Play.application().classloader().getResourceAsStream(path);
    }

    //设置对象属性值
    public static void setPropertyOfObject(Object obj, String name, Object val) {
        Class cls = obj.getClass();
        Field field = null;
        try {
            field = cls.getField(name);
            field.setAccessible(true);
            field.set(obj, val);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取对象的名为name的属性，使用反射。如果不存在，则返回null
     */
    public static Object getPropertyOfObject(Object obj, String name) {
        if (obj == null) {
            return null;
        }
        Class cls = obj.getClass();
        try {
            Field field = cls.getField(name);
            field.setAccessible(true);
            return field.get(obj);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }


    //获取对象中的所有属性
    public static List<Field> getFields(Object obj) {
        if (obj == null) {
            return null;
        }
        Class cls = obj.getClass();
        List<Field> result = new ArrayList<Field>();
        for (Field field : cls.getFields()) {
            result.add(field);
        }
        return result;
    }


    public static void filterEmoji(Object obj) throws IllegalAccessException {
        List<Field> fields = getFields(obj);
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.getType() == String.class) {
                Object o = field.get(obj);
                if (o != null) {
                    String value = String.valueOf(o);
                    setValueOfObjecProperty(obj, field, filterEmoji(value));
                }
            }
        }
    }


    //设置类object中属性field值为value
    public static void setValueOfObjecProperty(Object obj, Field field, String value) {
        Object val = null;
        Class type = field.getType();
        if (type == String.class) {
            val = value;
        } else if (type == Integer.class || type == int.class) {
            val = Integer.valueOf(value);
        } else if (type == Long.class || type == long.class) {
            val = Long.valueOf(value);
        } else if (type == Float.class || type == float.class) {
            val = Float.valueOf(value);
        } else if (type == Double.class || type == double.class) {
            val = Double.valueOf(value);
        }
        Common.setPropertyOfObject(obj, field.getName(), val);
    }

    public static boolean inArray(int val, int[] array) {
        for (int item : array) {
            if (item == val) {
                return true;
            }
        }
        return false;
    }

    private static boolean isNotEmojiCharacter(char codePoint) {
        return (codePoint == 0x0) ||
                (codePoint == 0x9) ||
                (codePoint == 0xA) ||
                (codePoint == 0xD) ||
                ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) ||
                ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) ||
                ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
    }

    /*
   *过滤emoji表情
   * 因为mysql utf-8是三位，而表情为四位
   *
   *
   * */
    public static String filterEmoji(String source) {
        int len = source.length();
        StringBuilder buf = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char codePoint = source.charAt(i);
            if (isNotEmojiCharacter(codePoint)) {
                buf.append(codePoint);
            }
        }
        return buf.toString();

    }


    public static File DownLoadUrlImg(String path, String fileName) throws IOException {
        URL url = new URL(path);
        java.net.HttpURLConnection conn = (java.net.HttpURLConnection) url.openConnection();
        DataInputStream input = new DataInputStream(conn.getInputStream());
        DataOutputStream output = new DataOutputStream(new FileOutputStream(fileName));
        byte[] buffer = new byte[1024 * 8];
        int count = 0;
        while ((count = input.read(buffer)) > 0) {
            output.write(buffer, 0, count);
        }
        output.close();
        input.close();
        return new File(fileName);
    }


    public static byte[] fullyReadStreamToBytes(InputStream inputStream) throws IOException {
        if (inputStream == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bytes = new byte[8092];
        while (inputStream.available() > 0) {
            int size = inputStream.read(bytes);
            byteArrayOutputStream.write(bytes, 0, size);
        }
        bytes = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        return bytes;
    }


    public static String fullyReadStream(InputStream inputStream) throws IOException {
        byte[] bytes = fullyReadStreamToBytes(inputStream);
        if (bytes == null) {
            return null;
        }
        return new String(bytes, "UTF-8");
    }


    public static Boolean isPhoneVisit(Http.Request request) {
        boolean isMoblie = false;
        String[] mobileAgents = {"iphone", "android", "phone", "mobile", "wap", "netfront", "java", "opera mobi",
                "opera mini", "ucweb", "windows ce", "symbian", "Googlebot-Mobile"};
        if (!isPadVisit(request)) {
            if (request.getHeader("User-Agent") != null) {
                for (String mobileAgent : mobileAgents) {
                    if (request.getHeader("User-Agent").toLowerCase().indexOf(mobileAgent) >= 0) {
                        isMoblie = true;
                        break;
                    }
                }
            }
        }
        return isMoblie;
    }


    public static Boolean isWeixinVisit(Http.Request request) {
        boolean isWeixin = false;
        if (request.getHeader("User-Agent") != null) {
            if (request.getHeader("User-Agent").toLowerCase().indexOf("micromessenger") >= 0) {
                isWeixin = true;
            }
        }
        return isWeixin;
    }

    public static Boolean isIeVisit(Http.Request request) {
        boolean isIE = false;
        if (request.getHeader("User-Agent") != null) {
            if (request.getHeader("User-Agent").toLowerCase().indexOf("msie") >= 0) {
                isIE = true;
            }
        }
        return isIE;
    }

    public static Boolean isIeVisitLess8(Http.Request request) {
        boolean isIE = false;
        if (request.getHeader("User-Agent") != null) {
            if (request.getHeader("User-Agent").toLowerCase().indexOf("msie 8.0") >= 0
                    || request.getHeader("User-Agent").toLowerCase().indexOf("msie 7.0") >= 0
                    || request.getHeader("User-Agent").toLowerCase().indexOf("msie 6.0") >= 0
                    || request.getHeader("User-Agent").toLowerCase().indexOf("msie 5.0") >= 0
                    ) {
                isIE = true;
            }
        }
        return isIE;
    }

    public static Boolean isPadVisit(Http.Request request) {
        boolean isPad = false;
        if (request.getHeader("User-Agent") != null) {
            if (request.getHeader("User-Agent").toLowerCase().indexOf("pad") >= 0) {
                isPad = true;
            }
        }
        return isPad;
    }


    public static Node w3cDocumentToScalaNode(org.w3c.dom.Document doc) throws IOException, SAXException {
        DOM2SAX dom2sax = new DOM2SAX(doc);
        NoBindingFactoryAdapter adapter = new NoBindingFactoryAdapter();
        dom2sax.setContentHandler(adapter);
        dom2sax.parse();
        return adapter.rootElem();
    }


    /**
     * 对字符串处理:将指定位置到指定位置的字符以星号代替
     * @param content 传入的字符串
     * @param begin 开始位置
     * @param end 结束位置
     * @return
     */
    public static String convertStringWithAsterisk(String content, int begin, int end) {

        if (begin >= content.length() || begin < 0) {
            return content;
        }
        if (end >= content.length() || end < 0) {
            return content;
        }
        if (begin >= end) {
            return content;
        }
        String starStr = "";
        for (int i = begin; i < end; i++) {
            starStr = starStr + "*";
        }
        return content.substring(0, begin) + starStr + content.substring(end, content.length());

    }

    /**
     * 对字符加星号处理：除前面几位和后面几位外，其他的字符以星号代替
     * @param content 传入的字符串
     * @param frontNum 保留前面字符的位数
     * @param endNum 保留后面字符的位数
     * @return 带星号的字符串
     */

    public static String convertStringWithAsterisk2(String content, int frontNum, int endNum) {

        if (frontNum >= content.length() || frontNum < 0) {
            return content;
        }
        if (endNum >= content.length() || endNum < 0) {
            return content;
        }
        if (frontNum + endNum >= content.length()) {
            return content;
        }
        String starStr = "";
        for (int i = 0; i < (content.length() - frontNum - endNum); i++) {
            starStr = starStr + "*";
        }
        return content.substring(0, frontNum) + starStr
                + content.substring(content.length() - endNum, content.length());

    }

    /**
     * 从XML转换过来的JSON中取出某个值
     * TODO: JSON中值为列表类型, JSONArray类型
     */
    public static Object getValueFromXmlJson(JSONObject jsonObject, Class type, String key) {
        if (!jsonObject.containsKey(key)) {
            return null;
        }
        Object valObj = jsonObject.get(key);
        if (valObj == null) {
            return null;
        }
        if (valObj instanceof List) {
            if (((List) valObj).size() > 0) {
                valObj = ((List) valObj).get(0);
                if (type == String.class) {
                    return valObj.toString();
                } else if (type == Integer.class || type == int.class) {
                    return Integer.valueOf(valObj.toString());
                } else if (type == Long.class || type == long.class) {
                    return Long.valueOf(valObj.toString());
                } else if (type == Float.class || type == float.class) {
                    return Float.valueOf(valObj.toString());
                } else if (type == Double.class || type == double.class) {
                    return Double.valueOf(valObj.toString());
                }
                return valObj.toString();
            } else {
                return null;
            }
        } else if (valObj instanceof JSONObject) {
            try {
                Object subVal = type.newInstance();
                putXmlParsedJsonToObject(subVal, (JSONObject) valObj);
                return subVal;
            } catch (InstantiationException e) {
                e.printStackTrace();
                return null;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            if (type == String.class) {
                return valObj.toString();
            } else if (type == Integer.class || type == int.class) {
                return Integer.valueOf(valObj.toString());
            } else if (type == Long.class || type == long.class) {
                return Long.valueOf(valObj.toString());
            } else if (type == Float.class || type == float.class) {
                return Float.valueOf(valObj.toString());
            } else if (type == Double.class || type == double.class) {
                return Double.valueOf(valObj.toString());
            }
            return valObj.toString();
        }
    }

    /**
     * 把从XML转换过来的JSON的内容放入Java对象中
     */
    public static void putXmlParsedJsonToObject(Object obj, JSONObject jsonObject) {
        List<Field> fields = getFields(obj);
        for (Field field : fields) {
            Class fieldType = field.getType();
            Object val = getValueFromXmlJson(jsonObject, fieldType, field.getName());
            if (val != null) {
                Common.setPropertyOfObject(obj, field.getName(), val);
            }
        }
    }
}
