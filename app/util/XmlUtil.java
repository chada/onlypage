package util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.parser.Parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XmlUtil {

    public static String parseJsonToXml(JSONObject json) {
        if (json.keySet().size() < 1) {
            return parseJsonToXml(null, json);
        } else {
            String root = json.keySet().toArray()[0].toString();
            return parseJsonToXml(root, json.get(root));
        }
    }

    /**
     * xml转义
     *
     * @param source
     */
    public static String escape(String source) {
        if (source == null) {
            return source;
        }
        return StringEscapeUtils.escapeXml(source);
    }

    /**
     * 把json对象转成不带xml版本头的xml格式字符串
     *
     * @param json
     * @return
     */
    public static String parseJsonToPartialXml(Object json) {
        StringBuilder builder = new StringBuilder();
        if (json instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) json;
            for (String key : jsonObject.keySet()) {
                Object value = jsonObject.get(key);
                builder.append("<" + key + ">" + parseJsonToPartialXml(value) + "</" + key + ">");
            }
        } else if (json instanceof JSONArray) {
            JSONArray jsonArray = (JSONArray) json;
            for (int i = 0; i < jsonArray.size(); ++i) {
                Object item = jsonArray.get(i);
                builder.append(parseJsonToPartialXml(item));
            }
        } else if (json == null) {
            return "";
        } else {
            return escape(json.toString()); // 字符串，数字，布尔类型等基本类型
        }
        return builder.toString();
    }

    /**
     * XML字符串中有些内容需要用[CDATA]包围，因为有做转移，所以暂时不需要做这个
     *
     * @param root String
     * @param json JSONArray|JSONObject
     * @return
     */
    public static String parseJsonToXml(String root, Object json) {
        StringBuilder builder = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        if (root == null) {
            return builder.toString();
        }
        builder.append("<" + root + ">");
        builder.append(parseJsonToPartialXml(json));
        builder.append("</" + root + ">");
        return builder.toString();
    }

    public static Object parseXmlNodeToJson(Node node) {
        if (node instanceof TextNode) {
            return ((TextNode) node).text().trim();
        }
        List<Node> nodeList = node.childNodes();
        return parseXmlNodeListToJson(nodeList);
    }

    public static Object parseXmlNodeListToJson(List<Node> nodeList) {
        if (nodeList.size() == 1 && nodeList.get(0) instanceof TextNode && nodeList.get(0).nodeName().equals("#text")) {
            return parseXmlNodeToJson(nodeList.get(0));
        }
        // 将其中的nodeName相同的Element合并成数组，如果全部都一样，则整个作为数组返回
        boolean isAllSameNodeName = true;
        String lastNodeName = null;
        for (int i = 0; i < nodeList.size(); ++i) {
            Node node = nodeList.get(i);
            if (node instanceof TextNode) {
                TextNode textNode = (TextNode) node;
                if (textNode.text().trim().length() < 1) {
                    continue;
                }
            }
            if (node instanceof Element) {
                if (isAllSameNodeName) {
                    if (lastNodeName == null) {
                        lastNodeName = node.nodeName();
                    } else {
                        if (!lastNodeName.equals(node.nodeName())) {
                            isAllSameNodeName = false;
                        }
                    }
                }
            }
        }
        if (isAllSameNodeName && lastNodeName != null) {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < nodeList.size(); ++i) {
                Node node = nodeList.get(i);
                if (node instanceof Element) {
                    JSONObject itemJson = new JSONObject();
                    itemJson.put(node.nodeName(), parseXmlNodeToJson(node));
                    jsonArray.add(itemJson);
                }
            }
            return jsonArray;
        } else if (isAllSameNodeName && lastNodeName == null) {
            return new JSONArray();
        } else {
            JSONObject json = new JSONObject();
            Map<String, List<Object>> content = new HashMap<String, List<Object>>();
            for (int i = 0; i < nodeList.size(); ++i) {
                Node node = nodeList.get(i);
                if (!content.containsKey(node.nodeName())) {
                    content.put(node.nodeName(), new ArrayList<Object>());
                }
                content.get(node.nodeName()).add(parseXmlNodeToJson(node));
            }
            for (String key : content.keySet()) {
                List<Object> items = content.get(key);
                if (items.size() == 1) {
                    json.put(key, items.get(0));
                } else if (items.size() > 1) {
                    JSONArray itemJsonArray = new JSONArray();
                    for (Object item : items) {
                        itemJsonArray.add(item);
                    }
                    json.put(key, itemJsonArray);
                }
            }
            return json;
        }
    }

    /**
     * 将xml转成JSON
     * 因为XML格式和JSON格式还是有不小差别的，所以不考虑XML中节点的属性，否则不好定义转换后的JSON格式是怎样的
     * 所以节点的text内容都转成字符串，根据需要再自行转换
     *
     * @param xmlStr 要被转换的XML字符串
     * @return 转换后的JSON对象
     */
    public static JSONObject parseXmlToJson(String xmlStr) {
        JSONObject json = new JSONObject();
        Document doc = Jsoup.parse(xmlStr, "", Parser.xmlParser());
        if (doc.childNodeSize() < 1) {
            return json;
        }
        Node rootNode = doc.childNode(doc.childNodeSize() - 1);
        json.put(rootNode.nodeName(), parseXmlNodeListToJson(rootNode.childNodes()));
        return json;

    }

    public static Node getXmlRootNode(String xmlStr) {
        Document doc = Jsoup.parse(xmlStr, "", Parser.xmlParser());
        if (doc.childNodeSize() < 1) {
            return null;
        }
        Node rootNode = doc.childNode(doc.childNodeSize() - 1);
        return rootNode;
    }

}
