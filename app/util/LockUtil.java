package util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/12/7.
 */
public class LockUtil {
    private String lockStr;

    private static int maximumSize = 10000;
    public static Map<String, LockUtil> lockMap = new LinkedHashMap<String, LockUtil>(10, (float) 0.75, true) {
        @Override
        protected boolean removeEldestEntry(Map.Entry<String, LockUtil> eldest) {
            return size() > maximumSize;
        }
    };


    private LockUtil(String lockStr) {
        this.lockStr = lockStr;
        lockMap.put(lockStr, this);
    }

    public static LockUtil getLock(String lockStr) {
        LockUtil lock = lockMap.get(lockStr);
        if (lock == null) {
            synchronized (LockUtil.class) {
                if (lock == null) {
                    return new LockUtil(lockStr);
                } else {
                    return lock;
                }
            }
        } else {
            return lock;
        }
    }


    public static void print() {
        Iterator iter = lockMap.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            System.out.print(entry.getKey() + "  ");
//            Object key = entry.getKey();
//            Object val = entry.getValue();
        }
        System.out.println();
    }
}
