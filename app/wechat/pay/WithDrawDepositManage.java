package wechat.pay;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import util.Common;
import util.XmlUtil;
import wechat.pay.beans.withdrawDeposit.WithdrawDepositResult;

import javax.net.ssl.SSLContext;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;

/**
 * Created by Administrator on 2016/12/6.
 */
public class WithDrawDepositManage extends Manage {
    final static String wechatAppAppid = Common.getStringConfig("wechat.app.appid");
    final static String wechatPayMchId = Common.getStringConfig("wechat.pay.mchid");
    final static String check_name = "NO_CHECK";
    final static String wechatPayCertUrl = Common.getStringConfig("wechat.pay.certurl");

    public static WithdrawDepositResult transfers(String nonce_str, String partner_trade_no, String openid, int amount, String desc, String spbill_create_ip) {
        JSONObject jsonData = new JSONObject();
        jsonData.put("mch_appid", wechatAppAppid);
        jsonData.put("mchid", wechatPayMchId);
        jsonData.put("check_name", check_name);
        jsonData.put("nonce_str", nonce_str);
        jsonData.put("partner_trade_no", partner_trade_no);
        jsonData.put("openid", openid);
        jsonData.put("amount", amount);
        jsonData.put("desc", desc);
        jsonData.put("spbill_create_ip", spbill_create_ip);


        String signUrl = "amount=" + amount + "&check_name=" + check_name + "&desc=" + desc + "&mch_appid=" + wechatAppAppid + "&mchid=" + wechatPayMchId +
                "&nonce_str=" + nonce_str + "&openid=" + openid +
                "&partner_trade_no=" + partner_trade_no + "&spbill_create_ip=" + spbill_create_ip;

        String sign = getSign(signUrl);
        jsonData.put("sign", sign);
        JSONObject jsonXml = new JSONObject();
        jsonXml.put("xml", jsonData);
        String xml = XmlUtil.parseJsonToPartialXml(jsonXml);

        KeyStore keyStore = null;
        FileInputStream instream = null;
        try {
            keyStore = KeyStore.getInstance("PKCS12");
            instream = new FileInputStream(new File(wechatPayCertUrl));
            keyStore.load(instream, wechatPayMchId.toCharArray());
            instream.close();
            SSLContext sslcontext = SSLContexts.custom()
                    .loadKeyMaterial(keyStore, wechatPayMchId.toCharArray())
                    .build();
            // Allow TLSv1 protocol only
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    sslcontext,
                    new String[]{"TLSv1"},
                    null,
                    SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
            CloseableHttpClient client = HttpClients.custom()
                    .setSSLSocketFactory(sslsf)
                    .build();

            HttpPost post = new HttpPost("https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers");
            StringEntity entity = new StringEntity(xml, HTTP.UTF_8);
            post.setEntity(entity);
            post.setHeader("Content-Type", "text/xml;charset=utf-8");
            org.apache.http.HttpResponse result = client.execute(post);


            int statusCode = result.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                String res = EntityUtils.toString(result.getEntity());
                JSONObject resultJson = XmlUtil.parseXmlToJson(res);
                return WithdrawDepositResult.create(resultJson);
            }

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        return null;
    }
}
