package wechat.pay;

import com.alibaba.fastjson.JSONObject;
import util.Common;

import java.util.Date;

/**
 * Created by Administrator on 2016/12/6.
 */
public class Manage {
    final static String wechatPayKey = Common.getStringConfig("wechat.pay.key");
    final static String wechatAppAppid = Common.getStringConfig("wechat.app.appid");


    static String getSign(String url) {
        //key猜测为微信支付平台设置参数
        String stringSignTemp = url + "&key=" + wechatPayKey;
        String sign = Common.md5(stringSignTemp, 32).toUpperCase();
        return sign;
    }


    public static JSONObject getJsapiInfo(String prepay_id) {
        String appId = wechatAppAppid;
        String time_stamp = String.valueOf(new Date().getTime() / 1000).trim();
        String nonce_str = Common.randomString(32).trim();
        String package_str = "prepay_id=" + prepay_id;
        String signType = "MD5";
        String signUrl = "appId=" + appId + "&nonceStr=" + nonce_str + "&package=" + package_str + "&signType=" + signType + "&timeStamp=" + time_stamp;
        String paySign = getSign(signUrl);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("appId", appId);
        jsonObject.put("timeStamp", time_stamp);
        jsonObject.put("nonceStr", nonce_str);
        jsonObject.put("package", package_str);
        jsonObject.put("signType", signType);
        jsonObject.put("paySign", paySign);
        return jsonObject;
    }
}
