package wechat.pay;

import com.alibaba.fastjson.JSONObject;
import util.Common;
import util.HttpUtil;
import util.XmlUtil;
import wechat.pay.beans.order.QueryOrderResult;
import wechat.pay.beans.order.UnifiedOrderResult;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2016/12/5.
 */
public class OrderManage extends Manage {
    final static String wechatAppAppid = Common.getStringConfig("wechat.app.appid");
    final static String wechatAppAppsecret = Common.getStringConfig("wechat.app.appsecret");
    final static String wechatPayMchId = Common.getStringConfig("wechat.pay.mchid");


    //统一下订单
    public static UnifiedOrderResult unifiedorderNative(String nonce_str, String body, String out_trade_no, int total_fee, String spbill_create_ip, String product_id, String notify_url) throws IOException {
        return unifiedorder(nonce_str, body, out_trade_no, total_fee, spbill_create_ip, product_id, "NATIVE", notify_url, null);
    }

    public static UnifiedOrderResult unifiedorderJsapi(String nonce_str, String body, String out_trade_no, int total_fee, String spbill_create_ip, String product_id, String notify_url, String openid) throws IOException {
        return unifiedorder(nonce_str, body, out_trade_no, total_fee, spbill_create_ip, product_id, "JSAPI", notify_url, openid);
    }

    private static UnifiedOrderResult unifiedorder(String nonce_str, String body, String out_trade_no, int total_fee, String spbill_create_ip, String product_id, String trade_type, String notify_url, String openid) throws IOException {
        JSONObject jsonData = new JSONObject();
        jsonData.put("appid", wechatAppAppid);
        jsonData.put("mch_id", wechatPayMchId);
        jsonData.put("nonce_str", nonce_str);
        jsonData.put("body", body);
        jsonData.put("out_trade_no", out_trade_no);
        jsonData.put("total_fee", total_fee);
        jsonData.put("spbill_create_ip", spbill_create_ip);
        jsonData.put("notify_url", notify_url);
        jsonData.put("trade_type", trade_type);
        jsonData.put("product_id", product_id);
        if (openid != null) {
            jsonData.put("openid", openid);
        }

        String signUrl = "appid=" + wechatAppAppid + "&body=" + body + "&mch_id=" + wechatPayMchId + "&nonce_str=" + nonce_str + "&notify_url=" + notify_url;
        if (openid != null) {
            signUrl += "&openid=" + openid;
        }
        signUrl += "&out_trade_no=" + out_trade_no + "&product_id=" + product_id + "&spbill_create_ip=" + spbill_create_ip + "&total_fee=" + String.valueOf(total_fee) + "&trade_type=" + trade_type;
        String sign = getSign(signUrl);
        jsonData.put("sign", sign);

        JSONObject jsonXml = new JSONObject();
        jsonXml.put("xml", jsonData);
        String xml = XmlUtil.parseJsonToPartialXml(jsonXml);

        String url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
        InputStream inputStream = HttpUtil.post(url, xml);
        String res = Common.fullyReadStream(inputStream);
        JSONObject resultJson = XmlUtil.parseXmlToJson(res);
        return UnifiedOrderResult.create(resultJson);
    }


    public static QueryOrderResult queryOrder(String out_trade_no) throws IOException {
        String nonce_str = Common.randomString(32).trim();
        JSONObject jsonData = new JSONObject();
        jsonData.put("nonce_str", nonce_str);
        jsonData.put("out_trade_no", out_trade_no);
        jsonData.put("mch_id", wechatPayMchId);
        jsonData.put("appid", wechatAppAppid);

        String signUrl = "appid=" + wechatAppAppid + "&mch_id=" + wechatPayMchId + "&nonce_str=" + nonce_str + "&out_trade_no=" + out_trade_no;
        String sign = getSign(signUrl);
        jsonData.put("sign", sign);

        JSONObject jsonXml = new JSONObject();
        jsonXml.put("xml", jsonData);
        String xml = XmlUtil.parseJsonToPartialXml(jsonXml);

        String url = "https://api.mch.weixin.qq.com/pay/orderquery";
        InputStream inputStream = HttpUtil.post(url, xml);
        String res = Common.fullyReadStream(inputStream);
        JSONObject resultJson = XmlUtil.parseXmlToJson(res);
//        System.out.println(resultJson);
        return QueryOrderResult.create(resultJson);
    }
}
