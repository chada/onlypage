package wechat.pay.beans.withdrawDeposit;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.pay.beans.Result;

/**
 * Created by Administrator on 2016/12/5.
 */
public class WithdrawDepositResult extends Result {
    //以下字段在return_code为SUCCESS的时候有返回
    public String mch_appid ;
    public String mch_id;
    public String device_info;
    public String nonce_str;


    //以下字段在return_code 和result_code都为SUCCESS的时候有返回
    public String partner_trade_no;
    public String payment_no;
    public String payment_time;


    public static WithdrawDepositResult create(JSONObject msg){
//        System.out.println(msg);
        WithdrawDepositResult result  = JSON.parseObject(msg.getJSONObject("xml").toJSONString(), WithdrawDepositResult.class);
        return result;
    }
}
