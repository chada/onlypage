package wechat.pay.beans;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.GlobalReturnCode;

/**
 * Created by Administrator on 2016/3/23.
 */
public class Result {

    public String return_code;
    public String return_msg;
    public String result_code;
    public String err_code;
    public String err_code_des;

    public Boolean isSuccess() {
        if (!this.return_code.equals("SUCCESS") || !this.result_code.equals("SUCCESS")) {
            return false;
        } else {
            return true;
        }
    }

    public String getErrorMsg() {
        if (!this.return_code.equals("SUCCESS")) {
            return this.return_msg;
        } else {
            return this.err_code_des;
        }
    }

}
