package wechat.pay.beans.order;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.pay.beans.Result;

/**
 * Created by Administrator on 2016/12/5.
 */
public class PayResult extends Result {
    //以下字段在return_code为SUCCESS的时候有返回
    public String appid;
    public String mch_id;
    public String device_info;
    public String nonce_str;
    public String sign;
    public String sign_type;
    //以下字段在return_code 和result_code都为SUCCESS的时候有返回
    public String openid;
    public String is_subscribe;
    public String trade_type;
    public String bank_type;
    public int total_fee;
    public int settlement_total_fee;
    public String fee_type;
    public int cash_fee;
    public String cash_fee_type;
    public int coupon_fee;
    public int coupon_count;
    public int coupon_type_$n;
    public String coupon_id_$n;
    public int coupon_fee_$n;
    public String transaction_id;
    public String out_trade_no;
    public String attach;
    public String time_end;

    public static PayResult create(JSONObject msg) {
        PayResult result = JSON.parseObject(msg.getJSONObject("xml").toJSONString(), PayResult.class);
        return result;
    }
}
