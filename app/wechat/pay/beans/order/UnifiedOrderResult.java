package wechat.pay.beans.order;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.pay.beans.Result;

/**
 * Created by Administrator on 2016/12/5.
 */
public class UnifiedOrderResult extends Result {
    //以下字段在return_code为SUCCESS的时候有返回
    public String appid;
    public String mch_id;
    public String device_info;
    public String nonce_str;
    public String sign;

    //以下字段在return_code 和result_code都为SUCCESS的时候有返回
    public String trade_type;
    public String prepay_id;
    //二维码
    public String code_url;


    public static UnifiedOrderResult create(JSONObject msg){
//        System.out.println(msg);
        UnifiedOrderResult result  = JSON.parseObject(msg.getJSONObject("xml").toJSONString(), UnifiedOrderResult.class);
        return result;
    }
}
