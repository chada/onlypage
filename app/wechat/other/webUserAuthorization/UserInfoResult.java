package wechat.other.webUserAuthorization;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2015/11/2.
 */
public class UserInfoResult  extends Result{
    public String openid;
    public String nickname;
    public String sex;
    public String province;
    public String city;
    public String country;
    public String headimgurl;
    public String unionid;
    public JSONArray privilege;
    public String errcode;
    public String errmsg;


    public static UserInfoResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new UserInfoResult(msg);
        } else {
            UserInfoResult result = JSON.parseObject(msg.toJSONString(), UserInfoResult.class);
            return result;
        }
    }

    public UserInfoResult(){

    }


    private UserInfoResult(JSONObject err_msg) {
        super(err_msg);
    }
}
