package wechat.other.webUserAuthorization;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import play.mvc.Call;
import util.Common;
import util.DataFormatUtil;
import util.HttpUtil;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2015/11/2.
 * 微信网页授权
 */
public class WeChatWebUserAuthorization {
    public static String serverAddress = Common.getStringConfig("server.address");
    /**
     * @param appid        授权公众号主体appid
     * @param redirect_uri 获得code的url地址
     * @param state        参数
     * @param scope        snsapi_base  snsapi_userinfo
     */
    private static String getGainCodeUrl(String appid, String redirect_uri, String state, String scope) {
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appid +
                "&redirect_uri=" + redirect_uri + "&response_type=code&scope=" + scope + "&state=" + state +
                "#wechat_redirect";
//        System.out.println(url);
        return url;
    }

    public static String getGainCodeUrl_Base(String appid, Call redirect_call, String state) {
        String redirect_uri = serverAddress + redirect_call.toString();
        return getGainCodeUrl(appid, redirect_uri, state, "snsapi_base");
    }

    public static String getGainCodeUrl_UserInfo(String appid, Call redirect_call, String state) {
        String redirect_uri = serverAddress + redirect_call.toString();
        return getGainCodeUrl(appid, redirect_uri, state, "snsapi_userinfo");
    }

    public static AccessTokenResult operate_Base(String code, String appid, String secret) throws IOException {
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid + "&secret=" + secret + "&code=" + code + "&grant_type=authorization_code";
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject obj = JSON.parseObject(res);
        return AccessTokenResult.create(obj);
    }


    public static UserInfoResult operate_UserInfo(String code, String appid, String secret) throws IOException {
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid + "&secret=" + secret + "&code=" + code + "&grant_type=authorization_code";
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject obj = JSON.parseObject(res);
        AccessTokenResult result = AccessTokenResult.create(obj);
        if (!result.isSuccess()) {
            return null;
        } else {
            String snsapi_userinfo_url = "https://api.weixin.qq.com/sns/userinfo?access_token=" + result.access_token + "&openid=" + result.openid + "&lang=zh_CN";
            InputStream snsapi_userinfo_inputStream = HttpUtil.get(snsapi_userinfo_url);
            String snsapi_userinfo_res = DataFormatUtil.fullyReadStream(snsapi_userinfo_inputStream);
            JSONObject snsapi_userinfo_obj = JSON.parseObject(snsapi_userinfo_res);
            return UserInfoResult.create(snsapi_userinfo_obj);
        }
    }
}
