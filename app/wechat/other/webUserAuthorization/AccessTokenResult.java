package wechat.other.webUserAuthorization;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.app.bean.card.CardCreateResult;
import wechat.bean.Result;

/**
 * Created by Administrator on 2015/11/2.
 */
public class AccessTokenResult extends Result{
    public Boolean success;
    public String access_token;
    public int expires_in;
    public String refresh_token;
    public String openid;
    public String scope;
//    public String unionid;
    public String errcode;
    public String errmsg;


    public static AccessTokenResult create(JSONObject msg) {
//        System.out.println(msg);
        if (msg.containsKey("errcode")) {
            return new AccessTokenResult(msg);
        } else {
            AccessTokenResult result = JSON.parseObject(msg.toJSONString(), AccessTokenResult.class);
            return result;
        }
    }

    public AccessTokenResult(){

    }

    private AccessTokenResult(JSONObject err_msg) {
        super(err_msg);
    }
}
