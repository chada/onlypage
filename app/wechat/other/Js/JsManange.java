package wechat.other.Js;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.Common;
import util.HttpUtil;
import util.cache.CacheUtil;
import wechat.app.getInterfaceCallCredentials.AccessToken;

import java.io.InputStream;
import java.security.MessageDigest;
import java.util.Formatter;
import java.util.UUID;
import java.util.concurrent.Callable;

/**
 * Created by Administrator on 2016/12/12.
 */
public class JsManange {
    private static String getJsapiTicket(final String appKey, final String appSecret) throws Exception {
        final String token = AccessToken.getAccessToken(appKey, appSecret);
        final String cacheKey = "get_weixin_jsapi_ticket_" + token;
        Callable<String> fallback = new Callable<String>() {
            @Override
            public String call() throws Exception {
                String url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + token + "&type=jsapi";
                InputStream inputStream = HttpUtil.get(url);
                String res = Common.fullyReadStream(inputStream);
                JSONObject resJson = JSON.parseObject(res);
                // TODO: 认证失败的情况

                if (!resJson.get("errmsg").equals("ok")) {
                    return null;
                }
                String ticket = resJson.getString("ticket");
                int expiresIn = resJson.getInteger("expires_in");
                CacheUtil.set(cacheKey, ticket, expiresIn);
                return ticket;
            }
        };
        return CacheUtil.get(cacheKey, fallback);
    }

    public static String getNonceStr() {
        return UUID.randomUUID().toString();
    }


    public static String getTimestamp() {
        return Long.toString(System.currentTimeMillis() / 1000);
    }

    public static String getSignature(String appKey, String appSecret, String nonceStr, String timestamp, String url) throws Exception {
//        System.out.println(appKey);
//        System.out.println(appSecret);
//        System.out.println(getJsapiTicket(appKey, appSecret));
//        System.out.println(nonceStr);
//        System.out.println(timestamp);
//        System.out.println(url);

        String str = "jsapi_ticket=" + getJsapiTicket(appKey, appSecret) +
                "&noncestr=" + nonceStr +
                "&timestamp=" + timestamp +
                "&url=" + url;
        MessageDigest crypt = MessageDigest.getInstance("SHA-1");
        crypt.reset();
        crypt.update(str.getBytes("UTF-8"));
        String signature = byteToHex(crypt.digest());
        return signature;
    }


    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}
