package wechat.other;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.Common;
import util.HttpUtil;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by whk on 2017/10/16.
 * 微信公众号客服Util
 */
public class WeChatCustomServiceUtil {
    public static boolean SendText(String accessToken, String content,String openId) {
        JSONObject reqJson = new JSONObject();
        reqJson.put("touser",openId);
        reqJson.put("msgtype","text");
        JSONObject text = new JSONObject();
        text.put("content",content);
        reqJson.put("text",text);
        String url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + accessToken;
        try {
            InputStream inputStream = HttpUtil.post(url, reqJson.toJSONString());
            String res = Common.fullyReadStream(inputStream);
            JSONObject resJson = JSON.parseObject(res);
            if("ok".equals(resJson.getString("errmsg"))){
                return true;
            }else{
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
