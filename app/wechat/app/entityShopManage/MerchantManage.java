package wechat.app.entityShopManage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.bean.Result;
import wechat.app.bean.shop.merchant.category.CategoryListInfoResult;
import wechat.app.bean.shop.merchant.property.PropertyResult;
import wechat.app.bean.shop.merchant.sku.SkuResult;
import wechat.app.bean.shop.image.ImageResult;
import wechat.app.bean.shop.merchant.MerchantCreateResult;
import wechat.app.bean.shop.merchant.MerchantInfo;
import wechat.app.bean.shop.merchant.MerchantQueryByStatusResult;
import wechat.app.bean.shop.merchant.MerchantQueryResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static wechat.util.PostFileUtil.postFile;

/**
 * Created by Administrator on 2016/7/28.
 * 商品管理接口
 */
public class MerchantManage {
    //添加商品
    public static MerchantCreateResult create(String token,MerchantInfo merchantInfo) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/create?access_token="+token;
        JSONObject data = JSON.parseObject(JSON.toJSONString(merchantInfo));
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return MerchantCreateResult.create(result);
    }


    //删除商品（根据商品创建时候的id）
    public static Result delete(String token,String product_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/del?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("product_id",product_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    //修改商品
    public static Result edit(String token,String product_id,MerchantInfo merchantInfo) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/update?access_token="+token;
        JSONObject data = JSON.parseObject(JSON.toJSONString(merchantInfo));
        data.put("product_id",product_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    //查询商品（根据商品id）
    public static MerchantQueryResult query(String token,String product_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/get?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("product_id",product_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return MerchantQueryResult.create(result);
    }

    //商品状态(0-全部, 1-上架, 2-下架)
    public enum QUERYSTATUS{all,up,down}
    //查询商品（根据商品状态）
    public static MerchantQueryByStatusResult queryByStatus(String token,QUERYSTATUS status) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/getbystatus?access_token="+token;
        JSONObject data = new JSONObject();
        switch (status){
            case all: data.put("status",0);break;
            case up: data.put("status",1);break;
            case down: data.put("status",2);break;
        }
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return MerchantQueryByStatusResult.create(result);
    }

    //商品上下架标识(0-下架, 1-上架)
    public enum OPERATESTATUS{up,down}
    //商品上架或者下架
    public static Result operate(String token,String product_id,OPERATESTATUS status) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/modproductstatus?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("product_id",product_id);
        switch (status){
            case down: data.put("status",0);break;
            case up: data.put("status",1);break;
        }
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    /**
     * 获取子分类
     * @param cate_id 大分类ID(根节点分类id为1)
     * */
    public static CategoryListInfoResult getCategorySubList(String token,String cate_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/category/getsub?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("cate_id",cate_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return CategoryListInfoResult.create(result);
    }

    /**
     * 获取指定子分类的所有SKU
     * 测试未通过---系统繁忙，此时请开发者稍候再试
     * **/
    public static SkuResult getSku(String token,String cate_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/category/getsku?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("cate_id",cate_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return SkuResult.create(result);
    }

    /**
     * 获取指定分类的所有属性
     * 测试未通过---系统繁忙，此时请开发者稍候再试
     * **/
    public static PropertyResult getProperty(String token,String cate_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/category/getproperty?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("cate_id",cate_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return PropertyResult.create(result);
    }

    //商品中的图片上传专用接口（与素材图片上传接口不一样）
    public static ImageResult uploadImg(String token,File file) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/common/upload_img?access_token=" + token+"&filename="+file.getName();
        InputStream inputStream = HttpUtil.post(url, new FileInputStream(file));
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return ImageResult.create(result);
    }

    //增加库存
    public static Result addStock(String token,String product_id,String sku_info,String quantity) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/stock/add?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("product_id",product_id);
        data.put("sku_info",sku_info);
        data.put("quantity",quantity);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    //减少库存
    public static Result reduceStock(String token,String product_id,String sku_info,String quantity) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/stock/reduce?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("product_id",product_id);
        data.put("sku_info",sku_info);
        data.put("quantity",quantity);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }




}
