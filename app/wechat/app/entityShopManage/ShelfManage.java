package wechat.app.entityShopManage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.bean.Result;
import wechat.app.bean.shop.shelf.*;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2016/8/10.
 * 货架管理
 */
public class ShelfManage {
    //增加货架
    public static ShelfAddResult add(String token,ShelfAddInfo shelfAddInfo) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/shelf/add?access_token="+token;
        JSONObject data = JSON.parseObject(JSON.toJSONString(shelfAddInfo));
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        System.out.println(res);
        return ShelfAddResult.create(result);
    }

    //删除货架
    public static Result delete(String token,String shelf_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/shelf/del?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("shelf_id", shelf_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    //编辑货架
    public static Result edit(String token,ShelfEditInfo shelfAddInfo) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/shelf/mod?access_token="+token;
        JSONObject data = JSON.parseObject(JSON.toJSONString(shelfAddInfo));
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    //获取所有货架
    public static ShelfGetAllResult getAll(String token) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/shelf/getall?access_token="+token;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return ShelfGetAllResult.create(result);
    }

    //根据货架id 获取货架信息
    public static ShelfQueryResult query(String token,String shelf_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/shelf/getbyid?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("shelf_id",shelf_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return ShelfQueryResult.create(result);
    }
}
