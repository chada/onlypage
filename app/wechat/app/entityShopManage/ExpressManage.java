package wechat.app.entityShopManage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.bean.Result;
import wechat.app.bean.shop.express.ExpressAddResult;
import wechat.app.bean.shop.express.ExpressGetAllResult;
import wechat.app.bean.shop.express.ExpressGetResult;
import wechat.app.bean.shop.express.ExpressInfo;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2016/8/8.
 * 快递管理
 */
public class ExpressManage {
    public static ExpressAddResult add(String token,ExpressInfo expressInfo) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/express/add?access_token="+token;
        JSONObject delivery_template = JSON.parseObject(JSON.toJSONString(expressInfo));
        JSONObject data = new JSONObject();
        data.put("delivery_template",delivery_template);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return ExpressAddResult.create(result);
    }


    public static Result delete(String token,String template_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/express/del?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("template_id", template_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }


    public static Result edit(String token,String template_id,ExpressInfo expressInfo) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/express/update?access_token="+token;
        JSONObject delivery_template = JSON.parseObject(JSON.toJSONString(expressInfo));
        JSONObject data = new JSONObject();
        data.put("template_id",template_id);
        data.put("delivery_template",delivery_template);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }



    public static ExpressGetResult get(String token,String template_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/express/getbyid?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("template_id",template_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return ExpressGetResult.create(result);
    }


    public static ExpressGetAllResult getAll(String token) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/express/getall?access_token="+token;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return ExpressGetAllResult.create(result);
    }
}
