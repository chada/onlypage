package wechat.app.entityShopManage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.bean.Result;
import wechat.app.bean.entityshop.*;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2016/4/5.
 * 门店管理
 */
public class EntityShopManage {
    public static Result createShop(String token,CreateShopInfo base_info) throws IOException {
        JSONObject body = new JSONObject();
        JSONObject business = new JSONObject();
        business.put("base_info",base_info);
        System.out.println(business);
        body.put("business", business);
        String url = "http://api.weixin.qq.com/cgi-bin/poi/addpoi?access_token="+token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }


    public static Result editShop(String token,EditShopInfo base_info) throws IOException {
        JSONObject body = new JSONObject();
        JSONObject business = new JSONObject();
        business.put("base_info",base_info);
        System.out.println(business);
        body.put("business", business);
        String url = "https://api.weixin.qq.com/cgi-bin/poi/updatepoi?access_token="+token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }


    public static QueryShopListResult queryShopList(String token,int begin,int limit) throws IOException {
        JSONObject body = new JSONObject();
        body.put("begin", begin);
        body.put("limit",limit);
        String url = "https://api.weixin.qq.com/cgi-bin/poi/getpoilist?access_token="+token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return QueryShopListResult.create(result);
    }


    public static QueryShopResult queryShop(String token,String poi_id) throws IOException {
        JSONObject body = new JSONObject();
        body.put("poi_id", poi_id);
        String url = "http://api.weixin.qq.com/cgi-bin/poi/getpoi?access_token="+token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return QueryShopResult.create(result);
    }



    public static Result deleteShop(String token,String poi_id) throws IOException {
        JSONObject body = new JSONObject();
        body.put("poi_id", poi_id);
        String url = "https://api.weixin.qq.com/cgi-bin/poi/delpoi?access_token="+token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }


    public static CategoryListResult queryCategoryList(String token) throws IOException {
        String url = "http://api.weixin.qq.com/cgi-bin/poi/getwxcategory?access_token="+token;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return CategoryListResult.create(result);
    }
}
