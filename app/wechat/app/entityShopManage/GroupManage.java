package wechat.app.entityShopManage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.bean.Result;
import wechat.app.bean.shop.group.GroupAddResult;
import wechat.app.bean.shop.group.GroupGetAllResult;
import wechat.app.bean.shop.group.GroupOperateInfo;
import wechat.app.bean.shop.group.GroupQueryResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Administrator on 2016/8/9.
 * 商品分组
 */
public class GroupManage {

    //增加分组
    public static GroupAddResult add(String token, String group_name, List<String> product_list) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/group/add?access_token="+token;
        JSONObject data = new JSONObject();
        JSONObject group_detail = new JSONObject();
        group_detail.put("group_name",group_name);
        group_detail.put("product_list",product_list);
        data.put("group_detail",group_detail);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return GroupAddResult.create(result);
    }

    //删除分组
    public static Result delete(String token, String group_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/group/del?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("group_id",group_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    //修改分组信息
    public static Result edit(String token, String group_id,String group_name) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/group/propertymod?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("group_id",group_id);
        data.put("group_name",group_name);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    //修改分组内商品的信息（增加或者删除）
    public static Result operate(String token, GroupOperateInfo groupOperateInfo) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/group/productmod?access_token="+token;
        JSONObject data = JSON.parseObject(JSON.toJSONString(groupOperateInfo));
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }


    //获取所有分组
    public static GroupGetAllResult getAll(String token) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/group/getall?access_token="+token;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return GroupGetAllResult.create(result);
    }

    //获取所有分组
    public static GroupQueryResult query(String token,String group_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/group/getbyid?access_token="+token;
        JSONObject data = new JSONObject();
        data.put("group_id",group_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return GroupQueryResult.create(result);
    }

}
