package wechat.app.entityShopManage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.bean.Result;
import wechat.app.bean.shop.order.OrderQueryByIdResult;
import wechat.app.bean.shop.order.OrderQueryByStatusAndTimeResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * Created by Administrator on 2016/8/10.
 * 订单管理
 */
public class OrderManage {
    public static OrderQueryByIdResult queryById(String token, String order_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/order/getbyid?access_token=" + token;
        JSONObject data = new JSONObject();
        data.put("order_id", order_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return OrderQueryByIdResult.create(result);
    }
    /**
     * @param status    订单状态(不带该字段-全部状态null, 2-待发货, 3-已发货, 5-已完成, 8-维权中, )
     * @param begintime 订单创建时间起始时间(不带该字段则不按照时间做筛选)   等于不显示
     * @param endtime   订单创建时间终止时间(不带该字段则不按照时间做筛选) 等于不显示
     **/
    public static OrderQueryByStatusAndTimeResult queryByStatusAndTime(String token, String status, Date begintime, Date endtime) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/order/getbyfilter?access_token=" + token;
        JSONObject data = new JSONObject();
        data.put("status", status);
        if (begintime != null) {
            data.put("begintime", begintime.getTime() / 1000);
        }
        if (endtime != null) {
            data.put("endtime", endtime.getTime() / 1000);
        }
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        System.out.println(res);
        JSONObject result = JSON.parseObject(res);
        return OrderQueryByStatusAndTimeResult.create(result);
    }

    /**
     * 设置物流
     * @param  order_id 订单ID
     * @param  delivery_company 物流公司ID(参考《物流公司ID》；    当need_delivery为0时，可不填本字段；    当need_delivery为1时，该字段不能为空；    当need_delivery为1且is_others为1时，本字段填写其它物流公司名称)
     * @param  delivery_track_no 运单ID(当need_delivery为0时，可不填本字段；当need_delivery为1时，该字段不能为空；)
     * @param  need_delivery 商品是否需要物流(0-不需要，1-需要，无该字段默认为需要物流)
     * @param  is_others 是否为6.4.5表之外的其它物流公司(0-否，1-是，无该字段默认为不是其它物流公司)
     * **/
    public static Result setDelivery(String token, String order_id, String delivery_company, String delivery_track_no, String need_delivery, String is_others) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/order/setdelivery?access_token=" + token;
        JSONObject data = new JSONObject();
        data.put("order_id", order_id);
        data.put("delivery_company", delivery_company);
        data.put("delivery_track_no", delivery_track_no);
        data.put("need_delivery", need_delivery);
        data.put("is_others", is_others);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    //订单关闭
    public static Result close(String token, String order_id) throws IOException {
        String url = "https://api.weixin.qq.com/merchant/order/close?access_token=" + token;
        JSONObject data = new JSONObject();
        data.put("order_id", order_id);
        InputStream inputStream = HttpUtil.post(url, data.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }
}
