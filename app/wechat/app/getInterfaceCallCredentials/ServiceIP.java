package wechat.app.getInterfaceCallCredentials;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import play.Logger;
import util.Common;
import util.DataFormatUtil;
import util.HttpUtil;
import util.cache.CacheUtil;
import wechat.app.bean.ServiceIPResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;

/**
 * Created by Administrator on 2016/3/7.
 */
public class ServiceIP {

    /**
     * 如果公众号基于安全等考虑，需要获知微信服务器的IP地址列表，以便进行相关限制，可以通过该接口获得微信服务器IP地址列表。
     * 正常返回{"ip_list":["127.0.0.1","127.0.0.1"]}
     * 错误返回{"errcode":40013,"errmsg":"invalid appid"}
     */
    public static ServiceIPResult getCallBackIP(final String token) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=" + token;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        // Logger.info(res);
        JSONObject resJson = JSON.parseObject(res);
        return ServiceIPResult.create(resJson);
    }
}
