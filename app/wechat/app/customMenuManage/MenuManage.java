package wechat.app.customMenuManage;

import com.alibaba.fastjson.JSON;

import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.bean.Result;
import wechat.app.bean.menu.*;

import java.io.IOException;
import java.io.InputStream;


/**
 * Created by Administrator on 2016/3/9.
 */
public class MenuManage {


    public static Result createCommon(String token, Menu menu) throws IOException {
        String url = " https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, menu.getResult());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }


    //普通与个性化菜单一起的
    public static GetMenuResult getCommon(String token) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=" + token;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return GetMenuResult.create(result);
    }

    /**
     * 使用接口创建自定义菜单后，开发者还可使用接口删除当前使用的自定义菜单。另请注意，在个性化菜单时，调用此接口会删除默认菜单及全部个性化菜单。
     **/
    public static Boolean deleteCommon(String token) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=" + token;
        try {
            InputStream inputStream = HttpUtil.get(url);
            String res = DataFormatUtil.fullyReadStream(inputStream);
            JSONObject result = JSON.parseObject(res);
            if (result.containsKey("errcode") && result.getInteger("errcode") != 0) {

            } else {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;

    }


    //创建个性化菜单
    public static String createConditional(String token, ConditionalMenu menu) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=" + token;
        try {
            InputStream inputStream = HttpUtil.post(url, menu.getResult());
            String res = DataFormatUtil.fullyReadStream(inputStream);
            JSONObject result = JSON.parseObject(res);
//            System.out.println(result);
            if (result.containsKey("errcode")) {

            } else {
                return result.getString("menuid");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Boolean deleteConditional(String token, String menuid) {
        JSONObject body = new JSONObject();
        body.put("menuid", menuid);
        String url = "https://api.weixin.qq.com/cgi-bin/menu/delconditional?access_token=" + token;
        try {
            InputStream inputStream = HttpUtil.post(url, body.toJSONString());
            String res = DataFormatUtil.fullyReadStream(inputStream);
            JSONObject result = JSON.parseObject(res);
            if (result.containsKey("errcode") && result.getInteger("errcode") != 0) {

            } else {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    //user_id可以是粉丝的OpenID，也可以是粉丝的微信号。
    public static Menu testConditional(String token, String user_id) {
        JSONObject body = new JSONObject();
        body.put("user_id", user_id);
        String url = "https://api.weixin.qq.com/cgi-bin/menu/trymatch?access_token=" + token;
        try {
            InputStream inputStream = HttpUtil.post(url, body.toJSONString());
            String res = DataFormatUtil.fullyReadStream(inputStream);
            JSONObject result = JSON.parseObject(res);
//            System.out.println(result);
            if (result.containsKey("errcode")) {

            } else {
                Menu menu = JSON.parseObject(result.getJSONObject("menu").toJSONString(), Menu.class);
                return menu;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    //本接口将会提供公众号当前使用的自定义菜单的配置，如果公众号是通过API调用设置的菜单，则返回菜单的开发配置，而如果公众号是在公众平台官网通过网站功能发布菜单，则本接口返回运营者设置的菜单配置。
    public static CurrentSelfMenu getCurrentSelfMenuInfo(String token) {
        String url = "https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=" + token;
        try {
            InputStream inputStream = HttpUtil.get(url);
            String res = DataFormatUtil.fullyReadStream(inputStream);
            JSONObject result = JSON.parseObject(res);
//            System.out.println(result);
            if (result.containsKey("errcode")) {

            } else {
                CurrentSelfMenu menu = JSON.parseObject(result.toJSONString(), CurrentSelfMenu.class);
                return menu;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }


}
