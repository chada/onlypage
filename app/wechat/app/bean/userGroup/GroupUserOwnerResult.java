package wechat.app.bean.userGroup;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/4/1.
 */
public class GroupUserOwnerResult extends Result {
    public int groupid;

    public static GroupUserOwnerResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new GroupUserOwnerResult(msg);
        } else {
            GroupUserOwnerResult result = new GroupUserOwnerResult();
            result.groupid = msg.getInteger("groupid");
            return result;
        }
    }

    private GroupUserOwnerResult() {

    }


    private GroupUserOwnerResult(JSONObject err_msg) {
        super(err_msg);
    }
}
