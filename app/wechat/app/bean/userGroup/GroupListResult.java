package wechat.app.bean.userGroup;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/4/1.
 */
public class GroupListResult extends Result {
    public List<Group> groups = new ArrayList<>();

    public static GroupListResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new GroupListResult(msg);
        } else {
            GroupListResult result = new GroupListResult();
            JSONArray array = msg.getJSONArray("groups");
            for (Object object : array) {
                Group group = JSON.parseObject(object.toString(), Group.class);
                result.groups.add(group);
            }
            return result;
        }
    }

    private GroupListResult(){

    }



    private GroupListResult(JSONObject err_msg){
        super(err_msg);
    }
}
