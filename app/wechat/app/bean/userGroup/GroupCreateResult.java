package wechat.app.bean.userGroup;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/3/8.
 */
public class GroupCreateResult extends Result{
    public int id;
    public String name;

    public static GroupCreateResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new GroupCreateResult(msg);
        } else {
            GroupCreateResult result = new GroupCreateResult();
            result.id = msg.getJSONObject("group").getInteger("id");
            result.name = msg.getJSONObject("group").getString("name");
            return result;
        }
    }

    private GroupCreateResult(){

    }



    private GroupCreateResult(JSONObject err_msg){
        super(err_msg);
    }
}
