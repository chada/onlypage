package wechat.app.bean.user;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Administrator on 2016/3/9.
 */
public class UserOpenidListResult extends Result {
    public int total;
    public int count;
    public Set<String> openidList = new HashSet<String>();
    public String next_openid;


    public void merge(UserOpenidListResult userOpenidList) {
        this.count = this.count > userOpenidList.count ? this.count : userOpenidList.count;
        this.next_openid = this.count > userOpenidList.count ? this.next_openid : userOpenidList.next_openid;
        this.openidList.addAll(userOpenidList.openidList);
    }


    public static UserOpenidListResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new UserOpenidListResult(msg);
        } else {
            UserOpenidListResult result = new UserOpenidListResult();
            result.total = msg.getInteger("total");
            result.count = msg.getInteger("count");
            result.next_openid = msg.getString("next_openid");
            JSONArray openidList = msg.getJSONObject("data").getJSONArray("openid");
            result.openidList = JSONArray.parseObject(openidList.toJSONString(), HashSet.class);
            return result;
        }
    }

    private UserOpenidListResult() {

    }


    private UserOpenidListResult(JSONObject err_msg) {
        super(err_msg);
    }
}
