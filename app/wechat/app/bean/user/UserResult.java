package wechat.app.bean.user;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.Date;

/**
 * Created by Administrator on 2016/3/9.
 */
public class UserResult extends Result{
    public int subscribe;
    public String openid;
    public String nickname;
    public int sex;
    public String language;
    public String city;
    public String province;
    public String country;
    public String headimgurl;
    public Date subscribe_time;
    public String unionid;
    public String remark;
    public int groupid;



    public static UserResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new UserResult(msg);
        } else {
            UserResult result = JSON.parseObject(msg.toJSONString(), UserResult.class);
            return result;
        }
    }

    private UserResult() {

    }


    private UserResult(JSONObject err_msg) {
        super(err_msg);
    }
}
