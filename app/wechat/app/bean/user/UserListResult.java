package wechat.app.bean.user;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/3/9.
 */
public class UserListResult extends Result{

    public List<UserResult> userResultList = new ArrayList<>();



    public static UserListResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new UserListResult(msg);
        } else {
            UserListResult result = new UserListResult();
            JSONArray array = msg.getJSONArray("user_info_list");
            for (Object object : array) {
                UserResult userResult = JSON.parseObject(object.toString(), UserResult.class);
                result.userResultList.add(userResult);
            }
            return result;
        }
    }

    private UserListResult() {

    }


    private UserListResult(JSONObject err_msg) {
        super(err_msg);
    }
}
