package wechat.app.bean.menu;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/3/9.
 */
public class CurrentSelfMenuButton {
    public String type;
    public String name;
    public String key;
    public String url;
    public String media_id;
    public CurrentSelfMenuSubButton sub_button;
}
