package wechat.app.bean.menu;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/3/11.
 * 个性化菜单类型
 */
public class ConditionalMenu {
    public List<Button> button = new ArrayList<Button>();
    public ConditionalMatchRule matchrule;
    public int menuid;

    public void addRuel( ConditionalMatchRule matchrule) throws Exception {
        this.matchrule = matchrule;
    }

    public ConditionalMenu(){

    }

    public ConditionalMenu(List<Button> buttons,ConditionalMatchRule matchrule) throws Exception {
        if(buttons.size()>3){
            throw new Exception("主菜单只能三个");
        }else{
            for(Button button:buttons){
                this.button.add(button);
            }
        }
        this.matchrule =matchrule;
    }


    //获得最终的结果
    public String getResult() throws Exception{
        JSONArray json_btn = new JSONArray();
        for(Button btn:button){
            json_btn.add(btn.toJsonObject());
        }
        JSONObject result = new JSONObject();
        result.put("button",json_btn);
        if(matchrule==null){
            throw new Exception("没有添加规则");
        }
        result.put("matchrule",matchrule.toJsonObject());
        return  result.toString();
    }
}
