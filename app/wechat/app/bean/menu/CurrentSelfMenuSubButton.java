package wechat.app.bean.menu;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/3/9.
 */
public class CurrentSelfMenuSubButton {
    public String type;
    public String name;
    public String key;
    public String url;
    public String media_id;
    public List<CurrentSelfMenuSubButton> list = new ArrayList<>();
}
