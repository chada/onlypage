package wechat.app.bean.menu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.List;

/**
 * Created by Administrator on 2016/3/14.
 * 微信端获取的菜单数据
 */
public class GetMenuResult extends Result {
    public Menu menu;
    public List<ConditionalMenu> conditionalmenu;


    public static GetMenuResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new GetMenuResult(msg);
        } else {
            GetMenuResult result  = JSON.parseObject(msg.toJSONString(), GetMenuResult.class);
            return result;
        }
    }

    private GetMenuResult() {

    }


    private GetMenuResult(JSONObject err_msg) {
        super(err_msg);
    }
}
