package wechat.app.bean.menu;

import com.alibaba.fastjson.JSONObject;
import util.Common;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/11.
 * 个性化菜单额外属性
 */
public class ConditionalMatchRule {
    //	用户分组id，可通过用户分组管理接口获取
    public String group_id;
    //性别：男（1）女（2），不填则不做匹配
    public String sex;
    //国家信息，是用户在微信中设置的地区，具体请参考地区信息表
    public String country;
    //省份信息，是用户在微信中设置的地区，具体请参考地区信息表
    public String province;
    //城市信息，是用户在微信中设置的地区，具体请参考地区信息表
    public String city;
    //客户端版本，当前只具体到系统型号：IOS(1), Android(2),Others(3)，不填则不做匹配
    public String client_platform_type;
    //1、简体中文 "zh_CN" 2、繁体中文TW "zh_TW" 3、繁体中文HK "zh_HK" 4、英文 "en" 5、印尼 "id" 6、马来 "ms" 7、西班牙 "es" 8、韩国 "ko" 9、意大利 "it" 10、日本 "ja" 11、波兰 "pl" 12、葡萄牙 "pt" 13、俄国 "ru" 14、泰文 "th" 15、越南 "vi" 16、阿拉伯语 "ar" 17、北印度 "hi" 18、希伯来 "he" 19、土耳其 "tr" 20、德语 "de" 21、法语 "fr"
    public String language;


    public  ConditionalMatchRule(String group_id,String sex,String country,String province,String city,String client_platform_type,String language){
        this.group_id = group_id;
        this.sex = sex;
        this.country = country;
        this.province = province;
        this.city = city;
        this.client_platform_type = client_platform_type;
        this.language = language;
    }

    public ConditionalMatchRule(){

    }

    public JSONObject toJsonObject(){
        JSONObject object= new JSONObject();
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            if(Common.getPropertyOfObject(this,field.getName())!=null){
                object.put(field.getName(),Common.getPropertyOfObject(this,field.getName()));
            }
        }
        return object;
    }
}
