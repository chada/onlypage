package wechat.app.bean.menu;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/3/9.
 * 普通菜单类型
 */
public class Menu {
    public List<Button> button = new ArrayList<Button>();
    public int menuid;


    public Menu(List<Button> buttons) throws Exception {
        if(buttons.size()>3){
            throw new Exception("主菜单只能三个");
        }else{
            for(Button button:buttons){
                this.button.add(button);
            }
        }
    }

    public Menu(){

    }

    public String getResult(){
        JSONArray json_btn = new JSONArray();
        for(Button btn:button){
            json_btn.add(btn.toJsonObject());
        }
        JSONObject result = new JSONObject();
        result.put("button",json_btn);
        return  result.toString();
    }

/*
* * Button button1 = Button.createClickBtn("点击1","出来一段话",1);
  * Button button2 = Button.createBtn("有子菜单");
  * Button button3 = Button.createViewkBtn("子点击1", "http://www.baidu.com", 2);
  * Button button4 = Button.createClickBtn("子点击2", "出来一个字", 2);
  * button2.addSubButton(button3);
  * button2.addSubButton(button4);
  * menu.addBtn(button1);
  * menu.addBtn(button2);
  * */
}
