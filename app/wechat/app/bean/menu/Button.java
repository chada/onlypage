package wechat.app.bean.menu;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/3/9.
 */
public class Button {


    public String type;
    public String name;
    public String key;
    public String url;
    public String media_id;
    public List<Button> sub_button = new ArrayList<>();
    public int level;

    //关键字
    public static Button createClickBtn(String name, String key, int level) {
        Button button = new Button();
        button.type = "click";
        button.name = name;
        button.key = key;
        button.level = level;
        return button;
    }

    //链接跳转
    public static Button createViewkBtn(String name, String url, int level) {
        Button button = new Button();
        button.type = "view";
        button.name = name;
        button.url = url;
        button.level = level;
        return button;
    }

    /**
     * scancode_waitmsg 扫码推事件且弹出“消息接收中”提示框
     * scancode_push：扫码推事件
     * pic_sysphoto：弹出系统拍照发图
     * pic_photo_or_album：弹出拍照或者相册发图
     * pic_weixin：弹出微信相册发图器
     * location_select：弹出地理位置选择器
     * media_id：下发消息（除文本消息）
     * view_limited：跳转图文消息URL
     **/
    public static Button createOtherBtn(String name, String value, String type) {
        Button button = new Button();
        button.type = type;
        button.name = name;
        if (type.equals("view_limited") || type.equals("media_id")) {
            button.media_id = value;
        } else {
            button.key = value;
        }

        return button;
    }


    public static Button createBtn(String name) {
        Button button = new Button();
        button.name = name;
        button.level = 1;
        return button;
    }

    private Button() {

    }

    public void addSubButton(Button button) throws Exception {
        if (this.level == 2) {
            throw new Exception("主菜单不是一级菜单");
        }
        if (button.level == 1) {
            throw new Exception("子菜单不是二级菜单");
        }
        if (sub_button.size() > 5) {
            throw new Exception("主菜单已包含了5个子菜单，无法添加更多的子菜单");
        }
        this.sub_button.add(button);
    }

    public Boolean notHasChildren() {
        return this.sub_button.size() == 0 ? true : false;
    }


    public JSONObject toJsonObject() {
        JSONObject reuslt = new JSONObject();
        if (notHasChildren()) {
            reuslt.put("type", type);
            reuslt.put("name", name);
            if (type.equals("click")) {
                reuslt.put("key", key);
            } else if (type.equals("view")) {
                reuslt.put("url", url);
            } else if (type.equals("view_limited") || type.equals("media_id")) {
                reuslt.put("media_id", media_id);
            } else{
                reuslt.put("key", key);
            }
        } else {
            reuslt.put("name", name);
            JSONArray json_sub_button = new JSONArray();
            for (Button child : sub_button) {
                json_sub_button.add(child.toJsonObject());
            }
            reuslt.put("sub_button", json_sub_button);
        }
        return reuslt;
    }
}
