package wechat.app.bean.card;

import com.alibaba.fastjson.JSONObject;
import wechat.app.bean.card.info.CardBaseInfo;

/**
 * Created by Administrator on 2016/4/5.
 */
public class GiftCard {
    public String card_type ="GIFT";
    //兑换券专用，填写兑换内容的名称。
    public String gift;
    public CardBaseInfo base_info;

    public GiftCard(CardBaseInfo base_info, String gift) {
        this.base_info = base_info;
        this.gift = gift;
    }


    public String toJsonString(){
        JSONObject body = new JSONObject();
        JSONObject card = new JSONObject();
        JSONObject giftJson = new JSONObject();
        giftJson.put("base_info",base_info.toJsonObject());
        giftJson.put("gift", gift);
        card.put("gift", giftJson);
        card.put("card_type", card_type);
        body.put("card",card);
        return body.toJSONString();
    }
}
