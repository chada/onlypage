package wechat.app.bean.card;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.SerializerFeature;
import wechat.app.bean.card.info.CardBaseInfo;

/**
 * Created by Administrator on 2016/4/5.
 */
public class CashCard {
    public String card_type ="CASH";
    //代金券专用，表示起用金额（单位为分）,如果无起用门槛则填0。
    public int least_cost;
    //代金券专用，表示减免金额。（单位为分）
    public int reduce_cost;
    public CardBaseInfo base_info;

    public CashCard(CardBaseInfo base_info,int least_cost,int reduce_cost){
        this.base_info = base_info;
        this.least_cost = least_cost;
        this.reduce_cost = reduce_cost;
    }

    public String toJsonString(){
        JSONObject body = new JSONObject();
        JSONObject card = new JSONObject();
        JSONObject cashJson = new JSONObject();
        cashJson.put("base_info",base_info.toJsonObject());
        cashJson.put("least_cost", least_cost);
        cashJson.put("reduce_cost", reduce_cost);
        card.put("cash", cashJson);
        card.put("card_type", card_type);
        body.put("card",card);
        return body.toJSONString();
    }
}
