package wechat.app.bean.card;

import com.alibaba.fastjson.JSONObject;
import wechat.app.bean.card.info.CardBaseInfo;

/**
 * Created by Administrator on 2016/4/5.
 */
public class GeneralCouponCard {
    public String card_type = "GENERAL_COUPON";
    //优惠券专用，填写优惠详情。
    public String default_detail;
    public CardBaseInfo base_info;

    public GeneralCouponCard(CardBaseInfo base_info, String default_detail) {
        this.base_info = base_info;
        this.default_detail = default_detail;
    }

    public String toJsonString(){
        JSONObject body = new JSONObject();
        JSONObject card = new JSONObject();
        JSONObject general_coupon = new JSONObject();
        general_coupon.put("base_info",base_info.toJsonObject());
        general_coupon.put("default_detail", default_detail);
        card.put("general_coupon", general_coupon);
        card.put("card_type", card_type);
        body.put("card",card);
        return body.toJSONString();
    }
}
