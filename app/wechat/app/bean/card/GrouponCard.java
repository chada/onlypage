package wechat.app.bean.card;

import com.alibaba.fastjson.JSONObject;
import wechat.app.bean.card.info.CardBaseInfo;

/**
 * Created by Administrator on 2016/4/5.
 */
public class GrouponCard {
    public String card_type = "GROUPON";
    //团购券专用，团购详情。
    public String deal_detail;
    public CardBaseInfo base_info;


    public GrouponCard(CardBaseInfo base_info, String deal_detail) {
        this.base_info = base_info;
        this.deal_detail = deal_detail;
    }

    public String toJsonString(){
        JSONObject body = new JSONObject();
        JSONObject card = new JSONObject();
        JSONObject groupon = new JSONObject();
        groupon.put("base_info",base_info.toJsonObject());
        groupon.put("deal_detail", deal_detail);
        card.put("groupon", groupon);
        card.put("card_type", card_type);
        body.put("card",card);
        return body.toJSONString();
    }
}
