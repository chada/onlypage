package wechat.app.bean.card.info;

import com.alibaba.fastjson.JSONObject;
import util.Common;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/4/5.
 */
public class CardAdvancedAbstractInfo {
    //封面摘要简介。
    public String abstractStr;
    //封面图片列表，仅支持填入一个封面图片链接，上传图片接口上传获取图片获得链接，填写非CDN链接会报错，并在此填入。建议图片尺寸像素850*350
    public List<String> icon_url_list;


    public JSONObject toJsonObject(){
        JSONObject object = new JSONObject();
        for(Field field: Common.getFields(this)){
            if(field.getName().equals("abstractStr")&&Common.getPropertyOfObject(this, field.getName())!=null){
                object.put("abstract", Common.getPropertyOfObject(this, field.getName()));
            }else{
                object.put(field.getName(),Common.getPropertyOfObject(this, field.getName()));
            }

        }
        return object;
    }
}
