package wechat.app.bean.card.info;

import com.alibaba.fastjson.JSONObject;
import util.Common;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/4/5.
 * 高级字段为商户额外展示信息字段，非必填；
 * 填入时间限制字段（time_limit）,只控制显示，不控制实际使用逻辑，不填默认不显示
 */
public class CardAdvancedInfo {
    public enum BussinessService{BIZ_SERVICE_DELIVER ,BIZ_SERVICE_FREE_PARK ,BIZ_SERVICE_WITH_PET ,BIZ_SERVICE_FREE_WIFI}
    //使用门槛（条件）字段
    public CardAdvancedUseConditionInfo use_condition;
    //满减门槛字段，可用于兑换券和代金券，填入后将在全面拼写消费满xx元可用。
    public String least_cost;
    //购买xx可用类型门槛，仅用于兑换，填入后自动拼写购买xxx可用。
    public String object_use_for;
    //封面摘要结构体名称
    public CardAdvancedAbstractInfo abstractInfo;
    //图文列表，显示在详情内页，优惠券券开发者须至少传入一组图文列表
    public List<CardAdvancedTextImageListInfo> text_image_list;
    //图片链接，必须调用上传图片接口上传图片获得链接，并在此填入，否则报错
    public String image_url;
    //图文描述，5000字以内
    public String text;
    //商家服务类型：
    public List<BussinessService> business_service;
    //使用时段限制
    public CardAdvancedTimeLimitInfo time_limit;


    public JSONObject toJsonObject(){
        JSONObject object = new JSONObject();
        for(Field field: Common.getFields(this)){
            if(field.getName().equals("abstractInfo")&&Common.getPropertyOfObject(this, field.getName())!=null){
                object.put("abstract",((CardAdvancedAbstractInfo)Common.getPropertyOfObject(this, field.getName())).toJsonObject());
            }else{
                object.put(field.getName(),Common.getPropertyOfObject(this, field.getName()));
            }

        }
        return object;
    }
}
