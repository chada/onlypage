package wechat.app.bean.card.info;

import java.util.List;

/**
 * Created by Administrator on 2016/4/5.
 */
public class CardAdvancedUseConditionInfo {
    //指定可用的商品类目，仅用于代金券类型，填入后将在券面拼写指定xx可用
    public String accept_category;
    //指定可用的商品类目，仅用于代金券类型，填入后将在券面拼写指定xx不可用。
    public String reject_category;
    //不可以与其他类型共享门槛，填写false时系统将在使用须知里拼写不可与其他优惠共享，默认为true
    public Boolean can_use_with_other_discount;
}
