package wechat.app.bean.card.info;

import com.alibaba.fastjson.JSONObject;
import util.Common;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/4/5.
 */
public class CardBaseInfo {
    public enum Color {
        Color010("#63b359"),Color020("#2c9f67"),Color030("#509fc9"),Color040("#5885cf"),Color050("#9062c0"),
        Color060("#d09a45"),Color070("#e4b138"),Color080("#ee903c"),Color081("#f08500"),Color082("#a9d92d"),
        Color090("#dd6549"),Color100("#cc463d"),Color101("#cf3e36"),Color102("#5E6671");
        String value;
        Color(String value) {
            this.value = value;
        }
        public String getValue(){
            return this.value;
        }
    }

    //---------------------------------必填----------------------------------------
    public enum CodeType {
        CODE_TYPE_TEXT, CODE_TYPE_BARCODE, CODE_TYPE_QRCODE, CODE_TYPE_ONLY_QRCODE, CODE_TYPE_ONLY_BARCODE
    }

    //卡券的商户logo，建议像素为300*300。
    public String logo_url;
    //Code展示类型，"CODE_TYPE_TEXT"，文本；"CODE_TYPE_BARCODE"，一维码 ；"CODE_TYPE_QRCODE"，二维码；"CODE_TYPE_ONLY_QRCODE",二维码无code显示；"CODE_TYPE_ONLY_BARCODE",一维码无code显示；CODE_TYPE_NONE，不显示code和条形码类型，须开发者传入"立即使用"自定义cell完成线上券核销。
    public CodeType code_type;
    //商户名字,字数上限为12个汉字。
    public String brand_name;
    //卡券名，字数上限为9个汉字。(建议涵盖卡券属性、服务及金额)。
    public String title;
    //券颜色。按色彩规范标注填写Color010-Color100。详情见获取颜色列表接口
    public Color color;
    //卡券使用提醒，字数上限为16个汉字
    public String notice;
    //卡券使用说明，字数上限为1024个汉字。
    public String description;
    //商品信息。   "sku": {"quantity": 500000},
    public CardSkuInfo sku;
    //使用日期，有效期的信息。
    public CardDateInfo date_info;



    public CardBaseInfo(String logo_url,CodeType code_type,String brand_name,String title,Color color,String notice,String description,CardSkuInfo sku,CardDateInfo date_info){
        this.logo_url = logo_url;
        this.code_type = code_type;
        this.brand_name = brand_name;
        this.title = title;
        this.color = color;
        this.notice = notice;
        this.description = description;
        this.sku = sku;
        this.date_info = date_info;
    }


    //---------------------------------非必填----------------------------------------
    //券名，字数上限为18个汉字。不是必须的
    public String sub_title;
    //可用于DATE_TYPE_FIX_TERM时间类型，表示卡券统一过期时间，建议设置为截止日期的23:59:59过期。（东八区时间，单位为秒），设置了fixed_term卡券，当时间达到end_timestamp时卡券统一过期
    public String end_timestamp;
    //是否自定义Code码。填写true或false，默认为false。通常自有优惠码系统的开发者选择自定义Code码，并在卡券投放时带入Code码，详情见是否自定义Code码。
    public Boolean use_custom_code;
    //是否指定用户领取，填写true或false。默认为false。通常指定特殊用户群体投放卡券或防止刷券时选择指定用户领取
    public Boolean bind_openid;
    //客服电话。
    public String service_phone;
    //门店位置poiid。调用POI门店管理接口获取门店位置poiid。具备线下门店的商户为必填。
    public List<String> location_id_list;
    //第三方来源名，例如同程旅游、大众点评。
    public String source;
    //自定义跳转外链的入口名字。详情见活用自定义入口
    public String custom_url_name;
    //卡券顶部居中的按钮，仅在卡券状态正常(可以核销)时显示，建议开发者设置此按钮时code_type选择CODE_TYPE_NONE类型。
    public String center_title;
    //显示在入口下方的提示语，仅在卡券状态正常(可以核销)时显示。
    public String center_sub_title;
    //顶部居中的url，仅在卡券状态正常(可以核销)时显示。
    public String center_url;
    //自定义跳转的URL。
    //为了满足商户基于卡券本身的扩展诉求，允许卡券内页添加url跳转外链。带有的的字段有encrypt_code、card_id。
    //注意事项： encrypt_code为加密码码，需调用解码接口获取真实Code码。 假如指定的url为http://www.qq.com，用户点击时，跳转的url则为： http://www.qq.com?encrypt_code=ENCRYPT_CODE&card_id=CARDID
    public String custom_url;
    //显示在入口右侧的提示语。
    public String custom_url_sub_title;
    //营销场景的自定义入口名称。
    public String promotion_url_name;
    //入口跳转外链的地址链接。
    public String promotion_url;
    //	显示在营销入口右侧的提示语。
    public String promotion_url_sub_title;
    //	每人可领券的数量限制,不填写默认为50。
    public String get_limit;
    //	卡券领取页面是否可分享。
    public Boolean can_share;
    //	卡券是否可转赠。
    public Boolean can_give_friend;
    //	创建优惠券特有的高级字段
    public CardAdvancedInfo advanced_info;


    public JSONObject toJsonObject(){
        JSONObject object = new JSONObject();
        for(Field field: Common.getFields(this)){
            if(field.getName().equals("advanced_info")&&Common.getPropertyOfObject(this, field.getName())!=null){
                object.put("advanced_info",((CardAdvancedInfo)Common.getPropertyOfObject(this, field.getName())).toJsonObject());
            }else{
                object.put(field.getName(),Common.getPropertyOfObject(this, field.getName()));
            }

        }
        return object;
    }
}
