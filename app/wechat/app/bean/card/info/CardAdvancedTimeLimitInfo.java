package wechat.app.bean.card.info;

/**
 * Created by Administrator on 2016/4/5.
 */
public class CardAdvancedTimeLimitInfo {

    public enum Type{MONDAY,TUESDAY ,WEDNESDAY,THURSDAY ,FRIDAY ,SATURDAY ,SUNDAY }
    //限制类型枚举值：支持填入
    public Type type;
    //当前type类型下的起始时间（小时），如当前结构体内填写了MONDAY，此处填写了10，则此处表示周一 10:00可用
    public String begin_hour;
    //	当前type类型下的起始时间（分钟），如当前结构体内填写了MONDAY，begin_hour填写10，此处填写了59，则此处表示周一 10:59可用
    public String begin_minute;
    //	当前type类型下的结束时间（小时），如当前结构体内填写了MONDAY，此处填写了20，则此处表示周一 10:00-20:00可用
    public String end_hour;
    //当前type类型下的结束时间（分钟），如当前结构体内填写了MONDAY，begin_hour填写10，此处填写了59，则此处表示周一 10:59-00:59可用
    public String end_minute;

}
