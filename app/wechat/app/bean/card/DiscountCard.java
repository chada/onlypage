package wechat.app.bean.card;

import com.alibaba.fastjson.JSONObject;
import wechat.app.bean.card.info.CardBaseInfo;

/**
 * Created by Administrator on 2016/4/5.
 */
public class DiscountCard {
    public String card_type ="DISCOUNT";
    //折扣券专用，表示打折额度（百分比）。填30就是七折。
    public int discount;
    public CardBaseInfo base_info;

    public DiscountCard(CardBaseInfo base_info,int discount){
        this.base_info = base_info;
        this.discount = discount;
    }

    public String toJsonString(){
        JSONObject body = new JSONObject();
        JSONObject card = new JSONObject();
        JSONObject discountJson = new JSONObject();
        discountJson.put("base_info",base_info.toJsonObject());
        discountJson.put("discount", discount);
        card.put("discount", discountJson);
        card.put("card_type", card_type);
        body.put("card",card);
        return body.toJSONString();
    }
}
