package wechat.app.bean.card;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/4/5.
 */
public class CardCreateResult extends Result {
    public String card_id;

    public static CardCreateResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new CardCreateResult(msg);
        } else {
            CardCreateResult result = JSON.parseObject(msg.toJSONString(), CardCreateResult.class);
            return result;
        }
    }

    private CardCreateResult() {

    }


    private CardCreateResult(JSONObject err_msg) {
        super(err_msg);
    }
}
