package wechat.app.bean.customer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class CustomerOnlineListResult extends Result{

    public List<CustomerOnline> customers = new ArrayList<CustomerOnline>();

    public static CustomerOnlineListResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new CustomerOnlineListResult(msg);
        } else {
            CustomerOnlineListResult result = new CustomerOnlineListResult();
            JSONArray array = msg.getJSONArray("kf_online_list");
            for (Object object : array) {
                CustomerOnline customer = JSON.parseObject(object.toString(), CustomerOnline.class);
                result.customers.add(customer);
            }
            return result;
        }
    }

    private CustomerOnlineListResult(){

    }



    private CustomerOnlineListResult(JSONObject err_msg){
        super(err_msg);
    }
}
