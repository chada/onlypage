package wechat.app.bean.customer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/4/1.
 */
public class SessionWaitListResult extends Result {

    public List<SessionWait> sessionWaitList= new ArrayList<SessionWait>();
    public int count;

    public static SessionWaitListResult create(JSONObject msg){
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new SessionWaitListResult(msg);
        } else {
            SessionWaitListResult result = new SessionWaitListResult();
            result.count = msg.getInteger("count");
            JSONArray array = msg.getJSONArray("waitcaselist");
            for (Object object : array) {
                SessionWait session = JSON.parseObject(object.toString(), SessionWait.class);
                result.sessionWaitList.add(session);
            }
            return result;
        }
    }

    private SessionWaitListResult(){

    }



    private SessionWaitListResult(JSONObject err_msg){
        super(err_msg);
    }
}
