package wechat.app.bean.customer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class CustomerListResult  extends Result{

    public List<Customer> customers = new ArrayList<Customer>();

    public static CustomerListResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new CustomerListResult(msg);
        } else {
            CustomerListResult result = new CustomerListResult();
            JSONArray array = msg.getJSONArray("kf_list");
            for (Object object : array) {
                Customer customer = JSON.parseObject(object.toString(), Customer.class);
                result.customers.add(customer);
            }
            return result;
        }
    }

    private CustomerListResult(){

    }



    private CustomerListResult(JSONObject err_msg){
        super(err_msg);
    }
}
