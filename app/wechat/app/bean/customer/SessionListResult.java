package wechat.app.bean.customer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/4/1.
 */
public class SessionListResult extends Result {

    public List<Session> sessionlist= new ArrayList<Session>();

    public static SessionListResult create(JSONObject msg){
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new SessionListResult(msg);
        } else {
            SessionListResult result = new SessionListResult();
            JSONArray array = msg.getJSONArray("sessionlist");
            for (Object object : array) {
                Session session = JSON.parseObject(object.toString(), Session.class);
                result.sessionlist.add(session);
            }
            return result;
        }
    }

    private SessionListResult(){

    }



    private SessionListResult(JSONObject err_msg){
        super(err_msg);
    }
}
