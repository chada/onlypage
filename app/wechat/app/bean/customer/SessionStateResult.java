package wechat.app.bean.customer;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/4/1.
 */
public class SessionStateResult extends Result {
    public String createtime;
    //正在接待的客服，为空表示没有人在接待
    public String kf_account;


    public static SessionStateResult create(JSONObject msg){
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new SessionStateResult(msg);
        } else {
            SessionStateResult result = new SessionStateResult();
            result.createtime =  msg.getString("createtime");
            result.kf_account =  msg.getString("kf_account");
            return result;
        }
    }

    private SessionStateResult(){

    }



    private SessionStateResult(JSONObject err_msg){
        super(err_msg);
    }
}
