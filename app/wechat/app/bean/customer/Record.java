package wechat.app.bean.customer;

/**
 * Created by Administrator on 2016/4/1.
 */
public class Record {

    public String openid;
    //1000	创建未接入会话
    //1001	接入会话
    //1002	主动发起会话
    //1003	转接会话
    //1004	关闭会话
    //1005	抢接会话
    //2001	公众号收到消息
    //2002	客服发送消息
    //2003	客服收到消息
    public int opercode;
    public String text;
    public String time;
    public String worker;
}
