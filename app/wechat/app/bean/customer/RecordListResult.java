package wechat.app.bean.customer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/4/1.
 */
public class RecordListResult extends Result {

    public List<Record> recordList= new ArrayList<Record>();
    public int retcode;

    public static RecordListResult create(JSONObject msg){
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new RecordListResult(msg);
        } else {
            RecordListResult result = new RecordListResult();
            result.retcode = msg.getInteger("retcode");
            JSONArray array = msg.getJSONArray("recordlist");
            for (Object object : array) {
                Record record = JSON.parseObject(object.toString(), Record.class);
                result.recordList.add(record);
            }
            return result;
        }
    }

    private RecordListResult(){

    }



    private RecordListResult(JSONObject err_msg){
        super(err_msg);
    }
}
