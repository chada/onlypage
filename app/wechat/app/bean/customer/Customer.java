package wechat.app.bean.customer;

/**
 * Created by Administrator on 2016/3/8.
 */
public class Customer {
    //完整客服账号，格式为：账号前缀@公众号微信号
    public String kf_account;
    //客服头像
    public String kf_headimgurl;
    //客服工号
    public String kf_id;
    //客服昵称
    public String kf_nick;
}
