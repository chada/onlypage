package wechat.app.bean.customer;

/**
 * Created by Administrator on 2016/3/8.
 */
public class CustomerOnline {
    //完整客服账号，格式为：账号前缀@公众号微信号
    public String kf_account;
    //客服工号
    public String kf_id;
    //客服在线状态 1：pc在线，2：手机在线。若pc和手机同时在线则为 1+2=3
    public int status;
    //客服设置的最大自动接入数
    public int auto_accept;
    //客服当前正在接待的会话数
    public int accepted_case;
}
