package wechat.app.bean.shop.merchant;

/**
 * Created by Administrator on 2016/7/28.
 * sku信息列表(可为多个)，每个sku信息串即为一个确定的商品，比如白色的37码的鞋子
 */
public class MerchantSkuListInfo {
    //sku信息, 参照上述sku_table的定义;
    //格式 : "id1:vid1;id2:vid2"
    //规则 : id_info的组合个数必须与sku_table个数一致(若商品无sku信息, 即商品为统一规格，则此处赋值为空字符串即可)
    public String sku_id;
    //sku原价(单位 : 分)
    public Long ori_price;
    //sku微信价(单位 : 分, 微信价必须比原价小, 否则添加商品失败)
    public Long price;
    //sku iconurl(图片需调用图片上传接口获得图片Url)
    public String icon_url;
    //sku库存
    public Long quantity;
    //商家商品编码
    public String product_code;

    public MerchantSkuListInfo() {

    }
}
