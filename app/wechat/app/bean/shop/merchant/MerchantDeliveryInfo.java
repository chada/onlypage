package wechat.app.bean.shop.merchant;

import java.util.List;

/**
 * Created by Administrator on 2016/7/28.
 * 运费信息 必选
 */
public class MerchantDeliveryInfo {
    //运费类型(0-使用下面express字段的默认模板, 1-使用template_id代表的邮费模板, 详见邮费模板相关API)
    public int delivery_type;
    //邮费模板ID
    public String template_id;
    //
    public List<Express> express;


    public MerchantDeliveryInfo(){}

    public MerchantDeliveryInfo(List<Express> express){
        this.express = express;
        this.delivery_type = 0;
    }


    public MerchantDeliveryInfo(String template_id){
        this.template_id = template_id;
        this.delivery_type = 1;
    }
}
