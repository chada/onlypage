package wechat.app.bean.shop.merchant.sku;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.List;

/**
 * Created by Administrator on 2016/8/5.
 */
public class SkuResult extends Result {
    public List<SkuInfo> sku_table;

    public static SkuResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new SkuResult(msg);
        } else {
            SkuResult result  = JSON.parseObject(msg.toJSONString(), SkuResult.class);
            return result;
        }
    }

    private SkuResult() {

    }


    private SkuResult(JSONObject err_msg) {
        super(err_msg);
    }
}


//List<String> category_id = new ArrayList<String>(){{
//    add("537119698");
//}};
//String image_url = "http://mmbiz.qpic.cn/mmbiz/Hw5SEaHYvPia60h3t4icV2AVh25qvvKHhq9szAvNlicviag7uHhia9fm2CuK04gnAbVAmXxzyFibGTB7kZJzqASibUlAg/0";
//List<String> imageList = new ArrayList<String>(){{
//    add("http://mmbiz.qpic.cn/mmbiz/Hw5SEaHYvPia60h3t4icV2AVh25qvvKHhq9szAvNlicviag7uHhia9fm2CuK04gnAbVAmXxzyFibGTB7kZJzqASibUlAg/0");
//}};

//List<MerchantDetailInfo> merchantDetailInfos = new ArrayList<MerchantDetailInfo>();
//MerchantDetailInfo merchantDetailInfo = new MerchantDetailInfo("这是一个苹果", MerchantDetailInfo.Type.text);
//merchantDetailInfos.add(merchantDetailInfo);
//
//        MerchantProductBaseInfo merchantProductBaseInfo = new MerchantProductBaseInfo("测试苹果",category_id,image_url,imageList,merchantDetailInfos);
//
//
//        Express express1 = new Express("10000027",Long.valueOf(100));
//        Express express2 = new Express("10000028",Long.valueOf(100));
//        Express express3 = new Express("10000029",Long.valueOf(100));
//        List<Express> expresses = new ArrayList<Express>();
//        expresses.add(express1);
//        expresses.add(express2);
//        expresses.add(express3);
//        MerchantDeliveryInfo merchantDeliveryInfo = new MerchantDeliveryInfo(expresses);
//
//
//        MerchantInfo merchantInfo = new MerchantInfo(merchantProductBaseInfo,merchantDeliveryInfo);
//
//        MerchantCreateResult result = MerchantManage.create(token,merchantInfo);
//
//        if(result.isSuccess()){
//        System.out.println(result.product_id);
//        }else{
//        System.out.println(result.errmsg_zn);
//        }
