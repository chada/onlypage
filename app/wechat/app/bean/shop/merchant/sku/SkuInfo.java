package wechat.app.bean.shop.merchant.sku;

import java.util.List;

/**
 * Created by Administrator on 2016/8/5.
 */
public class SkuInfo {
    public String id;
    public String name;
    public List<SkuValueInfo> value_list;
}
