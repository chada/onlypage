package wechat.app.bean.shop.merchant;

/**
 * Created by Administrator on 2016/7/28.
 * 快递信息
 */
public class Express {
    //快递ID
    public String id;
    //运费(单位 : 分)
    public Long price;


    public Express(){

    }

    public Express(String id,Long price){
        this.id = id;
        this.price = price;
    }

    //10000027	平邮
    //10000028	快递
    //10000029	EMS
}
