package wechat.app.bean.shop.merchant.category;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.List;

/**
 * Created by Administrator on 2016/8/5.
 */
public class CategoryListInfoResult extends Result{
    public List<CategoryInfo> cate_list;


    public static CategoryListInfoResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new CategoryListInfoResult(msg);
        } else {
            CategoryListInfoResult result  = JSON.parseObject(msg.toJSONString(), CategoryListInfoResult.class);
            return result;
        }
    }

    private CategoryListInfoResult() {

    }


    private CategoryListInfoResult(JSONObject err_msg) {
        super(err_msg);
    }
}
