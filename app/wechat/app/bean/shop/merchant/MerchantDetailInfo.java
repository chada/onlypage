package wechat.app.bean.shop.merchant;

/**
 * Created by Administrator on 2016/7/28.
 * 商品详情列表，显示在客户端的商品详情页内
 */
public class MerchantDetailInfo {
    //文字描述
    public String text;
    //图片(图片需调用图片上传接口获得图片Url填写至此，否则无法添加商品)
    public String img;

    public enum Type {text,img}

    public Type type;

    public MerchantDetailInfo(){

    }

    public MerchantDetailInfo(String str, Type type){
        switch (type){
            case text:this.text = str;break;
            case img:this.img = str;break;
        }
    }
}
