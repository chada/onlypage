package wechat.app.bean.shop.merchant;

import java.util.List;

/**
 * Created by Administrator on 2016/7/28.
 * 商品sku定义，SKU列表请通过《获取指定子分类的所有SKU》获取
 */
public class MerchantSkuInfo {
    //sku属性(SKU列表中id, 支持自定义SKU，格式为"$xxx"，xxx即为显示在客户端中的字符串)
    public String id;
    //sku值(SKU列表中vid, 如需自定义SKU，格式为"$xxx"，xxx即为显示在客户端中的字符串)
    public List<String> vid;

    public MerchantSkuInfo(){

    }

    public MerchantSkuInfo(String id, List<String> vid){
        this.id = id;
        this.vid = vid;
    }
}
