package wechat.app.bean.shop.merchant;

import java.util.List;

/**
 * Created by Administrator on 2016/7/28.
 * 基本属性
 */
public class MerchantProductBaseInfo {
    //必选    商品名称
    public String name;
    //必选    商品分类id，商品分类列表请通过《获取指定分类的所有子分类》获取
    public List<String> category_id;
    //必选    商品主图(图片需调用图片上传接口获得图片Url填写至此，否则无法添加商品。图片分辨率推荐尺寸为640×600)
    public String main_img;
    //必选    商品图片列表(图片需调用图片上传接口获得图片Url填写至此，否则无法添加商品。图片分辨率推荐尺寸为640×600)
    public List<String> img;
    //必选    商品详情列表，显示在客户端的商品详情页内
    public List<MerchantDetailInfo> detail;
    //商品属性列表，属性列表请通过《获取指定分类的所有属性》获取
    public List<MerchantPropertyInfo> property;
    //商品sku定义，SKU列表请通过《获取指定子分类的所有SKU》获取
    public List<MerchantSkuInfo> sku_info;
    //用户商品限购数量
    public int buy_limit;

    public MerchantProductBaseInfo() {

    }

    public MerchantProductBaseInfo(String name, List<String> category_id, String main_img, List<String> img, List<MerchantDetailInfo> detail) {
        this.name = name;
        this.category_id = category_id;
        this.main_img = main_img;
        this.img = img;
        this.detail = detail;
    }
}
