package wechat.app.bean.shop.merchant;

import java.util.List;

/**
 * Created by Administrator on 2016/7/28.
 * 商品信息
 */
public class MerchantInfo {
    //只有在商品查询时才有
    public String product_id;
    //有必填项  基本属性
    public MerchantProductBaseInfo product_base;
    //sku信息列表(可为多个)，每个sku信息串即为一个确定的商品，比如白色的37码的鞋子
    public List<MerchantSkuListInfo> sku_list;
    //商品其他属性
    public MerchantAttrextInfo attrext;
    //必选
    public MerchantDeliveryInfo delivery_info;

    public MerchantInfo(){

    }

    public MerchantInfo(MerchantProductBaseInfo product_base,MerchantDeliveryInfo delivery_info){
        this.product_base = product_base;
        this.delivery_info = delivery_info;
    }
}
