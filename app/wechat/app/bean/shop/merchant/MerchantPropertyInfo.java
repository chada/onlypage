package wechat.app.bean.shop.merchant;

/**
 * Created by Administrator on 2016/7/28.
 * 商品属性列表，属性列表请通过《获取指定分类的所有属性》获取
 */
public class MerchantPropertyInfo {
    //属性id
    public String id;
    //属性值id
    public String vid;

    public MerchantPropertyInfo(){

    }

    public MerchantPropertyInfo(String id, String vid){
        this.id = id;
        this.vid = vid;
    }
}
