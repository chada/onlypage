package wechat.app.bean.shop.merchant;

/**
 * Created by Administrator on 2016/7/28.
 * 商品其他属性
 */
public class MerchantAttrextInfo {
    //是否包邮(0-否, 1-是), 如果包邮delivery_info字段可省略
    public Boolean isPostFree;
    //是否提供发票(0-否, 1-是)
    public Boolean isHasReceipt;
    //是否支持退换货(0-否, 1-是)
    public Boolean isUnderGuaranty;
    //是否支持退换货(0-否, 1-是)
    public Boolean isSupportReplace;
    //商品所在地地址
    public LocationInfo location;

    public MerchantAttrextInfo(){

    }
}
