package wechat.app.bean.shop.merchant.property;

import java.util.List;

/**
 * Created by Administrator on 2016/8/5.
 */
public class PropertyInfo {
    public String id;
    public String name;
    public List<PropertyValueInfo> property_value;
}
