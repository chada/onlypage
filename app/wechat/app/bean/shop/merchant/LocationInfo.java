package wechat.app.bean.shop.merchant;

/**
 * Created by Administrator on 2016/7/28.
 */
public class LocationInfo {
    //国家(详见《地区列表》说明)
    public String country;
    //省份(详见《地区列表》说明)
    public String province;
    //城市(详见《地区列表》说明)
    public String city;
    //地址
    public String address;
}
