package wechat.app.bean.shop.image;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/8/5.
 */
public class ImageResult extends Result {
    public String image_url;

    public static ImageResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new ImageResult(msg);
        } else {
            ImageResult result  = JSON.parseObject(msg.toJSONString(), ImageResult.class);
            return result;
        }
    }

    private ImageResult() {

    }


    private ImageResult(JSONObject err_msg) {
        super(err_msg);
    }
}
