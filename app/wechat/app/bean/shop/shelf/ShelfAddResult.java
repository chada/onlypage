package wechat.app.bean.shop.shelf;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/8/5.
 */
public class ShelfAddResult extends Result {
    public String shelf_id;

    public static ShelfAddResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new ShelfAddResult(msg);
        } else {
            ShelfAddResult result  = JSON.parseObject(msg.toJSONString(), ShelfAddResult.class);
            return result;
        }
    }

    private ShelfAddResult() {

    }


    private ShelfAddResult(JSONObject err_msg) {
        super(err_msg);
    }
}
