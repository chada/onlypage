package wechat.app.bean.shop.shelf;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/8/5.
 */
public class ShelfGetAllResult extends Result {
   public List<ShelfGetAllShelfInfo> shelves = new ArrayList<>();

    public static ShelfGetAllResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new ShelfGetAllResult(msg);
        } else {
            ShelfGetAllResult result  = new ShelfGetAllResult(msg);
            for (Object o:msg.getJSONArray("shelves")){
                JSONObject object = (JSONObject)o;
                ShelfGetAllShelfInfo info = new ShelfGetAllShelfInfo(object);
                result.shelves.add(info);
            }
            return result;
        }
    }

    private ShelfGetAllResult() {

    }


    private ShelfGetAllResult(JSONObject err_msg) {
        super(err_msg);
    }
}
