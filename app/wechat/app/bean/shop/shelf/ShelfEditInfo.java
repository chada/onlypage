package wechat.app.bean.shop.shelf;

/**
 * Created by Administrator on 2016/8/10.
 */
public class ShelfEditInfo {
    public String shelf_id;
    //货架信息(数据说明详见《货架控件说明》)
    public ShelfData shelf_data;
    //货架招牌图片Url(图片需调用图片上传接口获得图片Url填写至此，否则添加货架失败，建议尺寸为640*120，仅控件1-4有banner，控件5没有banner)
    public String shelf_banner;
    //货架名称
    public String shelf_name;
}
