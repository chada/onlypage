package wechat.app.bean.shop.shelf;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;
import wechat.app.bean.shop.shelf.module.Module;
import wechat.app.bean.shop.shelf.module.moduleFive.ModuleFive;
import wechat.app.bean.shop.shelf.module.moduleFour.ModuleFour;
import wechat.app.bean.shop.shelf.module.moduleOne.ModuleOne;
import wechat.app.bean.shop.shelf.module.moduleThree.ModuleThree;
import wechat.app.bean.shop.shelf.module.moduleTwo.ModuleTwo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/8/5.
 */
public class ShelfGetAllShelfInfo extends Result {
    public String shelf_banner;
    public String shelf_name;
    public String shelf_id;
    //使用时，需要向下获取详细类型，获取更多信息
    public List<Module> modules = new ArrayList<>();


    public ShelfGetAllShelfInfo (){

    }

    public ShelfGetAllShelfInfo (JSONObject object){
//        System.out.println(object);
        this.shelf_banner = object.getString("shelf_banner");
        this.shelf_name = object.getString("shelf_name");
        this.shelf_id = object.getString("shelf_id");

        JSONObject shelf_info = object.getJSONObject("shelf_info");
        for(Object o:shelf_info.getJSONArray("module_infos")){
            JSONObject module_info = (JSONObject)o;
            int eid = module_info.getInteger("eid");
            Module module =null;
            switch (eid){
                case 1: module  = JSON.parseObject(module_info.toJSONString(), ModuleOne.class);break;
                case 2: module  = JSON.parseObject(module_info.toJSONString(), ModuleTwo.class);break;
                case 3: module  = JSON.parseObject(module_info.toJSONString(), ModuleThree.class);break;
                case 4: module  = JSON.parseObject(module_info.toJSONString(), ModuleFour.class);break;
                case 5: module  = JSON.parseObject(module_info.toJSONString(), ModuleFive.class);break;
            }
            this.modules.add(module);
        }
    }
}
