package wechat.app.bean.shop.shelf.module.moduleThree;

import wechat.app.bean.shop.shelf.module.Module;
import wechat.app.bean.shop.shelf.module.moduleThree.GroupInfo;

/**
 * Created by Administrator on 2016/8/10.
 */
public class ModuleThree extends Module {
    //分组信息
    public GroupInfo group_info;

    public ModuleThree() {
        this.eid = 3;
    }
}
