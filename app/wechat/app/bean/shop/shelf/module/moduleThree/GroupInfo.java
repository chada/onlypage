package wechat.app.bean.shop.shelf.module.moduleThree;

import wechat.app.bean.shop.shelf.module.moduleOne.Filter;

/**
 * Created by Administrator on 2016/8/10.
 */
public class GroupInfo {
    //分组ID
    public String group_id;
    //分组照片(图片需调用图片上传接口获得图片Url填写至此，否则添加货架失败，建议分辨率600*208)
    public String img;
}
