package wechat.app.bean.shop.shelf.module.moduleFive;

import wechat.app.bean.shop.shelf.module.moduleFive.Group;

import java.util.List;

/**
 * Created by Administrator on 2016/8/10.
 */
public class GroupInfos {
    //数量为3个
    public List<Group> groups;

    //分组照片(图片需调用图片上传接口获得图片Url填写至此，否则添加货架失败，建议分辨率640*1008)
    public String img_background;
}
