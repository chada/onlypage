package wechat.app.bean.shop.shelf.module.moduleFive;

import wechat.app.bean.shop.shelf.module.Module;
import wechat.app.bean.shop.shelf.module.moduleFive.GroupInfos;

/**
 * Created by Administrator on 2016/8/10.
 */
public class ModuleFive extends Module {
    //分组信息
    public GroupInfos group_infos;

    public ModuleFive(){
        this.eid =5;
    }
}
