package wechat.app.bean.shop.group;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/8/5.
 */
public class GroupQueryResult extends Result {
    public GroupQueryGroupInfo group_detail;

    public static GroupQueryResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new GroupQueryResult(msg);
        } else {
            GroupQueryResult result  = JSON.parseObject(msg.toJSONString(), GroupQueryResult.class);
            return result;
        }
    }

    private GroupQueryResult() {

    }


    private GroupQueryResult(JSONObject err_msg) {
        super(err_msg);
    }
}
