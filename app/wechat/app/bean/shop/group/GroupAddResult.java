package wechat.app.bean.shop.group;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/8/5.
 */
public class GroupAddResult extends Result {
    public String group_id;

    public static GroupAddResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new GroupAddResult(msg);
        } else {
            GroupAddResult result  = JSON.parseObject(msg.toJSONString(), GroupAddResult.class);
            return result;
        }
    }

    private GroupAddResult() {

    }


    private GroupAddResult(JSONObject err_msg) {
        super(err_msg);
    }
}
