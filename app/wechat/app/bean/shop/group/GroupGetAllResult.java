package wechat.app.bean.shop.group;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.List;

/**
 * Created by Administrator on 2016/8/5.
 */
public class GroupGetAllResult extends Result {
    public List<GroupGetAllGroupDetaiInfo> groups_detail;

    public static GroupGetAllResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new GroupGetAllResult(msg);
        } else {
            GroupGetAllResult result  = JSON.parseObject(msg.toJSONString(), GroupGetAllResult.class);
            return result;
        }
    }

    private GroupGetAllResult() {

    }


    private GroupGetAllResult(JSONObject err_msg) {
        super(err_msg);
    }
}
