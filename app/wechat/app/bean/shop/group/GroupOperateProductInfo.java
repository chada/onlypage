package wechat.app.bean.shop.group;

/**
 * Created by Administrator on 2016/8/9.
 * 分组的商品集合
 */
public class GroupOperateProductInfo {
    //商品ID
    public String product_id;
    //修改操作(0-删除, 1-增加)
    public int mod_action;

    private GroupOperateProductInfo() {

    }

    public GroupOperateProductInfo(String product_id, int mod_action) {
        this.product_id = product_id;
        this.mod_action = mod_action;
    }

}
