package wechat.app.bean.shop.group;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/8/9.
 * 4.4	修改分组商品
 */
public class GroupOperateInfo {
    public String group_id;
    public List<GroupOperateProductInfo> product = new ArrayList<>();

    private GroupOperateInfo(){

    }

    public GroupOperateInfo(String group_id){
        this.group_id = group_id;
    }

    public void addProduct(String product_id){
        GroupOperateProductInfo groupOperateProductInfo = new GroupOperateProductInfo(product_id,1);
        this.product.add(groupOperateProductInfo);
    }

    public void removeProduct(String product_id){
        GroupOperateProductInfo groupOperateProductInfo = new GroupOperateProductInfo(product_id,0);
        this.product.add(groupOperateProductInfo);
    }
}
