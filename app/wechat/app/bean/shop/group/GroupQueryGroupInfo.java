package wechat.app.bean.shop.group;

import java.util.List;

/**
 * Created by Administrator on 2016/8/9.
 * 分组集合
 */
public class GroupQueryGroupInfo {
    //分组ID
    public String group_id;
    //分组名称
    public String group_name;
    //商品ID集合
    public List<String> product_list;
}
