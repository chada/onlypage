package wechat.app.bean.shop.order;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/8/5.
 */
public class OrderQueryByIdResult extends Result {
    public OrderInfo order;

    public static OrderQueryByIdResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new OrderQueryByIdResult(msg);
        } else {
            OrderQueryByIdResult result  = JSON.parseObject(msg.toJSONString(), OrderQueryByIdResult.class);
            return result;
        }
    }

    private OrderQueryByIdResult() {

    }


    private OrderQueryByIdResult(JSONObject err_msg) {
        super(err_msg);
    }
}
