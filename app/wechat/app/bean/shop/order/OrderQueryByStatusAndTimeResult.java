package wechat.app.bean.shop.order;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.List;

/**
 * Created by Administrator on 2016/8/5.
 */
public class OrderQueryByStatusAndTimeResult extends Result {
    public List<OrderInfo> order_list;

    public static OrderQueryByStatusAndTimeResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new OrderQueryByStatusAndTimeResult(msg);
        } else {
            OrderQueryByStatusAndTimeResult result  = JSON.parseObject(msg.toJSONString(), OrderQueryByStatusAndTimeResult.class);
            return result;
        }
    }

    private OrderQueryByStatusAndTimeResult() {

    }


    private OrderQueryByStatusAndTimeResult(JSONObject err_msg) {
        super(err_msg);
    }
}
