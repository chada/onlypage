package wechat.app.bean.shop.order;

/**
 * Created by Administrator on 2016/8/5.
 */
public class OrderInfo {
    //订单ID
    public String order_id;
    //订单状态
    public String order_status;
    //订单总价格(单位 : 分)
    public String order_total_price;
    //订单创建时间
    public String order_create_time;
    //订单运费价格(单位 : 分)
    public String order_express_price;
    //买家微信OPENID
    public String buyer_openid;
    //买家微信昵称
    public String buyer_nick;
    //收货人姓名
    public String receiver_name;
    //收货地址省份
    public String receiver_province;
    //收货地址城市
    public String receiver_city;
    //收货地址区/县
    public String receiver_zone;
    //收货详细地址
    public String receiver_address;
    //收货人移动电话
    public String receiver_mobile;
    //收货人固定电话
    public String receiver_phone;
    //商品ID
    public String product_id;
    //商品名称
    public String product_name;
    //商品价格(单位 : 分)
    public String product_price;
    //商品SKU
    public String product_sku;
    //商品个数
    public String product_count;
    //商品图片
    public String product_img;
    //运单ID
    public String delivery_id;
    //物流公司编码
    public String delivery_company;
    //交易ID
    public String trans_id;
}

