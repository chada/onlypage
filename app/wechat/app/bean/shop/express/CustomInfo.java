package wechat.app.bean.shop.express;

/**
 * Created by Administrator on 2016/8/8.
 * 默认邮费计算方法
 */
public class CustomInfo {
    //起始计费数量(比如计费单位是按件, 填2代表起始计费为2件)
    public long StartStandards;
    //起始计费金额(单位: 分）
    public long StartFees;
    //递增计费数量
    public long AddStandards;
    //递增计费金额(单位 : 分)
    public long AddFees;
    //指定国家(详见《地区列表》说明)
    public String DestCountry;
    //指定国家(详见《地区列表》说明)
    public String DestProvince;
    //指定国家(详见《地区列表》说明)
    public String DestCity;

    public CustomInfo(){

    }


    public CustomInfo(long StartStandards, long StartFees, long AddStandards, long AddFees,String DestCountry,String DestProvince,String DestCity) {
        this.StartStandards = StartStandards;
        this.StartFees = StartFees;
        this.AddStandards = AddStandards;
        this.AddFees = AddFees;
        this.DestCountry = DestCountry;
        this.DestProvince = DestProvince;
        this.DestCity = DestCity;
    }
}
