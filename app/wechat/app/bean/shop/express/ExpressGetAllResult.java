package wechat.app.bean.shop.express;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.List;

/**
 * Created by Administrator on 2016/8/5.
 */
public class ExpressGetAllResult extends Result {

    public List<ExpressInfo> templates_info;

    public static ExpressGetAllResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new ExpressGetAllResult(msg);
        } else {
            ExpressGetAllResult result  = JSON.parseObject(msg.toJSONString(), ExpressGetAllResult.class);
            return result;
        }
    }

    private ExpressGetAllResult() {

    }


    private ExpressGetAllResult(JSONObject err_msg) {
        super(err_msg);
    }
}
