package wechat.app.bean.shop.express;

/**
 * Created by Administrator on 2016/8/8.
 * 默认邮费计算方法
 */
public class NormalInfo {
    //起始计费数量(比如计费单位是按件, 填2代表起始计费为2件)
    public long StartStandards;
    //起始计费金额(单位: 分）
    public long StartFees;
    //递增计费数量
    public long AddStandards;
    //递增计费金额(单位 : 分)
    public long AddFees;

    public NormalInfo() {

    }

    public NormalInfo(long StartStandards, long StartFees, long AddStandards, long AddFees) {
        this.StartStandards = StartStandards;
        this.StartFees = StartFees;
        this.AddStandards = AddStandards;
        this.AddFees = AddFees;
    }
}
