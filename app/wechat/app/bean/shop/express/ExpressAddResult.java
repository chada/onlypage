package wechat.app.bean.shop.express;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/8/5.
 */
public class ExpressAddResult extends Result {
    public String template_id;

    public static ExpressAddResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new ExpressAddResult(msg);
        } else {
            ExpressAddResult result  = JSON.parseObject(msg.toJSONString(), ExpressAddResult.class);
            return result;
        }
    }

    private ExpressAddResult() {

    }


    private ExpressAddResult(JSONObject err_msg) {
        super(err_msg);
    }
}
