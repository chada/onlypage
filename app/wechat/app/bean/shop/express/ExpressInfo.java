package wechat.app.bean.shop.express;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */
public class ExpressInfo {
    //只有在获取信息时才有
    public String Id;
    //邮费模板名称
    public String Name;
    //支付方式(0-买家承担运费, 1-卖家承担运费)
    public int Assumer;
    //计费单位(0-按件计费, 1-按重量计费, 2-按体积计费，目前只支持按件计费，默认为0)
    public int Valuation;
    //具体运费计算
    public List<TopFeeInfo> TopFee;

    public ExpressInfo(){

    }


    public ExpressInfo(String Name,int Assumer,int Valuation, List<TopFeeInfo> TopFee){
        this.Name = Name;
        this.Assumer = Assumer;
        this.Valuation = Valuation;
        this.TopFee = TopFee;
    }
}


//NormalInfo normalInfo = new NormalInfo(2,200,10,100);
//CustomInfo customInfo = new CustomInfo(2,200,10,100,"中国","江苏省","苏州市");
//List<CustomInfo> custom = new ArrayList<>();
//custom.add(customInfo);
//        TopFeeInfo topFeeInfo = new TopFeeInfo("10000027",normalInfo,custom);
//        List<TopFeeInfo> topFee = new ArrayList<>();
//        topFee.add(topFeeInfo);
//        ExpressInfo expressInfo = new ExpressInfo("模板一",1,0,topFee);
//
//        ExpressAddResult result = ExpressManage.add(token,expressInfo);
//        if(result.isSuccess()){
//        System.out.println(result.template_id);
//        }else{
//        System.out.println(result.errmsg_zn);
//        }