package wechat.app.bean.shop.express;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 * 具体运费计算
 */
public class TopFeeInfo {
    //快递类型ID(参见增加商品/快递列表)
    public String Type;
    //默认邮费计算方法
    public NormalInfo Normal;
    //指定地区邮费计算方法
    public List<CustomInfo> Custom;


    public TopFeeInfo(){

    }


    public TopFeeInfo(String Type,NormalInfo Normal,List<CustomInfo> Custom){
        this.Type = Type;
        this.Normal = Normal;
        this.Custom = Custom;
    }
}
