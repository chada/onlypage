package wechat.app.bean.shop.express;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/8/5.
 */
public class ExpressGetResult extends Result {

    public ExpressInfo template_info;

    public static ExpressGetResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new ExpressGetResult(msg);
        } else {
            ExpressGetResult result  = JSON.parseObject(msg.toJSONString(), ExpressGetResult.class);
            return result;
        }
    }

    private ExpressGetResult() {

    }


    private ExpressGetResult(JSONObject err_msg) {
        super(err_msg);
    }
}
