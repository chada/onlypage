package wechat.app.bean.entityshop;

import java.util.List;

/**
 * Created by Administrator on 2016/4/5.
 */
public class EditShopInfo {
    public String poi_id;
    public String telephone;
    public List<ShopPhote> photo_list;
    public String recommend;
    public String special;
    public String introduction;
    public String open_time;
    public String avg_price;

    public EditShopInfo(String poi_id, String telephone, List<ShopPhote> photo_list, String recommend, String special, String introduction, String open_time, String avg_price) {
        this.poi_id = poi_id;
        this.telephone = telephone;
        this.photo_list = photo_list;
        this.recommend = recommend;
        this.special = special;
        this.introduction = introduction;
        this.open_time = open_time;
        this.avg_price = avg_price;
    }
}
