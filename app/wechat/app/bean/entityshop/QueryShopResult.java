package wechat.app.bean.entityshop;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/4/5.
 */
public class QueryShopResult extends Result {

    public QueryShopInfo business;

    public static QueryShopResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new QueryShopResult(msg);
        } else {
            QueryShopResult result = new QueryShopResult();
            JSONObject business = msg.getJSONObject("business");
            QueryShopInfo info = JSON.parseObject(business.getJSONObject("base_info").toJSONString(), QueryShopInfo.class);
            result.business = info;
            return result;
        }
    }

    private QueryShopResult() {

    }


    private QueryShopResult(JSONObject err_msg) {
        super(err_msg);
    }
}
