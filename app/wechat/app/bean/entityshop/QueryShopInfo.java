package wechat.app.bean.entityshop;

import java.util.List;

/**
 * Created by Administrator on 2016/4/5.
 */
public class QueryShopInfo {

    public String poi_id;
    //门店是否可用状态。1 表示系统错误、2 表示审核中、3 审核通过、4 审核驳回。当该字段为1、2、4 状态时，poi_id 为空
    public int available_state;
    //扩展字段是否正在更新中。1 表示扩展字段正在更新中，尚未生效，不允许再次更新； 0 表示扩展字段没有在更新中或更新已生效，可以再次更新
    public int update_status;

    public String sid;
    //门店名称（仅为商户名，如：国美、麦当劳，不应包含地区、地址、分店名等信息，错误示例：北京国美）
    public String business_name;
    //分店名称（不应包含地区信息，不应与门店名有重复，错误示例：北京王府井店）
    public String branch_name;
    //门店所在的省份（直辖市填城市名,如：北京市）
    public String province;
    //	门店所在的城市
    public String city;
    //门店所在地区
    public String district;
    //门店所在的详细街道地址（不要填写省市信息）
    public String address;
    //门店的电话（纯数字，区号、分机号均由“-”隔开）
    public String telephone;
    //门店的类型（不同级分类用“,”隔开，如：美食，川菜，火锅。详细分类参见附件：微信门店类目表）
    public String categories ;
    //坐标类型，1 为火星坐标（目前只能选1）
    public String offset_type;
    //门店所在地理位置的经度
    public String longitude;
    //门店所在地理位置的纬度（经纬度均为火星坐标，最好选用腾讯地图标记的坐标）
    public String latitude;
    //图片列表，url 形式，可以有多张图片，尺寸为640*340px。必须为上一接口生成的url。图片内容不允许与门店不相关，不允许为二维码、员工合照（或模特肖像）、营业执照、无门店正门的街景、地图截图、公交地铁站牌、菜单截图等
    public List<String> photo_list ;
    //推荐品，餐厅可为推荐菜；酒店为推荐套房；景点为推荐游玩景点等，针对自己行业的推荐内容
    public String recommend;
    //特色服务，如免费wifi，免费停车，送货上门等商户能提供的特色功能或服务
    public String special;
    //商户简介，主要介绍商户信息等
    public String introduction;
    //	营业时间，24 小时制表示，用“-”连接，如 8:00-20:00
    public String open_time;
    //人均价格，大于0 的整数
    public String avg_price;
}
