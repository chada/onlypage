package wechat.app.bean.entityshop;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/4/5.
 */
public class CategoryListResult extends Result {
    public List<String> categoryList =  new ArrayList<>();

    public static CategoryListResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new CategoryListResult(msg);
        } else {
            CategoryListResult result = new CategoryListResult();
            JSONArray array = msg.getJSONArray("category_list");
            for (Object object : array) {
                result.categoryList.add(object.toString());
            }
            return result;
        }
    }

    private CategoryListResult() {

    }


    private CategoryListResult(JSONObject err_msg) {
        super(err_msg);
    }
}
