package wechat.app.bean.entityshop;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/4/5.
 */
public class QueryShopListResult extends Result {

    public List<QueryShopInfo> business_list= new ArrayList<>();


    public static QueryShopListResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new QueryShopListResult(msg);
        } else {
            QueryShopListResult result = new QueryShopListResult();
            JSONArray array = msg.getJSONArray("business_list");
            for (Object object : array) {
                JSONObject temp = (JSONObject)object;
                QueryShopInfo info = JSON.parseObject(temp.getJSONObject("base_info").toJSONString(), QueryShopInfo.class);
                result.business_list.add(info);
            }
            return result;
        }
    }

    private QueryShopListResult() {

    }


    private QueryShopListResult(JSONObject err_msg) {
        super(err_msg);
    }
}
