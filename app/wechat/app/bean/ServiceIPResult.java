package wechat.app.bean;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/4/1.
 */
public class ServiceIPResult extends Result {

    public List<String> ServiceIPList = new ArrayList<>();
    public static ServiceIPResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new ServiceIPResult(msg);
        } else {
            ServiceIPResult result = new ServiceIPResult();
            JSONArray array = msg.getJSONArray("ip_list");
            for (Object object : array) {
                result.ServiceIPList.add(object.toString());
            }
            return result;
        }
    }

    private ServiceIPResult() {

    }


    private ServiceIPResult(JSONObject err_msg) {
        super(err_msg);
    }
}
