package wechat.app.bean.dictionary;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.node.POJONode;
import util.LoggerUtil;

/**
 * Created by Administrator on 2016/8/8.
 */
public class LocationDictionary {


    public static JSONObject getDate(){
        JSONObject data = new JSONObject();
        JSONObject country = addCountry(data,"中国");

        JSONObject provice = addProvince(country, "北京市");
        addCity(provice,"北京市");


        provice = addProvince(country,"天津市");
        addCity(provice,"天津市");

        provice = addProvince(country,"河北省");
        addCity(provice,"石家庄市\n" +
                "唐山市\n" +
                "秦皇岛市\n" +
                "邯郸市\n" +
                "邢台市\n" +
                "保定市\n" +
                "张家口市\n" +
                "承德市\n" +
                "沧州市\n" +
                "廊坊市\n" +
                "衡水市");

        provice = addProvince(country,"山西省");
        addCity(provice,"太原市\n" +
                "大同市\n" +
                "阳泉市\n" +
                "长治市\n" +
                "晋城市\n" +
                "朔州市\n" +
                "晋中市\n" +
                "运城市\n" +
                "忻州市\n" +
                "临汾市\n" +
                "吕梁市\n");

        provice = addProvince(country,"内蒙古自治区");
        addCity(provice,"呼和浩特市\n" +
                "包头市\n" +
                "乌海市\n" +
                "赤峰市\n" +
                "通辽市\n" +
                "鄂尔多斯市\n" +
                "呼伦贝尔市\n" +
                "巴彦淖尔市\n" +
                "乌兰察布市\n" +
                "兴安盟\n" +
                "锡林郭勒盟\n" +
                "阿拉善盟\n");

        provice = addProvince(country,"辽宁省");
        addCity(provice,"沈阳市\n" +
                "大连市\n" +
                "鞍山市\n" +
                "抚顺市\n" +
                "本溪市\n" +
                "丹东市\n" +
                "锦州市\n" +
                "营口市\n" +
                "阜新市\n" +
                "辽阳市\n" +
                "盘锦市\n" +
                "铁岭市\n" +
                "朝阳市\n" +
                "葫芦岛市\n");

        provice = addProvince(country,"吉林省");
        addCity(provice,"长春市\n" +
                "吉林市\n" +
                "四平市\n" +
                "辽源市\n" +
                "通化市\n" +
                "白山市\n" +
                "松原市\n" +
                "白城市\n" +
                "延边朝鲜族自治州\n");

        provice = addProvince(country,"黑龙江省");
        addCity(provice,"哈尔滨市\n" +
                "齐齐哈尔市\n" +
                "鸡西市\n" +
                "鹤岗市\n" +
                "双鸭山市\n" +
                "大庆市\n" +
                "伊春市\n" +
                "佳木斯市\n" +
                "七台河市\n" +
                "牡丹江市\n" +
                "黑河市\n" +
                "绥化市\n" +
                "大兴安岭地区\n");

        provice = addProvince(country,"上海市");
        addCity(provice,"上海市");

        provice = addProvince(country,"江苏省");
        addCity(provice,"南京市\n" +
                "无锡市\n" +
                "徐州市\n" +
                "常州市\n" +
                "苏州市\n" +
                "南通市\n" +
                "连云港市\n" +
                "淮安市\n" +
                "盐城市\n" +
                "扬州市\n" +
                "镇江市\n" +
                "泰州市\n" +
                "宿迁市\n");

        provice = addProvince(country,"浙江省");
        addCity(provice,"杭州市\n" +
                "宁波市\n" +
                "温州市\n" +
                "嘉兴市\n" +
                "湖州市\n" +
                "绍兴市\n" +
                "金华市\n" +
                "衢州市\n" +
                "舟山市\n" +
                "台州市\n" +
                "丽水市\n");

        provice = addProvince(country,"安徽省");
        addCity(provice,"合肥市\n" +
                "芜湖市\n" +
                "蚌埠市\n" +
                "淮南市\n" +
                "马鞍山市\n" +
                "淮北市\n" +
                "铜陵市\n" +
                "安庆市\n" +
                "黄山市\n" +
                "滁州市\n" +
                "阜阳市\n" +
                "宿州市\n" +
                "六安市\n" +
                "亳州市\n" +
                "池州市\n" +
                "宣城市\n");

        provice = addProvince(country,"福建省");
        addCity(provice,"福州市\n" +
                "厦门市\n" +
                "莆田市\n" +
                "三明市\n" +
                "泉州市\n" +
                "漳州市\n" +
                "南平市\n" +
                "龙岩市\n" +
                "宁德市\n");

        provice = addProvince(country,"江西省");
        addCity(provice,"南昌市\n" +
                "景德镇市\n" +
                "萍乡市\n" +
                "九江市\n" +
                "新余市\n" +
                "鹰潭市\n" +
                "赣州市\n" +
                "吉安市\n" +
                "宜春市\n" +
                "抚州市\n" +
                "上饶市\n");

        provice = addProvince(country,"山东省");
        addCity(provice,"济南市\n" +
                "青岛市\n" +
                "淄博市\n" +
                "枣庄市\n" +
                "东营市\n" +
                "烟台市\n" +
                "潍坊市\n" +
                "济宁市\n" +
                "泰安市\n" +
                "威海市\n" +
                "日照市\n" +
                "莱芜市\n" +
                "临沂市\n" +
                "德州市\n" +
                "聊城市\n" +
                "滨州市\n" +
                "菏泽市\n");

        provice = addProvince(country,"河南省");
        addCity(provice,"郑州市\n" +
                "开封市\n" +
                "洛阳市\n" +
                "平顶山市\n" +
                "安阳市\n" +
                "鹤壁市\n" +
                "新乡市\n" +
                "焦作市\n" +
                "濮阳市\n" +
                "许昌市\n" +
                "漯河市\n" +
                "三门峡市\n" +
                "南阳市\n" +
                "商丘市\n" +
                "信阳市\n" +
                "周口市\n" +
                "驻马店市\n" +
                "省直辖县级行政区划\n");

        provice = addProvince(country,"湖北省");
        addCity(provice,"武汉市\n" +
                "黄石市\n" +
                "十堰市\n" +
                "宜昌市\n" +
                "襄阳市\n" +
                "鄂州市\n" +
                "荆门市\n" +
                "孝感市\n" +
                "荆州市\n" +
                "黄冈市\n" +
                "咸宁市\n" +
                "随州市\n" +
                "恩施土家族苗族自治州\n" +
                "省直辖县级行政区划\n");

        provice = addProvince(country,"湖南省");
        addCity(provice,"长沙市\n" +
                "株洲市\n" +
                "湘潭市\n" +
                "衡阳市\n" +
                "邵阳市\n" +
                "岳阳市\n" +
                "常德市\n" +
                "张家界市\n" +
                "益阳市\n" +
                "郴州市\n" +
                "永州市\n" +
                "怀化市\n" +
                "娄底市\n" +
                "湘西土家族苗族自治州\n");

        provice = addProvince(country,"广东省");
        addCity(provice,"广州市\n" +
                "韶关市\n" +
                "深圳市\n" +
                "珠海市\n" +
                "汕头市\n" +
                "佛山市\n" +
                "江门市\n" +
                "湛江市\n" +
                "茂名市\n" +
                "肇庆市\n" +
                "惠州市\n" +
                "梅州市\n" +
                "汕尾市\n" +
                "河源市\n" +
                "阳江市\n" +
                "清远市\n" +
                "东莞市\n" +
                "中山市\n" +
                "潮州市\n" +
                "揭阳市\n" +
                "云浮市\n");

        provice = addProvince(country,"广西壮族自治区");
        addCity(provice,"南宁市\n" +
                "柳州市\n" +
                "桂林市\n" +
                "梧州市\n" +
                "北海市\n" +
                "防城港市\n" +
                "钦州市\n" +
                "贵港市\n" +
                "玉林市\n" +
                "百色市\n" +
                "贺州市\n" +
                "河池市\n" +
                "来宾市\n" +
                "崇左市\n");

        provice = addProvince(country,"海南省");
        addCity(provice,"海口市\n" +
                "三亚市\n" +
                "三沙市\n" +
                "省直辖县级行政区划\n");

        provice = addProvince(country,"重庆市");
        addCity(provice,"重庆市");

        provice = addProvince(country,"四川省");
        addCity(provice,"成都市\n" +
                "自贡市\n" +
                "攀枝花市\n" +
                "泸州市\n" +
                "德阳市\n" +
                "绵阳市\n" +
                "广元市\n" +
                "遂宁市\n" +
                "内江市\n" +
                "乐山市\n" +
                "南充市\n" +
                "眉山市\n" +
                "宜宾市\n" +
                "广安市\n" +
                "达州市\n" +
                "雅安市\n" +
                "巴中市\n" +
                "资阳市\n" +
                "阿坝藏族羌族自治州\n" +
                "甘孜藏族自治州\n" +
                "凉山彝族自治州\n");

        provice = addProvince(country,"贵州省");
        addCity(provice,"贵阳市\n" +
                "六盘水市\n" +
                "遵义市\n" +
                "安顺市\n" +
                "毕节市\n" +
                "铜仁市\n" +
                "黔西南布依族苗族自治州\n" +
                "黔东南苗族侗族自治州\n" +
                "黔南布依族苗族自治州\n");

        provice = addProvince(country,"云南省");
        addCity(provice,"昆明市\n" +
                "曲靖市\n" +
                "玉溪市\n" +
                "保山市\n" +
                "昭通市\n" +
                "丽江市\n" +
                "普洱市\n" +
                "临沧市\n" +
                "楚雄彝族自治州\n" +
                "红河哈尼族彝族自治州\n" +
                "文山壮族苗族自治州\n" +
                "西双版纳傣族自治州\n" +
                "大理白族自治州\n" +
                "德宏傣族景颇族自治州\n" +
                "怒江傈僳族自治州\n" +
                "迪庆藏族自治州\n");

        provice = addProvince(country,"西藏自治区");
        addCity(provice,"拉萨市\n" +
                "昌都地区\n" +
                "山南地区\n" +
                "日喀则地区\n" +
                "那曲地区\n" +
                "阿里地区\n" +
                "林芝地区\n");

        provice = addProvince(country,"陕西省");
        addCity(provice,"西安市\n" +
                "铜川市\n" +
                "宝鸡市\n" +
                "咸阳市\n" +
                "渭南市\n" +
                "延安市\n" +
                "汉中市\n" +
                "榆林市\n" +
                "安康市\n" +
                "商洛市\n");

        provice = addProvince(country,"甘肃省");
        addCity(provice,"兰州市\n" +
                "嘉峪关市\n" +
                "白银市\n" +
                "武威市\n" +
                "张掖市\n" +
                "平凉市\n" +
                "酒泉市\n" +
                "庆阳市\n" +
                "定西市\n" +
                "陇南市\n" +
                "临夏回族自治州\n" +
                "甘南藏族自治州\n");

        provice = addProvince(country,"青海省");
        addCity(provice,"西宁市\n" +
                "海东地区\n" +
                "海北藏族自治州\n" +
                "黄南藏族自治州\n" +
                "海南藏族自治州\n" +
                "果洛藏族自治州\n" +
                "玉树藏族自治州\n" +
                "海西蒙古族藏族自治州\n");

        provice = addProvince(country,"宁夏回族自治区");
        addCity(provice,"银川市\n" +
                "石嘴山市\n" +
                "吴忠市\n" +
                "固原市\n" +
                "中卫市\n");

        provice = addProvince(country,"新疆维吾尔自治区");
        addCity(provice, "乌鲁木齐市\n" +
                "克拉玛依市\n" +
                "吐鲁番地区\n" +
                "哈密地区\n" +
                "昌吉回族自治州\n" +
                "博尔塔拉蒙古自治州\n" +
                "巴音郭楞蒙古自治州\n" +
                "阿克苏地区\n" +
                "克孜勒苏柯尔克孜自治州\n" +
                "喀什地区\n" +
                "和田地区\n" +
                "伊犁哈萨克自治州\n" +
                "塔城地区\n" +
                "阿勒泰地区\n" +
                "自治区直辖县级行政区划\n");


//        LoggerUtil.info(data.toString(), LoggerUtil.LoggerState.dir1);
        return data;
    }

    private static JSONObject addCountry(JSONObject parent,String name){
        JSONObject countryObject = new JSONObject();
        countryObject.put("countryName", name);
        if(parent.containsKey("country")){
            JSONArray country = parent.getJSONArray("country");
            country.add(countryObject);
        }else{
            JSONArray country = new JSONArray();
            country.add(countryObject);
            parent.put("country",country);
        }
        return  countryObject;
    }


    private static JSONObject addProvince(JSONObject parent,String name){
        JSONObject provinceObject = new JSONObject();
        provinceObject.put("provinceName",name);
        if(parent.containsKey("province")){
            JSONArray province = parent.getJSONArray("province");
            province.add(provinceObject);
        }else{
            JSONArray province = new JSONArray();
            province.add(provinceObject);
            parent.put("province",province);
        }
        return  provinceObject;
    }


    private static void addCity(JSONObject parent,String nameList){
        for(String name:nameList.split("\\n")){
            JSONObject cityObject = new JSONObject();
            cityObject.put("cityName",name);
            if(parent.containsKey("city")){
                JSONArray city = parent.getJSONArray("city");
                city.add(cityObject);
            }else{
                JSONArray city = new JSONArray();
                city.add(cityObject);
                parent.put("city",city);
            }
        }
    }
}
