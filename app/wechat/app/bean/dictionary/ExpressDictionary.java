package wechat.app.bean.dictionary;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by Administrator on 2016/8/8.
 */
public class ExpressDictionary {
    //10000027	平邮
    //10000028	快递
    //10000029	EMS

    public static JSONObject getDate() {
        JSONObject object = new JSONObject();

        JSONArray array = new JSONArray();

        JSONObject express = new JSONObject();
        express.put("name","平邮");
        express.put("id","10000027");
        array.add(express);

        express = new JSONObject();
        express.put("name","快递");
        express.put("id","10000028");
        array.add(express);

        express = new JSONObject();
        express.put("name","EMS");
        express.put("id","10000029");
        array.add(express);


        object.put("express", array);
//        System.out.println(object);
        return object;
    }
}
