package wechat.app.bean.dictionary;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by Administrator on 2016/8/8.
 * 快递公司
 */
public class ExpressCompanyDictionary {

    public static JSONObject getDate() {
        JSONObject object = new JSONObject();

        JSONArray array = new JSONArray();

        JSONObject express = new JSONObject();
        express.put("name", "邮政EMS");
        express.put("id", "Fsearch_code");
        array.add(express);

        express = new JSONObject();
        express.put("name", "申通快递");
        express.put("id", "002shentong");
        array.add(express);

        express = new JSONObject();
        express.put("name", "中通速递");
        express.put("id", "066zhongtong");
        array.add(express);

        express = new JSONObject();
        express.put("name", "圆通速递");
        express.put("id", "056yuantong");
        array.add(express);

        express = new JSONObject();
        express.put("name", "天天快递");
        express.put("id", "042tiantian");
        array.add(express);

        express = new JSONObject();
        express.put("name", "顺丰速运");
        express.put("id", "003shunfeng");
        array.add(express);

        express = new JSONObject();
        express.put("name", "韵达快运");
        express.put("id", "059Yunda");
        array.add(express);

        express = new JSONObject();
        express.put("name", "宅急送");
        express.put("id", "064zhaijisong");
        array.add(express);

        express = new JSONObject();
        express.put("name", "汇通快运");
        express.put("id", "020huitong");
        array.add(express);

        express = new JSONObject();
        express.put("name", "易迅快递");
        express.put("id", "zj001yixun");
        array.add(express);

        object.put("express", array);
        return object;
    }
}
