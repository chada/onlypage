package wechat.app.bean.media;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.List;

/**
 * Created by Administrator on 2016/3/23.
 */
public class GetPermanentNewsResult extends Result {
    public List<Article> articles;


    public static GetPermanentNewsResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new GetPermanentNewsResult(msg);
        } else {
            JSONArray news_item = msg.getJSONArray("news_item");
            List<Article> articles = JSON.parseArray(news_item.toJSONString(), Article.class);
            return new GetPermanentNewsResult(articles);
        }
    }

    public GetPermanentNewsResult(List<Article> articles){
        this.articles =articles;
    }

    public GetPermanentNewsResult(JSONObject err_msg){
        super(err_msg);
    }
}
