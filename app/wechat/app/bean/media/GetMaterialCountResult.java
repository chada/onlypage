package wechat.app.bean.media;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/3/29.
 */
public class GetMaterialCountResult extends Result {

    public int voice_count;
    public int video_count;
    public int image_count;
    public int news_count;

    public static GetMaterialCountResult create(JSONObject msg) {
        if (msg.containsKey("errcode") && msg.getInteger("errcode") != 0) {
            return new GetMaterialCountResult(msg);
        } else {
            GetMaterialCountResult result = new GetMaterialCountResult();
            result.voice_count = msg.getInteger("voice_count");
            result.video_count = msg.getInteger("video_count");
            result.image_count = msg.getInteger("image_count");
            result.news_count = msg.getInteger("news_count");
            return result;
        }
    }

    private GetMaterialCountResult() {

    }

    private GetMaterialCountResult(JSONObject err_msg) {
        super(err_msg);
    }
}
