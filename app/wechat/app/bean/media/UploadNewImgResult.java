package wechat.app.bean.media;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/3/29.
 */
public class UploadNewImgResult extends Result{
    public String url;


    public static UploadNewImgResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new UploadNewImgResult(msg);
        } else {
            UploadNewImgResult result = new UploadNewImgResult();
            result.url = msg.getString("url");
            return result;
        }
    }

    private UploadNewImgResult(){

    }

    private UploadNewImgResult(JSONObject err_msg){
        super(err_msg);
    }
}
