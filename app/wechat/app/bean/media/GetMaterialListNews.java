package wechat.app.bean.media;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/3/29.
 */
public class GetMaterialListNews {
    public String media_id;
    public List<Article> articles = new ArrayList<Article>();

    public GetMaterialListNews(JSONObject object){
        this.media_id = object.getString("media_id");
        this.articles = JSON.parseArray(object.getJSONObject("content").getJSONArray("news_item").toJSONString(), Article.class);
    }

}
