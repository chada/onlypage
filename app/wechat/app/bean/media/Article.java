package wechat.app.bean.media;

import com.alibaba.fastjson.JSONObject;
import util.Common;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/22.
 */
public class Article {
    //标题
    public String title;
    //图文消息的封面图片素材id（必须是永久mediaID）
    public String thumb_media_id;
    //作者
    public String author;
    //图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空
    public String digest;
    //是否显示封面，0为false，即不显示，1为true，即显示
    public String show_cover_pic;
    //图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS
    public String content;
    //图文消息的原文地址，即点击“阅读原文”后的URL
    public String content_source_url;

    //只有获取的时候有
    public String url;

    //用于json转换
    public Article(){

    }

    public Article(String title, String thumb_media_id, String author, String digest, String show_cover_pic, String content, String content_source_url){
        this.title = title;
        this.thumb_media_id = thumb_media_id;
        this.author = author;
        this.digest = digest;
        this.show_cover_pic = show_cover_pic;
        this.content = content;
        this.content_source_url = content_source_url;
    }

//    public JSONObject getJSONObject(){
//        JSONObject object = new JSONObject();
//        List<Field> fields = Common.getFields(this);
//        for(Field field :fields){
//            if(Common.getPropertyOfObject(this, field.getName())!=null){
//                object.put(field.getName(), Common.getPropertyOfObject(this, field.getName()));
//            }
//        }
//        return object;
//    }
}
