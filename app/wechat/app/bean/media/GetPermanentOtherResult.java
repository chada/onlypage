package wechat.app.bean.media;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.io.File;

/**
 * Created by Administrator on 2016/3/29.
 */
public class GetPermanentOtherResult extends Result {

    public File file;


    public static GetPermanentOtherResult create(File file){
        GetPermanentOtherResult permanentOtherResult = new GetPermanentOtherResult();
        permanentOtherResult.file = file;
        return permanentOtherResult;
    }

    public static GetPermanentOtherResult create( JSONObject err_msg){
        return new GetPermanentOtherResult(err_msg);
    }

    private GetPermanentOtherResult(){

    }

    private GetPermanentOtherResult(JSONObject err_msg){
        super(err_msg);
    }
}
