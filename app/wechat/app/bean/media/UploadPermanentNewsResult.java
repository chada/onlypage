package wechat.app.bean.media;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/3/29.
 */
public class UploadPermanentNewsResult extends Result {
    public String media_id;
    public static UploadPermanentNewsResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new UploadPermanentNewsResult(msg);
        } else {
            UploadPermanentNewsResult result = new UploadPermanentNewsResult();
            result.media_id = msg.getString("media_id");
            return result;
        }
    }

    private UploadPermanentNewsResult(){

    }

    private UploadPermanentNewsResult(JSONObject err_msg){
        super(err_msg);
    }
}
