package wechat.app.bean.media;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/3/29.
 */
public class GetMaterialListResult extends Result{
    //该类型的素材的总数
    public int total_count;
    //本次调用获取的素材的数量
    public int item_count;
    //素材的类型，图片（image）、视频（video）、语音 （voice）、图文（news）
    public String type;
    //图文（news）
    public List<GetMaterialListNews> news_items = new ArrayList<GetMaterialListNews>();
    //图片（image）、视频（video）、语音 （voice）
    public List<GetMaterialListOther> items = new ArrayList<GetMaterialListOther>();


    public static GetMaterialListResult create(String type,JSONObject msg){
        if(!msg.containsKey("errcode")){
            GetMaterialListResult result = new GetMaterialListResult();
            result.item_count = msg.getInteger("item_count");
            result.total_count = msg.getInteger("total_count");
            if(type.equals("news")){
                JSONArray item =  msg.getJSONArray("item");
                for(Object o:item){
                    JSONObject object = (JSONObject)o;
                    result.news_items.add(new GetMaterialListNews(object));
                }
            }else{
                result.items = JSON.parseArray(msg.getJSONArray("item").toJSONString(), GetMaterialListOther.class);
            }
            return result;
        }else {
            return  new GetMaterialListResult(msg);
        }
    }

    private GetMaterialListResult(){

    }

    private GetMaterialListResult(JSONObject err_msg){
        super(err_msg);
    }
}
