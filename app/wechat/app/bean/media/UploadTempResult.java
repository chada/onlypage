package wechat.app.bean.media;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/3/16.
 */
public class UploadTempResult extends Result{
    public String type;
    public String media_id;
    public String created_at;

    public static UploadTempResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new UploadTempResult(msg);
        } else {
            UploadTempResult result = new UploadTempResult();
            result.type = msg.getString("type");
            result.media_id = msg.getString("media_id");
            result.created_at = msg.getString("created_at");
            return result;
        }
    }

    private UploadTempResult(){

    }

    private UploadTempResult(JSONObject err_msg){
        super(err_msg);
    }
}
