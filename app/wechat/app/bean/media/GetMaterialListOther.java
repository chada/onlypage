package wechat.app.bean.media;

/**
 * Created by Administrator on 2016/3/29.
 */
public class GetMaterialListOther {
    public String media_id;
    //	文件名称
    public String name;
    //	这篇图文消息素材的最后更新时间
    public String update_time;
    //图文页的URL，或者，当获取的列表是图片素材列表时，该字段是图片的URL
    public String url;
}
