package wechat.app.bean.media;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.io.File;

/**
 * Created by Administrator on 2016/3/29.
 */
public class GetTempResult extends Result {

    public File file;


    public static GetTempResult create(File file){
        GetTempResult permanentOtherResult = new GetTempResult();
        permanentOtherResult.file = file;
        return permanentOtherResult;
    }

    public static GetTempResult create( JSONObject err_msg){
        return new GetTempResult(err_msg);
    }

    private GetTempResult(){

    }

    private GetTempResult(JSONObject err_msg){
        super(err_msg);
    }
}
