package wechat.app.bean.media;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/3/29.
 */
public class GetPermanentVedioResult extends Result {

    public String title;
    public String description;
    public String down_url;


    public static GetPermanentVedioResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new GetPermanentVedioResult(msg);
        } else {
            GetPermanentVedioResult result = new GetPermanentVedioResult();
            result.title = msg.getString("title");
            result.description = msg.getString("description");
            result.down_url = msg.getString("down_url");
            return  result;
        }
    }

    private GetPermanentVedioResult(){

    }

    private GetPermanentVedioResult(JSONObject err_msg){
        super(err_msg);
    }
}
