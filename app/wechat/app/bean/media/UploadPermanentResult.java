package wechat.app.bean.media;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/3/16.
 */
public class UploadPermanentResult extends Result{
    public String url;
    public String media_id;


    public static UploadPermanentResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new UploadPermanentResult(msg);
        } else {
            UploadPermanentResult result = new UploadPermanentResult();
            result.url = msg.getString("url");
            result.media_id = msg.getString("media_id");
            return result;
        }
    }

    private UploadPermanentResult(){

    }

    private UploadPermanentResult(JSONObject err_msg){
        super(err_msg);
    }
}
