package wechat.app.bean.account;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;
/**
 * Created by Administrator on 2016/3/30.
 */
public class GetShortUrlResult extends Result {
    public String short_url;

    public static GetShortUrlResult create(JSONObject msg){
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new GetShortUrlResult(msg);
        } else {
            GetShortUrlResult result = new GetShortUrlResult();
            result.short_url = msg.getString("short_url");
            return result;
        }
    }

    private GetShortUrlResult(){

    }

    private GetShortUrlResult(JSONObject err_msg){
        super(err_msg);
    }
}
