package wechat.app.bean.account;

import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/3/30.
 */
public class QRCodeTicketResult extends Result {
    public String ticket;


    public static QRCodeTicketResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new QRCodeTicketResult(msg);
        } else {
            QRCodeTicketResult result = new QRCodeTicketResult();
            result.ticket = msg.getString("ticket");
            return result;
        }
    }

    private QRCodeTicketResult(){

    }

    private QRCodeTicketResult(JSONObject err_msg){
        super(err_msg);
    }
}
