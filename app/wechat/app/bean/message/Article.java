package wechat.app.bean.message;

/**
 * Created by Administrator on 2016/3/31.
 * 被动回复的图文信息
 * 与主动是有区别的
 */
public class Article {
    public String Title;
    public String Description;
    public String PicUrl;
    public String Url;

    public Article(String Title,String Description,String PicUrl,String Url){
        this.Title = Title;
        this.Description = Description;
        this.PicUrl = PicUrl;
        this.Url = Url;
    }

    public String toXml(){
        String str ="";
        str +="<Title>"+Title+"</Title>";
        str +="<Description>"+Description+"</Description>";
        str +="<PicUrl>"+PicUrl+"</PicUrl>";
        str +="<Url>"+Url+"</Url>";
        return str;
    }
}
