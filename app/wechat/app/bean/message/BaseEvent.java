package wechat.app.bean.message;

/**
 * Created by Administrator on 2016/3/31.
 */
public class BaseEvent {
    //开发者微信号
    public String ToUserName = null;
    //发送方帐号（一个OpenID）
    public String FromUserName = null;
    //消息创建时间 （整型）
    public long CreateTime;
    //text,image,voice,video,shortvideo,location,link,event
    public Message.MsgTypeEnum MsgType;

    //事件类型，subscribe(订阅)、unsubscribe(取消订阅)，SCAN（扫描带参数二维码事件），LOCATION（上报地理位置事件），CLICK（点击菜单拉取消息时的事件推送），VIEW（点击菜单跳转链接时的事件推送）
    public Message.EventEnum Event;

}
