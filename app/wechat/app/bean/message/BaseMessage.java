package wechat.app.bean.message;

/**
 * Created by Administrator on 2016/3/31.
 */
public class BaseMessage {
    //开发者微信号
    public String ToUserName = null;
    //发送方帐号（一个OpenID）
    public String FromUserName = null;
    //消息创建时间 （整型）
    public long CreateTime;
    //text,image,voice,video,shortvideo,location,link,event
    public Message.MsgTypeEnum MsgType;
}
