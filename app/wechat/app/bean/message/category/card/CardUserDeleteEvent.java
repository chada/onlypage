package wechat.app.bean.message.category.card;

import util.Common;
import wechat.app.bean.message.BaseEvent;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class CardUserDeleteEvent extends BaseEvent {

    //卡券ID。
    public String CardId;
    //code序列号。自定义code及非自定义code的卡券被领取后都支持事件推送。
    public String UserCardCode;

    public CardUserDeleteEvent(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
