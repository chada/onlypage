package wechat.app.bean.message.category.card;

import util.Common;
import wechat.app.bean.message.BaseEvent;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 * 券点流水详情事件
 */
public class CardPayOrderEvent extends BaseEvent {

    public enum Status{ORDER_STATUS_WAITING ,ORDER_STATUS_SUCC ,ORDER_STATUS_FINANCE_SUCC ,ORDER_STATUS_QUANTITY_SUCC ,ORDER_STATUS_HAS_REFUND
    ,ORDER_STATUS_REFUND_WAITING ,ORDER_STATUS_ROLLBACK ,ORDER_STATUS_HAS_RECEIPT }
    public enum OrderType{ORDER_TYPE_SYS_ADD ,ORDER_TYPE_WXPAY ,ORDER_TYPE_REFUND ,ORDER_TYPE_REDUCE,ORDER_TYPE_SYS_REDUCE }
    //本次推送对应的订单号。
    public String OrderId;
    //本次订单号的状态,ORDER_STATUS_WAITING 等待支付 ORDER_STATUS_SUCC 支付成功 ORDER_STATUS_FINANCE_SUCC 加代币成功 ORDER_STATUS_QUANTITY_SUCC 加库存成功 ORDER_STATUS_HAS_REFUND 已退币 ORDER_STATUS_REFUND_WAITING 等待退币确认 ORDER_STATUS_ROLLBACK 已回退,系统失败 ORDER_STATUS_HAS_RECEIPT 已开发票
    public Status Status;
    //购买券点时，支付二维码的生成时间
    public String CreateTime;
    //购买券点时，实际支付成功的时间
    public String PayFinishTime;
    //支付方式，一般为微信支付充值
    public String Desc;
    //	剩余免费券点数量
    public int FreeCoinCount;
    //剩余付费券点数量
    public int PayCoinCount;
    //本次变动的免费券点数量
    public int RefundFreeCoinCount;
    //本次变动的付费券点数量
    public int RefundPayCoinCount;
    //所要拉取的订单类型    ORDER_TYPE_SYS_ADD 平台赠送券点 ORDER_TYPE_WXPAY 充值券点 ORDER_TYPE_REFUND 库存未使用回退券点 ORDER_TYPE_REDUCE 券点兑换库存 ORDER_TYPE_SYS_REDUCE 平台扣减
    public OrderType OrderType;
    //系统备注，说明此次变动的缘由，如开通账户奖励、门店奖励、核销奖励以及充值、扣减。
    public String Memo;
    //所开发票的详情
    public String ReceiptInfo;

    public CardPayOrderEvent(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
