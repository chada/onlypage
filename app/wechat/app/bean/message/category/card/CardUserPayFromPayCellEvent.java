package wechat.app.bean.message.category.card;

import util.Common;
import wechat.app.bean.message.BaseEvent;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class CardUserPayFromPayCellEvent extends BaseEvent {

    //卡券ID。
    public String CardId;
    //卡券Code码。
    public String UserCardCode;
    //	微信支付交易订单号（只有使用买单功能核销的卡券才会出现）
    public String TransId;
    //门店ID，当前卡券核销的门店ID（只有通过卡券商户助手和买单核销时才会出现）
    public String LocationId;
    //实付金额，单位为分
    public String Fee;
    //应付金额，单位为分
    public String OriginalFee;


    public CardUserPayFromPayCellEvent(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
