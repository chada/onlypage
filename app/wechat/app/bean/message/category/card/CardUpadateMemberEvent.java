package wechat.app.bean.message.category.card;

import util.Common;
import wechat.app.bean.message.BaseEvent;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class CardUpadateMemberEvent extends BaseEvent {

    //卡券ID。
    public String CardId;
    //	Code码。
    public String UserCardCode;
    //变动的积分值。
    public int ModifyBonus;
    //变动的余额值。
    public int ModifyBalance;


    public CardUpadateMemberEvent(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
