package wechat.app.bean.message.category.card;

import util.Common;
import wechat.app.bean.message.BaseEvent;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class CardUserConsumeEvent extends BaseEvent {

    //卡券ID。
    public String CardId;
    //卡券Code码。
    public String UserCardCode;
    //核销来源。支持开发者统计API核销（FROM_API）、公众平台核销（FROM_MP）、卡券商户助手核销（FROM_MOBILE_HELPER）（核销员微信号）
    public String ConsumeSource;
    //门店名称，当前卡券核销的门店名称（只有通过卡券商户助手和买单核销时才会出现）
    public String LocationId;
    //核销该卡券核销员的openid（只有通过卡券商户助手核销时才会出现）
    public String StaffOpenId;


    public CardUserConsumeEvent(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
