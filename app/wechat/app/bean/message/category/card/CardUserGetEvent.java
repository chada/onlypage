package wechat.app.bean.message.category.card;

import util.Common;
import wechat.app.bean.message.BaseEvent;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class CardUserGetEvent extends BaseEvent {

    //卡券ID。
    public String CardId;
    //是否为转赠，1代表是，0代表否。
    public String IsGiveByFriend;
    //code序列号。自定义code及非自定义code的卡券被领取后都支持事件推送。
    public String UserCardCode;
    //转赠前的code序列号。
    public String OldUserCardCode;
    //领取场景值，用于领取渠道数据统计。可在生成二维码接口及添加JS API接口中自定义该字段的整型值。
    public String OuterId;
    //赠送方账号（一个OpenID），"IsGiveByFriend”为1时填写该参数。
    public String FriendUserName;

    public CardUserGetEvent(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
