package wechat.app.bean.message.category.msg;

import util.Common;
import wechat.app.bean.message.BaseMessage;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class ShortViedoMessage extends BaseMessage {

    //视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
    public String ThumbMediaId;
    //消息id，可以调用多媒体文件下载接口拉取数据。
    public String MediaId;
    //消息id，64位整型
    public long MsgId; // TODO: MsgId记录下是否处理过或者在处理中,避免重复处理

    public ShortViedoMessage(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
