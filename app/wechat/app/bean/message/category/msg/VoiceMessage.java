package wechat.app.bean.message.category.msg;

import util.Common;
import wechat.app.bean.message.BaseMessage;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class VoiceMessage extends BaseMessage {

    //语音格式，如amr，speex等
    public String Format;
    //	语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
    public String MediaId;
    //消息id，64位整型
    public long MsgId; // TODO: MsgId记录下是否处理过或者在处理中,避免重复处理

    //请注意，开通语音识别后，用户每次发送语音给公众号时，微信会在推送的语音消息XML数据包中，增加一个Recongnition字段（注：由于客户端缓存，开发者开启或者关闭语音识别功能，对新关注者立刻生效，对已关注用户需要24小时生效。开发者可以重新关注此帐号进行测试）
    public String Recongnition;

    public VoiceMessage(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
