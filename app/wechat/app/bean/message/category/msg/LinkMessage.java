package wechat.app.bean.message.category.msg;

import util.Common;
import wechat.app.bean.message.BaseMessage;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class LinkMessage extends BaseMessage {
    //消息标题
    public String Title;
    //消息描述
    public String Description;
    //消息链接
    public String Url;
    //消息id，64位整型
    public long MsgId; // TODO: MsgId记录下是否处理过或者在处理中,避免重复处理

    public LinkMessage(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
