package wechat.app.bean.message.category.event;

import util.Common;
import wechat.app.bean.message.BaseEvent;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class EntityShopEvent extends BaseEvent {

    //商户自己内部ID，即字段中的sid
    public String UniqId;
    //微信的门店ID，微信内门店唯一标示ID
    public String PoiId;
    //审核结果，成功succ 或失败fail
    public String Result;
    //成功的通知信息，或审核失败的驳回理由
    public String Msg;

    public EntityShopEvent(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
