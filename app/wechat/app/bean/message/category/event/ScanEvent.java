package wechat.app.bean.message.category.event;

import util.Common;
import wechat.app.bean.message.BaseEvent;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class ScanEvent extends BaseEvent {

    //	事件KEY值，是一个32位无符号整数，即创建二维码时的二维码scene_id
    public String EventKey;

    //二维码的ticket，可用来换取二维码图片
    public String Ticket;

    public ScanEvent(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
