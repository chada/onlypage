package wechat.app.bean.message.category.event;

import util.Common;
import wechat.app.bean.message.BaseEvent;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 * 进入微信公众号时，如果打开了位置信息，则触发此事件
 */
public class LocationEvent extends BaseEvent {


    public String Latitude;
    public String Longitude;
    public String Precision;

    public LocationEvent(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
