package wechat.app.bean.message.category.event;

import util.Common;
import wechat.app.bean.message.BaseEvent;
import wechat.app.bean.message.Message;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Administrator on 2016/3/31.
 */
public class TemplateSendEvent extends BaseEvent {


    public int MsgID;
    /**
     * success 成功
     * failed:user block 用户拒绝接收
     * failed: system failed 发送失败（非用户拒绝）
     * */
    public String Status;

    public TemplateSendEvent(Message message){
        List<Field> fields = Common.getFields(this);
        for(Field field :fields){
            Common.setPropertyOfObject(this, field.getName(), Common.getPropertyOfObject(message, field.getName()));
        }
    }
}
