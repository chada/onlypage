package wechat.app.bean.message.send;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/4/1.
 */
public class TemplateIdResult extends Result {

    public String template_id;


    public static TemplateIdResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new TemplateIdResult(msg);
        } else {
            TemplateIdResult result = JSON.parseObject(msg.toJSONString(), TemplateIdResult.class);
            return result;
        }
    }

    private TemplateIdResult() {

    }


    private TemplateIdResult(JSONObject err_msg) {
        super(err_msg);
    }
}
