package wechat.app.bean.message.send;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/4/1.
 */
public class IndustryResult extends Result {

    public Industry primary_industry;
    public Industry secondary_industry;

    public static IndustryResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new IndustryResult(msg);
        } else {
            IndustryResult result = JSON.parseObject(msg.toJSONString(), IndustryResult.class);
            return result;
        }
    }

    private IndustryResult() {

    }


    private IndustryResult(JSONObject err_msg) {
        super(err_msg);
    }
}
