package wechat.app.bean.message.send;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/4/1.
 */
public class MassMessageSendResult extends Result {

    public int msg_id;
    public int msg_data_id;

    public static MassMessageSendResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new MassMessageSendResult(msg);
        } else {
            MassMessageSendResult result = JSON.parseObject(msg.toJSONString(), MassMessageSendResult.class);
            return result;
        }
    }

    private MassMessageSendResult() {

    }


    private MassMessageSendResult(JSONObject err_msg) {
        super(err_msg);
    }
}
