package wechat.app.bean.message.send;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/4/1.
 */
public class TemplateSendResult extends Result {

    public int msgid;


    public static TemplateSendResult create(JSONObject msg) {
        if (msg.containsKey("errcode")&&msg.getInteger("errcode")!=0) {
            return new TemplateSendResult(msg);
        } else {
            TemplateSendResult result = JSON.parseObject(msg.toJSONString(), TemplateSendResult.class);
            return result;
        }
    }

    private TemplateSendResult() {

    }


    private TemplateSendResult(JSONObject err_msg) {
        super(err_msg);
    }
}
