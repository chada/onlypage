package wechat.app.bean.message.send;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/4/1.
 */
public class TemplateListResult extends Result {

    public List<Template> template_list = new ArrayList<>();


    public static TemplateListResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new TemplateListResult(msg);
        } else {
            TemplateListResult result = JSON.parseObject(msg.toJSONString(), TemplateListResult.class);
            return result;
        }
    }

    private TemplateListResult() {

    }


    private TemplateListResult(JSONObject err_msg) {
        super(err_msg);
    }
}
