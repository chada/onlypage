package wechat.app.bean.message.send;

/**
 * Created by Administrator on 2016/4/1.
 */
public class Template {
    public String template_id;
    public String title;
    public String primary_industry;
    public String deputy_industry;
    public String content;
    public String example;
}
