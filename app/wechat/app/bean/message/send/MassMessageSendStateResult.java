package wechat.app.bean.message.send;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/4/1.
 */
public class MassMessageSendStateResult extends Result {

    public int msg_id;
    //消息发送后的状态，SEND_SUCCESS表示发送成功
    public String msg_status;

    public static MassMessageSendStateResult create(JSONObject msg) {
        if (msg.containsKey("errcode")) {
            return new MassMessageSendStateResult(msg);
        } else {
            MassMessageSendStateResult result = JSON.parseObject(msg.toJSONString(), MassMessageSendStateResult.class);
            return result;
        }
    }

    private MassMessageSendStateResult() {

    }


    private MassMessageSendStateResult(JSONObject err_msg) {
        super(err_msg);
    }
}
