package wechat.app.sendMassage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.Common;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.bean.Result;
import wechat.app.bean.customer.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import static wechat.util.PostFileUtil.postFile;

/**
 * Created by Administrator on 2016/3/8.
 * 客服接口
 */
public class CustomerServiceManage {

    /**
     * 通过本接口可为公众号添加客服账号，每个公众号最多添加100个客服账号，如有额外需要可申请扩容。
     *
     * @param preAccount       账号前缀，最多10个字符，必须是英文或者数字字符
     * @param wechatAppAccount 微信公众号账号
     * @param nickname         客服昵称，最长6个汉字或12个英文字符
     * @param password         客服密码，32位md5值
     **/
    public static Result create(String token, String wechatAppAccount, String preAccount, String nickname, String password) throws IOException {
        JSONObject body = new JSONObject();
        body.put("kf_account", preAccount + "@" + wechatAppAccount);
        body.put("nickname", nickname);
        body.put("password", Common.md5(password, 32));
        String url = "https://api.weixin.qq.com/customservice/kfaccount/add?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    /**
     * 开发者通过本接口，根据AppID获取公众号中所设置的客服基本信息，包括客服工号、客服昵称、客服登录账号。开发者利用客服基本信息，结合客服接待情况，可以开发例如“指定客服接待”等功能。
     **/
    public static CustomerListResult getList(String token) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token=" + token;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return CustomerListResult.create(result);
    }


    /**
     * 开发者通过本接口，根据AppID获取公众号中当前在线的客服的接待信息，
     * 包括客服工号、客服登录账号、客服在线状态（手机在线、PC客户端在线、手机和PC客户端全都在线）、客服自动接入最大值、客服当前接待客户数。
     * 开发者利用本接口提供的信息，结合客服基本信息，可以开发例如“指定客服接待”等功能；结合会话记录，可以开发”在线客服实时服务质量监控“等功能。
     **/
    public static CustomerOnlineListResult getOnLineList(String token) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/customservice/getonlinekflist?access_token=" + token;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return CustomerOnlineListResult.create(result);
    }

    //设置客服信息
    public static Result set(String token, String wechatAppAccount, String preAccount, String nickname, String password) throws IOException {
        JSONObject body = new JSONObject();
        body.put("kf_account", preAccount + "@" + wechatAppAccount);
        body.put("nickname", nickname);
        body.put("password", Common.md5(password, 32));
        String url = "https://api.weixin.qq.com/customservice/kfaccount/update?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    //上传客服头像
    public static Result setHeadImg(String token, String wechatAppAccount, String preAccount, File file) throws IOException {
        String url = "http://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token=" + token + "&kf_account=" + preAccount + "@" + wechatAppAccount;
        String res = postFile(url, file);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }


    //删除客服
    public static CustomerListResult delete(String token, String wechatAppAccount, String preAccount) throws IOException {
        String url = "https://api.weixin.qq.com/customservice/kfaccount/del?access_token=" + token + "&kf_account=" + preAccount + "@" + wechatAppAccount;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return CustomerListResult.create(result);
    }


    /**
     * 创建会话
     *
     * @param openid 客户openid
     * @param text   附加信息，文本会展示在客服人员的多客服客户端
     */
    public static Result creeateSession(String token, String wechatAppAccount, String preAccount, String openid, String text) throws IOException {
        String url = "https://api.weixin.qq.com/customservice/kfsession/create?access_token=" + token;
        JSONObject body = new JSONObject();
        body.put("kf_account", preAccount + "@" + wechatAppAccount);
        body.put("openid", openid);
        body.put("text", text);
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    /**
     * 关闭会话 *
     */
    public static Result closeSession(String token, String wechatAppAccount, String preAccount, String openid, String text) throws IOException {
        String url = "https://api.weixin.qq.com/customservice/kfsession/close?access_token=" + token;
        JSONObject body = new JSONObject();
        body.put("kf_account", preAccount + "@" + wechatAppAccount);
        body.put("openid", openid);
        body.put("text", text);
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    //获取客户的会话状态
    public static SessionStateResult getSessionState(String token, String openid) throws IOException {
        String url = "https://api.weixin.qq.com/customservice/kfsession/getsession?access_token=" + token + "&openid=" + openid;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        System.out.println(result);
        return SessionStateResult.create(result);
    }


    //获取客服的会话列表
    public static SessionListResult getSessionList(String token, String wechatAppAccount, String preAccount) throws IOException {
        String url = "https://api.weixin.qq.com/customservice/kfsession/getsessionlist?access_token=" + token + "&kf_account=" + preAccount + "@" + wechatAppAccount;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        System.out.println(result);
        return SessionListResult.create(result);
    }


    //获取未接入会话列表
    public static SessionWaitListResult getSessionWaitList(String token) throws IOException {
        String url = " https://api.weixin.qq.com/customservice/kfsession/getwaitcase?access_token=" + token;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        System.out.println(result);
        return SessionWaitListResult.create(result);
    }


    /**
     * 获取客服聊天记录接口
     *
     * @param starttime 查询开始时间，UNIX时间戳
     * @param endtime   查询结束时间，UNIX时间戳，每次查询不能跨日查询
     * @param pageindex 查询第几页，从1开始
     * @param pagesize 每页大小，每页最多拉取50条
     **/
    public static RecordListResult getRecordList(String token, Date starttime, Date endtime, int pageindex, int pagesize) throws IOException {
        String url = "https://api.weixin.qq.com/customservice/msgrecord/getrecord?access_token=" + token;
        JSONObject body = new JSONObject();
        body.put("endtime", endtime.getTime() / 1000);
        body.put("starttime", starttime.getTime() / 1000);
        body.put("pageindex", pageindex);
        body.put("pagesize", pagesize);
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return RecordListResult.create(result);
    }

}
