package wechat.app.sendMassage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.bean.Result;
import wechat.app.bean.message.send.IndustryResult;
import wechat.app.bean.message.send.TemplateIdResult;
import wechat.app.bean.message.send.TemplateListResult;
import wechat.app.bean.message.send.TemplateSendResult;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2016/4/1.
 */
public class TemplateMessageManage {
    /**
     * IT科技	互联网/电子商务	1
     * IT科技	IT软件与服务	2
     * IT科技	IT硬件与设备	3
     * IT科技	电子技术	4
     * IT科技	通信与运营商	5
     * IT科技	网络游戏	6
     * 金融业	银行	7
     * 金融业	基金|理财|信托	8
     * 金融业	保险	9
     * 餐饮	餐饮	10
     * 酒店旅游	酒店	11
     * 酒店旅游	旅游	12
     * 运输与仓储	快递	13
     * 运输与仓储	物流	14
     * 运输与仓储	仓储	15
     * 教育	培训	16
     * 教育	院校	17
     * 政府与公共事业	学术科研	18
     * 政府与公共事业	交警	19
     * 政府与公共事业	博物馆	20
     * 政府与公共事业	公共事业|非盈利机构	21
     * 医药护理	医药医疗	22
     * 医药护理	护理美容	23
     * 医药护理	保健与卫生	24
     * 交通工具	汽车相关	25
     * 交通工具	摩托车相关	26
     * 交通工具	火车相关	27
     * 交通工具	飞机相关	28
     * 房地产	建筑	29
     * 房地产	物业	30
     * 消费品	消费品	31
     * 商业服务	法律	32
     * 商业服务	会展	33
     * 商业服务	中介服务	34
     * 商业服务	认证	35
     * 商业服务	审计	36
     * 文体娱乐	传媒	37
     * 文体娱乐	体育	38
     * 文体娱乐	娱乐休闲	39
     * 印刷	印刷	40
     * 其它	其它	41
     */
    public static Result setIndustry(String token, int industry_id1, int industry_id2) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=" + token;
        JSONObject body = new JSONObject();
        body.put("industry_id1", industry_id1);
        body.put("industry_id2", industry_id2);
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    public static IndustryResult getIndustry(String token) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=" + token;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return IndustryResult.create(result);
    }


    /**
     * @param template_id_short 模板库中模板的编号，有“TM**”和“OPENTMTM**”等形式
     */
    public static TemplateIdResult getTemplateId(String token, String template_id_short) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token=" + token;
        JSONObject body = new JSONObject();
        body.put("template_id_short", template_id_short);
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return TemplateIdResult.create(result);
    }

    public static TemplateListResult getTemplateList(String token) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token=" + token;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return TemplateListResult.create(result);
    }


    public static Result deleteTemplate(String token, String template_id) throws IOException {
        String url = "https://api,weixin.qq.com/cgi-bin/template/del_private_template?access_token=" + token;
        JSONObject body = new JSONObject();
        body.put("template_id", template_id);
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    public static TemplateSendResult send(String token, String openid,String template_id,String linkurl,String data) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + token;
        JSONObject body = new JSONObject();
        body.put("openid", openid);
        body.put("template_id", template_id);
        body.put("url", linkurl);
        body.put("data", data);
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return TemplateSendResult.create(result);
    }
}
