package wechat.app.sendMassage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.app.bean.message.send.MassMessageSendResult;
import wechat.app.bean.message.send.MassMessageSendStateResult;
import wechat.bean.Result;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Administrator on 2016/4/1.
 */
public class MassMessageManage {

    //---------------------------------------根据分组进行群发【订阅号与服务号认证后均可用】--------------------------------------------------
    public static MassMessageSendResult sendNews(String token, Boolean is_to_all, int group_id, String media_id) throws IOException {
        JSONObject body = new JSONObject();
        JSONObject filter = new JSONObject();
        filter.put("is_to_all", is_to_all);
        filter.put("group_id", group_id);
        body.put("filter", filter);

        JSONObject mpnews = new JSONObject();
        mpnews.put("media_id", media_id);
        body.put("mpnews", mpnews);

        body.put("msgtype", "mpnews");

        return send(token, body.toJSONString());
    }


    public static MassMessageSendResult sendText(String token, Boolean is_to_all, int group_id, String content) throws IOException {
        JSONObject body = new JSONObject();
        JSONObject filter = new JSONObject();
        filter.put("is_to_all", is_to_all);
        filter.put("group_id", group_id);
        body.put("filter", filter);

        JSONObject text = new JSONObject();
        text.put("content", content);
        body.put("text", text);

        body.put("msgtype", "text");

        return send(token, body.toJSONString());
    }


    public static MassMessageSendResult sendVoice(String token, Boolean is_to_all, int group_id, String media_id) throws IOException {
        JSONObject body = new JSONObject();
        JSONObject filter = new JSONObject();
        filter.put("is_to_all", is_to_all);
        filter.put("group_id", group_id);
        body.put("filter", filter);

        JSONObject voice = new JSONObject();
        voice.put("media_id", media_id);
        body.put("voice", voice);

        body.put("msgtype", "voice");

        return send(token, body.toJSONString());
    }

    public static MassMessageSendResult sendImagee(String token, Boolean is_to_all, int group_id, String media_id) throws IOException {
        JSONObject body = new JSONObject();
        JSONObject filter = new JSONObject();
        filter.put("is_to_all", is_to_all);
        filter.put("group_id", group_id);
        body.put("filter", filter);

        JSONObject image = new JSONObject();
        image.put("media_id", media_id);
        body.put("image", image);

        body.put("msgtype", "image");

        return send(token, body.toJSONString());
    }

    public static MassMessageSendResult sendVideo(String token, Boolean is_to_all, int group_id, String media_id) throws IOException {
        JSONObject body = new JSONObject();
        JSONObject filter = new JSONObject();
        filter.put("is_to_all", is_to_all);
        filter.put("group_id", group_id);
        body.put("filter", filter);

        JSONObject mpvideo = new JSONObject();
        mpvideo.put("media_id", media_id);
        body.put("mpvideo", mpvideo);

        body.put("msgtype", "mpvideo");

        return send(token, body.toJSONString());
    }

    public static MassMessageSendResult sendWXCard(String token, Boolean is_to_all, int group_id, String card_id) throws IOException {
        JSONObject body = new JSONObject();
        JSONObject filter = new JSONObject();
        filter.put("is_to_all", is_to_all);
        filter.put("group_id", group_id);
        body.put("filter", filter);

        JSONObject wxcard = new JSONObject();
        wxcard.put("card_id", card_id);
        body.put("wxcard", wxcard);

        body.put("msgtype", "wxcard");

        return send(token, body.toJSONString());
    }


    private static MassMessageSendResult send(String token, String body) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return MassMessageSendResult.create(result);
    }


//---------------------------------------根据OpenID列表群发【订阅号不可用，服务号认证后可用】--------------------------------------------------


    public static MassMessageSendResult sendNewsByOpenidList(String token, List<String> openidList, String media_id) throws IOException {
        JSONObject body = new JSONObject();
        JSONArray touser = new JSONArray();
        for (String openid : openidList) {
            touser.add(openid);
        }
        body.put("touser", touser);

        JSONObject mpnews = new JSONObject();
        mpnews.put("media_id", media_id);
        body.put("mpnews", mpnews);

        body.put("msgtype", "mpnews");

        return sendByOpenidList(token, body.toJSONString());
    }

    public static MassMessageSendResult sendTextByOpenidList(String token, List<String> openidList, String content) throws IOException {
        JSONObject body = new JSONObject();
        JSONArray touser = new JSONArray();
        for (String openid : openidList) {
            touser.add(openid);
        }
        body.put("touser", touser);

        JSONObject text = new JSONObject();
        text.put("content", content);
        body.put("text", text);

        body.put("msgtype", "mpnews");

        return send(token, body.toJSONString());
    }

    public static MassMessageSendResult sendVoiceByOpenidList(String token, List<String> openidList, String media_id) throws IOException {
        JSONObject body = new JSONObject();
        JSONArray touser = new JSONArray();
        for (String openid : openidList) {
            touser.add(openid);
        }
        body.put("touser", touser);

        JSONObject voice = new JSONObject();
        voice.put("media_id", media_id);
        body.put("voice", voice);

        body.put("msgtype", "voice");

        return sendByOpenidList(token, body.toJSONString());
    }

    public static MassMessageSendResult sendImageByOpenidList(String token, List<String> openidList, String media_id) throws IOException {
        JSONObject body = new JSONObject();
        JSONArray touser = new JSONArray();
        for (String openid : openidList) {
            touser.add(openid);
        }
        body.put("touser", touser);

        JSONObject image = new JSONObject();
        image.put("media_id", media_id);
        body.put("image", image);

        body.put("msgtype", "image");

        return sendByOpenidList(token, body.toJSONString());
    }

    public static MassMessageSendResult sendVideoByOpenidList(String token, List<String> openidList, String media_id, String title, String description) throws IOException {
        JSONObject body = new JSONObject();
        JSONArray touser = new JSONArray();
        for (String openid : openidList) {
            touser.add(openid);
        }
        body.put("touser", touser);

        JSONObject video = new JSONObject();
        video.put("media_id", media_id);
        video.put("title", title);
        video.put("description", description);
        body.put("video", video);

        body.put("msgtype", "video");

        return sendByOpenidList(token, body.toJSONString());
    }

    public static MassMessageSendResult sendWXCardByOpenidList(String token, List<String> openidList, String card_id) throws IOException {
        JSONObject body = new JSONObject();
        JSONArray touser = new JSONArray();
        for (String openid : openidList) {
            touser.add(openid);
        }
        body.put("touser", touser);

        JSONObject wxcard = new JSONObject();
        wxcard.put("card_id", card_id);
        body.put("wxcard", wxcard);

        body.put("msgtype", "wxcard");

        return sendByOpenidList(token, body.toJSONString());
    }

    private static MassMessageSendResult sendByOpenidList(String token, String body) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return MassMessageSendResult.create(result);
    }

    //---------------------------------------删除群发【订阅号与服务号认证后均可用】--------------------------------------------------
    private static Result delete(String token, String msg_id) throws IOException {
        JSONObject body = new JSONObject();
        body.put("msg_id", msg_id);
        String url = "https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    //---------------------------------------查询群发消息发送状态【订阅号与服务号认证后均可用】--------------------------------------------------
    private static MassMessageSendStateResult getState(String token, String msg_id) throws IOException {
        JSONObject body = new JSONObject();
        body.put("msg_id", msg_id);
        String url = "https://api.weixin.qq.com/cgi-bin/message/mass/get?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return MassMessageSendStateResult.create(result);
    }


    //---------------------------------------预览接口【订阅号与服务号认证后均可用】,media必须为永久id--------------------------------------------------

    public enum previewInfoType {towxname, openid}

    public static MassMessageSendResult sendPreviewNews(String token, previewInfoType type, String userInfo, String media_id) throws IOException {
        JSONObject body = new JSONObject();
        switch (type) {
            case towxname:
                body.put("towxname", userInfo);
                break;
            case openid:
                body.put("touser", userInfo);
                break;
        }
        JSONObject mpnews = new JSONObject();
        mpnews.put("media_id", media_id);
        body.put("mpnews", mpnews);

        body.put("msgtype", "mpnews");

        return preview(token, body.toJSONString());
    }

    public static MassMessageSendResult sendPreviewText(String token, previewInfoType type, String userInfo, String content) throws IOException {
        JSONObject body = new JSONObject();
        switch (type) {
            case towxname:
                body.put("towxname", userInfo);
                break;
            case openid:
                body.put("touser", userInfo);
                break;
        }

        JSONObject text = new JSONObject();
        text.put("content", content);
        body.put("text", text);

        body.put("msgtype", "text");

        return preview(token, body.toJSONString());
    }

    public static MassMessageSendResult sendPreviewVoice(String token, previewInfoType type, String userInfo, String media_id) throws IOException {
        JSONObject body = new JSONObject();
        switch (type) {
            case towxname:
                body.put("towxname", userInfo);
                break;
            case openid:
                body.put("touser", userInfo);
                break;
        }

        JSONObject voice = new JSONObject();
        voice.put("media_id", media_id);
        body.put("voice", voice);

        body.put("msgtype", "voice");

        return preview(token, body.toJSONString());
    }

    public static MassMessageSendResult sendPreviewImagee(String token, previewInfoType type, String userInfo, String media_id) throws IOException {
        JSONObject body = new JSONObject();
        switch (type) {
            case towxname:
                body.put("towxname", userInfo);
                break;
            case openid:
                body.put("touser", userInfo);
                break;
        }

        JSONObject image = new JSONObject();
        image.put("media_id", media_id);
        body.put("image", image);

        body.put("msgtype", "image");

        return preview(token, body.toJSONString());
    }

    public static MassMessageSendResult sendPreviewVideo(String token, previewInfoType type, String userInfo, String media_id) throws IOException {
        JSONObject body = new JSONObject();
        switch (type) {
            case towxname:
                body.put("towxname", userInfo);
                break;
            case openid:
                body.put("touser", userInfo);
                break;
        }

        JSONObject mpvideo = new JSONObject();
        mpvideo.put("media_id", media_id);
        body.put("mpvideo", mpvideo);

        body.put("msgtype", "mpvideo");

        return preview(token, body.toJSONString());
    }

    public static MassMessageSendResult sendPreviewWXCard(String token, previewInfoType type, String userInfo, String card_id, String card_ext) throws IOException {
        JSONObject body = new JSONObject();
        switch (type) {
            case towxname:
                body.put("towxname", userInfo);
                break;
            case openid:
                body.put("touser", userInfo);
                break;
        }

        JSONObject wxcard = new JSONObject();
        wxcard.put("card_id", card_id);
        body.put("wxcard", wxcard);
        body.put("card_ext", card_ext);

        body.put("msgtype", "wxcard");

        return preview(token, body.toJSONString());
    }

    private static MassMessageSendResult preview(String token, String body) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return MassMessageSendResult.create(result);
    }

}
