package wechat.app.sendMassage;

import util.DataFormatUtil;
import wechat.app.bean.message.Article;
import wechat.app.bean.message.Message;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2016/3/8.
 */
public class PassiveReplyMessage {

    /**
     * @param Content 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
     * */
    public static String sendText(Message message,String Content ){
        HashMap<String ,String> map = new HashMap<String ,String>();
        map.put("ToUserName",message.FromUserName);
        map.put("FromUserName",message.ToUserName);
        map.put("Content",Content);
        map.put("MsgType","text");
        map.put("CreateTime", String.valueOf(new Date().getTime()));
        return send(map);
    }

    /**
     * @param MediaId 通过素材管理接口上传多媒体文件，得到的id。
     * */
    public static String sendImage(Message message,String MediaId ){
        HashMap<String ,String> map = new HashMap<String ,String>();
        map.put("ToUserName",message.FromUserName);
        map.put("FromUserName",message.ToUserName);
        map.put("MsgType","image");
        map.put("CreateTime", String.valueOf(new Date().getTime()));
        HashMap<String ,String> Image = new HashMap<String ,String>();
        Image.put("MediaId",MediaId);
        map.put("Image",DataFormatUtil.mapToXmlString(Image));
        return send(map);
    }

    /**
     * @param MediaId 通过素材管理接口上传多媒体文件，得到的id
     * @param Title 视频消息的标题(不是必须的)
     * @param Description 视频消息的描述(不是必须的)
     * */
    public static String sendVideo(Message message,String MediaId,String Title,String Description ){
        HashMap<String ,String> map = new HashMap<String ,String>();
        map.put("ToUserName",message.FromUserName);
        map.put("FromUserName",message.ToUserName);
        map.put("MsgType","image");
        map.put("CreateTime", String.valueOf(new Date().getTime()));
        HashMap<String ,String> Video = new HashMap<String ,String>();
        Video.put("MediaId",MediaId);
        Video.put("Title",Title);
        Video.put("Description",Description);
        map.put("Video",DataFormatUtil.mapToXmlString(Video));
        return send(map);
    }

    /**
     * @param MediaId 通过素材管理接口上传多媒体文件，得到的id。
     * */
    public static String sendVoice(Message message,String MediaId ){
        HashMap<String ,String> map = new HashMap<String ,String>();
        map.put("ToUserName",message.FromUserName);
        map.put("FromUserName",message.ToUserName);
        map.put("MsgType","image");
        map.put("CreateTime", String.valueOf(new Date().getTime()));
        HashMap<String ,String> Voice = new HashMap<String ,String>();
        Voice.put("MediaId",MediaId);
        map.put("Voice",DataFormatUtil.mapToXmlString(Voice));
        return send(map);
    }

    /**
     * @param MediaId 通过素材管理接口上传多媒体文件，得到的id。
     * @param Title 音乐标题(不是必须的)
     * @param Description 音乐描述(不是必须的)
     * @param MusicURL 音乐链接(不是必须的)
     * @param HQMusicUrl 高质量音乐链接，WIFI环境优先使用该链接播放音乐(不是必须的)
     * @param ThumbMediaId 缩略图的媒体id，通过素材管理接口上传多媒体文件，得到的id(不是必须的)
     * */
    public static String sendMusic(Message message,String MediaId,String Title,String Description,String MusicURL,String HQMusicUrl,String ThumbMediaId ){
        HashMap<String ,String> map = new HashMap<String ,String>();
        map.put("ToUserName",message.FromUserName);
        map.put("FromUserName",message.ToUserName);
        map.put("MsgType","image");
        map.put("CreateTime", String.valueOf(new Date().getTime()));
        HashMap<String ,String> Music = new HashMap<String ,String>();
        Music.put("MediaId",MediaId);
        Music.put("Title",Title);
        Music.put("Description",Description);
        Music.put("MusicURL",MusicURL);
        Music.put("HQMusicUrl",HQMusicUrl);
        Music.put("ThumbMediaId",ThumbMediaId);
        map.put("Music",DataFormatUtil.mapToXmlString(Music));
        return send(map);
    }


    public static String sendNews(Message message,List<Article> articles){
        HashMap<String ,String> map = new HashMap<String ,String>();
        map.put("ToUserName",message.FromUserName);
        map.put("FromUserName",message.ToUserName);
        map.put("MsgType","news");

        map.put("CreateTime", String.valueOf(new Date().getTime()));
        String Articles="";
        for(Article article:articles){
            Articles+="<item>"+article.toXml()+"</item>";
        }
        map.put("ArticleCount", String.valueOf(articles.size()));
        map.put("Articles",Articles);
        return send(map);
    }


    public static String sendToCustomer(Message message){
        HashMap<String ,String> map = new HashMap<String ,String>();
        map.put("ToUserName",message.FromUserName);
        map.put("FromUserName",message.ToUserName);
        map.put("MsgType","transfer_customer_service");
        map.put("CreateTime", String.valueOf(new Date().getTime()));
        return send(map);
    }

    public static String sendToCustomer(Message message, String wechatAppAccount, String preAccount){
        HashMap<String ,String> map = new HashMap<String ,String>();
        map.put("ToUserName",message.FromUserName);
        map.put("FromUserName",message.ToUserName);
        map.put("MsgType","transfer_customer_service");
        map.put("CreateTime", String.valueOf(new Date().getTime()));
        String TransInfo = "<KfAccount>"+preAccount + "@" + wechatAppAccount+"</KfAccount>";
        map.put("TransInfo",TransInfo);
        return send(map);
    }


    private static String send( HashMap<String ,String> map){
        return DataFormatUtil.mapToXmlString(map, "xml");
    }
}
