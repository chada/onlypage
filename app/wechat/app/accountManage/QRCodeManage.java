package wechat.app.accountManage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.app.bean.account.QRCodeTicketResult;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2016/3/29.
 * 二维码类型，QR_SCENE为临时,QR_LIMIT_SCENE为永久,QR_LIMIT_STR_SCENE为永久的字符串参数值
 */
public class QRCodeManage {


    /**
     * @param scene_id 最大值是:4294967296
     * @param expire_seconds 最大值是:2592000(30天)
     * **/
    public static QRCodeTicketResult createTemp(String token,String expire_seconds,String scene_id) throws IOException {
        JSONObject body = new JSONObject();
        body.put("expire_seconds",expire_seconds);
        body.put("action_name","QR_SCENE");
        JSONObject action_info = new JSONObject();
        JSONObject scene = new JSONObject();
        scene.put("scene_id",scene_id);
        action_info.put("scene",scene);
        body.put("action_info",action_info);
        String url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return QRCodeTicketResult.create(result);
    }


    /**
     * @param scene_id 最大值为100000（目前参数只支持1--100000）
     * **/
    public static QRCodeTicketResult createPermanentSceneId(String token,String scene_id) throws IOException {
        JSONObject body = new JSONObject();
        body.put("action_name","QR_LIMIT_SCENE");
        JSONObject action_info = new JSONObject();
        JSONObject scene = new JSONObject();
        scene.put("scene_id",scene_id);
        action_info.put("scene",scene);
        body.put("action_info",action_info);
        String url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return QRCodeTicketResult.create(result);
    }


    /**
     * @param scene_str 字符串类型，长度限制为1到64，仅永久二维码支持此字段
     * **/
    public static QRCodeTicketResult createPermanentSceneStr(String token,String scene_str) throws IOException {
        JSONObject body = new JSONObject();
        body.put("action_name","QR_LIMIT_STR_SCENE");
        JSONObject action_info = new JSONObject();
        JSONObject scene = new JSONObject();
        scene.put("scene_str",scene_str);
        action_info.put("scene",scene);
        body.put("action_info",action_info);
        String url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return QRCodeTicketResult.create(result);
    }


    public static Boolean getQRCode(String ticket,String filePath) throws IOException {
        String url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=" + ticket;
        InputStream inputStream = HttpUtil.get(url);
        if(inputStream==null){
            return false;
        }
        BufferedInputStream bis = new BufferedInputStream(inputStream);
        FileOutputStream fos = new FileOutputStream(filePath);
        int size = 0;
        byte[] buf = new byte[8096];
        while ((size = bis.read(buf)) != -1)
            fos.write(buf, 0, size);
        fos.close();
        bis.close();
        return true;
    }
}
