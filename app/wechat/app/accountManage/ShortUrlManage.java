package wechat.app.accountManage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.app.bean.account.GetShortUrlResult;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2016/3/30.
 */
public class ShortUrlManage {
    public static GetShortUrlResult get(String token,String long_url) throws IOException {
        JSONObject body = new JSONObject();
        body.put("action","long2short");
        body.put("long_url",long_url);
        String url = "https://api.weixin.qq.com/cgi-bin/shorturl?access_token="+token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return GetShortUrlResult.create(result);
    }
}
