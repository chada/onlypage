package wechat.app.card;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.app.bean.card.*;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2016/4/5.
 * 卡卷管理
 */
public class CardManage {

    public static CardCreateResult create(String token, CashCard card) throws IOException {
        return create(token,card.toJsonString());
    }
    public static CardCreateResult create(String token, GrouponCard card) throws IOException {
        return create(token,card.toJsonString());
    }
    public static CardCreateResult create(String token, DiscountCard card) throws IOException {
        return create(token,card.toJsonString());
    }
    public static CardCreateResult create(String token, GeneralCouponCard card) throws IOException {
        return create(token,card.toJsonString());
    }
    public static CardCreateResult create(String token, GiftCard card) throws IOException {
        return create(token,card.toJsonString());
    }

    private static CardCreateResult create(String token,String body) throws IOException {
        String url = "https://api.weixin.qq.com/card/create?access_token="+token;
        InputStream inputStream = HttpUtil.post(url, body);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return CardCreateResult.create(result);
    }

}
