package wechat.app.userManage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.bean.Result;
import wechat.app.bean.user.UserListResult;
import wechat.app.bean.user.UserResult;
import wechat.app.bean.user.UserOpenidListResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Administrator on 2016/3/9.
 */
public class UserManage {
    /**
     * 设置用户备注名
     * http://mp.weixin.qq.com/wiki/1/4a566d20d67def0b3c1afc55121d2419.html
     *
     * @param
     **/
    public static Result setUserRemark(String token, String openid, String remark) throws IOException {
        if (remark.length() > 30) return null;
        JSONObject body = new JSONObject();
        body.put("openid", openid);
        body.put("remark", remark);
        String url = "https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=" + token;

        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }

    /**
     * 在关注者与公众号产生消息交互后，公众号可获得关注者的OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的。对于不同公众号，同一用户的openid不同）。公众号可通过本接口来根据OpenID获取用户基本信息，包括昵称、头像、性别、所在城市、语言和关注时间。
     **/
    public static UserResult getUserInfo(String token, String openid) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" + token + "&openid=" + openid + "&lang=zh_CN";
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return UserResult.create(result);
    }


    public static UserListResult getUserInfo(String token, List<String> openidList) throws IOException {
        JSONArray user_list = new JSONArray();
        for (String openid : openidList) {
            JSONObject object = new JSONObject();
            object.put("openid", openid);
            object.put("lang", "zh-CN");
            user_list.add(object);
        }
        JSONObject body = new JSONObject();
        body.put("user_list", user_list);
        String url = "https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return UserListResult.create(result);
    }


    /**
     * 获取所有用户openid
     **/
    public static UserOpenidListResult getAllUserOpenidList(String token) throws IOException {
        UserOpenidListResult userOpenidList = getUserOpenidList(token);
        UserOpenidListResult temp = userOpenidList;
        if (temp != null) {
            while (temp.total > temp.count) {
                UserOpenidListResult new_userOpenidList = getUserOpenidList(token, temp.next_openid);
                temp = new_userOpenidList;
                userOpenidList.merge(new_userOpenidList);
            }
        }
        return userOpenidList;
    }

    //第一次获取，没有起始openid
    public static UserOpenidListResult getUserOpenidList(String token) throws IOException {
        return getUserOpenidList(token, null);
    }

    private static UserOpenidListResult getUserOpenidList(String token, String next_openid) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=" + token;
        if (next_openid != null) {
            url += "&next_openid=" + next_openid;
        }
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return UserOpenidListResult.create(result);
    }
}
