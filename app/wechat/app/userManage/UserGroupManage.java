package wechat.app.userManage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.bean.Result;
import wechat.app.bean.userGroup.GroupCreateResult;
import wechat.app.bean.userGroup.GroupListResult;
import wechat.app.bean.userGroup.GroupUserOwnerResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Administrator on 2016/3/8.
 * http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html#.E6.9F.A5.E8.AF.A2.E6.89.80.E6.9C.89.E5.88.86.E7.BB.84 *
 */
public class UserGroupManage {


    /**
     * 创建分组
     **/
    public static GroupCreateResult create(String token, String name) throws IOException {
        JSONObject bady = new JSONObject();
        JSONObject group = new JSONObject();
        group.put("name", name);
        bady.put("group", group);
        String url = "https://api.weixin.qq.com/cgi-bin/groups/create?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, bady.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return GroupCreateResult.create(result);
    }


    /**
     * 查询所有分组
     **/
    public static GroupListResult getAllGroups(String token) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/groups/get?access_token=" + token;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return GroupListResult.create(result);
    }


    /**
     * 查询用户所在组
     **/
    public static GroupUserOwnerResult getGroupIdByUser(String token, String openid) throws IOException {
        JSONObject body = new JSONObject();
        body.put("openid", openid);
        String url = "https://api.weixin.qq.com/cgi-bin/groups/getid?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return GroupUserOwnerResult.create(result);
    }


    /**
     * 查询用户所在组
     *
     * @param name 分组名字（30个字符以内）
     **/
    public static Result editGroupName(String token, int groupId, String name) throws IOException {
        if (name.length() > 30) {
            return null;
        }
        JSONObject group = new JSONObject();
        group.put("id", groupId);
        group.put("name", name);
        JSONObject body = new JSONObject();
        body.put("group", group);
        String url = "https://api.weixin.qq.com/cgi-bin/groups/update?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }


    /**
     * 移动用户分组
     *
     * @param to_groupid 移动到的分组id
     **/
    public static Result moveUserGroup(String token, int to_groupid, String openid) throws IOException {
        JSONObject body = new JSONObject();
        body.put("openid", openid);
        body.put("to_groupid", to_groupid);
        String url = "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }


    /**
     * 批量移动用户分组
     **/
    public static Result moveBatchUserGroup(String token, int to_groupid, List<String> openidList) throws IOException {
        JSONArray openid_list = new JSONArray();
        for (String openid : openidList) {
            openid_list.add(openid);
        }
        JSONObject body = new JSONObject();
        body.put("openid_list", openid_list);
        body.put("to_groupid", to_groupid);
        String url = "https://api.weixin.qq.com/cgi-bin/groups/members/batchupdate?access_token=" + token;

        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }


    /**
     * 删除用户分组
     * 假如该组中有成员，则删除之后，此成员为无组成员
     **/
    public static Result delete(String token, int groupid) throws IOException {
        JSONObject group = new JSONObject();
        group.put("id", groupid);
        JSONObject body = new JSONObject();
        body.put("group", group);
        String url = "https://api.weixin.qq.com/cgi-bin/groups/delete?access_token=" + token;
        InputStream inputStream = HttpUtil.post(url, body.toJSONString());
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return new Result(result);
    }
}
