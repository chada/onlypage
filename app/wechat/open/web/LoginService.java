package wechat.open.web;

import util.Common;
import wechat.open.web.beans.LoginResult;
import wechat.open.web.beans.LoginUserResult;

import java.io.IOException;

/**
 * Created by Administrator on 2016/6/29.
 */
public class LoginService {

    //*
    // appid	是	应用唯一标识
    // redirect_uri	是	重定向地址，需要进行UrlEncode
    // response_type	是	填code
    // scope	是	应用授权作用域，拥有多个作用域用逗号（,）分隔，网页应用目前仅填写snsapi_login即可
    // state	否	用于保持请求和回调的状态，授权请求后原样带回给第三方。该参数可用于防止csrf攻击（跨站请求伪造攻击），建议第三方带上该参数，可设置为简单的随机数加session进行校验
    // **//
    public static String getUrl(String appid, String redirect_url, String state) {
        return LoginUtil.getUrl(appid,redirect_url,"code","snsapi_login",state);
    }

    public static LoginResult getAccessToken(String code,String appid,String appsecret) throws IOException {
        return  LoginUtil.getAccessToken(appid,appsecret,"authorization_code",code);
    }

    public static LoginUserResult getUsreInfo(String access_token, String OPENID) throws IOException {
        return LoginUtil.getUserInfo(access_token,OPENID);
    }
}
