package wechat.open.web.beans;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/6/29.
 */
public class LoginUserResult extends Result {
    public String openid;
    public String nickname;
    public int sex;
    public String province;
    public String city;
    public String country;
    public String headimgurl;
    public String privilege;
    public String unionid;


    public static LoginUserResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new LoginUserResult(msg);
        } else {
            return JSON.parseObject(msg.toJSONString(),LoginUserResult.class);
        }
    }

    private LoginUserResult(){

    }



    private LoginUserResult(JSONObject err_msg){
        super(err_msg);
    }
}
