package wechat.open.web.beans;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import wechat.bean.Result;

/**
 * Created by Administrator on 2016/6/29.
 */
public class LoginResult extends Result {
    public String access_token;
    public int expires_in;
    public String refresh_token;
    public String openid;
    public String scope;
    public String unionid;

    public static LoginResult create(JSONObject msg){
        if (msg.containsKey("errcode")) {
            return new LoginResult(msg);
        } else {
            return JSON.parseObject(msg.toJSONString(),LoginResult.class);
        }
    }

    private LoginResult(){

    }



    private LoginResult(JSONObject err_msg){
        super(err_msg);
    }
}
