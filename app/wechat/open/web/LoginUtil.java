package wechat.open.web;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import util.DataFormatUtil;
import util.HttpUtil;
import wechat.open.web.beans.LoginResult;
import wechat.open.web.beans.LoginUserResult;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;

/**
 * Created by Administrator on 2016/6/29.
 */
public class LoginUtil {

    //*
    // appid	是	应用唯一标识
    // redirect_uri	是	重定向地址，需要进行UrlEncode
    // response_type	是	填code
    // scope	是	应用授权作用域，拥有多个作用域用逗号（,）分隔，网页应用目前仅填写snsapi_login即可
    // state	否	用于保持请求和回调的状态，授权请求后原样带回给第三方。该参数可用于防止csrf攻击（跨站请求伪造攻击），建议第三方带上该参数，可设置为简单的随机数加session进行校验
    // **//
    public static String getUrl(String appid, String redirect_url, String response_type, String scope, String state) {
        return "https://open.weixin.qq.com/connect/qrconnect?appid=" + appid + "&redirect_uri=" + URLEncoder.encode(redirect_url) + "&response_type=" + response_type + "&scope=" + scope + "&state=" + URLEncoder.encode(state) + "#wechat_redirect";
    }

    public static LoginResult getAccessToken(String appid, String secret, String grant_type, String code) throws IOException {
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid + "" +
                "&secret=" + secret + "&code=" + code + "&grant_type=" + grant_type;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return LoginResult.create(result);
    }


    public static LoginUserResult getUserInfo(String access_token, String OPENID) throws IOException {
        String url = "https://api.weixin.qq.com/sns/userinfo?access_token=" + access_token + "&openid=" + OPENID;
        InputStream inputStream = HttpUtil.get(url);
        String res = DataFormatUtil.fullyReadStream(inputStream);
        JSONObject result = JSON.parseObject(res);
        return LoginUserResult.create(result);
    }
}
