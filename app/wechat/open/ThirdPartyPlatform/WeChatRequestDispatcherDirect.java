package wechat.open.ThirdPartyPlatform;

import models.db1.wechat.B_Wechat_App;
import wechat.app.bean.message.Article;
import wechat.app.bean.message.Message;
import wechat.app.sendMassage.PassiveReplyMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by whk on 2017/10/16.
 */
public class WeChatRequestDispatcherDirect {
    //关键词回复
    public static String WeChatTextResponse(B_Wechat_App app, Message message) {
        return PassiveReplyMessage.sendText(message, "1");

    }

    //默认回复
    public static String WeChatDeafultResponse(B_Wechat_App app,Message message) {
        return PassiveReplyMessage.sendText(message,"");
    }

    //关注回复
    public static String WeChatConcernResponse(B_Wechat_App app, Message message) {
        return PassiveReplyMessage.sendText(message, "");
    }

    //菜单点击事件
    public static String WeChatClickEventResponse(B_Wechat_App app, Message message) {
        return PassiveReplyMessage.sendText(message, "");
    }


    public static String dispatch(B_Wechat_App app, Message message) {
        if (app == null) {
            return " ";
        }
        if (message.MsgType == Message.MsgTypeEnum.text) {
            //默认回复

            //关键字回复

            return WeChatTextResponse(app, message);

        }else if(message.MsgType == Message.MsgTypeEnum.event){
            if (message.Event== Message.EventEnum.subscribe) {//关注
                return WeChatConcernResponse(app, message);
            } else if (message.Event== Message.EventEnum.unsubscribe) {//取消关注
                return WeChatClickEventResponse(app, message);
            } else if (message.Event== Message.EventEnum.SCAN) {//扫描带参数二维码事件(已关注)
                return WeChatClickEventResponse(app, message);
            }  else if (message.Event== Message.EventEnum.CLICK) {//自定义菜单事件
                return WeChatClickEventResponse(app, message);
            }
        }
        return " ";
    }
}
