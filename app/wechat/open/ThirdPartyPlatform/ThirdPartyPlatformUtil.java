package wechat.open.ThirdPartyPlatform;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import controllers.util.UrlController;
import models.db1.wechat.B_Wechat_Component_Verify_Ticket;
import util.Common;
import util.HttpUtil;
import util.LoggerUtil;
import util.cache.CacheUtil;
import wechat.bean.thirdPartyPlatform.AuthorizerAccessTokenAndRefreshTokenData;
import wechat.bean.thirdPartyPlatform.AuthorizerRefreshTokenData;
import wechat.bean.Result;
import wechat.bean.thirdPartyPlatform.WechatAppAccountInfoData;

import java.io.InputStream;
import java.util.concurrent.Callable;

/**
 * Created by whk on 2017/10/9.
 */
public class ThirdPartyPlatformUtil {
    //第三方平台配置信息
    final static String wechatThirdToken = Common.getStringConfig("wechat.open.third.componentToken");
    final static String wechatThirdEncodingAesKey = Common.getStringConfig("wechat.open.third.componenteEncodingAesKey");
    final static String wechatThirdAppId = Common.getStringConfig("wechat.open.third.appid");
    final static String wechatThirdAppSecret = Common.getStringConfig("wechat.open.third.appsecret");


    /**
     * 微信第三方平台获取token
     */
    public static String getAccessToken(final String component_appid, final String component_appsecret) throws Exception {
        final String cacheKey = "get_third_party_platform_access_token_" + component_appid + "_" + component_appsecret;
        B_Wechat_Component_Verify_Ticket b_component_verify_ticket= B_Wechat_Component_Verify_Ticket.FindByAppId(component_appid);
        final String component_verify_ticket = b_component_verify_ticket==null?null:b_component_verify_ticket.componentVerifyTicket;
        if(component_verify_ticket==null){
            return null;
        }
        Callable<String> fallback = new Callable<String>() {
            @Override
            public String call() throws Exception {
                String url = "https://api.weixin.qq.com/cgi-bin/component/api_component_token";
                JSONObject body = new JSONObject();
                body.put("component_appid", component_appid);
                body.put("component_appsecret", component_appsecret);
                body.put("component_verify_ticket", component_verify_ticket);
                try {
                    InputStream inputStream = HttpUtil.post(url, body.toJSONString());
                    String res = Common.fullyReadStream(inputStream);
                    JSONObject resJson = JSON.parseObject(res);
                    if(resJson.containsKey("errcode")){
                        return null;
                    }
                    String component_access_token = resJson.getString("component_access_token");
                    int expires_in = resJson.getInteger("expires_in");
                    CacheUtil.set(cacheKey, component_access_token, expires_in);
                    return component_access_token;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        return CacheUtil.get(cacheKey, fallback);
    }


    /**
    * 获取预授权码。预授权码用于公众号授权时的第三方平台方安全验证。
    */
    public static String getPreAuthCode(final String component_appid, final String component_appsecret) throws Exception {
        final String cacheKey = "get_pre_auth_code_" + component_appid + "_" + component_appsecret;
        final String component_access_token = getAccessToken(component_appid, component_appsecret);
        Callable<String> fallback = new Callable<String>() {
            @Override
            public String call() throws Exception {
                String url = "https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token=" + component_access_token;
                JSONObject body = new JSONObject();
                body.put("component_appid", component_appid);
                try {
                    InputStream inputStream = HttpUtil.post(url, body.toJSONString());
                    String res = Common.fullyReadStream(inputStream);
                    JSONObject resJson = JSON.parseObject(res);
                    if (resJson.containsKey("errcode")) {
                        Result result = new Result(resJson);
                        LoggerUtil.error("微信第三方平台获取授权跳转链接中pre_auth_code错误:"+result.errmsg_zn,LoggerUtil.LoggerState.wechat);
                        return null;
                    }
                    String pre_auth_code = resJson.getString("pre_auth_code");
                    int expires_in = resJson.getInteger("expires_in");
                    CacheUtil.set(cacheKey, pre_auth_code, expires_in);
                    return pre_auth_code;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        return CacheUtil.get(cacheKey, fallback);
    }


    /**
    * 获取第三方平台授权跳转链接，跳转到微信平台进行公众号的账号密码输入
    */
    public static String getComponentLoginUrl() {
        String redirect_uri = UrlController.WechatAppComponentLoginPageRedirectUrl;
        String code = null;
        try {
            code = getPreAuthCode(wechatThirdAppId, wechatThirdAppSecret);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        String url = "https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid=" + wechatThirdAppId + "&pre_auth_code=" + code + "&redirect_uri=" + redirect_uri;
        return code==null?null:url;
    }


    /**
    * 用授权码换取授权公众号或小程序的授权信息，并换取authorizer_access_token和authorizer_refresh_token
    */
    public static AuthorizerAccessTokenAndRefreshTokenData getAuthorizerAccessTokenAndRefreshToken(final String authorization_code) throws Exception {
        String component_access_token = getAccessToken(wechatThirdAppId, wechatThirdAppSecret);
        String url = "https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token=" + component_access_token;
        JSONObject body = new JSONObject();
        body.put("component_appid", wechatThirdAppId);
        body.put("authorization_code", authorization_code);
        try {
            InputStream inputStream = HttpUtil.post(url, body.toJSONString());
            String res = Common.fullyReadStream(inputStream);
            JSONObject resJson = JSON.parseObject(res);
            if (resJson.containsKey("errcode")) {
                return null;
            }

            return new AuthorizerAccessTokenAndRefreshTokenData(resJson);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
    * 在授权方令牌（authorizer_access_token）失效时，可用刷新令牌（authorizer_refresh_token）获取新的令牌。
    */
    public static AuthorizerRefreshTokenData authorizerRefreshToken(String authorizer_appid, String authorizer_refresh_token) throws Exception {
        String component_access_token = getAccessToken(wechatThirdAppId, wechatThirdAppSecret);
        String url = "https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token=" + component_access_token;
        JSONObject body = new JSONObject();
        body.put("component_appid", wechatThirdAppId);
        body.put("authorizer_appid", authorizer_appid);
        body.put("authorizer_refresh_token", authorizer_refresh_token);
        try {
            InputStream inputStream = HttpUtil.post(url, body.toJSONString());
            String res = Common.fullyReadStream(inputStream);
            JSONObject resJson = JSON.parseObject(res);
            if (resJson.containsKey("errcode")) {
                return null;
            }
            return  new AuthorizerRefreshTokenData(resJson);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
    * 获取授权方的公众号基本信息，包括头像、昵称、帐号类型、认证类型、微信号、原始ID和二维码图片URL。
    */
    public static WechatAppAccountInfoData getWechatAppAccountInfo(String authorizer_appid) throws Exception {
        String component_access_token = getAccessToken(wechatThirdAppId, wechatThirdAppSecret);
        String url = "https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_info?component_access_token=" + component_access_token;
        JSONObject body = new JSONObject();
        body.put("component_appid", wechatThirdAppId);
        body.put("authorizer_appid", authorizer_appid);
        try {
            InputStream inputStream = HttpUtil.post(url, body.toJSONString());
            String res = Common.fullyReadStream(inputStream);
            JSONObject resJson = JSON.parseObject(res);
            if (resJson.containsKey("errcode")) {
                return null;
            }
            return new WechatAppAccountInfoData(resJson);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
