package wechat.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by Administrator on 2016/3/23.
 */
public class Result {
    public int errcode = 0;
    public String errmsg = "ok";
    public String errmsg_zn ="操作成功";
    //原数据
    public String data;

    private Boolean success = true;


    public Boolean isSuccess(){
        return this.success;
    }

    public Result(){

    }

    public Result(JSONObject err_msg){
        this.errcode = err_msg.getInteger("errcode");
        this.data = err_msg.toJSONString();
        if(errcode==0){

        }else{
            this.success = false;
            this.errmsg = err_msg.getString("errmsg");
            this.errmsg_zn = err_msg.getString("errmsg");
            this.errmsg_zn = new GlobalReturnCode(errcode,errmsg).getErrmsgZn();
        }
    }
}
