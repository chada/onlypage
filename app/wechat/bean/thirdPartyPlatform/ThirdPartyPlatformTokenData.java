package wechat.bean.thirdPartyPlatform;

import com.alibaba.fastjson.JSONObject;
import scala.xml.Node;
import util.Common;

/**
 * Created by whk on 2017/10/9.
 */
public class ThirdPartyPlatformTokenData {
    public String AppId;
    public String CreateTime;
    public String InfoType;
    public String ComponentVerifyTicket;

    public static ThirdPartyPlatformTokenData fromXml(String xmlStr) {
        if (xmlStr == null) {
            return null;
        }
        JSONObject reqJson = com.zoowii.xml_json.parser.Parser.xmlTreeToJson(xmlStr);
        return fromReqJson(reqJson);
    }

    public static ThirdPartyPlatformTokenData fromXml(Node node) {
        if (node == null) {
            return null;
        }
        JSONObject reqJson = com.zoowii.xml_json.parser.Parser.xmlTreeToJson(node);
        return fromReqJson(reqJson);
    }

    public static ThirdPartyPlatformTokenData fromReqJson(JSONObject reqJson) {
        if (reqJson == null) {
            return null;
        }
        JSONObject xmlJson = reqJson.getJSONObject("xml");
        ThirdPartyPlatformTokenData req = new ThirdPartyPlatformTokenData();
        Common.putXmlParsedJsonToObject(req, xmlJson);
        return req;
    }
}
