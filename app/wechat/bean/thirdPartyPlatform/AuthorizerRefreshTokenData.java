package wechat.bean.thirdPartyPlatform;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by whk on 2017/10/9.
 */
public class AuthorizerRefreshTokenData {
    public String authorizer_access_token;
    public String expires_in;
    public String authorizer_refresh_token;


    public  AuthorizerRefreshTokenData(JSONObject object){
        this.authorizer_access_token = object.getString("authorizer_access_token");
        this.expires_in = object.getString("expires_in");
        this.authorizer_refresh_token = object.getString("authorizer_refresh_token");
    }
}
