package wechat.bean.thirdPartyPlatform;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by whk on 2017/10/9.
 */
public class AuthorizerAccessTokenAndRefreshTokenData {
    public String authorizer_appid;
    public String authorizer_access_token;
    public String expires_in;
    public String authorizer_refresh_token;
    public JSONArray func_info = new JSONArray();
    public Boolean apiAuthority = false;


    public AuthorizerAccessTokenAndRefreshTokenData(){

    }

    public AuthorizerAccessTokenAndRefreshTokenData(JSONObject jsonObject){
        JSONObject authorization_info = jsonObject.getJSONObject("authorization_info");
        this.authorizer_appid = authorization_info.getString("authorizer_appid");
        if(authorization_info.containsKey("authorizer_access_token")){
            this.apiAuthority = true;
            this.authorizer_access_token = authorization_info.getString("authorizer_access_token");
            this.expires_in = authorization_info.getString("expires_in");
            this.authorizer_refresh_token = authorization_info.getString("authorizer_refresh_token");
        }
        JSONArray func_info_array= authorization_info.getJSONArray("func_info");
        for(int i=0;i<func_info_array.size();i++){
            JSONObject object =  func_info_array.getJSONObject(i);
            JSONObject funcscope_category =  object.getJSONObject("funcscope_category");
            func_info.add(funcscope_category.getInteger("id"));
        }
    }
}
