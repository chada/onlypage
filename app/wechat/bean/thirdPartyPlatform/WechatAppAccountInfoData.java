package wechat.bean.thirdPartyPlatform;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by whk on 2017/10/9.
 */
public class WechatAppAccountInfoData {
    public String nick_name;
    public String head_img;
    public int service_type_info;
    public int verify_type_info;
    public String user_name;
    public String alias;
    public String qrcode_url;
    public String authorizer_appid;
    public JSONArray func_info = new JSONArray();

    public WechatAppAccountInfoData(JSONObject jsonObject){
        JSONObject authorizer_info = jsonObject.getJSONObject("authorizer_info");
        this.nick_name = authorizer_info.getString("nick_name");
        this.head_img = authorizer_info.getString("head_img");

        JSONObject service_type_info = authorizer_info.getJSONObject("service_type_info");
        this.service_type_info = service_type_info.getInteger("id");

        JSONObject verify_type_info = authorizer_info.getJSONObject("verify_type_info");
        this.verify_type_info = verify_type_info.getInteger("id");

        this.user_name = authorizer_info.getString("user_name");
        this.alias = authorizer_info.getString("alias");
        this.qrcode_url = authorizer_info.getString("qrcode_url");


        JSONObject authorization_info = jsonObject.getJSONObject("authorization_info");
        this.authorizer_appid = authorization_info.getString("authorizer_appid");

        JSONArray func_info_array= authorization_info.getJSONArray("func_info");
        for(int i=0;i<func_info_array.size();i++){
            JSONObject object =  func_info_array.getJSONObject(i);
            JSONObject funcscope_category =  object.getJSONObject("funcscope_category");
            this.func_info.add(funcscope_category.getInteger("id"));
        }
    }
}
