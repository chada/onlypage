package wechat.bean;

/**
 * Created by Administrator on 2016/3/8.
 * 微信全局返回码
 */
public class GlobalReturnCode {
    public int errcode;
    public String errmsg;


    public GlobalReturnCode(){

    }

    public GlobalReturnCode(int errcode,String errmsg){
        this.errcode= errcode;
        this.errmsg= errmsg;
    }

    public String getErrmsgZn() {
        String msg = "";
        switch (errcode) {
            case -1:
                msg = "系统繁忙，此时请开发者稍候再试";
                break;
            case 0:
                msg = "请求成功";
                break;
            case 40001:
                msg = "获取access_token时AppSecret错误，或者access_token无效。请开发者认真比对AppSecret的正确性，或查看是否正在为恰当的公众号调用接口";
                break;
            case 40002:
                msg = "不合法的凭证类型";
                break;
            case 40003:
                msg = "不合法的OpenID，请开发者确认OpenID（该用户）是否已关注公众号，或是否是其他公众号的OpenID";
                break;
            case 40004:
                msg = "不合法的媒体文件类型";
                break;
            case 40005:
                msg = "不合法的文件类型";
                break;
            case 40006:
                msg = "不合法的文件大小";
                break;
            case 40007:
                msg = "不合法的媒体文件id";
                break;
            case 40008:
                msg = "不合法的消息类型";
                break;
            case 40009:
                msg = "不合法的图片文件大小";
                break;
            case 40010:
                msg = "不合法的语音文件大小";
                break;
            case 40011:
                msg = "不合法的视频文件大小";
                break;
            case 40012:
                msg = "不合法的缩略图文件大小";
                break;
            case 40013:
                msg = "不合法的AppID，请开发者检查AppID的正确性，避免异常字符，注意大小写";
                break;
            case 40014:
                msg = "不合法的access_token，请开发者认真比对access_token的有效性（如是否过期），或查看是否正在为恰当的公众号调用接口";
                break;
            case 40015:
                msg = "不合法的菜单类型";
                break;
            case 40016:
                msg = "不合法的按钮个数";
                break;
            case 40017:
                msg = "不合法的按钮个数";
                break;
            case 40018:
                msg = "不合法的按钮名字长度";
                break;
            case 40019:
                msg = "不合法的按钮KEY长度";
                break;
            case 40020:
                msg = "不合法的按钮URL长度";
                break;
            case 40021:
                msg = "不合法的菜单版本号";
                break;
            case 40022:
                msg = "不合法的子菜单级数";
                break;
            case 40023:
                msg = "不合法的子菜单按钮个数";
                break;
            case 40024:
                msg = "不合法的子菜单按钮类型";
                break;
            case 40025:
                msg = "不合法的子菜单按钮名字长度";
                break;
            case 40026:
                msg = "不合法的子菜单按钮KEY长度";
                break;
            case 40027:
                msg = "不合法的子菜单按钮URL长度";
                break;
            case 40028:
                msg = "不合法的自定义菜单使用用户";
                break;
            case 40029:
                msg = "不合法的oauth_code";
                break;
            case 40030:
                msg = "不合法的refresh_token";
                break;
            case 40031:
                msg = "不合法的openid列表";
                break;
            case 40032:
                msg = "不合法的openid列表长度";
                break;
            case 40033:
                msg = "不合法的请求字符，不能包含\\uxxxx格式的字符";
                break;
            case 40035:
                msg = "不合法的参数";
                break;
            case 40038:
                msg = "不合法的请求格式";
                break;
            case 40039:
                msg = "不合法的URL长度";
                break;
            case 40050:
                msg = "不合法的分组id";
                break;
            case 40051:
                msg = "分组名字不合法";
                break;
            case 40117:
                msg = "分组名字不合法";
                break;
            case 40118:
                msg = "media_id大小不合法";
                break;
            case 40119:
                msg = "button类型错误";
                break;
            case 40120:
                msg = "button类型错误";
                break;
            case 40121:
                msg = "不合法的media_id类型";
                break;
            case 40132:
                msg = "微信号不合法";
                break;
            case 40137:
                msg = "不支持的图片格式";
                break;
            case 41001:
                msg = "缺少access_token参数";
                break;
            case 41002:
                msg = "缺少appid参数";
                break;
            case 41003:
                msg = "缺少refresh_token参数";
                break;
            case 41004:
                msg = "缺少secret参数";
                break;
            case 41005:
                msg = "缺少多媒体文件数据";
                break;
            case 41006:
                msg = "缺少media_id参数";
                break;
            case 41007:
                msg = "缺少子菜单数据";
                break;
            case 41008:
                msg = "缺少oauth code";
                break;
            case 41009:
                msg = "缺少openid";
                break;
            case 42001:
                msg = "access_token超时，请检查access_token的有效期，请参考基础支持-获取access_token中，对access_token的详细机制说明";
                break;
            case 42002:
                msg = "refresh_token超时";
                break;
            case 42003:
                msg = "oauth_code超时";
                break;
            case 43001:
                msg = "需要GET请求";
                break;
            case 43002:
                msg = "需要POST请求";
                break;
            case 43003:
                msg = "需要HTTPS请求";
                break;
            case 43004:
                msg = "需要接收者关注";
                break;
            case 43005:
                msg = "需要好友关系";
                break;
            case 44001:
                msg = "多媒体文件为空";
                break;
            case 44002:
                msg = "POST的数据包为空";
                break;
            case 44003:
                msg = "图文消息内容为空";
                break;
            case 44004:
                msg = "文本消息内容为空";
                break;
            case 45001:
                msg = "多媒体文件大小超过限制";
                break;
            case 45002:
                msg = "消息内容超过限制";
                break;
            case 45003:
                msg = "标题字段超过限制";
                break;
            case 45004:
                msg = "描述字段超过限制";
                break;
            case 45005:
                msg = "链接字段超过限制";
                break;
            case 45006:
                msg = "图片链接字段超过限制";
                break;
            case 45007:
                msg = "语音播放时间超过限制";
                break;
            case 45008:
                msg = "图文消息超过限制";
                break;
            case 45009:
                msg = "接口调用超过限制";
                break;
            case 45010:
                msg = "创建菜单个数超过限制";
                break;
            case 45015:
                msg = "回复时间超过限制";
                break;
            case 45016:
                msg = "系统分组，不允许修改";
                break;
            case 45017:
                msg = "分组名字过长";
                break;
            case 45018:
                msg = "分组数量超过上限";
                break;
            case 46001:
                msg = "不存在媒体数据";
                break;
            case 46002:
                msg = "不存在的菜单版本";
                break;
            case 46003:
                msg = "不存在的菜单数据";
                break;
            case 46004:
                msg = "不存在的用户";
                break;
            case 47001:
                msg = "解析JSON/XML内容错误";
                break;
            case 48001:
                msg = "api功能未授权，请确认公众号已获得该接口，可以在公众平台官网-开发者中心页中查看接口权限";
                break;
            case 50001:
                msg = "用户未授权该api";
                break;
            case 50002:
                msg = "用户受限，可能是违规后接口被封禁";
                break;
            case 61451:
                msg = "参数错误(invalid parameter)";
                break;
            case 61452:
                msg = "无效客服账号(invalid kf_account)";
                break;
            case 61453:
                msg = "客服帐号已存在(kf_account exsited)";
                break;
            case 61454:
                msg = "客服帐号名长度超过限制(仅允许10个英文字符，不包括@及@后的公众号的微信号)(invalid kf_acount length)";
                break;
            case 61455:
                msg = "客服帐号名包含非法字符(仅允许英文+数字)(illegal character in kf_account)";
                break;
            case 61456:
                msg = "客服帐号个数超过限制(10个客服账号)(kf_account count exceeded)";
                break;
            case 61457:
                msg = "无效头像文件类型(invalid file type)";
                break;
            case 61450:
                msg = "系统错误(system error)";
                break;
            case 61500:
                msg = "日期格式错误";
                break;
            case 61501:
                msg = "日期范围错误";
                break;
            case 9001001:
                msg = "POST数据参数不合法";
                break;
            case 9001002:
                msg = "远端服务不可用";
                break;
            case 9001003:
                msg = "Ticket不合法";
                break;
            case 9001004:
                msg = "获取摇周边用户信息失败";
                break;
            case 9001005:
                msg = "获取商户信息失败";
                break;
            case 9001006:
                msg = "获取OpenID失败";
                break;
            case 9001007:
                msg = "上传文件缺失";
                break;
            case 9001008:
                msg = "上传素材的文件类型不合法";
                break;
            case 9001009:
                msg = "上传素材的文件尺寸不合法";
                break;
            case 9001010:
                msg = "上传失败";
                break;
            case 9001020:
                msg = "帐号不合法";
                break;
            case 9001021:
                msg = "已有设备激活率低于50%，不能新增设备";
                break;
            case 9001022:
                msg = "设备申请数不合法，必须为大于0的数字";
                break;
            case 9001023:
                msg = "已存在审核中的设备ID申请";
                break;
            case 9001024:
                msg = "一次查询设备ID数量不能超过50";
                break;
            case 9001025:
                msg = "设备ID不合法";
                break;
            case 9001026:
                msg = "页面ID不合法";
                break;
            case 9001027:
                msg = "页面参数不合法";
                break;
            case 9001028:
                msg = "一次删除页面ID数量不能超过10";
                break;
            case 9001029:
                msg = "页面已应用在设备中，请先解除应用关系再删除";
                break;
            case 9001030:
                msg = "一次查询页面ID数量不能超过50";
                break;
            case 9001031:
                msg = "时间区间不合法";
                break;
            case 9001032:
                msg = "保存设备与页面的绑定关系参数错误";
                break;
            case 9001033:
                msg = "门店ID不合法";
                break;
            case 9001034:
                msg = "设备备注信息过长";
                break;
            case 9001035:
                msg = "设备申请参数不合法";
                break;
            case 9001036:
                msg = "查询起始值begin不合法";
                break;
            default:
                msg = "未知";
                break;
        }
        return msg;
    }
}
