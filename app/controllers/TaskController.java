package controllers;


import beans.task.strategy.DefaultStrategyOne;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.avaje.ebean.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.sun.org.apache.xpath.internal.operations.Bool;
import controllers.util.UrlController;
import models.db1.article.B_Article;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Info;
import models.db1.task.B_Task_Propagation_Log;
import models.db1.task.promotion.B_Task_Promotion;
import models.db1.task.schedule.B_Task_User_Statistics_Data;
import models.db1.task.strategy.B_Task_Strategy;
import models.db1.task.strategy.B_Task_Strategy_Execute_Log;
import models.db1.task.strategy.B_Task_Strategy_Info;
import models.db1.task.view.*;
import models.db1.user.S_User;
import models.db1.user.coin.B_User_Coin_Log;
import models.db1.user.coin.B_User_Coin_Out_Log;
import models.db1.user.tag.B_User_Tag;
import models.db1.user.tag.B_User_Tag_Enum;
import models.db1.wechat.B_Wechat_Pay;
import org.h2.util.StringUtils;
import models.db1.wechat.B_Wechat_Withdraw;
import play.db.ebean.Transactional;
import play.mvc.BodyParser;
import play.mvc.Call;
import play.mvc.Result;
import play.mvc.With;
import security.UserLoginSecuredAction;
import util.Common;
import wechat.other.webUserAuthorization.AccessTokenResult;
import wechat.other.webUserAuthorization.UserInfoResult;
import wechat.other.webUserAuthorization.WeChatWebUserAuthorization;
import wechat.pay.OrderManage;
import wechat.pay.WithDrawDepositManage;
import wechat.pay.beans.order.UnifiedOrderResult;
import wechat.pay.beans.withdrawDeposit.WithdrawDepositResult;

import java.io.IOException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class TaskController extends BaseController {

    @Transactional
    @With(UserLoginSecuredAction.class)
    public static Result edit() throws Exception {
        JsonNode node = request().body().asJson();
        Boolean isNew = node.get("isNew").asBoolean();
        String coverImg = node.get("coverImg").asText();
        String title = node.get("title").asText();
        String remark = node.get("remark").asText();
        String description = node.get("description").asText();
        String dataTypeStr = node.get("dataType").asText();
        String taskType = node.get("taskType").asText();
        String data = node.get("data").asText();
        String visibleTypeStr = node.get("visibleType").asText();
        int coin = node.get("coin").asInt();
        String autoWithdrawDepositMessage = node.get("autoWithdrawDepositMessage") == null ? null : node.get("autoWithdrawDepositMessage").asText();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date endTime = sdf.parse(node.get("endTime").asText());

        String viewTagStr = node.get("viewTag").asText();
        JSONObject viewTag = JSON.parseObject(viewTagStr);

        String strategyStr = node.get("strategy").asText();
        JSONObject strategy = JSON.parseObject(strategyStr);

        S_User owner = currentUser();
        B_Task.DataType dataType;
        B_Article article = null;


        if (dataTypeStr.equals("url")) {
            dataType = B_Task.DataType.url;
        } else if (dataTypeStr.equals("article")) {
            dataType = B_Task.DataType.article;

            article = B_Article.find.byId(Integer.valueOf(data));
            if (article == null) {
                return ajaxFail("不存在此文章");
            }
            if (!article.owner.id.equals(owner.id)) {
                return ajaxFail("您没有权限使用此文章");
            }
            //清空data数据，因为无效
            data = null;
        } else {
            return ajaxFail("不存在此任务内容类型");
        }

        B_Task.VisibleType visibleType;
        if (visibleTypeStr.equals("all")) {
            visibleType = B_Task.VisibleType.all;
        } else if (visibleTypeStr.equals("friend")) {
            visibleType = B_Task.VisibleType.friend;
        } else if (visibleTypeStr.equals("self")) {
            visibleType = B_Task.VisibleType.self;
        } else {
            return ajaxFail("不存在此任务可见类型");
        }

        if (isNew) {

            B_Task task = B_Task.create(owner, title, remark, coverImg, endTime,B_Task.stringConvertTaskType(taskType), dataType, data, article, visibleType,description);
            if(task.taskType== B_Task.TaskType.promotion){
                B_Task_Promotion promotion=B_Task_Promotion.initByTask(task);
            }
            B_Task_Strategy_Info info = B_Task_Strategy_Info.create(coin, autoWithdrawDepositMessage, strategy, task);
            B_Task_View_Tag tag = B_Task_View_Tag.create(viewTag, task);
            return ajaxSuccess("创建成功");
        } else {
            String uuid = node.get("uuid").asText();
            B_Task task = B_Task.findByUuid(uuid);
            if (task == null) {
                return ajaxFail("任务不存在");
            }
            if (!task.owner.id.equals(currentUser().id)) {
                return ajaxFail("您没有权限修改");
            }

            if (task.state != B_Task.State.init) {
                return ajaxFail("任务不能修改");
            }

            task.edit(title, remark, coverImg, endTime,B_Task.stringConvertTaskType(taskType), dataType, data, article, visibleType,description);
            if(task.taskType== B_Task.TaskType.promotion){
                B_Task_Promotion promotion=B_Task_Promotion.initByTask(task);
            }
            task.strategyInfo.edit(coin, autoWithdrawDepositMessage, strategy);
            task.viewTag.edit(viewTag);


            return ajaxSuccess("修改成功");
        }
    }


    @With(UserLoginSecuredAction.class)
    public static Result getTaskEditInfo() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        if (!task.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有权限修改");
        }

        if (task.state != B_Task.State.init) {
            return ajaxFail("任务不能修改");
        }
        JSONObject result = new JSONObject();
        result.put("coverImg", task.coverImg);
        result.put("title", task.title);
        result.put("remark", task.remark);
        result.put("description", task.description);
        result.put("dataType", task.dataType);
        result.put("taskType", task.taskType);
        if (task.dataType == B_Task.DataType.url) {
            result.put("data", task.data);
        } else if (task.dataType == B_Task.DataType.article) {
            JSONObject article = new JSONObject();
            article.put("id", task.article.id);
            article.put("title", task.article.title);
            result.put("article", article);
        }

        result.put("visibleType", task.visibleType);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        result.put("endTime", sdf.format(task.endTime));

        B_Task_Strategy_Info strategyInfo = task.strategyInfo;
        result.put("coin", strategyInfo.coin);
        result.put("autoWithdrawDepositMessage", strategyInfo.autoWithdrawDepositMessage);

        B_Task_Strategy strategy = strategyInfo.strategy;
        if (strategyInfo.strategy.strategyType == B_Task_Strategy.StrategyType.defaultStrategyOne) {
            DefaultStrategyOne defaultStrategyOne = DefaultStrategyOne.getDefaultStrategyOneByStragetyId(strategy);
            result.put("defaultStrategyOne", defaultStrategyOne);
            result.put("strategyType", strategy.strategyType);
        } else if (strategyInfo.strategy.strategyType == B_Task_Strategy.StrategyType.advancedStrategy) {

            result.put("strategyType", strategy.strategyType);
            JSONObject advancedStrategy = new JSONObject();
            advancedStrategy.put("title", strategyInfo.strategy.title);
            advancedStrategy.put("uuid", strategyInfo.strategy.uuid);
            result.put("advancedStrategy", advancedStrategy);
        }

        JSONObject tag = new JSONObject();
        B_Task_View_Tag task_view_tag = task.viewTag;
        tag.put("isShow", task_view_tag.isShow);
        tag.put("isShowRedPackage", task_view_tag.isShowRedPackage);
        tag.put("isShowRedPackageBillBoard", task_view_tag.isShowRedPackageBillBoard);
        tag.put("isShowCreditBillBoard", task_view_tag.isShowCreditBillBoard);
        tag.put("isShowUserInput", task_view_tag.isShowUserInput);
        result.put("tag", tag);

        if (task_view_tag.isShowUserInput) {
            B_Task_View_Tag_Lable lable = task_view_tag.lable;
            JSONObject tagLable = new JSONObject();
            tagLable.put("title", lable.title);
            tagLable.put("remark", lable.remark);
            JSONArray infoList = new JSONArray();
            for (B_Task_View_Tag_Lable_Info info : lable.getInfoList()) {
                JSONObject object = new JSONObject();
                object.put("rowName", info.rowName);

                object.put("checked", true);
                if (info.type == B_Task_View_Tag_Lable_Info.Type.phone) {
                    object.put("edit", false);
                } else {
                    object.put("edit", true);
                }
                object.put("type", info.type);
                object.put("id", info.id);
                infoList.add(object);
            }
            tagLable.put("infoList", infoList);
            result.put("tagLable", tagLable);
        }


        return ajaxSuccess(result);
    }


    @With(UserLoginSecuredAction.class)
    public static Result getRunningTaskEditInfo() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        if (!task.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有权限修改");
        }

        if (!(task.state == B_Task.State.init || task.state == B_Task.State.publish)) {
            return ajaxFail("任务不能修改");
        }
        JSONObject result = new JSONObject();
        result.put("uuid", task.uuid);
        result.put("coverImg", task.coverImg);
        result.put("title", task.title);
        result.put("remark", task.remark);
        result.put("description", task.description);
        result.put("visibleType", task.visibleType);
        B_Task_Strategy_Info strategyInfo = task.strategyInfo;
        result.put("autoWithdrawDepositMessage", strategyInfo.autoWithdrawDepositMessage);


        JSONObject tag = new JSONObject();
        B_Task_View_Tag task_view_tag = task.viewTag;
        tag.put("isShow", task_view_tag.isShow);
        tag.put("isShowRedPackage", task_view_tag.isShowRedPackage);
        tag.put("isShowRedPackageBillBoard", task_view_tag.isShowRedPackageBillBoard);
        tag.put("isShowCreditBillBoard", task_view_tag.isShowCreditBillBoard);
        tag.put("isShowUserInput", task_view_tag.isShowUserInput);


        if (task_view_tag.isShowUserInput) {
            B_Task_View_Tag_Lable lable = task_view_tag.lable;
            JSONObject tagLable = new JSONObject();
            tagLable.put("title", lable.title);
            tagLable.put("remark", lable.remark);
            JSONArray infoList = new JSONArray();
            for (B_Task_View_Tag_Lable_Info info : lable.getInfoList()) {
                JSONObject object = new JSONObject();
                object.put("rowName", info.rowName);

                object.put("checked", true);
                if (info.type == B_Task_View_Tag_Lable_Info.Type.phone) {
                    object.put("edit", false);
                } else {
                    object.put("edit", true);
                }
                object.put("type", info.type);
                object.put("id", info.id);
                infoList.add(object);
            }
            tagLable.put("infoList", infoList);
            tag.put("tagLable", tagLable);
        }

        result.put("viewTag", tag);
        return ajaxSuccess(result);
    }


    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result runningTaskEditSave() {
        JsonNode node = request().body().asJson();
        JSONObject data = JSON.parseObject(node.get("data").asText());
        String uuid = data.getString("uuid");
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        if (!task.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有权限修改");
        }

        if (!(task.state == B_Task.State.init || task.state == B_Task.State.publish)) {
            return ajaxFail("任务不能修改");
        }
        String title = data.getString("title");
        String remark = data.getString("remark");
        String description = data.getString("description");
        String coverImg = data.getString("coverImg");
        String visibleTypeStr = data.getJSONObject("visibleType").getString("value");

        B_Task.VisibleType visibleType;
        if (visibleTypeStr.equals("all")) {
            visibleType = B_Task.VisibleType.all;
        } else if (visibleTypeStr.equals("friend")) {
            visibleType = B_Task.VisibleType.friend;
        } else if (visibleTypeStr.equals("self")) {
            visibleType = B_Task.VisibleType.self;
        } else {
            return ajaxFail("不存在此任务可见类型");
        }
        task.edit(title, remark, coverImg, visibleType,description);

        String autoWithdrawDepositMessage = data.containsKey("autoWithdrawDepositMessage") ? data.getString("autoWithdrawDepositMessage") : null;
        task.strategyInfo.changeAutoWithdrawDepositMessage(autoWithdrawDepositMessage);

        JSONObject viewTag = data.getJSONObject("viewTag");
        task.viewTag.edit(viewTag);
        return ajaxSuccess("修改成功");
    }


    @Transactional
    @With(UserLoginSecuredAction.class)
    public static Result delete() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        if (task.state != B_Task.State.init) {
            return ajaxFail("任务状态不能删除");
        }
        if (!task.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有权限删除");
        }
        task.delete();
        return ajaxSuccess("删除成功");
    }


    @Transactional
    @With(UserLoginSecuredAction.class)
    public static Result checkout() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        if (!task.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有权限结算此任务");
        }
        if (!task.isRunning()) {
            return ajaxFail("任务状态不能进行结算");
        }
        task.finish();
        return ajaxSuccess("结算成功");
    }


    @With(UserLoginSecuredAction.class)
    public static Result getTaskOwnerList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String queryType = node.get("queryType").asText();

        PagingList<B_Task> pagingList = null;
        if ("search".equals(queryType)) {
            String search_taskType = node.get("search_taskType").asText();
            String search_taskName = node.get("search_taskName").asText();
            ExpressionList expressionList = B_Task.find.where().eq("owner", currentUser()).eq("deleteLogic", false);

            if (!search_taskType.equals("null")) {
                expressionList = expressionList.eq("taskType", B_Task.stringConvertTaskType(search_taskType));
            }
            if (!search_taskName.equals("null")) {
                expressionList = expressionList.like("title", "%" + search_taskName + "%");
            }
            pagingList = expressionList.order("createdTime desc").findPagingList(pageSize);
        }else {
            pagingList = B_Task.find.where().eq("owner", currentUser()).eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        }

        Page<B_Task> page = pagingList.getPage(pageNum - 1);
        List<B_Task> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("editedTime", info.editedTime);
            object.put("title", info.title);
            object.put("taskType", info.taskType);
            object.put("endTime", info.endTime);
            object.put("description", info.description==null?"":info.description);
            object.put("coin", info.strategyInfo.coin);
            object.put("hasCost", info.strategyInfo.hasAllocationCoin);
            object.put("state", info.state);
            object.put("shareUrl", info.getStartUrl());
            object.put("isShowUserInput", info.viewTag.isShowUserInput);
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    public static Result getTaskFrontList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        List<B_Task.State> states = new ArrayList<B_Task.State>() {{
            add(B_Task.State.publish);
            add(B_Task.State.finish);
        }};
        PagingList<B_Task> pagingList = B_Task.find.where().in("state", states).eq("deleteLogic", false).eq("visibleType", B_Task.VisibleType.all).order("createdTime desc").findPagingList(pageSize);
        Page<B_Task> page = pagingList.getPage(pageNum - 1);
        List<B_Task> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("editedTime", info.editedTime);
            object.put("title", info.title);
            object.put("endTime", info.endTime);
            object.put("state", info.state);
            object.put("username", info.owner.getShowName());
            object.put("useruuid", info.owner.getRecordUuid());
            object.put("coverImg", info.coverImg);
            object.put("coin", info.strategyInfo.coin);
            object.put("count", info.getParticipantCount());
            object.put("startUrl", info.getStartUrl());
            if (info.dataType == B_Task.DataType.url) {
                object.put("viewUrl", info.data);
            } else if (info.dataType == B_Task.DataType.article) {
                object.put("viewUrl", info.article.getPcViewLink());
            }

            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result commitCheck() throws Exception {
        int id = request().body().asJson().get("id").asInt();
        S_User user = currentUser();
        B_Task task = B_Task.find.byId(id);
        if (task == null) {
            return ajaxFail("不存在此任务");
        }
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有此权限");
        }
        if (!task.state.equals(B_Task.State.init)) {
            if (task.state == B_Task.State.checking) {
                return ajaxFail("任务正在审核中");
            }
            return ajaxFail("任务状态错误，禁止提交审核");
        }

        if (task.strategyInfo.coin == 0) {
            task.changeState(B_Task.State.publish);
            return ajaxSuccess("审核通过");
        } else {
            int total_fee = task.strategyInfo.coin + task.strategyInfo.fee;
//            total_fee = 1;

            if (task.strategyInfo.pay != null && task.strategyInfo.pay.isInitAndUseful() && task.strategyInfo.pay.isNativeTradeType()) {
                JSONObject object = new JSONObject();
                object.put("url", task.strategyInfo.pay.code_url);
                object.put("coin", total_fee);
                return ajaxSuccess(object);
            } else {
                String nonce_str = Common.randomString(32).trim();
                String body = "任务\"" + task.title + "\"发布费用(包含平台手续费)";
                String out_trade_no = B_Wechat_Pay.getOneUnUseOutTradeNo();
                String spbill_create_ip = request().remoteAddress();
                String product_id = B_Wechat_Pay.getOneUnUseProductId();
                String notify_url = UrlController.serverAddress + "/api/wechat/pay/taskCommitCheckResult";
                UnifiedOrderResult result = OrderManage.unifiedorderNative(nonce_str, body, out_trade_no, total_fee, spbill_create_ip, product_id, notify_url);
                if (result.isSuccess()) {
                    B_Wechat_Pay pay = B_Wechat_Pay.create(currentUser(), product_id, total_fee, out_trade_no, nonce_str, result.trade_type, result.prepay_id, result.code_url);
                    task.updateWechatPay(pay);

                    JSONObject object = new JSONObject();
                    object.put("url", result.code_url);
                    object.put("coin", total_fee);
                    return ajaxSuccess(object);
                } else {
                    return ajaxFail(result.getErrorMsg());
                }
            }
        }
    }

    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result renew() throws Exception {
        JsonNode node = request().body().asJson();
        int id = node.get("id").asInt();
        int coin = node.get("coin").asInt();
        S_User user = currentUser();
        B_Task task = B_Task.find.byId(id);
        if (coin <= 0) {
            return ajaxFail("充值金额必须大于0元");
        }
        if (task == null) {
            return ajaxFail("不存在此任务");
        }
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有此权限");
        }
        if (!(task.state.equals(B_Task.State.publish) || task.state.equals(B_Task.State.finish))) {
            return ajaxFail("任务目前状态不允许充值");
        }

        int fee = (int) (coin * Common.getIntConfig("task.check.fee") / 100);
        int total_fee = coin + fee;
        String nonce_str = Common.randomString(32).trim();
        String body = "任务\"" + task.title + "\"充值费用(包含" + Common.getIntConfig("task.check.fee") + "%微信手续费)";
        String out_trade_no = B_Wechat_Pay.getOneUnUseOutTradeNo();
        String spbill_create_ip = request().remoteAddress();
        String product_id = B_Wechat_Pay.getOneUnUseProductId();
        String notify_url = UrlController.serverAddress + "/api/wechat/pay/taskRenewResult";
        UnifiedOrderResult result = OrderManage.unifiedorderNative(nonce_str, body, out_trade_no, total_fee, spbill_create_ip, product_id, notify_url);
        if (result.isSuccess()) {
            B_Wechat_Pay pay = B_Wechat_Pay.create(currentUser(), product_id, total_fee, out_trade_no, nonce_str, result.trade_type, result.prepay_id, result.code_url);
            task.updateRenew(pay, coin, fee);
            JSONObject object = new JSONObject();
            object.put("url", result.code_url);
            object.put("coin", total_fee);
            return ajaxSuccess(object);
        } else {
            return ajaxFail(result.getErrorMsg());
        }
    }


    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result getCloneStr() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        S_User user = currentUser();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("不存在此任务");
        }
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有此权限");
        }
        return ajaxSuccess(task.getCloneStr());
    }


    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result copy() throws Exception {
        JsonNode node = request().body().asJson();
        String cloneStr = node.get("cloneStr").asText();
        B_Task task = B_Task.findByCloneStr(cloneStr);
        if (task == null) {
            return ajaxFail("复制码错误");
        }
        task.copy(currentUser());
        return ajaxSuccess("复制成功");
    }


    @With(UserLoginSecuredAction.class)
    public static Result getTaskPublishViewTagRedPackageInfo() {
        int taskId = getIntPostParam("taskId");
        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        B_Task_User_Statistics_Data statisticsData = B_Task_User_Statistics_Data.get(currentUser(), task);
        JSONObject result = new JSONObject();
        result.put("coin", statisticsData.coin);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        result.put("endTime", df.format(task.endTime));
        return ajaxSuccess(result);
    }

    @With(UserLoginSecuredAction.class)
    public static Result getTaskPublishViewTagCoinBillboardInfo() {
        int taskId = getIntPostParam("taskId");
        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        JSONObject result = new JSONObject();
        JSONArray billboard = new JSONArray();
        List<B_Task_User_Statistics_Data> dataList = B_Task_User_Statistics_Data.findForCoinTop(task, 3);
        for (B_Task_User_Statistics_Data data : dataList) {
            JSONObject object = new JSONObject();
            object.put("username", data.owner.getShowName());
            object.put("avatar", data.owner.getShowAvatar());
            object.put("rank", dataList.indexOf(data) + 1);
            object.put("coin", data.coin);
            billboard.add(object);
        }
        result.put("billboard", billboard);

        result.put("selfRank", B_Task_User_Statistics_Data.getSelfCoinBillBoard(task, currentUser()));
        return ajaxSuccess(result);
    }


    @With(UserLoginSecuredAction.class)
    public static Result getTaskPublishViewTagCreditBillboardInfo() {
        int taskId = getIntPostParam("taskId");
        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        JSONObject result = new JSONObject();
        JSONArray billboard = new JSONArray();
        List<B_Task_User_Statistics_Data> dataList = B_Task_User_Statistics_Data.findForCreditTop(task, 3);
        for (B_Task_User_Statistics_Data data : dataList) {
            JSONObject object = new JSONObject();
            object.put("username", data.owner.getShowName());
            object.put("avatar", data.owner.getShowAvatar());
            object.put("rank", dataList.indexOf(data) + 1);
            object.put("credit", data.credit);
            billboard.add(object);
        }
        result.put("billboard", billboard);

        result.put("selfRank", B_Task_User_Statistics_Data.getSelfCreditBillBoard(task, currentUser()));
        return ajaxSuccess(result);
    }


    @With(UserLoginSecuredAction.class)
    public static Result getTaskPublishViewTagUserInputInfo() {
        int taskId = getIntPostParam("taskId");
        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        JSONObject result = new JSONObject();
        B_Task_View_Tag_Lable lable = task.viewTag.lable;
        if (lable == null) {
            return ajaxFail("数据错误");
        }
        result.put("title", lable.title);
        result.put("remark", lable.remark);
        result.put("id", lable.id);
        JSONArray row = new JSONArray();
        for (B_Task_View_Tag_Lable_Info info : lable.getInfoList()) {
            JSONObject object = new JSONObject();
            object.put("rowName", info.rowName);
            object.put("type", info.type);
            object.put("id", info.id);
            row.add(object);
        }
        result.put("rows", row);
        return ajaxSuccess(result);
    }

    @Transactional
    @With(UserLoginSecuredAction.class)
    public static Result submitTaskPublishViewTagUserInputInfo() {
        String data = getPostParam("data");
        int taskId = getIntPostParam("taskId");
        int lableId = getIntPostParam("lableId");

        B_Task_View_Tag_Lable tagLable = B_Task_View_Tag_Lable.find.byId(lableId);
        if (tagLable == null) {
            return ajaxFail("数据错误");
        }

        HashMap<B_Task_View_Tag_Lable_Info, String> map = new HashMap<>();
        JSONArray array = JSON.parseArray(data);
        for (Object o : array) {
            JSONObject object = (JSONObject) o;
            int id = object.getInteger("id");
            String value = object.getString("data");
            if (id == -1) {

                String phone = null;
                for (B_Task_View_Tag_Lable_Info info : tagLable.getInfoList()) {
                    if (info.type.equals(B_Task_View_Tag_Lable_Info.Type.phone)) {
                        for (Object temp : array) {
                            JSONObject tempJson = (JSONObject) temp;
                            if (tempJson.getInteger("id").equals(info.id)) {
                                phone = tempJson.getString("data");
                            }
                        }
                    }
                }
                if (session("taskUserInputSms_" + phone + "_" + taskId) == null || !session("taskUserInputSms_" + phone + "_" + taskId).equals(value)) {
                    return ajaxFail("手机验证码错误");
                }
            } else {
                B_Task_View_Tag_Lable_Info info = B_Task_View_Tag_Lable_Info.find.byId(id);
                if (info == null) {
                    return ajaxFail("数据错误");
                }
                map.put(info, value);
            }
        }
        //数据存储
        B_Task_View_Tag_Lable_Submit submit = B_Task_View_Tag_Lable_Submit.create(tagLable, currentUser());
        Iterator iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            B_Task_View_Tag_Lable_Info info = (B_Task_View_Tag_Lable_Info) iterator.next();
            String value = map.get(info);
            B_Task_View_Tag_Lable_Value.create(info, submit, value);
        }

        return ajaxSuccess("提交成功");
    }


    //任务统计页面任务基本信息
    @With(UserLoginSecuredAction.class)
    public static Result getStatisticalBaseInfo() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        S_User user = currentUser();
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有查看权限");
        }
        JSONObject result = new JSONObject();
        result.put("title", task.title);
        result.put("endTime", task.endTime);
        result.put("state", task.state);
        result.put("dataType", task.dataType);
        if (task.dataType == B_Task.DataType.url) {
            result.put("data", task.data);
            result.put("viewUrl", task.data);
        } else if (task.dataType == B_Task.DataType.article) {
            result.put("articleTitle", task.article.title);
            result.put("articleId", task.article.id);
            result.put("viewUrl", task.article.getPcViewLink());
        }

        B_Task_Strategy_Info strategy_info = task.strategyInfo;
        result.put("coin", strategy_info.coin);
        result.put("hasAllocationCoin", strategy_info.hasAllocationCoin);

        result.put("count", task.propagationLogs.size());

        return ajaxSuccess(result);
    }


    @With(UserLoginSecuredAction.class)
    public static Result getTaskAccepterList() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        S_User user = currentUser();
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有查看权限");
        }
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        PagingList<B_Task_User_Statistics_Data> pagingList = B_Task_User_Statistics_Data.find.where().eq("task", task).eq("deleteLogic", false).order("coin desc").findPagingList(pageSize);
        Page<B_Task_User_Statistics_Data> page = pagingList.getPage(pageNum - 1);
        List<B_Task_User_Statistics_Data> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task_User_Statistics_Data info : list) {
            JSONObject object = new JSONObject();
            S_User accepter = info.owner;
            object.put("username", accepter.getShowName());
            object.put("userUUID", accepter.uuid);
            object.put("avatar", accepter.getShowAvatar());
            object.put("createdTime", info.createdTime);
            object.put("coin", info.coin);
            object.put("credit", info.credit);
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }

    @With(UserLoginSecuredAction.class)
    public static Result getPropagationInfoList() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        S_User user = currentUser();
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有查看权限");
        }
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();

        String sql= MessageFormat.format("SELECT taa.propagation_id,taa.nickname,taa.latest_date,taa.user_uuid,tbb.count from (\n" +
                "SELECT DISTINCT(proinfo.executant_propagation_log_id) 'propagation_id',wechatinfo.nickname 'nickname',max(proinfo.created_time) 'latest_date',task.id 'task_id',suser.uuid 'user_uuid'\n" +
                "from b_task_propagation_info proinfo \n" +
                "INNER JOIN b_task_propagation_log prolog on prolog.id=proinfo.executant_propagation_log_id\n" +
                "INNER JOIN b_task task on task.id=prolog.task_id\n" +
                "INNER JOIN s_user suser on suser.id=prolog.executant_id\n" +
                "LEFT JOIN b_user_wechat_info wechatinfo on wechatinfo.owner_id=suser.id\n" +
                "where task.uuid={0}\n" +
                "GROUP BY propagation_id,nickname,task_id,user_uuid) taa\n" +
                "LEFT JOIN(\n" +
                "\tSELECT count(*) 'count',a.task_id 'task_id' from(SELECT DISTINCT(proinfo.executant_propagation_log_id) 'propagation_id',wechatinfo.nickname 'nickname',max(proinfo.created_time) 'latest_date',task.id 'task_id'\n" +
                "\tfrom b_task_propagation_info proinfo \n" +
                "\tINNER JOIN b_task_propagation_log prolog on prolog.id=proinfo.executant_propagation_log_id\n" +
                "\tINNER JOIN b_task task on task.id=prolog.task_id\n" +
                "\tINNER JOIN s_user suser on suser.id=prolog.executant_id\n" +
                "\tLEFT JOIN b_user_wechat_info wechatinfo on wechatinfo.owner_id=suser.id\n" +
                "\twhere task.uuid={0}\n" +
                "\tGROUP BY propagation_id,nickname,task_id) a\n" +
                ")tbb on taa.task_id=tbb.task_id\n" +
                "limit {1},{2};",String.valueOf("'"+uuid+"'"),String.valueOf((pageNum-1)*pageSize),String.valueOf(pageSize));
        List<SqlRow> sqlRowList = Ebean.createSqlQuery(sql).findList();
        JSONArray array = new JSONArray();
        int totalCount=0;
        for (SqlRow sqlRow : sqlRowList) {
            int propagation_id = sqlRow.getInteger("propagation_id");
            String nickname = sqlRow.getString("nickname");
            Date latest_date = sqlRow.getDate("latest_date");
            String user_uuid = sqlRow.getString("user_uuid");
            int count = sqlRow.getInteger("count");
            JSONObject object = new JSONObject();
            S_User infouser =S_User.findByUuid(user_uuid);
            object.put("propagation_id",propagation_id);
            object.put("user_uuid", user_uuid);
            object.put("username", infouser.getShowName());
            object.put("avatar", infouser.getShowAvatar());
            object.put("latest_date", latest_date);
            totalCount=(totalCount>0?totalCount:count);
            array.add(object);
        }
        return paginatorSuccess(array, totalCount/pageSize+1);
    }

    /* */
    @With(UserLoginSecuredAction.class)
    public static Result getPropagationInfoListByPropagation() throws Exception {
        JsonNode node = request().body().asJson();
        Integer id = node.get("id").asInt();
        B_Task_Propagation_Log log = B_Task_Propagation_Log.find.byId(id);
        if (log == null) {
            return ajaxFail("节点不存在");
        }

        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        PagingList<B_Task_Propagation_Info> pagingList = B_Task_Propagation_Info.find.where().eq("executantPropagationLog", log).eq("deleteLogic", false).findPagingList(pageSize);
        Page<B_Task_Propagation_Info> page = pagingList.getPage(pageNum - 1);
        List<B_Task_Propagation_Info> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task_Propagation_Info info : list) {
            JSONObject object = new JSONObject();
            S_User accepter = info.executantPropagationLog.executant;
            object.put("username", accepter.getShowName());
            object.put("userUUID", accepter.uuid);
            object.put("avatar", accepter.getShowAvatar());
            object.put("createdTime", info.createdTime);
            object.put("realName", info.realName);
            object.put("telePhone", info.telePhone);
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }

    @With(UserLoginSecuredAction.class)
    public static Result getTaskViewTagUserInputList() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.find.fetch("viewTag.lable.infoList").where().eq("uuid", uuid).findUnique();
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        S_User user = currentUser();
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有查看权限");
        }

        JSONObject result = new JSONObject();
        JSONArray title = new JSONArray();
        for (B_Task_View_Tag_Lable_Info info : task.viewTag.lable.getInfoList()) {
            JSONObject row = new JSONObject();
            row.put("rowName", info.rowName);
            row.put("id", info.id);
            title.add(row);
        }
        result.put("title", title);

        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        PagingList<B_Task_View_Tag_Lable_Submit> pagingList = B_Task_View_Tag_Lable_Submit.find.fetch("valueList").where().eq("lable.viewTag.task", task).eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        Page<B_Task_View_Tag_Lable_Submit> page = pagingList.getPage(pageNum - 1);
        List<B_Task_View_Tag_Lable_Submit> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task_View_Tag_Lable_Submit info : list) {
            JSONObject object = new JSONObject();
            S_User accepter = info.owner;
            object.put("username", accepter.getShowName());
            object.put("avatar", accepter.getShowAvatar());
            object.put("createdTime", info.createdTime);
            JSONArray data = new JSONArray();
            for (B_Task_View_Tag_Lable_Value value : info.valueList) {
                JSONObject temp = new JSONObject();
                temp.put("value", value.value);
                temp.put("id", value.info.id);
                data.add(temp);
            }
            object.put("data", data);
            array.add(object);
        }
        result.put("info", array);
        return paginatorSuccess(result, pagingList.getTotalPageCount());
    }


    @With(UserLoginSecuredAction.class)
    public static Result getStatisticalTimeLineInfo() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        String tag = node.get("tag").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        S_User user = currentUser();
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有查看权限");
        }

        //返回数据列表
        JSONArray result = new JSONArray();

        Calendar calendar = Calendar.getInstance();
        if ("today".equals(tag)) {
            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date currDate = calendar.getTime();
            calendar.set(Calendar.HOUR_OF_DAY, 24);
            Date endDate = calendar.getTime();
            while (endDate.compareTo(currDate) > 0) {
                JSONObject object = new JSONObject();
                object.put("startDate", currDate);
                calendar.setTime(currDate);
                calendar.add(Calendar.HOUR_OF_DAY, 1);
                currDate = calendar.getTime();
                object.put("endDate", currDate);
                result.add(object);
            }
        } else if ("yesterday".equals(tag)) {
            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.add(Calendar.HOUR_OF_DAY, -24);
            Date currDate = calendar.getTime();
            calendar.set(Calendar.HOUR_OF_DAY, 24);
            Date endDate = calendar.getTime();
            while (endDate.compareTo(currDate) > 0) {
                JSONObject object = new JSONObject();
                object.put("startDate", currDate);
                calendar.setTime(currDate);
                calendar.add(Calendar.HOUR_OF_DAY, 1);
                currDate = calendar.getTime();
                object.put("endDate", currDate);
                result.add(object);
            }
        } else if ("last15".equals(tag)) {
            calendar.setTime(new Date());
            calendar.add(Calendar.DAY_OF_YEAR, -15);
            Date currDate = calendar.getTime();
            if (currDate.compareTo(task.createdTime) < 0) {
                calendar.setTime(task.createdTime);
            }
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            currDate = calendar.getTime();

            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.add(Calendar.HOUR_OF_DAY, 23);
            Date endDate = calendar.getTime();

            while (endDate.compareTo(currDate) > 0) {
                JSONObject object = new JSONObject();
                object.put("startDate", currDate);
                calendar.setTime(currDate);
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                currDate = calendar.getTime();
                object.put("endDate", currDate);
                result.add(object);
            }
        } else if ("last30".equals(tag)) {
            calendar.setTime(new Date());
            calendar.add(Calendar.DAY_OF_YEAR, -30);
            Date currDate = calendar.getTime();
            if (currDate.compareTo(task.createdTime) < 0) {
                calendar.setTime(task.createdTime);
            }
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            currDate = calendar.getTime();

            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.add(Calendar.HOUR_OF_DAY, 23);
            Date endDate = calendar.getTime();

            while (endDate.compareTo(currDate) > 0) {
                JSONObject object = new JSONObject();
                object.put("startDate", currDate);
                calendar.setTime(currDate);
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                currDate = calendar.getTime();
                object.put("endDate", currDate);
                result.add(object);
            }
        }

        for (Object o : result) {
            JSONObject object = (JSONObject) o;
            Date startDate = object.getDate("startDate");
            Date endDate = object.getDate("endDate");
            object.put("visitCount", B_Task_Propagation_Log.getCountBetweenStartTimeAndEndTime(task, startDate, endDate));
        }
        return ajaxSuccess(result);
    }


    @With(UserLoginSecuredAction.class)
    public static Result getStatisticalPropagationInfo() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        String propagation = node.get("propagation").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        S_User user = currentUser();
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有查看权限");
        }
        List<B_Task_Propagation_Log> logs = null;
        if (propagation.equals("0")) {
            logs = B_Task_Propagation_Log.findRootByTask(task);
        } else {
            B_Task_Propagation_Log parent = B_Task_Propagation_Log.findByUuid(propagation);
            if (parent == null) {
                return ajaxFail("数据错误");
            }
            logs = B_Task_Propagation_Log.findChildren(parent);
        }
        JSONArray result = new JSONArray();
        for (B_Task_Propagation_Log log : logs) {
            JSONObject object = new JSONObject();
            S_User executant = log.executant;
            object.put("userUUID", executant.uuid);
            object.put("username", executant.getShowName());
            object.put("avatar", executant.getShowAvatar());
            object.put("uuid", log.uuid);
            object.put("open", false);
            result.add(object);
        }
        return ajaxSuccess(result);
    }

    @With(UserLoginSecuredAction.class)
    public static Result getNodeUserPropagationInfo() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        String propagation = node.get("propagation").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        S_User user = currentUser();
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有查看权限");
        }
        List<B_Task_Propagation_Log> children = null;
        List<B_Task_Propagation_Log> parents = null;
        B_Task_Propagation_Log currentLog = B_Task_Propagation_Log.findByUuid(propagation);
        if (currentLog == null) {
            return ajaxFail("数据错误");
        }
        children = B_Task_Propagation_Log.findChildren(currentLog);
        parents = B_Task_Propagation_Log.findAllParent(currentLog);
        //按照Level升序排序，最顶层的父在第一个,依次往下
        Collections.sort(parents, new Comparator<B_Task_Propagation_Log>(){
            public int compare(B_Task_Propagation_Log o1, B_Task_Propagation_Log o2) {
                return o1.level - o2.level;
            }
        });
        JSONObject result = new JSONObject();
        JSONArray parentNode= new JSONArray();
        JSONArray childrenNode= new JSONArray();
        for (B_Task_Propagation_Log log : parents) {
            JSONObject object = new JSONObject();
            S_User executant = log.executant;
            object.put("userUUID", executant.uuid);
            object.put("username", executant.getShowName());
            object.put("avatar", executant.getShowAvatar());
            object.put("uuid", log.uuid);
            object.put("open", true);
            object.put("isParent", true);
            parentNode.add(object);
        }
        for (B_Task_Propagation_Log log : children) {
            JSONObject object = new JSONObject();
            S_User executant = log.executant;
            object.put("userUUID", executant.uuid);
            object.put("username", executant.getShowName());
            object.put("avatar", executant.getShowAvatar());
            object.put("uuid", log.uuid);
            object.put("open", false);
            object.put("isParent", false);
            childrenNode.add(object);
        }
        result.put("parentNode", parentNode);
        result.put("childrenNode", childrenNode);
        return ajaxSuccess(result);
    }

    @With(UserLoginSecuredAction.class)
    public static Result getStatisticalPropagationDepth() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        S_User user = currentUser();
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有查看权限");
        }
        int propagationDepth = B_Task_Propagation_Log.getPropagationDepth(task);
        JSONObject result = new JSONObject();
        result.put("propagationDepth", propagationDepth);
        return ajaxSuccess(result);
    }


    //获取当前登录用户给某一层级用户贴的所有标签
    @With(UserLoginSecuredAction.class)
    public static Result getStatisticalAllTags() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        int level = node.get("level").asInt();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        S_User currentUser = currentUser();
        if (!task.owner.id.equals(currentUser.id)) {
            return ajaxFail("您没有查看权限");
        }
        List<Integer> userIdList = B_Task_Propagation_Log.getTaskLevelUserIds(task,level);
        JSONArray result = new JSONArray();
        Set<B_User_Tag_Enum> tagSet = new HashSet<B_User_Tag_Enum>();
        for(Integer userId : userIdList) {
            S_User taggedUser = S_User.find.byId(userId);
            List<B_User_Tag> tagList = B_User_Tag.findTagListByTaggedUserAndTaggingUser(currentUser, taggedUser);
            for(B_User_Tag item : tagList) {
                B_User_Tag_Enum tagEnum = item.tag;
                tagSet.add(tagEnum);
            }
        }
        for(B_User_Tag_Enum item : tagSet) {
            JSONObject tag = new JSONObject();
            tag.put("tagId",item.id);
            tag.put("tagName",item.tag);
            result.add(tag);
        }
        return ajaxSuccess(result);
    }

    //根据查询条件查找需要打赏的用户
    @With(UserLoginSecuredAction.class)
    public static Result queryRewardUserList() {
        JsonNode node = request().body().asJson();
        JSONArray result = new JSONArray();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        S_User currentUser = currentUser();
        if (!task.owner.id.equals(currentUser.id)) {
            return ajaxFail("您没有查看权限");
        }

        String queryBodyText = node.get("queryBody").toString();
        JSONObject queryBody = JSON.parseObject(queryBodyText);
        boolean hasChildrenQuery = queryBody.getJSONObject("childrenCount").getFloat("data") == null ? false : true;
        boolean hasSaleQuery = queryBody.getJSONObject("salesCount").getFloat("data") == null ? false : true;
        boolean hasTagQuery = queryBody.getJSONArray("tagList").size() == 0 ? false : true;

        //总的存放符合条件的节点用户
        List<B_Task_Propagation_Log> targetLogList = new ArrayList<B_Task_Propagation_Log>();

        //抽取子节点数量查询条件
        if(hasChildrenQuery) {
            JSONObject childrenCount = queryBody.getJSONObject("childrenCount");
            String op = childrenCount.getString("op");
            int data = childrenCount.getIntValue("data");
            String levelName = childrenCount.getJSONObject("level").getString("name");
            int levelId = childrenCount.getJSONObject("level").getIntValue("value");
            String logicOp = childrenCount.getString("logicOp");

            //如何levelId为-1，说明选择的是查询"所有节点"，否则是查询某一层级的节点（2级子节点数量=3，要返回的是层级为1的节点,所以传递进去的level要减去1）
            //目前前端传递过来的Level>=2
            //levelId为-1的时候不能赋值为1，因为这样会和levelId为2的情况混淆
            int level = levelId == -1 ? -1 : levelId-1;
            //获取所有的一级节点或者某一层级的节点
            List<B_Task_Propagation_Log> logList = new ArrayList<B_Task_Propagation_Log>();
            if(level == -1) {
                logList = B_Task_Propagation_Log.levelChildNodes(task,1);
            } else {
                logList = B_Task_Propagation_Log.levelChildNodes(task,level);
            }
            for(B_Task_Propagation_Log log : logList) {
                int numValue = 0;
                if(level == -1) {//只有查询所有节点时，才会去统计所有的节点数量
                    //查看一级节点的所有子节点数量（迭代查询所有子节点）
                    numValue = log.allChildNodeNum();
                } else {
                    //查询当前节点的子节点数（只查询一层,也就是当前层级节点的直属子节点,不查询迭代查询）
                    numValue = log.numberOfChildNodes();
                }
                switch(op) {
                    case "eq" : {
                        if(numValue == data ) {
                            targetLogList.add(log);
                        }
                        break;
                    }
                    case "lt" : {
                        if(numValue < data ) {
                            targetLogList.add(log);
                        }
                        break;
                    }
                    case "le" : {
                        if(numValue <= data ) {
                            targetLogList.add(log);
                        }
                        break;
                    }
                    case "gt" : {
                        if(numValue > data ) {
                            targetLogList.add(log);
                        }
                        break;
                    }
                    case "ge" : {
                        if(numValue >= data ) {
                            targetLogList.add(log);
                        }
                    }
                    case "ne" : {
                        if(numValue != data ) {
                            targetLogList.add(log);
                        }
                        break;
                    }
                }
            }
        }

        //抽取销售量查询条件
        if(hasSaleQuery) {
            JSONObject salesCount = queryBody.getJSONObject("salesCount");
            String op = salesCount.getString("op");
            int data = salesCount.getIntValue("data");
            String levelName = salesCount.getJSONObject("level").getString("name");
            int levelId = salesCount.getJSONObject("level").getIntValue("value");
            String logicOp = salesCount.getString("logicOp");
        }

        //抽取标签查询条件
        if(hasTagQuery) {
            JSONArray tagList = queryBody.getJSONArray("tagList");
            //先把第一个标签查询里的level值作为最小值
            //有多个tag查询条件的话,我们需要知道level的最小值，因为最小值对应的那层节点是我们的目标节点
            int levelMin = tagList.getJSONObject(0).getJSONObject("level").getIntValue("value");
            //重新包装一下tag查询条件，方便后台使用
            JSONArray tagQueryList = new JSONArray();
            for(int i=0;i<tagList.size();i++){
                String levelName = tagList.getJSONObject(i).getJSONObject("level").getString("name");
                int levelId = tagList.getJSONObject(i).getJSONObject("level").getIntValue("value");
                //有多个标签查询条件的话，获取里面的最小层级，最小层级的节点是我们查询的目标对象
                if(levelId <= levelMin) {
                    levelMin = levelId;
                }
                int tagId = tagList.getJSONObject(i).getJSONObject("tag").getIntValue("value");
                String tagName = tagList.getJSONObject(i).getJSONObject("tag").getString("name");
                int data =  tagList.getJSONObject(i).getIntValue("data");
                String logicOp = tagList.getJSONObject(i).getString("logicOp");
                JSONObject childTagQuery = new JSONObject();
                childTagQuery.put("levelId", levelId);
                childTagQuery.put("levelName", levelName);
                childTagQuery.put("data", data);
                childTagQuery.put("tagId", tagId);
                childTagQuery.put("tagName", tagName);
                tagQueryList.add(childTagQuery);
                //标签之间目前都是And连接，先不把逻辑操作符放进来
                //tagQueryList.add(logicOp);
            }
            //存放在标签查询模块查询出来的结果
            List<B_Task_Propagation_Log> tagQueryTargetLogList = new ArrayList<B_Task_Propagation_Log>();
            boolean hasOtherQuery = targetLogList.size() == 0 ? false : true;
            if(levelMin == -1) {
                //如果是查询"所有节点"，那么目标节点就是所有的一级节点(level=1)
                List<B_Task_Propagation_Log> logList = B_Task_Propagation_Log.levelChildNodes(task,1);
                tagQueryTargetLogList = B_Task_Propagation_Log.getTagQueryLogList(true,logList,tagQueryList,currentUser);
            } else {
                //如果是多个查询条件，那么目标节点就是level最小的那一级节点(level=levelMin)
                List<B_Task_Propagation_Log> logList = B_Task_Propagation_Log.levelChildNodes(task,levelMin);
                tagQueryTargetLogList = B_Task_Propagation_Log.getTagQueryLogList(false,logList,tagQueryList,currentUser);
            }
            //下面根据前两个查询条件，来和当前的标签查询做并集或者交集
            if(hasOtherQuery) {
                //在标签查询之前，前面的子节点或者销售量查询也有查询结果
                //这种情况下，把标签查询的结果和之前的结果做交集
                targetLogList.retainAll(tagQueryTargetLogList);
            } else {
                //在标签查询之前没有子节点查询和销售量查询或者是前面的查询结果为空
                //这种情况下，把标签查询的结果和之前的结果做并集
                targetLogList.addAll(tagQueryTargetLogList);
            }
        }


        //包装前面三层筛选出来的节点数据，返回前端
        for(B_Task_Propagation_Log log : targetLogList) {
            JSONObject object = new JSONObject();
            S_User executant = log.executant;
            object.put("userUUID", executant.uuid);
            object.put("username", executant.getShowName());
            object.put("avatar", executant.getShowAvatar());
            object.put("uuid", log.uuid);
            result.add(object);
        }

        return ajaxSuccess(result);
    }


    //************************************************************
    //**************************promotion相关*********************
    //************************************************************
    //获取我的地推任务列表
    @With(UserLoginSecuredAction.class)
    public static Result getPromotionTaskOwnerList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String queryType = node.get("queryType").asText();

        PagingList<B_Task> pagingList = null;
        if ("search".equals(queryType)) {
            String search_taskName = node.get("search_taskName").asText();
            ExpressionList expressionList = B_Task.find.fetch("promotion").where().eq("owner", currentUser()).eq("deleteLogic", false).eq("taskType",B_Task.TaskType.promotion);

            if (!search_taskName.equals("null")) {
                expressionList = expressionList.like("title", "%" + search_taskName + "%");
            }
            pagingList = expressionList.order("createdTime desc").findPagingList(pageSize);
        }else {
            pagingList = B_Task.find.fetch("promotion").where().eq("owner", currentUser()).eq("deleteLogic", false).eq("taskType",B_Task.TaskType.promotion).order("createdTime desc").findPagingList(pageSize);
        }

        Page<B_Task> page = pagingList.getPage(pageNum - 1);
        List<B_Task> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("editedTime", info.editedTime);
            object.put("title", info.title);
            object.put("taskType", info.taskType);
            object.put("promotionProcess", info.promotion==null?"":info.promotion.promotionProcess);
            object.put("endTime", info.endTime);
            object.put("description", info.description==null?"":info.description);
            object.put("coin", info.strategyInfo.coin);
            object.put("hasCost", info.strategyInfo.hasAllocationCoin);
            object.put("state", info.state);
            object.put("shareUrl", info.getStartUrl());
            object.put("isShowUserInput", info.viewTag.isShowUserInput);
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }

    //任务分配给地推人员
    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result bindGroundPromoter() throws Exception {
        JsonNode node = request().body().asJson();

        int taskId = node.get("taskId").asInt();
        int groundPromoterUserId = node.get("groundPromoterUserId").asInt();

        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("不存在此任务");
        }

        S_User currentUser = currentUser();
        if (!task.owner.id.equals(currentUser.id)) {
            return ajaxFail("您没有该任务的操作权限");
        }

        S_User groundPromoterUser=S_User.find.byId(groundPromoterUserId);

        B_Task_Promotion taskPromotion=task.promotion;
        if(taskPromotion==null){
            return ajaxFail("任务不是地推任务或者任务尚未添加地推信息");
        }
        if(taskPromotion.promotionProcess!= B_Task_Promotion.Process.init){
            return ajaxFail("任务已分配");
        }
        taskPromotion.groundPromoter=groundPromoterUser;
        taskPromotion.promotionProcess= B_Task_Promotion.Process.groundPromoterBind;
        taskPromotion.saveOrUpdate();
        return ajaxSuccess("分配成功");

    }

    //取消任务分配
    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result cancelBindGroundPromoter() throws Exception {
        JsonNode node = request().body().asJson();

        int taskId = node.get("taskId").asInt();

        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("不存在此任务");
        }

        S_User currentUser = currentUser();
        if (!task.owner.id.equals(currentUser.id)) {
            return ajaxFail("您没有该任务的操作权限");
        }


        B_Task_Promotion taskPromotion=task.promotion;
        if(taskPromotion==null){
            return ajaxFail("任务不是地推任务或者任务尚未添加地推信息");
        }
        if(taskPromotion.promotionProcess!= B_Task_Promotion.Process.groundPromoterBind){
            return ajaxFail("任务不在可取消绑定地推人员的阶段");
        }
        taskPromotion.groundPromoter=null;
        taskPromotion.promotionProcess= B_Task_Promotion.Process.init;
        taskPromotion.saveOrUpdate();
        return ajaxSuccess("取消绑定成功");

    }

    //确认地推任务信息设置完成
    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result setPromotionComplete() throws Exception {
        JsonNode node = request().body().asJson();

        int taskId = node.get("taskId").asInt();

        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("不存在此任务");
        }

        S_User currentUser = currentUser();
        if (!task.owner.id.equals(currentUser.id)) {
            return ajaxFail("您没有该任务的操作权限");
        }

        B_Task_Promotion taskPromotion=task.promotion;
        if(taskPromotion==null){
            return ajaxFail("任务不是地推任务或者任务尚未添加地推信息");
        }
        if(taskPromotion.promotionProcess!= B_Task_Promotion.Process.promoterBind){
            return ajaxFail("任务信息尚未填写完全");
        }

        taskPromotion.promotionProcess= B_Task_Promotion.Process.complete;
        taskPromotion.saveOrUpdate();
        return ajaxSuccess("确认成功");

    }


    //获取地推任务信息
    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result getPromotionInfo() throws Exception {
        JsonNode node = request().body().asJson();

        int taskId = node.get("taskId").asInt();

        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("不存在此任务");
        }

        S_User currentUser = currentUser();
        if (!task.owner.id.equals(currentUser.id)) {
            return ajaxFail("您没有该任务的操作权限");
        }

        B_Task_Promotion taskPromotion=task.promotion;
        if(taskPromotion==null){
            return ajaxFail("任务不是地推任务或者任务尚未添加地推信息");
        }

        JSONObject result = new JSONObject();
        result.put("groundPromoterUserName", taskPromotion.groundPromoter==null?"尚未分配":taskPromotion.groundPromoter.getShowName());
        result.put("groundPromoterAvatar", taskPromotion.groundPromoter==null?"尚未分配":taskPromotion.groundPromoter.getShowAvatar());
        result.put("promoterUserName", taskPromotion.promoter==null?"尚未绑定":taskPromotion.promoter.getShowName());
        result.put("promoterAvatar", taskPromotion.promoter==null?"尚未绑定":taskPromotion.promoter.getShowAvatar());

        result.put("promoterName", taskPromotion.promoterName);
        result.put("promoterLocation", taskPromotion.promoterLocation);
        result.put("promoterContactName", taskPromotion.promoterContactName);
        result.put("promoterPhone", taskPromotion.promoterPhone);
        result.put("promoterLatitude", taskPromotion.promoterLatitude);
        result.put("promoterLongitude", taskPromotion.promoterLongitude);

        return ajaxSuccess(result);

    }


    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result rewardNodeUser() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        int coin = node.get("coin").asInt();
        String rewardDesc = node.get("desc").asText();
        String rewardUserUUID = node.get("rewardUserUUID").asText();
        B_Task task = B_Task.findByUuid(uuid);
        S_User rewardUser = S_User.findByUuid(rewardUserUUID);
        S_User currentUser = currentUser();
        if (coin < 100) {
            return ajaxFail("打赏金额必须大于1元");
        }
        if (coin > currentUser.account.coin) {
            JSONObject needMoreMoney = new JSONObject();
            needMoreMoney.put("msg","账户余额不足,请充值！");
            needMoreMoney.put("errorType","NotEnoughMoney");
            return ajaxFail(needMoreMoney);
        }

        //每个账号每天最多打赏1000元,需要计算当天的打赏总数
        int daShangTodayTotal = 0;
        int daShangMaxValue = (int) Common.getIntConfig("daShangMaxValue") * 100;
        List<B_User_Coin_Log> userCoinLogList = B_User_Coin_Log.find.where().eq("owner", currentUser).eq("type",B_User_Coin_Log.Type.out).findList();
        for(B_User_Coin_Log log : userCoinLogList) {
            if(log.outLog != null && log.outLog.type == B_User_Coin_Out_Log.Type.DaShang) {
                SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
                if(fmt.format(log.outLog.createdTime).equals(fmt.format(new Date()))) {//只算今天的打赏金额数
                    daShangTodayTotal += log.coin;
                }
            }
        }
        System.out.println("TodayDaShangTotal =" + daShangTodayTotal);
        if(daShangTodayTotal >= daShangMaxValue) {
            return ajaxFail("每天打赏总额上限是" + daShangMaxValue/100 + "元，请明天再打赏！");
        }

        if (rewardUser.wechatInfo == null || rewardUser.wechatInfo.appOpenid == null) {
            return ajaxFail("打赏对象缺少微信信息，无法打赏");
        }

        String nonce_str = Common.randomString(32).toUpperCase();
        String partner_trade_no = B_Wechat_Withdraw.getOnePartnerTradeNo();
        String openid = rewardUser.wechatInfo.appOpenid;
        int amount = coin;
        String spbill_create_ip = request().remoteAddress();
//        String desc = "唯页平台";
        String desc = rewardDesc;
        currentUser.account.reduceCoin(coin);

        B_Wechat_Withdraw withdraw = B_Wechat_Withdraw.create(currentUser, nonce_str, partner_trade_no, openid, amount, desc);

        WithdrawDepositResult result = WithDrawDepositManage.transfers(nonce_str, partner_trade_no, openid, amount, desc, spbill_create_ip);

        if (result.isSuccess()) {
            withdraw.success(result.payment_no, result.payment_time);
            //打赏者新增一条Out记录，类型是打赏
            B_User_Coin_Log.create_wechatReward(currentUser, amount);
            //被打赏者新增一条In记录，类型也是打赏
            B_User_Coin_Log.create_wechatRewarded(rewardUser, amount);
            //被打赏者新增一条Out记录，类型是提现（直接把钱打赏到微信里，相当于用户自己从平台提现了一次）
            B_User_Coin_Log.create_wechatRewardedWithDraw(rewardUser, amount);
            return ajaxSuccess("打赏成功");
        } else {
            withdraw.fail(result.getErrorMsg());
            currentUser.account.addCoin(coin);
            return ajaxFail(result.getErrorMsg());
        }
    }

}
