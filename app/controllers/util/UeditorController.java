package controllers.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import controllers.BaseController;
import models.db1.B_Upload_Img;
import play.mvc.Http;
import play.mvc.Result;
import services.ALiYunService;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Administrator on 2015/6/19.
 */
public class UeditorController extends BaseController {

    //后端配置参数设置
    public static Result ueditorServer() {
        JSONObject object = new JSONObject();
        object.put("imageActionName", "uploadimage");
        JSONArray AllowFiles = new JSONArray();
        AllowFiles.add(".png");
        AllowFiles.add(".jpg");
        object.put("imageAllowFiles", AllowFiles);
        object.put("imageFieldName", "upfile");
        object.put("imageMaxSize", 102400);
        object.put("imageUrlPrefix", "");
        object.put("imagePathFormat", "/ueditor/jsp/upload/image/{yyyy}{mm}{dd}/{time}{rand:6}");
        return ok(object.toJSONString());
    }

    //上传处理函数
    public static Result ueditorUpload() throws IOException {
        //图片上传
        if (getParam("action").equals("uploadimage")) {
            Http.MultipartFormData formBody = request().body().asMultipartFormData();
            Http.MultipartFormData.FilePart filePart = formBody.getFile("upfile");
            String url;

            B_Upload_Img img = B_Upload_Img.findByFile(filePart.getFile());
            if (img != null) {
                url = img.url;
            } else {
                String key = new Date().getTime() + "_" + filePart.getFilename();
                ALiYunService.UploadResult result = ALiYunService.upload(null, "article/content/", filePart.getFile(), key);
                url = result.url;
                B_Upload_Img.create(filePart.getFile(), url, currentUser());
            }

            JSONObject object = new JSONObject();
            object.put("url", url);
            object.put("title", "唯页图片");
            object.put("state", "SUCCESS");
            return ok(object.toJSONString());
        }
        return ok("");
    }

    public static Result ueditorUploadFile() {
        return ok();
    }
}
