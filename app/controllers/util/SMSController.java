package controllers.util;

import beans.util.sms.SendSmsResult;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.BaseController;
import models.db1.user.S_User;
import org.apache.commons.lang3.time.DateUtils;
import play.mvc.Result;
import play.mvc.With;
import security.UserLoginSecuredAction;
import util.Common;
import services.LuosimaoService;
import util.cache.CacheUtil;

import java.util.Calendar;


/**
 * Created by Administrator on 2014/9/3.
 */
public class SMSController extends BaseController {

    public static Result getSMS() throws Exception {
        JsonNode node = request().body().asJson();
        String tag = node.get("tag").asText();
        S_User user = currentUser();

        String cacheTag = null;
        if ("regist".equals(tag)) {
            cacheTag = node.get("phone").asText();
        } else {
            if (currentUser() == null) {
                return ajaxSuccess("请先登录");
            }
            cacheTag = String.valueOf(user.id);
        }


        final String cahceSendTimeStr = "sendTime_" + tag + "_" + cacheTag;
        final String cahceSendCountStr = "sendCount_" + tag + "_" + cacheTag;
        int maxCount = Common.getIntConfig("service.sendSMSCount", 5);
        String cacheSendTimeStr_result = (String) CacheUtil.get(cahceSendTimeStr);
        Integer count = CacheUtil.get(cahceSendCountStr) == null ? 0 : Integer.valueOf((String) CacheUtil.get(cahceSendCountStr));
        if (cacheSendTimeStr_result != null) {
//            System.out.println("60s内不能重发");
            return ajaxFail("60s内不能重发");
        }
        if (count >= maxCount) {
//            System.out.println("你已超过每天限额发送" + maxCount + "次短信，请明日再试");
            return ajaxFail("你已超过每天限额发送" + maxCount + "次短信，请明日再试");
        }

        String phone = null;
        if ("checkOldTelephone".equals(tag)) {
            phone = user.telephone;
            if (phone == null) {
                return ajaxFail("没有旧的电话号码信息");
            }
        } else if ("changeTelephone".equals(tag)) {
            phone = node.get("phone").asText();
            if (phone.equals("null")) {
                return ajaxFail("请输入电话号码");
            }
        } else if ("regist".equals(tag)) {
            phone = node.get("phone").asText();
            if (phone.equals("null")) {
                return ajaxFail("请输入电话号码");
            }
            if (S_User.findByTelephone(phone) != null) {
                return ajaxFail("电话号码已注册，请重新输入一个");
            }
        } else if ("pcWithdrawDeposit".equals(tag)) {
            phone = user.telephone;
            if (phone == null) {
                return ajaxFail("请先晚上您的电话号码信息");
            }
        }

        String securityCode = Common.randomStringForNum(6);
        String msg = "验证码:" + securityCode + "【唯页】";
        SendSmsResult result = LuosimaoService.SendSMSTimely(phone, msg);

        if (result.isSuccess()) {
            long milliSecondsLeftToday = 86400000 -
                    DateUtils.getFragmentInMilliseconds(Calendar.getInstance(), Calendar.DATE);
            CacheUtil.set(cahceSendTimeStr, "true", 60);
            CacheUtil.set(cahceSendCountStr, String.valueOf(count + 1), ((Long) (milliSecondsLeftToday / 1000)).intValue());
//            session("taskUserInputSms_" + taskId, securityCode);

            if ("checkOldTelephone".equals(tag)) {
                session("checkOldTelephone", securityCode);
            } else if ("changeTelephone".equals(tag)) {
                session("changeTelephoneCheckCode_" + phone, securityCode);
            } else if ("regist".equals(tag)) {
                session("registCheckCode_" + phone, securityCode);
            } else if ("pcWithdrawDeposit".equals(tag)) {
                session("pcWithdrawDepositCheckCode_" + phone, securityCode);
            }

            return ajaxSuccess("发送成功");
        } else {
            return ajaxFail(result.getErrorMsg());
        }
    }


    @With(UserLoginSecuredAction.class)
    public static Result sendSms_TaskUserInput() throws Exception {
        String phone = getPostParam("phone");
        int taskId = getIntPostParam("taskId");
        String securityCode = Common.randomStringForNum(6);

        S_User user = currentUser();
        final String cahceSendTimeStr = "sendTime" + "sendSms_TaskUserInput" + user.id;
        final String cahceSendCountStr = "sendCount" + "sendSms_TaskUserInput" + user.id;
        int maxCount = Common.getIntConfig("service.sendSMSCount", 5);
        String cacheSendTimeStr_result = (String) CacheUtil.get(cahceSendTimeStr);
        Integer count = CacheUtil.get(cahceSendCountStr) == null ? 0 : Integer.valueOf((String) CacheUtil.get(cahceSendCountStr));
        if (cacheSendTimeStr_result != null) {
//            System.out.println("60s内不能重发");
            return ajaxFail("60s内不能重发");
        }
        if (count >= maxCount) {
//            System.out.println("你已超过每天限额发送" + maxCount + "次短信，请明日再试");
            return ajaxFail("你已超过每天限额发送" + maxCount + "次短信，请明日再试");
        }

        String msg = "验证码:" + securityCode + "【唯页】";
        SendSmsResult result = LuosimaoService.SendSMSTimely(phone, msg);
        if (result.isSuccess()) {
            long milliSecondsLeftToday = 86400000 -
                    DateUtils.getFragmentInMilliseconds(Calendar.getInstance(), Calendar.DATE);
            CacheUtil.set(cahceSendTimeStr, "true", 60);
            CacheUtil.set(cahceSendCountStr, String.valueOf(count + 1), ((Long) (milliSecondsLeftToday / 1000)).intValue());
            session("taskUserInputSms_" + phone + "_" + taskId, securityCode);
            return ajaxSuccess("发送成功");
        } else {
            return ajaxFail(result.getErrorMsg());
        }

    }
}
