package controllers.util;

import util.Common;

/**
 * Created by Administrator on 2016/12/2.
 */
public class UrlController {
    public static String serverAddress = Common.getStringConfig("server.address");
    public static String aliyunOssAddress = Common.getStringConfig("aliyun.oss.address");

    public static String logoMiniImg = aliyunOssAddress+"/system/images/logo_min.png";


    //第三方平台
    public static String WechatAppComponentLoginPageRedirectUrl = serverAddress+ "/wechat/thirdPlatform/component/login/page/redirect".trim();
    public static String WechatAppAfterCreateRedirectUrl = serverAddress+"/#/wechat/list".trim();
}
