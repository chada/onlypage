package controllers.util;


import beans.task.TaskConsumer;
import beans.task.TestConsumer;
import beans.task.strategy.StrategyExecuteResult;
import beans.task.strategy.DefaultStrategyOne;
import beans.task.strategy.StrategyTriggerEvent;
import beans.task.view.PublishViewInfo;
import beans.wechat.JsShareInfo;
import com.fasterxml.jackson.databind.JsonNode;

import controllers.BaseController;
import controllers.util.routes;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Log;


import models.db1.task.schedule.B_Task_User_Statistics_Data;
import models.db1.task.strategy.B_Task_Strategy;
import models.db1.task.strategy.B_Task_Strategy_Execute_Log;
import models.db1.task.strategy.B_Task_Strategy_Rule;

import models.db1.user.S_User;
import models.db1.user.coin.B_User_Coin_In_Log;
import models.db1.user.coin.B_User_Coin_Log;
import models.db1.wechat.B_Wechat_Withdraw;
import play.mvc.Result;
import util.Common;
import wechat.pay.WithDrawDepositManage;
import wechat.pay.beans.withdrawDeposit.WithdrawDepositResult;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Administrator on 2015/6/19.
 */
public class TestController extends BaseController {

    public static Result test(String tag) throws IOException {
        Boolean develop = Common.getBooleanConfig("develop", false);
        if (develop) {
            if (tag.equals("addTestUser")) {
                addTestUser();
            } else if (tag.equals("addPropagationList")) {
                addPropagationList();
            } else if (tag.equals("taskAccept")) {
                taskAccept();
            } else if (tag.equals("taskRenew")) {
                taskRenew();
            } else if (tag.equals("taskPage")) {
                return redirect(controllers.util.routes.TestController.taskPage());
            } else if (tag.equals("logCheck")) {
                logCheck();
            } else if (tag.equals("test")) {
                test();
            }
        }
        return ok(views.html.util.other.operate_success.render());
    }


    public static Result addUser() throws IOException {
        for (int i = 0; i < 500; i++) {
            S_User.regist("测试用户" + i, "12345678", "1111111111" + i);
        }
        return ajaxSuccess("增加用户成功");
    }


    public static Result taskTest() throws IOException {
        B_Task task = B_Task.find.byId(574);
        List<S_User> userList = new ArrayList<>();
        for (int i = 0; i < 500; i++) {
            S_User user = S_User.find.byId(i);
            if (user == null) continue;
            userList.add(user);
        }

        for (int i = 0; i < userList.size(); i++) {
            S_User user = userList.get(i);
            B_Task_Propagation_Log.create(task, user, user, 1);
            B_Task_User_Statistics_Data.create(user, task);


        }

        for (int i = 0; i < userList.size(); i++) {
            S_User user = userList.get(i);
            if (task.isRunning()) {
                StrategyTriggerEvent strategyTriggerEvent = new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.enterpage);
                pool.execute(new TaskConsumer(task.id, strategyTriggerEvent, user, request().remoteAddress()));
            }
        }
        return ajaxSuccess("测试任务完成");
    }


    public static ExecutorService pool = Executors.newFixedThreadPool(20);

    public static Result taskPage() throws Exception {
        B_Task task = B_Task.find.byId(619);
        List<B_Task_Propagation_Log> propagationLogList = B_Task_Propagation_Log.find.where().eq("task", task).findList().subList(0, 13500);
        System.out.println(propagationLogList.size());
        List<B_Task_Strategy_Execute_Log> executeLogList = B_Task_Strategy_Execute_Log.find.where().eq("task", task).findList();
        System.out.println(executeLogList.size());
        List<Integer> ids = new ArrayList<>();
        for (B_Task_Strategy_Execute_Log executeLog : executeLogList) {
            ids.add(executeLog.beneficialUser.id);
        }
        int i = 0;
        for (B_Task_Propagation_Log propagationLog : propagationLogList) {
            if (ids.contains(propagationLog.executant.id)) {

            } else {
                System.out.println(propagationLog.executant.id);
                i++;
                pool.execute(new TestConsumer(propagationLog, propagationLog.executant, task, request().remoteAddress()));
            }
        }
        System.out.println(i);
        return ok(String.valueOf(i));
//        return redirect(controllers.api.routes.TaskController.operate(0, 6));
    }

    public static void test() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("当前日期：               " + sf.format(c.getTime()));
        c.add(Calendar.DAY_OF_MONTH, 15);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        System.out.println("增加一天后日期 ：  " + sf.format(c.getTime()));
    }

    public static void taskRenew() {

    }


    public static void logCheck() {
        List<B_User_Coin_Log> logList = B_User_Coin_Log.find.fetch("inLog").fetch("outLog").where().eq("type", B_User_Coin_Log.Type.in).isNotNull("inLog").eq("inLog.type", B_User_Coin_In_Log.Type.TaskRenew).setMaxRows(3).findList();
        System.out.println(logList.size());
    }


    public static void addTestUser() {
        if (S_User.find.where().findRowCount() < 100) {
            for (int i = 0; i < 100; i++) {
                S_User.regist("测试用户" + i, "12345678", "1111111111" + i);
            }
        }
    }

    public static void addPropagationList() {
        S_User user = S_User.find.byId(1);
        S_User user1 = S_User.findByUsername("测试用户1");
        S_User user2 = S_User.findByUsername("测试用户2");
        S_User user3 = S_User.findByUsername("测试用户3");
        S_User user4 = S_User.findByUsername("测试用户4");
        S_User user5 = S_User.findByUsername("测试用户5");
        S_User user6 = S_User.findByUsername("测试用户6");
        S_User user7 = S_User.findByUsername("测试用户7");
        S_User user8 = S_User.findByUsername("测试用户8");
        S_User user9 = S_User.findByUsername("测试用户9");

        B_Task task = B_Task.find.byId(1);
        B_Task_Propagation_Log.create(task, user, user1, 2);
        B_Task_Propagation_Log.create(task, user1, user2, 3);
        B_Task_Propagation_Log.create(task, user, user3, 2);
        B_Task_Propagation_Log.create(task, user1, user4, 3);
        B_Task_Propagation_Log.create(task, user1, user5, 3);
        B_Task_Propagation_Log.create(task, user1, user6, 3);
        B_Task_Propagation_Log.create(task, user6, user7, 4);

        B_Task_Propagation_Log.create(task, user8, user8, 1);
        B_Task_Propagation_Log.create(task, user8, user9, 2);
    }


    public static void taskAccept() {
        B_Task task = B_Task.find.byId(1);
        S_User user = S_User.find.byId(1);
        S_User user1 = S_User.findByUsername("测试用户1");
        S_User user2 = S_User.findByUsername("测试用户2");
        S_User user3 = S_User.findByUsername("测试用户3");
        S_User user4 = S_User.findByUsername("测试用户4");
        S_User user5 = S_User.findByUsername("测试用户5");
        S_User user6 = S_User.findByUsername("测试用户6");
        S_User user7 = S_User.findByUsername("测试用户7");
        S_User user8 = S_User.findByUsername("测试用户8");
        S_User user9 = S_User.findByUsername("测试用户9");

//        B_Task_Propagation_Log.create(task, user, user, 1);
//        B_Task_User_Statistics_Data.create(user, task);
//        task.checkout(user, new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.enterpage), request());
//
//        B_Task_Propagation_Log.create(task, user, user2, 2);
//        B_Task_User_Statistics_Data.create(user2, task);
//        task.checkout(user2, new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.enterpage), request());
//
//        B_Task_Propagation_Log.create(task, user, user3, 2);
//        B_Task_User_Statistics_Data.create(user3, task);
//        task.checkout(user3, new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.enterpage), request());
//
//        B_Task_Propagation_Log.create(task, user, user4, 2);
//        B_Task_User_Statistics_Data.create(user4, task);
//        task.checkout(user4, new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.enterpage), request());

    }


    public static Result strategyTestEntrance() throws Exception {
        JsonNode node = request().body().asJson();
        String strategyTestType = node.get("strategyTestType").asText();

        switch (strategyTestType) {
            case "createDefaultStrategyOne": {
                Integer startTriggerNodeLevel = node.get("startTriggerNodeLevel").asInt();
                Integer endTriggerNodeLevel = node.get("endTriggerNodeLevel") == null ? null : node.get("endTriggerNodeLevel").asInt();
                String rewardTypeString = node.get("rewardType").asText();
                B_Task_Strategy_Rule.RewardType rewardType = B_Task_Strategy_Rule.stringConvertRewardType(rewardTypeString);
                if (rewardType == null) {
                    return ajaxFail("rewardType错误");
                }
                Integer fixedRewardAmount = node.get("fixedRewardAmount") == null ? null : node.get("fixedRewardAmount").asInt();
                Integer minRewardAmount = node.get("minRewardAmount") == null ? null : node.get("minRewardAmount").asInt();
                Integer maxRewardAmount = node.get("maxRewardAmount") == null ? null : node.get("maxRewardAmount").asInt();

                DefaultStrategyOne.createDefaultStrategyOne(startTriggerNodeLevel, endTriggerNodeLevel, rewardType, fixedRewardAmount, minRewardAmount, maxRewardAmount);

                return ajaxSuccess("添加成功");
            }
            case "triggerEventSimulation": {
                Integer userId = node.get("userId").asInt();
                Integer taskId = node.get("taskId").asInt();
                Integer strategyId = node.get("strategyId").asInt();
                String triggerEventString = node.get("triggerEvent").asText();
                B_Task_Strategy_Rule.TriggerEvent triggerEvent = B_Task_Strategy_Rule.stringConvertTriggerEvent(triggerEventString);
                Integer readTime = node.get("readTime") == null ? null : node.get("readTime").asInt();
                StrategyTriggerEvent strategyTriggerEvent = new StrategyTriggerEvent(triggerEvent, readTime);


                S_User user = S_User.find.where().eq("id", userId).findUnique();
                B_Task task = B_Task.find.where().eq("id", taskId).findUnique();
                B_Task_Strategy strategy = B_Task_Strategy.find.where().eq("id", strategyId).findUnique();
                List<StrategyExecuteResult> strategyExecuteResultList = strategy.executeStrategy(task, user, strategyTriggerEvent);
                for (StrategyExecuteResult strategyExecuteResult : strategyExecuteResultList) {
                    //策略标记
                    B_Task_Strategy.taskStrategyExecuteLogVerifyExistAndCreate(strategyExecuteResult.beneficialUser, strategyExecuteResult.giveBenefitUser, task, strategyExecuteResult.effectiveRule, strategyExecuteResult.rewardAmount);
                }
                return ajaxSuccess(strategyExecuteResultList);
            }
            case "strategyCopy": {
                String uuid = node.get("uuid").asText();
                Integer userId = node.get("userId").asInt();
                S_User owner = S_User.find.where().eq("id", userId).findUnique();
                if (owner == null) {
                    return ajaxFail("用户不存在");
                }
//                B_Task_Strategy taskStrategy=B_Task_Strategy.copyByUuid(uuid,owner);

//                return ajaxSuccess(taskStrategy);
            }
        }

        return ajaxSuccess("");
    }


}
