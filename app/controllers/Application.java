package controllers;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Query;
import models.db1.B_Upload_Img;
import models.db1.user.B_User_Account;
import models.db1.user.B_User_Wechat_Info;
import models.db1.user.S_User;
import org.apache.commons.codec.digest.DigestUtils;
import play.mvc.Http;
import play.mvc.Result;
import redis.clients.jedis.Jedis;
import services.ALiYunService;
import util.Common;
import views.html.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;


public class Application extends BaseController {
    public static Result pageIndex() throws Exception {
        if (develop) {
            loginAsUser(S_User.find.byId(1));
        }
//        response().setHeader("Access-Control-Allow-Origin", "*");
        if (Common.isWeixinVisit(request())) {
            return redirect(controllers.mobile.routes.SiteController.pageIndex());
        }
        if (Common.isIeVisit(request()) && Common.isIeVisitLess8(request())) {
            return controllers.SiteController.error("请使用ie8版本以上浏览器访问");
        }
        return ok(front_layout.render());
    }


    public static Result uploadImg() throws IOException {

        Http.MultipartFormData formBody = request().body().asMultipartFormData();
        JSONObject data = JSON.parseObject(formBody.asFormUrlEncoded().get("data")[0]);
        String tag = data.getString("tag");
        String FolderName;
        if ("taskCoverImg".equals(tag)) {
            FolderName = "task/coverimg/";
        } else if ("user".equals(tag)) {
            FolderName = "user/avatar/";
        } else {
            return ajaxFail("上传错误");
        }

        String url;
        Http.MultipartFormData.FilePart filePart = formBody.getFile("file");
        B_Upload_Img img = B_Upload_Img.findByFile(filePart.getFile());
        if (img != null) {
            url = img.url;
        } else {
//            String key = new Date().getTime() + "_" + filePart.getFilename();
            String key = new Date().getTime() + "_" + DigestUtils.md5Hex(new FileInputStream(filePart.getFile()));
            ALiYunService.UploadResult result = ALiYunService.upload(null, FolderName, filePart.getFile(), key);
            if (!result.isSuccess()) {
                return ajaxFail(result.errorMsg);
            }
            url = result.url;
            B_Upload_Img.create(filePart.getFile(), url, currentUser());
        }
        return ajaxSuccess(url);
    }

}
