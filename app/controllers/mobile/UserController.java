package controllers.mobile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.BaseController;
import models.db1.task.B_Task;
import models.db1.user.S_User;
import models.db1.user.coin.B_User_Coin_In_Log;
import play.mvc.Call;
import play.mvc.Result;
import play.mvc.With;
import security.UserLoginSecuredAction;
import util.Common;

import java.util.List;


/**
 * Created by Administrator on 2016/12/19.
 */
public class UserController extends BaseController {

    @With(UserLoginSecuredAction.class)
    public static Result getUserInfo() throws Exception {
        S_User user = currentUser();
        JSONObject result = new JSONObject();
        result.put("username", user.getShowName());
        result.put("id", user.id);
        result.put("avatar", user.getShowAvatar());
        return ajaxSuccess(result);

    }

    //用户统计信息
    @With(UserLoginSecuredAction.class)
    public static Result getUserStatisticInfo() throws Exception {
        //我参与的任务数量（从中获取利益）
        Integer userCoinLogCount = B_User_Coin_In_Log.find.fetch("log").where().eq("type", B_User_Coin_In_Log.Type.TaskAcquisition).eq("log.owner", currentUser()).eq("deleteLogic", false).findRowCount();

        //我发布的任务数量
        Integer myTaskCount = B_Task.find.where().eq("owner", currentUser()).eq("deleteLogic", false).findRowCount();

        //我的任务获益金额
        int gainCoinCount=0;
        List<B_User_Coin_In_Log> coinInLogList= B_User_Coin_In_Log.find.fetch("log").where().eq("type", B_User_Coin_In_Log.Type.TaskAcquisition).eq("log.owner", currentUser()).eq("deleteLogic", false).findList();
        for(B_User_Coin_In_Log coinInLog:coinInLogList){
            gainCoinCount+=coinInLog.log.coin;
        }


        JSONObject statisticInfo = new JSONObject();
        statisticInfo.put("gainTaskCount", userCoinLogCount);
        statisticInfo.put("myTaskCount", myTaskCount);
        statisticInfo.put("gainCoinCount", gainCoinCount);


        return ajaxSuccess(statisticInfo);
    }
}
