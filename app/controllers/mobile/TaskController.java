package controllers.mobile;

import beans.task.strategy.DefaultStrategyOne;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.BaseController;
import controllers.util.UrlController;
import models.db1.article.B_Article;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Info;
import models.db1.task.B_Task_Propagation_Log;
import models.db1.task.promotion.B_Task_Promotion;
import models.db1.task.strategy.B_Task_Strategy;
import models.db1.task.strategy.B_Task_Strategy_Info;
import models.db1.task.view.B_Task_View_Tag;
import models.db1.user.S_User;
import models.db1.user.coin.B_User_Coin_In_Log;
import models.db1.wechat.B_Wechat_Pay;
import play.db.ebean.Transactional;
import play.mvc.Result;
import play.mvc.With;
import security.UserLoginSecuredAction;
import util.Common;
import wechat.pay.Manage;
import wechat.pay.OrderManage;
import wechat.pay.beans.order.UnifiedOrderResult;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2016/12/19.
 */
public class TaskController extends BaseController {

    public static Result getTaskFrontList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        List<B_Task.State> states = new ArrayList<B_Task.State>() {{
            add(B_Task.State.publish);
            add(B_Task.State.finish);
        }};
        PagingList<B_Task> pagingList = B_Task.find.where().in("state", states).eq("deleteLogic", false).eq("visibleType", B_Task.VisibleType.all).order("createdTime desc").findPagingList(pageSize);
        Page<B_Task> page = pagingList.getPage(pageNum - 1);
        List<B_Task> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("editedTime", info.editedTime);
            object.put("title", info.title);
            object.put("endTime", info.endTime);
            object.put("state", info.state);
            object.put("username", info.owner.getShowName());
            object.put("coverImg", info.coverImg);
            object.put("coin", info.strategyInfo.coin);
            object.put("hasAllocationCoin", info.strategyInfo.hasAllocationCoin);
            object.put("count", info.getParticipantCount());
            object.put("startUrl", info.getStartUrl());
            object.put("username", info.owner.getShowName());
            object.put("avatar", info.owner.getShowAvatar());
            object.put("shareUrl", info.getStartUrl());
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    @Transactional
    @With(UserLoginSecuredAction.class)
    public static Result edit() throws Exception {
        //获取的一些信息
        JsonNode node = request().body().asJson();
        Boolean isNew = node.get("isNew").asBoolean();
        int coin = node.get("taskCoin").asInt();
        String taskStr = node.get("task").asText();
        JSONObject taskJson = JSON.parseObject(taskStr);
        String coverImg = taskJson.getString("coverImg");
        String title = taskJson.getString("title");
        String remark = taskJson.getString("remark");
        String description = node.get("description").asText();
        String taskType = node.get("taskType").asText();
        String dataTypeStr = taskJson.getString("dataType");
        String visibleTypeStr = taskJson.getString("visibleType");
        String endTimeStr = taskJson.getString("endTime");
        String autoWithdrawDepositMessage = taskJson.getString("autoWithdrawDepositMessage") == null ? null : taskJson.getString("autoWithdrawDepositMessage");
        String strategyStr = node.get("strategy").asText();


        //任务创建所需的信息
        S_User owner = currentUser();
        B_Task.DataType dataType;
        String data = null;
        B_Article article = null;
        B_Task.VisibleType visibleType;
        JSONObject strategy = JSON.parseObject(strategyStr);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date endTime = sdf.parse(endTimeStr);

        //任务内容类型+任务内容
        if ("url".equals(dataTypeStr)) {
            dataType = B_Task.DataType.url;
            data = taskJson.getString("data");
        } else if ("article".equals(dataTypeStr)) {
            dataType = B_Task.DataType.article;
            JSONObject articleObj = taskJson.getJSONObject("article");
            Integer articleId = articleObj.getInteger("id");
            article = B_Article.find.byId(Integer.valueOf(articleId));
            if (article == null) {
                return ajaxFail("不存在此文章");
            }
            if (!article.owner.id.equals(owner.id)) {
                return ajaxFail("您没有权限使用此文章");
            }
        } else {
            return ajaxFail("不存在此任务内容类型");
        }

        //任务可见级别
        if ("all".equals(visibleTypeStr)) {
            visibleType = B_Task.VisibleType.all;
        } else if ("friend".equals(visibleTypeStr)) {
            visibleType = B_Task.VisibleType.friend;
        } else if ("self".equals(visibleTypeStr)) {
            visibleType = B_Task.VisibleType.self;
        } else {
            return ajaxFail("不存在此任务可见类型");
        }

        JSONObject viewTag = new JSONObject();
        viewTag.put("isShow", false);


        if (isNew) {
            B_Task task = B_Task.create(owner, title, remark, coverImg, endTime,B_Task.stringConvertTaskType(taskType), dataType, data, article, visibleType,description);
            if(task.taskType== B_Task.TaskType.promotion){
                B_Task_Promotion promotion=B_Task_Promotion.initByTask(task);
            }
            B_Task_Strategy_Info info = B_Task_Strategy_Info.create(coin, autoWithdrawDepositMessage, strategy, task);
            B_Task_View_Tag.create(viewTag, task);
            return ajaxSuccess("创建成功");
        } else {
            String uuid = node.get("uuid").asText();
            B_Task task = B_Task.findByUuid(uuid);
            if (task == null) {
                return ajaxFail("任务不存在");
            }
            if (!task.owner.id.equals(currentUser().id)) {
                return ajaxFail("您没有权限修改");
            }

            if (task.state != B_Task.State.init) {
                return ajaxFail("任务不能修改");
            }
            task.edit(title, remark, coverImg, endTime,B_Task.stringConvertTaskType(taskType), dataType, data, article, visibleType,description);
            if(task.taskType== B_Task.TaskType.promotion){
                B_Task_Promotion promotion=B_Task_Promotion.initByTask(task);
            }
            task.strategyInfo.edit(coin, autoWithdrawDepositMessage, strategy);
            task.viewTag.edit(viewTag);
            return ajaxSuccess("修改成功");
        }
    }

    @With(UserLoginSecuredAction.class)
    public static Result getTaskEditInfo() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        if (!task.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有权限修改");
        }

        if (task.state != B_Task.State.init) {
            return ajaxFail("任务不能修改");
        }

        JSONObject result = new JSONObject();

        JSONObject taskResult = new JSONObject();
        taskResult.put("coverImg", task.coverImg);
        taskResult.put("title", task.title);
        taskResult.put("remark", task.remark);
        taskResult.put("dataType", task.dataType);
        if (task.dataType == B_Task.DataType.url) {
            taskResult.put("data", task.data);
        } else if (task.dataType == B_Task.DataType.article) {
            JSONObject article = new JSONObject();
            article.put("id", task.article.id);
            article.put("title", task.article.title);
            taskResult.put("article", article);
        }
        taskResult.put("visibleType", task.visibleType);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        taskResult.put("endTime", sdf.format(task.endTime));

        B_Task_Strategy_Info strategyInfo = task.strategyInfo;
        result.put("taskCoin", strategyInfo.coin);
        taskResult.put("autoWithdrawDepositMessage", strategyInfo.autoWithdrawDepositMessage);
        result.put("task", taskResult);

        B_Task_Strategy strategy = strategyInfo.strategy;
        JSONObject strategyResult = new JSONObject();
        if (strategyInfo.strategy.strategyType == B_Task_Strategy.StrategyType.defaultStrategyOne) {
            DefaultStrategyOne defaultStrategyOne = DefaultStrategyOne.getDefaultStrategyOneByStragetyId(strategy);
            strategyResult.put("defaultStrategyOne", defaultStrategyOne);
            strategyResult.put("strategyType", strategy.strategyType);
        } else if (strategyInfo.strategy.strategyType == B_Task_Strategy.StrategyType.advancedStrategy) {
            JSONObject advancedStrategy = new JSONObject();
            advancedStrategy.put("title", strategyInfo.strategy.title);
            advancedStrategy.put("uuid", strategyInfo.strategy.uuid);

            strategyResult.put("strategyType", strategy.strategyType);
            strategyResult.put("advancedStrategy", advancedStrategy);
        }
        result.put("strategy", strategyResult);


        return ajaxSuccess(result);
    }

    @With(UserLoginSecuredAction.class)
    public static Result getTaskOwnerList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        PagingList<B_Task> pagingList = B_Task.find.where().eq("owner", currentUser()).eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        Page<B_Task> page = pagingList.getPage(pageNum - 1);
        List<B_Task> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("editedTime", info.editedTime);
            object.put("title", info.title);
            object.put("coverImg", info.coverImg);
            object.put("endTime", info.endTime);
            object.put("coin", info.strategyInfo.coin);
            object.put("fee", info.strategyInfo.fee);
            object.put("hasAllocationCoin", info.strategyInfo.hasAllocationCoin);
            object.put("hasCost", info.strategyInfo.hasAllocationCoin);
            object.put("state", info.state);
            object.put("shareUrl", info.getStartUrl());
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    //用户获益的任务列表
    @With(UserLoginSecuredAction.class)
    public static Result getGainList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        PagingList<B_User_Coin_In_Log> pagingList = B_User_Coin_In_Log.find.fetch("log").where().eq("type", B_User_Coin_In_Log.Type.TaskAcquisition).eq("log.owner", currentUser()).eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        Page<B_User_Coin_In_Log> page = pagingList.getPage(pageNum - 1);
        List<B_User_Coin_In_Log> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_User_Coin_In_Log info : list) {
            JSONObject object = new JSONObject();
             object.put("id", info.task.id);
            object.put("uuid", info.task.uuid);
            object.put("editedTime", info.editedTime);
            object.put("title", info.task.title);
            object.put("coverImg", info.task.coverImg);
            object.put("endTime", info.task.endTime);
            object.put("coin", info.log.coin);
            object.put("state", info.task.state);
            object.put("shareUrl", info.task.getStartUrl());
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }

    //用户参与的任务列表
    @With(UserLoginSecuredAction.class)
    public static Result getParticipateList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        PagingList<B_Task_Propagation_Log> pagingList = B_Task_Propagation_Log.find.where().eq("executant", currentUser()).eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        Page<B_Task_Propagation_Log> page = pagingList.getPage(pageNum - 1);
        List<B_Task_Propagation_Log> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task_Propagation_Log info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.task.id);
            object.put("uuid", info.task.uuid);
            object.put("participateTime", info.createdTime);
            object.put("editedTime", info.task.editedTime);
            object.put("title", info.task.title);
            object.put("coverImg", info.task.coverImg);
            object.put("endTime", info.task.endTime);
            object.put("coin", info.task.strategyInfo.coin);
            object.put("fee", info.task.strategyInfo.fee);
            object.put("hasAllocationCoin", info.task.strategyInfo.hasAllocationCoin);
            object.put("state", info.task.state);
            object.put("shareUrl", info.task.getStartUrl());
            object.put("propagationCount", info.numberOfChildNodes());
            object.put("propagationInfoCount", info.getPropagationInfoCount());
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }

    //我是地推人员的任务列表
    @With(UserLoginSecuredAction.class)
    public static Result getGroundPromoterList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        PagingList<B_Task> pagingList = B_Task.find.fetch("promotion").where().eq("promotion.groundPromoter", currentUser()).eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        Page<B_Task> page = pagingList.getPage(pageNum - 1);
        List<B_Task> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("editedTime", info.promotion.createdTime);
            object.put("title", info.title);
            object.put("coverImg", info.coverImg);
            object.put("endTime", info.endTime);
            object.put("coin", info.strategyInfo.coin);
            object.put("fee", info.strategyInfo.fee);
            object.put("hasAllocationCoin", info.strategyInfo.hasAllocationCoin);
            object.put("hasCost", info.strategyInfo.hasAllocationCoin);
            object.put("state", info.state);
            object.put("promotionState", info.promotion.promotionProcess);
            object.put("shareUrl", info.getStartUrl());
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    //我是商家的任务列表
    @With(UserLoginSecuredAction.class)
    public static Result getPromoterList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        PagingList<B_Task> pagingList = B_Task.find.fetch("promotion").where().eq("promotion.promoter", currentUser()).eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        Page<B_Task> page = pagingList.getPage(pageNum - 1);
        List<B_Task> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("editedTime", info.promotion.editedTime);
            object.put("title", info.title);
            object.put("coverImg", info.coverImg);
            object.put("endTime", info.endTime);
            object.put("coin", info.strategyInfo.coin);
            object.put("fee", info.strategyInfo.fee);
            object.put("hasAllocationCoin", info.strategyInfo.hasAllocationCoin);
            object.put("state", info.state);
            object.put("promotionState", info.promotion.promotionProcess);
            object.put("shareUrl", info.getStartUrl());
            object.put("propagationCount", info.propagationLogs.size());
            object.put("propagationInfoCount", info.findPropagationInfoCount());
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }



    @Transactional
    @With(UserLoginSecuredAction.class)
    public static Result delete() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        if (task.state != B_Task.State.init) {
            return ajaxFail("任务状态不能删除");
        }
        if (!task.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有权限删除");
        }
        task.delete();
        return ajaxSuccess("删除成功");
    }


    @With(UserLoginSecuredAction.class)
    public static Result getCommitCheckRechargeInfo() throws IOException {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        S_User user = currentUser();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("不存在此任务");
        }
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有此权限");
        }
        if (!task.state.equals(B_Task.State.init)) {
            if (task.state == B_Task.State.checking) {
                return ajaxFail("任务正在审核中");
            }
            return ajaxFail("任务状态错误，禁止提交审核");
        }



        if (task.strategyInfo.coin == 0) {
            task.changeState(B_Task.State.publish);
            return ajaxSuccess("审核通过");
        } else {

            if (user.wechatInfo == null || user.wechatInfo.appOpenid == null) {
                return ajaxFail("缺少用户信息无法充值");
            }

            int total_fee = task.strategyInfo.coin + task.strategyInfo.fee;
            if (task.strategyInfo.pay != null && task.strategyInfo.pay.isInitAndUseful() && task.strategyInfo.pay.isJsapiTradeType()) {
                return ajaxSuccess(Manage.getJsapiInfo(task.strategyInfo.pay.prepay_id));
            } else {
                String nonce_str = Common.randomString(32).trim();
                String body = "任务\"" + task.title + "\"发布费用(包含平台手续费)";
                String out_trade_no = B_Wechat_Pay.getOneUnUseOutTradeNo();
                String spbill_create_ip = request().remoteAddress();
                String product_id = B_Wechat_Pay.getOneUnUseProductId();
                String notify_url = UrlController.serverAddress + "/api/wechat/pay/taskCommitCheckResult";
                UnifiedOrderResult result = OrderManage.unifiedorderJsapi(nonce_str, body, out_trade_no, total_fee, spbill_create_ip, product_id, notify_url, user.wechatInfo.appOpenid);
                if (result.isSuccess()) {
                    B_Wechat_Pay pay = B_Wechat_Pay.create(currentUser(), product_id, total_fee, out_trade_no, nonce_str, result.trade_type, result.prepay_id, result.code_url);
                    task.updateWechatPay(pay);
                    return ajaxSuccess(Manage.getJsapiInfo(result.prepay_id));
                } else {
                    return ajaxFail(result.getErrorMsg());
                }
            }
        }
    }


    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result renew() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        S_User user = currentUser();
        B_Task task = B_Task.findByUuid(uuid);
        int coin = node.get("coin").asInt();
        if (coin <= 0) {
            return ajaxFail("充值金额必须大于0元");
        }
        if (task == null) {
            return ajaxFail("不存在此任务");
        }
        if (!task.owner.id.equals(user.id)) {
            return ajaxFail("您没有此权限");
        }
        if (!(task.state.equals(B_Task.State.publish) || task.state.equals(B_Task.State.finish))) {
            return ajaxFail("任务目前状态不允许充值");
        }
        if (user.wechatInfo == null || user.wechatInfo.appOpenid == null) {
            return ajaxFail("缺少用户信息无法充值");
        }

        int fee = (int) (coin * Common.getIntConfig("task.check.fee") / 100);
        int total_fee = coin + fee;
        String nonce_str = Common.randomString(32).trim();
        String body = "任务\"" + task.title + "\"充值费用(包含" + Common.getIntConfig("task.check.fee") + "%微信手续费)";
        String out_trade_no = B_Wechat_Pay.getOneUnUseOutTradeNo();
        String spbill_create_ip = request().remoteAddress();
        String product_id = B_Wechat_Pay.getOneUnUseProductId();
        String notify_url = UrlController.serverAddress + "/api/wechat/pay/taskRenewResult";
        UnifiedOrderResult result = OrderManage.unifiedorderJsapi(nonce_str, body, out_trade_no, total_fee, spbill_create_ip, product_id, notify_url, user.wechatInfo.appOpenid);
        if (result.isSuccess()) {
            B_Wechat_Pay pay = B_Wechat_Pay.create(currentUser(), product_id, total_fee, out_trade_no, nonce_str, result.trade_type, result.prepay_id, result.code_url);
            task.updateRenew(pay, coin,fee);
            return ajaxSuccess(Manage.getJsapiInfo(result.prepay_id));
        } else {
            return ajaxFail(result.getErrorMsg());
        }
    }


    @Transactional
    @With(UserLoginSecuredAction.class)
    public static Result checkout() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if (task == null) {
            return ajaxFail("任务不存在");
        }
        if (!task.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有权限结算此任务");
        }
        if (!task.isRunning()) {
            return ajaxFail("任务状态不能进行结算");
        }
        task.finish();
        return ajaxSuccess("结算成功");
    }

    //任务的填写信息列表
    @With(UserLoginSecuredAction.class)
    public static Result getPropagationInfoList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String uuid = node.get("uuid").asText();
        B_Task task=B_Task.findByUuid(uuid);
        if(task==null){
            return paginatorSuccess(new JSONArray(), 0);
        }
        PagingList<B_Task_Propagation_Info> pagingList = B_Task_Propagation_Info.find.where().eq("task", task).eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        Page<B_Task_Propagation_Info> page = pagingList.getPage(pageNum - 1);
        List<B_Task_Propagation_Info> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task_Propagation_Info info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("editedTime", info.editedTime);
            object.put("realName", info.realName);
            object.put("telePhone", info.telePhone);
            object.put("useruuid", info.executantPropagationLog.executant.uuid);
            object.put("username", info.executantPropagationLog.executant.getShowName());
            object.put("avatar", info.executantPropagationLog.executant.getShowAvatar());

            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }



    //我参与任务中的子节点列表
    @With(UserLoginSecuredAction.class)
    public static Result getChildPropagationList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String uuid = node.get("uuid").asText();

        //查找对应任务
        B_Task task=B_Task.findByUuid(uuid);
        if(task==null){
            return paginatorSuccess(new JSONArray(), 0);
        }


        PagingList<B_Task_Propagation_Log> pagingList = B_Task_Propagation_Log.find.where().eq("task", task).eq("preExecutant",currentUser()).ne("executant", currentUser()).eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        Page<B_Task_Propagation_Log> page = pagingList.getPage(pageNum - 1);
        List<B_Task_Propagation_Log> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task_Propagation_Log info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("createdTime", info.createdTime);
            object.put("useruuid", info.executant.uuid);
            object.put("username", info.executant.getShowName());
            object.put("avatar", info.executant.getShowAvatar());

            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }

    //我参与任务中的子节点填写信息列表
    @With(UserLoginSecuredAction.class)
    public static Result getChildPropagationInfoList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String uuid = node.get("uuid").asText();

        //查找对应任务
        B_Task task=B_Task.findByUuid(uuid);
        if(task==null){
            return paginatorSuccess(new JSONArray(), 0);
        }

        PagingList<B_Task_Propagation_Info> pagingList = B_Task_Propagation_Info.find.where().eq("task", task).eq("executantPropagationLog.preExecutant",currentUser()).ne("executantPropagationLog.executant", currentUser()).eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        Page<B_Task_Propagation_Info> page = pagingList.getPage(pageNum - 1);
        List<B_Task_Propagation_Info> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task_Propagation_Info info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("editedTime", info.editedTime);
            object.put("realName", Common.convertStringWithAsterisk2(info.realName,1,0));
            object.put("telePhone", Common.convertStringWithAsterisk2(info.telePhone,3,4));
            object.put("useruuid", info.executantPropagationLog.executant.uuid);
            object.put("username", info.executantPropagationLog.executant.getShowName());
            object.put("avatar", info.executantPropagationLog.executant.getShowAvatar());

            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }
}
