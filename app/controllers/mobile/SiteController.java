package controllers.mobile;

import beans.util.sms.SendSmsResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.BaseController;
import controllers.mobile.routes;
import models.db1.user.S_User;
import play.mvc.Call;
import play.mvc.Result;
import services.LuosimaoService;
import util.Common;
import util.cache.CacheUtil;
import views.html.mobile.front_layout;
import wechat.other.webUserAuthorization.AccessTokenResult;
import wechat.other.webUserAuthorization.UserInfoResult;
import wechat.other.webUserAuthorization.WeChatWebUserAuthorization;

/**
 * Created by Administrator on 2016/12/19.
 */
public class SiteController extends BaseController {
    final static String wechatAppAppid = Common.getStringConfig("wechat.app.appid");
    final static String wechatAppAppsecret = Common.getStringConfig("wechat.app.appsecret");


    public static Result pageIndex() throws Exception {
        if(develop){
            loginAsUser(S_User.find.byId(1));
//            logout();
            return ok(front_layout.render());
        }

        if (Common.isWeixinVisit(request())) {
            S_User user = currentUser();
            if (user != null && !user.isMissingWechatDetailedData()) {
                return ok(front_layout.render());
            }

            Call redirect_url = controllers.mobile.routes.SiteController.simpleReaderInfoEntrance();
            String url = WeChatWebUserAuthorization.getGainCodeUrl_Base(wechatAppAppid, redirect_url, null);
            return redirect(url);
        }
        return controllers.SiteController.error("请在微信浏览器访问");
    }

    public static Result simpleReaderInfoEntrance() throws Exception {
        String code = getParam("code");
        String state = getParam("state");
        AccessTokenResult result = WeChatWebUserAuthorization.operate_Base(code, wechatAppAppid, wechatAppAppsecret);
        if (result.isSuccess()) {
            S_User user = S_User.findByAppOpenId(result.openid);
            if (user == null || user.isMissingWechatDetailedData()) {
                Call redirect_url = controllers.mobile.routes.SiteController.allReaderInfoEntrance();
                String url = WeChatWebUserAuthorization.getGainCodeUrl_UserInfo(wechatAppAppid, redirect_url, state);
                return redirect(url);
            } else {
                loginAsUser(user);
                return redirect(controllers.mobile.routes.SiteController.pageIndex());
            }
        } else {
            return controllers.SiteController.error("微信服务器错误");
        }
    }


    public static Result allReaderInfoEntrance() throws Exception {
        String code = getParam("code");
        JSONObject state = JSON.parseObject(getParam("state"));
        UserInfoResult result = WeChatWebUserAuthorization.operate_UserInfo(code, wechatAppAppid, wechatAppAppsecret);
        if (result != null && result.isSuccess()) {
            S_User user = S_User.findByUnionid(result.unionid);
            if (user == null) {
                user = S_User.regist(result);
            } else if (user.isMissingWechatDetailedData()) {
                user.updateWechatInfo(result);
            } else {
                user.updateAppOpenId(result.openid);
            }
            loginAsUser(user);
            return redirect(controllers.mobile.routes.SiteController.pageIndex());
        } else {
            return controllers.SiteController.error("微信服务器错误");
        }
    }

}
