package controllers.mobile;


import beans.article.view.PublishViewInfo;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.BaseController;
import models.db1.article.B_Article;
import play.mvc.Result;
import play.mvc.With;
import security.UserLoginSecuredAction;

import java.util.List;


public class ArticleController extends BaseController {

    @With(UserLoginSecuredAction.class)
    public static Result articleEdit() throws Exception {
        JsonNode node = request().body().asJson();
        Boolean isNew = node.get("isNew").asBoolean();
        String title = node.get("title").asText();
        String content = node.get("content").asText();
        String coverImg = node.get("coverImg") == null ? null : node.get("coverImg").asText();
        Boolean isComment = node.get("isComment").asBoolean();
        Boolean isPublic = node.get("isPublic").asBoolean();

        B_Article.create(title, content, isComment, isPublic, coverImg, currentUser());
        return ajaxSuccess("创建成功");
    }
}
