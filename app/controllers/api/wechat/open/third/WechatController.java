package controllers.api.wechat.open.third;

import models.db1.wechat.B_Wechat_App;
import wechat.app.bean.message.Message;
import wechat.app.sendMassage.PassiveReplyMessage;
import wechat.bean.thirdPartyPlatform.AuthorizerAccessTokenAndRefreshTokenData;
import wechat.bean.thirdPartyPlatform.ThirdPartyPlatformTokenData;
import controllers.BaseController;
import models.db1.wechat.B_Wechat_Component_Verify_Ticket;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import play.mvc.Result;
import scala.xml.Node;
import util.Common;
import wechat.app.aes.WXBizMsgCrypt;
import wechat.open.ThirdPartyPlatform.ThirdPartyPlatformUtil;
import wechat.open.ThirdPartyPlatform.WeChatRequestDispatcherDirect;
import wechat.other.WeChatCustomServiceUtil;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

/**
 * Created by whk on 2017/10/9.
 */
public class WechatController extends BaseController {

    //第三方平台配置信息
    final static String wechatThirdToken = Common.getStringConfig("wechat.open.third.componentToken");
    final static String wechatThirdEncodingAesKey = Common.getStringConfig("wechat.open.third.componenteEncodingAesKey");
    final static String wechatThirdAppId = Common.getStringConfig("wechat.open.third.appid");
    final static String wechatThirdAppSecret = Common.getStringConfig("wechat.open.third.appsecret");


    //获取第三方平台认证信息
    public static Result getComponentVerifyTicket() {
        synchronized ("getComponentVerifyTicket") {
            try {
                Node reqData = Common.w3cDocumentToScalaNode(request().body().asXml());
                String xmlStr = reqData.toString();
                String msg_signature = getParam("msg_signature");
                String signature = getParam("signature");
                String timestamp = getParam("timestamp");
                String nonce = getParam("nonce");
                String encrypt_type = getParam("encrypt_type");
                WXBizMsgCrypt pc = new WXBizMsgCrypt(wechatThirdToken, wechatThirdEncodingAesKey, wechatThirdAppId);
                String result = pc.decryptMsg(msg_signature, timestamp, nonce, xmlStr.replaceAll("AppId", "ToUserName"));
                StringReader sr = new StringReader(result);
                InputSource is = new InputSource(sr);
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse(is);
                Node xmlNode = Common.w3cDocumentToScalaNode(doc);
                ThirdPartyPlatformTokenData data = ThirdPartyPlatformTokenData.fromXml(xmlNode);
                String cacheKey = "component_verify_ticket";
                if (data.InfoType.equals("unauthorized") || data.ComponentVerifyTicket.equals("null")) {

                } else {
                    new B_Wechat_Component_Verify_Ticket(data.AppId, data.CreateTime, data.InfoType, data.ComponentVerifyTicket);
                }
            } catch (Exception e) {
                play.Logger.info("有可能密钥长度超过限制，需要替换jdk中文件");
            }
        }
        return ok("SUCCESS");
    }

    //托管微信公众号，关键词回复、菜单点击统一入口
    public static Result apiInterface(String authorizer_appid) throws Exception {
        String msg_signature = getParam("msg_signature");
        String signature = getParam("signature");
        String timestamp = getParam("timestamp");
        String nonce = getParam("nonce");
        String encrypt_type = getParam("encrypt_type");
        String appId = wechatThirdAppId;
        String token = wechatThirdToken;
        String encodingAesKey = wechatThirdEncodingAesKey;


        Message message = Message.fromXml(msg_signature, timestamp, nonce, request().body().asXml(), token, encodingAesKey, appId);

        WXBizMsgCrypt pc = new WXBizMsgCrypt(token, encodingAesKey, appId);


        if ("gh_3c884a361561".equals(message.ToUserName)) {
            //全网测试发布
            String encodingMsg = pc.encryptMsg(WechatAppGolbalWebPublishTest(message), timestamp, nonce);
            return ok(encodingMsg);
        } else {
            B_Wechat_App app = B_Wechat_App.FindByUserName(message.ToUserName);
            if (app == null) {
                return ok("Can't find app #" + appId);
            }
            String replyMsg = "";
            replyMsg = WeChatRequestDispatcherDirect.dispatch(app, message);
            String encodingMsg = pc.encryptMsg(replyMsg, timestamp, nonce);
            return ok(encodingMsg);
        }
    }

    //全网发布测试
    public static String WechatAppGolbalWebPublishTest(Message message) throws Exception {
        if (message.MsgType == Message.MsgTypeEnum.text) {
            if ("TESTCOMPONENT_MSG_TYPE_TEXT".equals(message.Content)) {
                return PassiveReplyMessage.sendText(message, message.Content + "_callback");
            }
            if (message.Content.contains("QUERY_AUTH_CODE:")) {
                final String query_auth_code = message.Content.replace("QUERY_AUTH_CODE:", "");
                try {
                    final AuthorizerAccessTokenAndRefreshTokenData authorizerAccessTokenAndRefreshTokenData = ThirdPartyPlatformUtil.getAuthorizerAccessTokenAndRefreshToken(query_auth_code);
                    final Message mess = message;
                    Thread t1 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            WeChatCustomServiceUtil.SendText(authorizerAccessTokenAndRefreshTokenData.authorizer_access_token, query_auth_code + "_from_api", mess.FromUserName);
                        }
                    });
                    t1.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return PassiveReplyMessage.sendText(message, "");
            }
        }
        if (message.MsgType == Message.MsgTypeEnum.event) {
            return PassiveReplyMessage.sendText(message, message.Event + "from_callback");
        }
        return "";
    }

}
