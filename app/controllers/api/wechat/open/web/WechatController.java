package controllers.api.wechat.open.web;


import controllers.BaseController;
import models.db1.user.B_User_Wechat_Info;
import models.db1.user.S_User;
import play.db.ebean.Transactional;
import play.mvc.Result;
import util.Common;
import wechat.open.web.LoginService;
import wechat.open.web.beans.LoginResult;
import wechat.open.web.beans.LoginUserResult;

import java.io.IOException;


public class WechatController extends BaseController {

    final static String wechatOpenWebAppid = Common.getStringConfig("wechat.open.web.appid");
    final static String wechatOpenWebAppsecret = Common.getStringConfig("wechat.open.web.appsecret");

    @Transactional
    public static Result getUserInfo() throws IOException {
        if (!containsParam("code")) {

        } else {
            String code = getParam("code");
            LoginResult loginResult = LoginService.getAccessToken(code, wechatOpenWebAppid, wechatOpenWebAppsecret);
            if (loginResult.isSuccess()) {
                S_User user = S_User.findByUnionid(loginResult.unionid);
                if (user != null) {
                    if (user.isMissingWechatDetailedData()) {
                        LoginUserResult loginUserResult = LoginService.getUsreInfo(loginResult.access_token, loginResult.openid);
                        if (loginUserResult.isSuccess()) {
                            user.updateWechatInfo(loginUserResult);
                        } else {
                            return controllers.SiteController.error("微信服务器崩溃中,请再试一次");
                        }

                    } else if (user.wechatInfo.webOpenid == null) {
                        user.updateWebOpenId(loginResult.openid);
                    }
                    loginAsUser(user);
                } else {
                    LoginUserResult loginUserResult = LoginService.getUsreInfo(loginResult.access_token, loginResult.openid);
                    if (loginUserResult.isSuccess()) {
                        loginAsUser(S_User.regist(loginUserResult));
                    } else {
                        return controllers.SiteController.error("微信服务器崩溃中,请再试一次");
                    }
                }
            } else {

            }
        }
        String url = Common.getStringConfig("server.address");
        return redirect(url);
    }
}
