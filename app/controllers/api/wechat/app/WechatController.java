package controllers.api.wechat.app;

import controllers.BaseController;
import play.mvc.Result;
import util.Common;
import wechat.app.aes.AesException;
import wechat.app.aes.WXBizMsgCrypt;
import wechat.app.bean.message.Message;
import wechat.app.bean.message.category.card.*;
import wechat.app.bean.message.category.msg.*;
import wechat.app.bean.message.category.event.*;

import static wechat.app.sendMassage.PassiveReplyMessage.*;

/**
 * Created by Administrator on 2016/3/7.
 */
public class WechatController extends BaseController {

    final static String wechatAppAppid = Common.getStringConfig("wechat.app.appid");
    final static String wechatAppAppsecret = Common.getStringConfig("wechat.app.appsecret");
    /**
     * signature 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
     * timestamp 时间戳
     * nonce 随机数
     * echostr 随机字符串
     * 开发者通过检验signature对请求进行校验（下面有校验方式）。若确认此次GET请求来自微信服务器，请原样返回echostr参数内容，则接入生效，成为开发者成功，否则接入失败。
     */
    public static Result check() {
        String signature = request().getQueryString("signature");
        String timestampStr = request().getQueryString("timestamp");
        String nonce = request().getQueryString("nonce");
        String echoStr = request().getQueryString("echostr");
        return ok(echoStr);
    }

    /**
     * 微信API操作入口
     */
    public static Result operate() throws AesException {


        String timestampStr = request().getQueryString("timestamp");
        String nonce = request().getQueryString("nonce");
        //明文时是null，只有兼容或者密文时才有数据
        String msg_signature = request().getQueryString("msg_signature");
        String token = Common.getStringConfig("Test_WechatToken");
        String encodingAesKey = Common.getStringConfig("Test_WechatEncodingAESKey");
        String appId = Common.getStringConfig("Test_WechatAppId");

        Message message = Message.fromXml(msg_signature, timestampStr, nonce, request().body().asXml(), token, encodingAesKey, appId);
        switch (message.MsgType) {
            case text: {
                TextMessage textMessage = new TextMessage(message);
                break;
            }
            case image: {
                ImageMessage imageMessage = new ImageMessage(message);
                break;
            }
            case voice: {
                VoiceMessage voiceMessage = new VoiceMessage(message);
                break;
            }
            case shortvideo: {
                ShortViedoMessage shortViedoMessage = new ShortViedoMessage(message);
                break;
            }
            case video: {
                ViedoMessage viedoMessage = new ViedoMessage(message);
                break;
            }
            case location: {
                LocationMessage locationMessage = new LocationMessage(message);
                break;
            }
            case link: {
                LinkMessage linkMessage = new LinkMessage(message);
                break;
            }
            case event: {
                switch (message.Event) {
                    case subscribe: {
                        if (message.EventKey == null) {
                            SubscribeEvent event = new SubscribeEvent(message);
                        } else {
                            SubscribeScanEvent event = new SubscribeScanEvent(message);
                        }
                        break;
                    }
                    case unsubscribe: {
                        UnSubscribeEvent event = new UnSubscribeEvent(message);
                        break;
                    }
                    case SCAN: {
                        ScanEvent event = new ScanEvent(message);
                        break;
                    }
                    case LOCATION: {
                        LocationEvent event = new LocationEvent(message);
                        break;
                    }
                    case CLICK: {
                        ClickEvent event = new ClickEvent(message);
                        break;
                    }
                    case VIEW: {
                        ViewEvent event = new ViewEvent(message);
                        break;
                    }
                    case kf_create_session: {
                        CustomerEvent event = new CustomerEvent(message);
                        System.out.println("客服信息,接入会话");
                        break;
                    }
                    case kf_close_session: {
                        CustomerEvent event = new CustomerEvent(message);
                        System.out.println("客服信息,关闭会话");
                        break;
                    }
                    case kf_switch_session: {
                        CustomerEvent event = new CustomerEvent(message);
                        System.out.println("客服信息,转接会话");
                        break;
                    }
                    case MASSSENDJOBFINISH: {
                        MassMessageSendEvent event = new MassMessageSendEvent(message);
                        System.out.println("群发结束");
                        break;
                    }
                    case TEMPLATESENDJOBFINISH: {
                        TemplateSendEvent event = new TemplateSendEvent(message);
                        System.out.println("模板信息发送情况");
                        break;
                    }
                    case poi_check_notify: {
                        EntityShopEvent event = new EntityShopEvent(message);
                        System.out.println("模板信息发送情况");
                        break;
                    }
                    case card_pass_check: {
                        CardPassCheckEvent event = new CardPassCheckEvent(message);
                        System.out.println("卡劵创建通过");
                        break;
                    }
                    case card_not_pass_check: {
                        CardNotPassCheckEvent event = new CardNotPassCheckEvent(message);
                        System.out.println("卡劵创建未通过");
                        break;
                    }
                    case user_get_card: {
                        CardUserGetEvent event = new CardUserGetEvent(message);
                        System.out.println("领取事件推送");
                        break;
                    }
                    case user_del_card:{
                        CardUserDeleteEvent event = new CardUserDeleteEvent(message);
                        System.out.println("删除事件推送");
                        break;
                    }
                    case user_consume_card:{
                        CardUserConsumeEvent event = new CardUserConsumeEvent(message);
                        System.out.println("核销事件推送");
                        break;
                    }
                    case user_pay_from_pay_cell:{
                        CardUserPayFromPayCellEvent event = new CardUserPayFromPayCellEvent(message);
                        System.out.println("买单事件推送");
                        break;
                    }
                    case user_view_card:{
                        CardUserViewEvent event = new CardUserViewEvent(message);
                        System.out.println("进入会员卡事件推送");
                        break;
                    }
                    case user_enter_session_from_card:{
                        CardUserEnterSessionEvent event = new CardUserEnterSessionEvent(message);
                        System.out.println("从卡券进入公众号会话事件推送");
                        break;
                    }
                    case update_member_card:{
                        CardUpadateMemberEvent event = new CardUpadateMemberEvent(message);
                        System.out.println("会员卡内容更新事件");
                        break;
                    }
                    case card_sku_remind:{
                        CardSkuRemindEvent event = new CardSkuRemindEvent(message);
                        System.out.println("库存报警事件");
                        break;
                    }
                    case card_pay_order:{
                        CardPayOrderEvent event = new CardPayOrderEvent(message);
                        System.out.println("券点流水详情事件");
                        break;
                    }
                    case merchant_order:{
                        MerchantOrderEvent event = new MerchantOrderEvent(message);
                        System.out.println("订单付款通知事件");
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            }
            default: {
                break;
            }
        }

        String result = sendToCustomer(message, "chada_0", "sjj");
        if (message.Encrypt != null) {
            WXBizMsgCrypt pc = new WXBizMsgCrypt(token, encodingAesKey, appId);
            result = pc.encryptMsg(result, timestampStr, nonce);
        }
        return ok(result);
    }
}
