package controllers.api.wechat.pay;


import com.alibaba.fastjson.JSONObject;
import controllers.BaseController;
import models.db1.task.B_Task;
import models.db1.task.strategy.B_Task_Strategy_Info;
import models.db1.task.strategy.B_Task_Strategy_Renew;
import models.db1.user.B_User_Account;
import models.db1.user.S_User;
import models.db1.user.coin.B_User_Coin_Log;
import models.db1.wechat.B_Wechat_Pay;
import org.xml.sax.SAXException;
import play.db.ebean.Transactional;
import play.mvc.Result;
import scala.xml.Node;
import util.Common;
import util.XmlUtil;
import wechat.open.web.LoginService;
import wechat.open.web.beans.LoginResult;
import wechat.open.web.beans.LoginUserResult;
import wechat.pay.beans.order.PayResult;

import java.io.IOException;


public class WechatController extends BaseController {

    final static String wechatOpenWebAppid = Common.getStringConfig("wechat.open.web.appid");
    final static String wechatOpenWebAppsecret = Common.getStringConfig("wechat.open.web.appsecret");

    @Transactional
    //由于这个会多次发送，所以必须做处理
    public static Result taskCommitCheckResult() throws Exception {
        Node reqData = Common.w3cDocumentToScalaNode(request().body().asXml());
        String xmlStr = reqData.toString();
        JSONObject resultJson = XmlUtil.parseXmlToJson(xmlStr);
        PayResult payResult = PayResult.create(resultJson);
        if (payResult.isSuccess()) {
            B_Wechat_Pay pay = B_Wechat_Pay.findByOutTradeNo(payResult.out_trade_no);
            if (pay != null && pay.state == B_Wechat_Pay.State.Init) {
                B_Task_Strategy_Info info = B_Task_Strategy_Info.findByWechatPay(pay);
                if (info != null) {
                    pay.updateInfo(payResult.openid, payResult.bank_type, payResult.transaction_id, payResult.time_end);
                    if (payResult.total_fee == (info.coin + info.fee)) {
                        info.task.owner.account.addCoin(payResult.total_fee);
                        B_User_Coin_Log.create_taskCommit(info.task, payResult.total_fee, pay);
                        info.task.owner.account.reduceCoin(payResult.total_fee);
                        B_User_Coin_Log.create_taskCheck(info.task, payResult.total_fee);
                        info.task.publish();
                    } else {
                        //任务金额与订单金额不一样，则任务处于异常状态
                        info.task.changeState(B_Task.State.exception);
                    }


                }
            }
        }
        return ajaxSuccess("ok");
    }


    @Transactional
    //由于这个会多次发送，所以必须做处理
    public static Result taskRenewResult() throws Exception {
        Node reqData = Common.w3cDocumentToScalaNode(request().body().asXml());
        String xmlStr = reqData.toString();
        JSONObject resultJson = XmlUtil.parseXmlToJson(xmlStr);
        PayResult payResult = PayResult.create(resultJson);
        if (payResult.isSuccess()) {
            B_Wechat_Pay pay = B_Wechat_Pay.findByOutTradeNo(payResult.out_trade_no);
            if (pay != null && pay.state == B_Wechat_Pay.State.Init) {
                B_Task_Strategy_Renew renew = B_Task_Strategy_Renew.findByWechatPay(pay);
                if (renew != null) {
                    if (payResult.total_fee == (renew.coin + renew.fee)) {
                        renew.info.task.renew(renew.coin );
                        pay.updateInfo(payResult.openid, payResult.bank_type, payResult.transaction_id, payResult.time_end);
                        renew.info.task.owner.account.addCoin(payResult.total_fee);
                        B_User_Coin_Log.create_taskRenewIn(renew.info.task, payResult.total_fee, pay);
                        renew.info.task.owner.account.reduceCoin(payResult.total_fee);
                        B_User_Coin_Log.create_taskRenewOut(renew.info.task, payResult.total_fee);
                    }
                }
            }
        }
        return ajaxSuccess("ok");
    }

    @Transactional
    //用户账号充值
    //由于这个会多次发送，所以必须做处理
    public static Result userRechargeResult() throws Exception {
        Node reqData = Common.w3cDocumentToScalaNode(request().body().asXml());
        String xmlStr = reqData.toString();
        JSONObject resultJson = XmlUtil.parseXmlToJson(xmlStr);
        PayResult payResult = PayResult.create(resultJson);
        if (payResult.isSuccess()) {
            B_Wechat_Pay pay = B_Wechat_Pay.findByOutTradeNo(payResult.out_trade_no);
            if (pay != null && pay.state == B_Wechat_Pay.State.Init) {
                pay.updateInfo(payResult.openid, payResult.bank_type, payResult.transaction_id, payResult.time_end);
                S_User rechargeUser = pay.owner;
                rechargeUser.account.addCoin(pay.total_fee);
                //充值这新增一条In记录
                B_User_Coin_Log.create_userRechargeIn(rechargeUser, payResult.total_fee,pay);
            }
        }
        return ajaxSuccess("ok");
    }
}
