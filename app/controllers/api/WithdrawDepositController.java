package controllers.api;

import beans.article.view.PublishViewInfo;
import beans.user.view.WithdrawDepositMobileInfo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import controllers.BaseController;
import models.db1.article.B_Article;
import models.db1.user.S_User;
import play.db.ebean.Transactional;
import play.mvc.Call;
import play.mvc.Result;
import util.Common;
import wechat.other.webUserAuthorization.AccessTokenResult;
import wechat.other.webUserAuthorization.UserInfoResult;
import wechat.other.webUserAuthorization.WeChatWebUserAuthorization;

/**
 * Created by Administrator on 2016/11/30.
 */
public class WithdrawDepositController extends BaseController {

    final static String wechatAppAppid = Common.getStringConfig("wechat.app.appid");
    final static String wechatAppAppsecret = Common.getStringConfig("wechat.app.appsecret");


    public static Result entrance() throws Exception {
        if (Common.isWeixinVisit(request())) {
            Call redirect_url = controllers.api.routes.WithdrawDepositController.simpleReaderInfoEntrance();
            String url = WeChatWebUserAuthorization.getGainCodeUrl_Base(wechatAppAppid, redirect_url, null);
            return redirect(url);
        } else {
            return controllers.SiteController.error("必须在微信中访问此页面");
        }
    }


    public static Result simpleReaderInfoEntrance() throws Exception {
        String code = getParam("code");
        AccessTokenResult result = WeChatWebUserAuthorization.operate_Base(code, wechatAppAppid, wechatAppAppsecret);
        if (result.isSuccess()) {
            S_User user = S_User.findByAppOpenId(result.openid);
            if (user == null) {
                Call redirect_url = controllers.api.routes.WithdrawDepositController.allReaderInfoEntrance();
                String url = WeChatWebUserAuthorization.getGainCodeUrl_UserInfo(wechatAppAppid, redirect_url, null);
                return redirect(url);
            } else {
                loginAsUser(user);
                return redirect(controllers.api.routes.WithdrawDepositController.operate());
            }
        } else {
            return controllers.SiteController.error("微信服务器错误");
        }
//        return ajaxSuccess("ok");
    }

    @Transactional
    public static Result allReaderInfoEntrance() throws Exception {
        String code = getParam("code");
        UserInfoResult result = WeChatWebUserAuthorization.operate_UserInfo(code, wechatAppAppid, wechatAppAppsecret);
        if (result.isSuccess()) {
            S_User user = S_User.findByUnionid(result.unionid);
            if (user == null) {
                user = S_User.regist(result);
            } else {
                user.updateAppOpenId(result.openid);
            }
            loginAsUser(user);
            return redirect(controllers.api.routes.WithdrawDepositController.operate());
        } else {
            return controllers.SiteController.error("微信服务器错误");
        }
//        return ajaxSuccess("ok");
    }


    public static Result operate() throws Exception {
        S_User user = currentUser();
        WithdrawDepositMobileInfo info = new WithdrawDepositMobileInfo(user.wechatInfo.nickname,user.account.coin);
        return ok(views.html.user.withdrawDeposit_mobile.render(info));
    }
}
