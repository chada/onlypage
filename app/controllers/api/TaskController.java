package controllers.api;

import beans.task.TaskConsumer;
import beans.task.strategy.StrategyExecuteResult;
import beans.task.strategy.StrategyTriggerEvent;
import beans.task.view.PublishViewInfo;
import beans.wechat.JsShareInfo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.BaseController;
import controllers.api.routes;
import controllers.util.UrlController;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Log;
import models.db1.task.promotion.B_Task_Promotion;
import models.db1.task.schedule.B_Task_User_Statistics_Data;
import models.db1.task.strategy.B_Task_Strategy;
import models.db1.task.strategy.B_Task_Strategy_Info;
import models.db1.task.strategy.B_Task_Strategy_Rule;
import models.db1.task.strategy.B_Task_Strategy_Rule_Condition;
import models.db1.user.S_User;
import play.db.ebean.Transactional;
import play.mvc.Call;
import play.mvc.Result;
import play.mvc.With;
import redis.clients.jedis.Jedis;
import security.UserLoginSecuredAction;
import util.Common;
import views.html.task.publish_view;
import views.html.task.publish_view_special_one;
import views.html.task.promotion.bindPromoter;
import views.html.task.promotion.setPromotionTaskInfo;
import wechat.other.webUserAuthorization.AccessTokenResult;
import wechat.other.webUserAuthorization.UserInfoResult;
import wechat.other.webUserAuthorization.WeChatWebUserAuthorization;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Administrator on 2016/11/30.
 */
public class TaskController extends BaseController {

    final static String wechatAppAppid = Common.getStringConfig("wechat.app.appid");
    final static String wechatAppAppsecret = Common.getStringConfig("wechat.app.appsecret");
    //固定大小线程池，项目关闭时，需要关闭线程池，否则无法关闭jvm
    public static ExecutorService pool = Executors.newFixedThreadPool(10);

    public static Result entrance() throws Exception {
        if (!containsParam("taskId") && !containsParam("executantId")) {
            return controllers.SiteController.error("数据错误");
        }
        int taskId = getIntParam("taskId");
        int executantId = !containsParam("executantId") ? getIntParam("amp;executantId") : getIntParam("executantId");

        S_User user = currentUser();
        if (user != null && !user.isMissingWechatDetailedData()) {
            return redirect(controllers.api.routes.TaskController.operate(executantId, taskId));
        }

        if (Common.isWeixinVisit(request())) {
            JSONObject state = new JSONObject();
            state.put("taskId", String.valueOf(taskId));
            state.put("executantId", String.valueOf(executantId));

            Call redirect_url = controllers.api.routes.TaskController.simpleReaderInfoEntrance();
            String url = WeChatWebUserAuthorization.getGainCodeUrl_Base(wechatAppAppid, redirect_url, state.toJSONString());
            return redirect(url);
        } else {
            return controllers.SiteController.error("必须在微信中访问此页面");
        }
    }


    public static Result simpleReaderInfoEntrance() throws Exception {
        String code = getParam("code");
        String state = getParam("state");
        JSONObject stateObject = JSON.parseObject(getParam("state"));
        AccessTokenResult result = WeChatWebUserAuthorization.operate_Base(code, wechatAppAppid, wechatAppAppsecret);
        if (result.isSuccess()) {
            S_User user = S_User.findByAppOpenId(result.openid);
            if (user == null || user.isMissingWechatDetailedData()) {
                Call redirect_url = controllers.api.routes.TaskController.allReaderInfoEntrance();
                String url = WeChatWebUserAuthorization.getGainCodeUrl_UserInfo(wechatAppAppid, redirect_url, state);
                return redirect(url);
            } else {
                loginAsUser(user);
                return redirect(controllers.api.routes.TaskController.operate(stateObject.getInteger("executantId"), stateObject.getInteger("taskId")));
            }
        } else {
            return controllers.SiteController.error("微信服务器错误");
        }
//        return ajaxSuccess("ok");
    }

    @Transactional
    public static Result allReaderInfoEntrance() throws Exception {
        String code = getParam("code");
        JSONObject state = JSON.parseObject(getParam("state"));
        UserInfoResult result = WeChatWebUserAuthorization.operate_UserInfo(code, wechatAppAppid, wechatAppAppsecret);
        if (result != null && result.isSuccess()) {
            S_User user = S_User.findByUnionid(result.unionid);
            if (user == null) {
                user = S_User.regist(result);
            } else if (user.isMissingWechatDetailedData()) {
                user.updateWechatInfo(result);
            } else {
                user.updateAppOpenId(result.openid);
            }
            loginAsUser(user);
            return redirect(controllers.api.routes.TaskController.operate(state.getInteger("executantId"), state.getInteger("taskId")));
        } else {
            return controllers.SiteController.error("微信服务器错误");
        }
//        return ajaxSuccess("ok");
    }


    public static Result operate(int executantId, int taskId) throws Exception {
        S_User user = currentUser();
        B_Task task = B_Task.find.byId(taskId);
        if (user == null) {
            String url = null;
            if (executantId == 0) {
                url = task.getStartUrl();
            } else {
                S_User pre_user = S_User.find.byId(executantId);
                if (pre_user == null) {
                    return controllers.SiteController.error("不存在传播者信息");
                }
                url = task.getStartUrl(pre_user);
            }
            return redirect(url);
        }
        if (task == null) {
            return controllers.SiteController.error("不存在此任务");
        }

        S_User pre_user;
        int level;
        if (executantId == 0) {
            pre_user = user;
            level = 1;
        } else {
            pre_user = S_User.find.byId(executantId);
            if (pre_user == null) {
                return controllers.SiteController.error("不存在传播者信息");
            }
            B_Task_Propagation_Log preLog = B_Task_Propagation_Log.findByTaskAndExecutant(task, pre_user);
            if (preLog == null) {
                return controllers.SiteController.error("信息错误");
            }
            level = preLog.level + 1;
        }
        B_Task_Propagation_Log.create(task, pre_user, user, level);
        B_Task_User_Statistics_Data.create(user, task);

        if (task.isRunning()) {
//            Jedis jedis = new Jedis("localhost");
//            String str = user.id + "_" + taskId + "_" + request().remoteAddress() + "_" + "enterpage" + "_" + "0";
            StrategyTriggerEvent strategyTriggerEvent = new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.enterpage);
            pool.execute(new TaskConsumer(taskId, strategyTriggerEvent, user, request().remoteAddress()));
//            jedis.lpush("task", str);
//            StrategyTriggerEvent strategyTriggerEvent = new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.enterpage);
//            task.checkout(user, strategyTriggerEvent, request());
        }

        DateFormat sf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        session("taskStartReadTimeDate" + taskId, sf.format(new Date()));

        if(task.migrationid==-1){
            return ok(publish_view_special_one.render(new PublishViewInfo(user,pre_user, task), new JsShareInfo(task, user, request())));
        }

        return ok(publish_view.render(new PublishViewInfo(user,pre_user, task), new JsShareInfo(task, user, request())));

    }

    @With(UserLoginSecuredAction.class)
    public static Result eventTrigger() throws Exception {
        int taskId = getIntPostParam("taskId");
        String triggerEvent = getPostParam("triggerEvent");

        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("任务不存在");
        }


        StrategyTriggerEvent strategyTriggerEvent = null;
        if ("readtime".equals(triggerEvent)) {
            int readTime = getIntPostParam("readTime");
            String taskStartReadTimeDateStr = session("taskStartReadTimeDate" + taskId);
            if (taskStartReadTimeDateStr == null) {
                return ajaxFail("数据错误");
            } else {
                DateFormat sf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date taskStartReadTimeDate = sf.parse(taskStartReadTimeDateStr);
                long interval = (new Date().getTime() - taskStartReadTimeDate.getTime()) / 1000;
                if (interval < readTime) {
                    return ajaxFail("阅读时间不够");
                }
            }
            strategyTriggerEvent = new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.readtime, readTime);
            pool.execute(new TaskConsumer(taskId, strategyTriggerEvent, currentUser(), request().remoteAddress()));
//            Jedis jedis = new Jedis("localhost");
//            String str = currentUser().id + "_" + taskId + "_" + request().remoteAddress() + "_" + B_Task_Strategy_Rule.TriggerEvent.readtime.toString() + "_" + readTime;
//            jedis.lpush("task", str);
//            strategyTriggerEvent = new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.readtime, readTime);
//            task.checkout(currentUser(), strategyTriggerEvent, request());
        } else if ("repost".equals(triggerEvent)) {
            strategyTriggerEvent = new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.repost);
            pool.execute(new TaskConsumer(taskId, strategyTriggerEvent, currentUser(), request().remoteAddress()));

//            Jedis jedis = new Jedis("localhost");
//            String str = currentUser().id + "_" + taskId + "_" + request().remoteAddress() + "_" + B_Task_Strategy_Rule.TriggerEvent.repost.toString() + "_" + "0";
//            jedis.lpush("task", str);
//            strategyTriggerEvent = new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.repost);
//            task.checkout(currentUser(), strategyTriggerEvent, request());
        } else if ("infosubmit".equals(triggerEvent)) {
            strategyTriggerEvent = new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.infosubmit);
            pool.execute(new TaskConsumer(taskId, strategyTriggerEvent, currentUser(), request().remoteAddress()));
//            Jedis jedis = new Jedis("localhost");
//            String str = currentUser().id + "_" + taskId + "_" + request().remoteAddress() + "_" + B_Task_Strategy_Rule.TriggerEvent.infosubmit.toString() + "_" + "0";
//            jedis.lpush("task", str);
//            strategyTriggerEvent = new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.infosubmit);
//            task.checkout(currentUser(), strategyTriggerEvent, request());
        } else if ("sharetofriendcircle".equals(triggerEvent)) {//分享到朋友圈
            strategyTriggerEvent = new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.sharetofriendcircle);
            pool.execute(new TaskConsumer(taskId, strategyTriggerEvent, currentUser(), request().remoteAddress()));
//            Jedis jedis = new Jedis("localhost");
//            String str = currentUser().id + "_" + taskId + "_" + request().remoteAddress() + "_" + B_Task_Strategy_Rule.TriggerEvent.sharetofriendcircle.toString() + "_" + "0";
//            jedis.lpush("task", str);
//            strategyTriggerEvent = new StrategyTriggerEvent(B_Task_Strategy_Rule.TriggerEvent.sharetofriendcircle);
//            task.checkout(currentUser(), strategyTriggerEvent, request());
        } else {
            return ajaxFail("未知错误");
        }


        if (task.isRunning() && strategyTriggerEvent != null) {
            S_User user = currentUser();
            List<StrategyExecuteResult> strategyExecuteResultList = task.strategyInfo.strategy.executeStrategy(task, user, strategyTriggerEvent);
            int wechatAccount = 0;
            int onlypageAccount = 0;
            for (StrategyExecuteResult strategyExecuteResult : strategyExecuteResultList) {
                if (strategyExecuteResult.success) {
                    if (strategyExecuteResult.rewardAmount > 0 && strategyExecuteResult.beneficialUser.id.equals(user.id)) {
                        if (strategyExecuteResult.rewardAmount >= 100) {
                            wechatAccount += strategyExecuteResult.rewardAmount;
                        } else {
                            onlypageAccount += strategyExecuteResult.rewardAmount;
                        }
                    }
                }
            }
            JSONObject object = new JSONObject();
            object.put("wechatAccount", wechatAccount);
            object.put("onlypageAccount", onlypageAccount);
            return ajaxSuccess(object);
        } else if (!task.isRunning()) {
            return ajaxFail("任务已结束");
        } else {
            return ajaxFail("未知错误");
        }
    }


    //更新阅读任务时的地址（坐标经纬度）
    @With(UserLoginSecuredAction.class)
    public static Result updateUserReadLocationCoordinate() throws Exception {
        int taskId = getIntPostParam("taskId");

        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("任务不存在");
        }

        if (task.isRunning()) {
            Double longitude = Double.parseDouble(getPostParam("longitude"));
            Double latitude = Double.parseDouble(getPostParam("latitude"));
            S_User user = currentUser();
            B_Task_User_Statistics_Data.get(user, task).updateLocationCoordinate(longitude, latitude);
            return ajaxSuccess("成功");
        } else if (!task.isRunning()) {
            return ajaxFail("任务已结束");
        } else {
            return ajaxFail("未知错误");
        }
    }


    //更新是否分享到朋友圈
    @With(UserLoginSecuredAction.class)
    public static Result updateShareToFriendCircleState() throws Exception {
        int taskId = getIntPostParam("taskId");

        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("任务不存在");
        }

        if (task.isRunning()) {
            Boolean hasShareToFriendCircle = getBoolPostParam("hasShareToFriendCircle");
            S_User user = currentUser();
            B_Task_User_Statistics_Data taskUserStatisticsData = B_Task_User_Statistics_Data.get(user, task);
            if (taskUserStatisticsData.hasShareToFriendCircle) {

            } else {
                taskUserStatisticsData.updateShareToFriendCircleState(hasShareToFriendCircle);
            }
            return ajaxSuccess("成功");
        } else if (!task.isRunning()) {
            return ajaxFail("任务已结束");
        } else {
            return ajaxFail("未知错误");
        }
    }


    //地推人员设置任务信息页面入口
    //目前入口为任务页面中的标记点击，判断是否尚未设定且是否为绑定者，所以暂不写用户不为登录的后续判断
    @With(UserLoginSecuredAction.class)
    public static Result setPromotionInfoPage() throws Exception {
        int taskId = getIntParam("taskId");
        S_User user = currentUser();

        if (user == null || user.isMissingWechatDetailedData()) {
            return controllers.SiteController.error("尚未登录");
        }

        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return controllers.SiteController.error("不存在此任务");
        }

        B_Task_Promotion taskPromotion = task.promotion;
        if (taskPromotion == null) {
            return controllers.SiteController.error("任务不是地推任务或者任务尚未添加地推信息");
        }
        if (!((taskPromotion.promotionProcess == B_Task_Promotion.Process.groundPromoterBind) || (taskPromotion.promotionProcess == B_Task_Promotion.Process.infoSet))) {
            return controllers.SiteController.error("任务不处于地推信息填写阶段");
        }
        if (!taskPromotion.groundPromoter.id.equals(user.id)) {
            return controllers.SiteController.error("您不是该任务的认领者");
        }
        return ok(setPromotionTaskInfo.render(task, new JsShareInfo(request())));

    }


    //地推人员设置任务信息
    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result setPromotionInfo() throws Exception {
        JsonNode node = request().body().asJson();

        int taskId = getIntPostParam("taskId");
        String promoterName = getPostParam("promoterName");
        String promoterLocation = getPostParam("promoterLocation");
        String promoterContactName = getPostParam("promoterContactName");
        String promoterPhone = getPostParam("promoterPhone");
        Double latitude = getDoublePostParam("latitude");
        Double longitude = getDoublePostParam("longitude");

        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("不存在此任务");
        }

        B_Task_Promotion taskPromotion = B_Task_Promotion.find.byId(task.promotion.id);
        if (taskPromotion == null) {
            return ajaxFail("任务不是地推任务或者任务尚未添加地推信息");
        }

        S_User currentUser = currentUser();
        S_User groundPromoter = taskPromotion.groundPromoter;

        //任务拥有者和地推人员才能设置信息
        if (!(groundPromoter.id.equals(currentUser.id) || task.owner.id.equals(currentUser.id))) {
            return ajaxFail("您没有该任务的信息填写权限");
        }

        if (taskPromotion.promotionProcess != B_Task_Promotion.Process.groundPromoterBind) {
            return ajaxFail("任务不在可填写信息的状态");
        }

        taskPromotion.promoterName = promoterName;
        taskPromotion.promoterLocation = promoterLocation;
        taskPromotion.promoterContactName = promoterContactName;
        taskPromotion.promoterPhone = promoterPhone;
        taskPromotion.promotionProcess = B_Task_Promotion.Process.infoSet;
        taskPromotion.promoterLatitude = latitude;
        taskPromotion.promoterLongitude = longitude;
        taskPromotion.saveOrUpdate();

        //策略添加地址信息
        B_Task_Strategy_Info info = B_Task_Strategy_Info.find.where().eq("task", task).findUnique();
        if (info == null) {
            return ajaxFail("任务策略信息不存在");
        }

        B_Task_Strategy strategy = info.strategy;
        if (strategy == null) {
            return ajaxFail("任务策略不存在");
        }

        List<B_Task_Strategy_Rule> ruleList = strategy.strategyRuleList;
        for (B_Task_Strategy_Rule rule : ruleList) {
            B_Task_Strategy_Rule_Condition condition = new B_Task_Strategy_Rule_Condition();
            condition.strategyRule = rule;
            condition.conditionNodeType = B_Task_Strategy_Rule_Condition.ConditionNodeType.self;
            condition.allocationConditions = B_Task_Strategy_Rule_Condition.AllocationConditions.location;
            condition.longitude = longitude;
            condition.latitude = latitude;
            condition.locationRange = 500;
            condition.saveOrUpdate();
        }


        String url = UrlController.serverAddress + "/api/task/promotion/bindPromoterPage/" + taskId + "/" + currentUser.id + "/1";
        JSONObject object = new JSONObject();
        object.put("url", url);
        return ajaxSuccess(object);

    }

    //获取绑定商家页面的url
    @With(UserLoginSecuredAction.class)
    public static Result getPromoterBindUrl() throws Exception {
        int taskId = getIntPostParam("taskId");
        S_User user = currentUser();
        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("不存在此任务");
        }

        String url = UrlController.serverAddress + "/api/task/promotion/bindPromoterPage/" + taskId + "/" + user.id + "/1";
        JSONObject object = new JSONObject();
        object.put("url", url);
        return ajaxSuccess(object);

    }

    //绑定商家页面
    //enterType:1-初始进入;2-静默登录;3-授权登录
    public static Result bindPromoterPage(int taskId, int preExecutantId, int enterType) throws Exception {
        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return controllers.SiteController.error("不存在此任务");
        }

        S_User user = currentUser();
        if (user != null && !user.isMissingWechatDetailedData()) {
            return ok(bindPromoter.render(task));
        }

        if (Common.isWeixinVisit(request())) {
            if (enterType == 1) {
                JSONObject state = new JSONObject();
                state.put("taskId", String.valueOf(taskId));
                Call redirect_url = controllers.api.routes.TaskController.bindPromoterPage(taskId, preExecutantId, 2);
                String url = WeChatWebUserAuthorization.getGainCodeUrl_Base(wechatAppAppid, redirect_url, state.toJSONString());
                return redirect(url);
            } else if (enterType == 2) {
                String code = getParam("code");
                String state = getParam("state");
                JSONObject stateObject = JSON.parseObject(getParam("state"));
                AccessTokenResult result = WeChatWebUserAuthorization.operate_Base(code, wechatAppAppid, wechatAppAppsecret);
                if (result.isSuccess()) {
                    user = S_User.findByAppOpenId(result.openid);
                    if (user == null || user.isMissingWechatDetailedData()) {
                        Call redirect_url = controllers.api.routes.TaskController.bindPromoterPage(taskId, preExecutantId, 3);
                        String url = WeChatWebUserAuthorization.getGainCodeUrl_UserInfo(wechatAppAppid, redirect_url, state);
                        return redirect(url);
                    } else {
                        loginAsUser(user);
                        return redirect(controllers.api.routes.TaskController.bindPromoterPage(taskId, preExecutantId, 1));
                    }
                } else {
                    return controllers.SiteController.error("微信服务器错误");
                }
            } else if (enterType == 3) {
                String code = getParam("code");
                JSONObject state = JSON.parseObject(getParam("state"));
                UserInfoResult result = WeChatWebUserAuthorization.operate_UserInfo(code, wechatAppAppid, wechatAppAppsecret);
                if (result != null && result.isSuccess()) {
                    user = S_User.findByUnionid(result.unionid);
                    if (user == null) {
                        user = S_User.regist(result);
                    } else if (user.isMissingWechatDetailedData()) {
                        user.updateWechatInfo(result);
                    } else {
                        user.updateAppOpenId(result.openid);
                    }
                    loginAsUser(user);
                    return redirect(controllers.api.routes.TaskController.bindPromoterPage(taskId, preExecutantId, 1));
                } else {
                    return controllers.SiteController.error("微信服务器错误");
                }
            } else {
                return controllers.SiteController.error("参数出错");
            }

        } else {
            return controllers.SiteController.error("必须在微信中访问此页面");
        }

    }

    //商家绑定
    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result bindPromoter() throws Exception {
        JsonNode node = request().body().asJson();

        int taskId = getIntPostParam("taskId");

        B_Task task = B_Task.find.byId(taskId);
        if (task == null) {
            return ajaxFail("不存在此任务");
        }

        B_Task_Promotion taskPromotion = task.promotion;
        if (taskPromotion == null) {
            return ajaxFail("任务不是地推任务或者任务尚未添加地推信息");
        }
        if (taskPromotion.promotionProcess != B_Task_Promotion.Process.infoSet) {
            return ajaxFail("任务不在商家信息绑定阶段");
        }

        S_User currentUser = currentUser();
        if (task.owner.id.equals(currentUser.id)) {
            return ajaxFail("您是该任务的所有者，不能进行商家绑定操作");
        }
        if (taskPromotion.groundPromoter.id.equals(currentUser.id)) {
            return ajaxFail("您是该任务的地推人员，不能进行商家绑定操作");
        }


        taskPromotion.promoter = currentUser;
        taskPromotion.promotionProcess = B_Task_Promotion.Process.promoterBind;
        taskPromotion.saveOrUpdate();
        return ajaxSuccess("绑定成功");

    }


}
