package controllers.api;

import beans.customization.HtmlViewInfo;
import com.alibaba.fastjson.JSONObject;
import controllers.BaseController;
import models.db1.B_Customization_Html;
import play.mvc.Result;
import util.Common;
import views.html.customization.minshengBankCreditCard;
import views.html.customization.minshengBankCreditCard2;
import views.html.customization.customizationHtmlMobile;

/**
 * Created by whk on 2017/7/23.
 */
public class CustomizationPageController extends BaseController {

    public static Result minshengBankCreditCard() throws Exception {
        return ok(minshengBankCreditCard.render());
    }
    public static Result minshengBankCreditCard2() throws Exception {
        return ok(minshengBankCreditCard2.render());
    }

    public static Result customizationHtml(String customizationHtmlFileName) throws Exception {
        B_Customization_Html customizationHtml=B_Customization_Html.findByFileName(customizationHtmlFileName);
        if (customizationHtml==null) {
            return controllers.SiteController.error("网页不存在");
        }
        return ok(customizationHtmlMobile.render(new HtmlViewInfo(customizationHtml)));
    }
}
