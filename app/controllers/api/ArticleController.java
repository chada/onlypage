package controllers.api;

import beans.article.view.PublishViewInfo;
import beans.wechat.JsShareInfo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import controllers.BaseController;
import models.db1.article.B_Article;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Log;
import models.db1.user.S_User;
import play.db.ebean.Transactional;
import play.mvc.Call;
import play.mvc.Result;
import util.Common;
import views.html.article.publish_view;
import wechat.other.webUserAuthorization.AccessTokenResult;
import wechat.other.webUserAuthorization.UserInfoResult;
import wechat.other.webUserAuthorization.WeChatWebUserAuthorization;

/**
 * Created by Administrator on 2016/11/30.
 */
public class ArticleController extends BaseController {

    final static String wechatAppAppid = Common.getStringConfig("wechat.app.appid");
    final static String wechatAppAppsecret = Common.getStringConfig("wechat.app.appsecret");


    public static Result entrance() throws Exception {
        if (!containsParam("id")) {
            return controllers.SiteController.error("数据错误");
        }
        int id = getIntParam("id");


        if (currentUser() != null) {
            return redirect(controllers.api.routes.ArticleController.operate(id));
        }

        if (Common.isWeixinVisit(request())) {
            JSONObject state = new JSONObject();
            state.put("id", String.valueOf(id));
            Call redirect_url = controllers.api.routes.ArticleController.simpleReaderInfoEntrance();
            String url = WeChatWebUserAuthorization.getGainCodeUrl_Base(wechatAppAppid, redirect_url, state.toJSONString());
            return redirect(url);
        } else {
            return controllers.SiteController.error("必须在微信中访问此页面");
        }
    }


    public static Result simpleReaderInfoEntrance() throws Exception {
        String code = getParam("code");
        String state = getParam("state");
        JSONObject stateObject = JSON.parseObject(getParam("state"));
        AccessTokenResult result = WeChatWebUserAuthorization.operate_Base(code, wechatAppAppid, wechatAppAppsecret);
        if (result.isSuccess()) {
            S_User user = S_User.findByAppOpenId(result.openid);
            if (user == null) {
                Call redirect_url = controllers.api.routes.ArticleController.allReaderInfoEntrance();
                String url = WeChatWebUserAuthorization.getGainCodeUrl_UserInfo(wechatAppAppid, redirect_url, state);
                return redirect(url);
            } else {
                loginAsUser(user);
                return redirect(controllers.api.routes.ArticleController.operate(stateObject.getInteger("id")));
            }
        } else {
            return controllers.SiteController.error("微信服务器错误");
        }
//        return ajaxSuccess("ok");
    }

    @Transactional
    public static Result allReaderInfoEntrance() throws Exception {
        String code = getParam("code");
        JSONObject state = JSON.parseObject(getParam("state"));
        UserInfoResult result = WeChatWebUserAuthorization.operate_UserInfo(code, wechatAppAppid, wechatAppAppsecret);
        if (result != null && result.isSuccess()) {
            S_User user = S_User.findByUnionid(result.unionid);
            if (user == null) {
                user = S_User.regist(result);
            } else {
                user.updateAppOpenId(result.openid);
            }
            loginAsUser(user);
            return redirect(controllers.api.routes.ArticleController.operate(state.getInteger("id")));
        } else {
            return controllers.SiteController.error("微信服务器错误");
        }
//        return ajaxSuccess("ok");
    }


    public static Result operate(int id) throws Exception {
        S_User user = currentUser();
        B_Article article = B_Article.find.byId(id);
        if (article == null) {
            return controllers.SiteController.error("不存在此文章");
        }
        if (user == null) {
            return controllers.SiteController.error("您未登录");
        }
        article.addVisitLog(user);
        return ok(publish_view.render(new PublishViewInfo(article),new JsShareInfo(article, request())));
    }
}
