package controllers;


import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import models.db1.user.S_User;
import play.db.ebean.Transactional;
import play.mvc.Result;
import util.Common;
import views.html.error.error_pc;
import views.html.error.error_mobile;


public class SiteController extends BaseController {

    @Transactional
    public static Result regist() throws Exception {
        JsonNode node = request().body().asJson();
        String username = node.get("username").asText();
        String password = node.get("password").asText();
        String telephone = node.get("telphone").asText();
        String telephoneCheckCode = node.get("telephoneCheckCode").asText();
        if (S_User.findByTelephone(telephone) != null) {
            return ajaxFail("电话号码已注册，请重新输入一个");
        }
        if (S_User.findByUsername(username) != null) {
            return ajaxFail("用户名已注册，请重新输入一个");
        }
        String checkCode = session("registCheckCode_" + telephone);
        if(checkCode==null||!checkCode.equals(telephoneCheckCode)){
            return ajaxFail("短信验证码错误");
        }



        S_User user = S_User.regist(username, password, telephone);
        loginAsUser(user);
        return ajaxSuccess("注册成功");
    }


    public static Result login() throws Exception {
        JsonNode node = request().body().asJson();
        String usernameAndTelphone = node.get("usernameAndTelphone").asText();
        String password = node.get("password").asText();
        S_User user = S_User.findByUsernameAndPassword(usernameAndTelphone, password);
        if (user == null) {
            user = S_User.findByTelphoneAndPassword(usernameAndTelphone, password);
        }
        if (user == null) {
            return ajaxFail("用户名，手机号或者密码错误");
        } else {
            loginAsUser(user);
            return ajaxSuccess("登录成功");
        }
    }


    public static Result logoutOperate() throws Exception {
        logout();
        return ajaxSuccess("退出成功");
    }


    public static Result getLoginInfo() throws Exception {
        JSONObject result = new JSONObject();
        S_User user = currentUser();
        if (user == null) {
            result.put("login", false);
        } else {
            result.put("login", true);
            result.put("username", user.getShowName());
            result.put("avatar", user.getShowAvatar());
            result.put("telephone", user.telephone);
            result.put("uuid", user.uuid);
        }
        return ajaxSuccess(result);
    }


    public static Result error(String msg) {
        if (Common.isPhoneVisit(request())) {
            return ok(error_pc.render(msg));
        } else {
            return ok(error_mobile.render(msg));
        }
    }
}
