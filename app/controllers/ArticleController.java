package controllers;


import beans.article.view.PublishViewInfo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;
import com.fasterxml.jackson.databind.JsonNode;
import models.db1.article.B_Article;
import models.db1.user.S_User;
import play.mvc.Result;
import play.mvc.With;
import security.UserLoginSecuredAction;

import java.util.List;


public class ArticleController extends BaseController {

    @With(UserLoginSecuredAction.class)
    public static Result articleEdit() throws Exception {
        JsonNode node = request().body().asJson();
        Boolean isNew = node.get("isNew").asBoolean();
        String title = node.get("title").asText();
        String content = node.get("content").asText();
        String coverImg = node.get("coverImg") == null ? null : node.get("coverImg").asText();
        Boolean isComment = node.get("isComment").asBoolean();
        Boolean isPublic = node.get("isPublic").asBoolean();

        if (isNew) {
            B_Article.create(title, content, isComment, isPublic, coverImg, currentUser());
            return ajaxSuccess("创建成功");
        } else {
            String uuid = node.get("uuid").asText();
            B_Article article = B_Article.findByUuid(uuid);
            if (article == null || article.isLogicDelete()) {
                return ajaxFail("文章不存在");
            }
            if (!article.owner.id.equals(currentUser().id)) {
                return ajaxFail("您没有编辑此文章的权限");
            }
            article.edit(title, content, isComment, isPublic, coverImg);
            return ajaxSuccess("编辑成功");
        }

    }

    @With(UserLoginSecuredAction.class)
    public static Result articleDelete() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Article article = B_Article.findByUuid(uuid);
        if (article == null) {
            return ajaxFail("文章不存在");
        }
        if (!article.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有删除此文章的权限");
        }
        article.delete();
        return ajaxSuccess("删除成功");
    }


    @With(UserLoginSecuredAction.class)
    public static Result getArticleEditInfo() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Article article = B_Article.findByUuid(uuid);

        if (article == null || article.isLogicDelete()) {
            return ajaxFail("文章不存在");
        }

        if (!article.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有编辑此文章的权限");
        }

        JSONObject result = new JSONObject();
        result.put("title", article.title);
        result.put("content", article.content);
        result.put("isComment", article.isComment);
        result.put("isPublic", article.isPublic);
        return ajaxSuccess(result);
    }


    @With(UserLoginSecuredAction.class)
    public static Result getArticleOwnerList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        PagingList<B_Article> pagingList = B_Article.find.where().eq("owner", currentUser()).eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        Page<B_Article> page = pagingList.getPage(pageNum - 1);
        List<B_Article> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Article info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("editedTime", info.editedTime);
            object.put("title", info.title);
//            object.put("content", info.content);
            object.put("isPublic", info.isPublic);
            object.put("isComment", info.isComment);
            object.put("coverImg", info.coverImg);
            object.put("url", info.getWechatShareLink());
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    @With(UserLoginSecuredAction.class)
    public static Result articleChangeIsComment() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Article article = B_Article.findByUuid(uuid);

        if (article == null || article.isLogicDelete()) {
            return ajaxFail("文章不存在");
        }

        if (!article.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有编辑此文章的权限");
        }

        Boolean isComment = node.get("isComment").asBoolean();
        article.changeIsComment(isComment);
        return ajaxSuccess("修改成功");
    }


    @With(UserLoginSecuredAction.class)
    public static Result articleChangeIsPublic() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Article article = B_Article.findByUuid(uuid);

        if (article == null || article.isLogicDelete()) {
            return ajaxFail("文章不存在");
        }

        if (!article.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有编辑此文章的权限");
        }

        Boolean isPublic = node.get("isPublic").asBoolean();
        article.changeIsPublic(isPublic);
        return ajaxSuccess("修改成功");
    }


//    @With(UserLoginSecuredAction.class)
    public static Result getArticleViewInfo() throws Exception {
        JsonNode node = request().body().asJson();
        int id = node.get("id").asInt();
        B_Article article = B_Article.find.byId(id);
        if (article == null) {
            return ajaxFail("不存在此文章");
        }
//        if (!article.isPublic) {
//            if (!article.owner.id.equals(currentUser().id)) {
//                return ajaxFail("你没有权限阅读此文章");
//            }
//        }
        article.addVisitLog(currentUser());
        return ajaxSuccess(new PublishViewInfo(article));
    }


}
