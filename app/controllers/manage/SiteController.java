package controllers.manage;

import beans.util.sms.SendSmsResult;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.BaseController;
import models.db1.user.S_User;
import play.mvc.Result;
import services.LuosimaoService;
import util.Common;
import util.cache.CacheUtil;
import views.html.manage.front_layout;

/**
 * Created by Administrator on 2016/12/19.
 */
public class SiteController extends BaseController {

    public static Result pageIndex() throws Exception {
        if (develop) {
            loginAsManager("18652442317");
        }
        return ok(front_layout.render());
    }


    public static Result sendLoginSms() throws Exception {
        JsonNode node = request().body().asJson();
        String tel = node.get("tel").asText();
        String telListStr = Common.getStringConfig("manage.manager.tel");
        String[] telList = telListStr.split(",");

        Boolean isHaving = false;
        for (String info : telList) {
            if (info.equals(tel)) {
                isHaving = true;
            }
        }

        final String cahceSendTimeStr = "sendTime" + "sendSms_TaskUserInput" + tel;
        String cacheSendTimeStr_result = (String) CacheUtil.get(cahceSendTimeStr);
        if (cacheSendTimeStr_result != null) {
//            System.out.println("60s内不能重发");
            return ajaxFail("60s内不能重发");
        }


        if (isHaving) {
            String securityCode = Common.randomString(6);
            String msg = "管理员登录密码:" + securityCode + "【唯页】";

            if (develop) {
                session("loginSms" + tel, securityCode);
                System.out.println(securityCode);
                CacheUtil.set(cahceSendTimeStr, "true", 60);
                return ajaxSuccess("发送成功");
            }

            SendSmsResult result = LuosimaoService.SendSMSTimely(tel, msg);
            if (result.isSuccess()) {
                session("loginSms" + tel, securityCode);
                CacheUtil.set(cahceSendTimeStr, "true", 60);
                return ajaxSuccess("发送成功");
            } else {
                return ajaxFail(result.getErrorMsg());
            }
        } else {
            return ajaxFail("你想干啥！！！！");
        }
    }


    public static Result login() throws Exception {
        JsonNode node = request().body().asJson();
        String tel = node.get("tel").asText();
        String sms = node.get("sms").asText();
        String loginSms = session("loginSms" + tel);
        if (loginSms == null || !loginSms.equals(sms)) {
            return ajaxFail("你想干啥！！！！");
        } else {
            loginAsManager(tel);
            return ajaxSuccess("登录成功");
        }
    }


    public static Result getLoginInfo() throws Exception {
        JSONObject result = new JSONObject();
        String manager = currentManager();
        if (manager == null) {
            result.put("login", false);
        } else {
            result.put("login", true);
        }
        return ajaxSuccess(result);
    }

}
