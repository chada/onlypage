package controllers.manage;

import beans.util.sms.SendSmsResult;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.BaseController;
import models.db1.task.B_Task;
import models.db1.user.coin.B_User_Coin_Log;
import play.mvc.Result;
import play.mvc.With;
import security.ManagerLoginSecuredAction;
import security.UserLoginSecuredAction;
import services.LuosimaoService;
import util.Common;
import util.cache.CacheUtil;
import views.html.manage.front_layout;
import wechat.pay.OrderManage;
import wechat.pay.beans.order.QueryOrderResult;

import java.util.List;

/**
 * Created by Administrator on 2016/12/19.
 */
@With(ManagerLoginSecuredAction.class)
public class TaskController extends BaseController {

    public static Result getTaskAllList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        PagingList<B_Task> pagingList = B_Task.find.where().eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        Page<B_Task> page = pagingList.getPage(pageNum - 1);
        List<B_Task> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("username", info.owner.getShowName());
            object.put("avatar", info.owner.getShowAvatar());
            object.put("editedTime", info.editedTime);
            object.put("title", info.title);
            object.put("endTime", info.endTime);
            object.put("coin", info.strategyInfo.coin);
            object.put("hasCost", info.strategyInfo.hasAllocationCoin);
            object.put("state", info.state);
            object.put("shareUrl", info.getStartUrl());
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    public static Result getTaskOrderInfo() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task task = B_Task.findByUuid(uuid);
        if(task==null){
            return ajaxFail("任务不存在");
        }
        String out_trade_no = task.getWechatRechargeOrderInfo();
        if(out_trade_no==null){
            return ajaxFail("订单数据不存在");
        }else{
            QueryOrderResult result = OrderManage.queryOrder(out_trade_no);
            if(result.isSuccess()){
                JSONObject data= new JSONObject();
                data.put("out_trade_no",result.out_trade_no);
                data.put("trade_state",result.trade_state);
                data.put("total_fee",result.total_fee);
                data.put("time_end",result.time_end.substring(0,4)+"年"+result.time_end.substring(4,6)+"月"+result.time_end.substring(6,8)+"日"
                        +result.time_end.substring(6,8)+"点"+result.time_end.substring(8,10)+"分"+result.time_end.substring(10,12)+"秒");
                data.put("coin",task.strategyInfo.coin);
                return ajaxSuccess(data);
            }else{
                return ajaxFail(result.getErrorMsg());
            }
        }
    }

}
