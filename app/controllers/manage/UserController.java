package controllers.manage;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.BaseController;
import controllers.util.UrlController;
import models.db1.task.B_Task;
import models.db1.user.S_User;
import play.mvc.Result;
import play.mvc.With;
import security.ManagerLoginSecuredAction;
import wechat.pay.OrderManage;
import wechat.pay.beans.order.QueryOrderResult;

import java.util.List;

/**
 * Created by Administrator on 2016/12/19.
 */
@With(ManagerLoginSecuredAction.class)
public class UserController extends BaseController {

    public static Result getUserAllList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        PagingList<S_User> pagingList = S_User.find.where().eq("deleteLogic", false).order("createdTime asc").findPagingList(pageSize);
        Page<S_User> page = pagingList.getPage(pageNum - 1);
        List<S_User> list = page.getList();
        JSONArray array = new JSONArray();
        for (S_User info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("username", info.getShowName());
            object.put("avatar", info.getShowAvatar());
            object.put("createdTime", info.createdTime);
            object.put("coin", info.account.coin);
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }

    //用户模拟登录
    public static Result login() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        S_User user = S_User.findByUuid(uuid);
        if(user==null){
            return ajaxFail("用户不存在");
        }else{
            loginAsUser(user);
            String url = UrlController.serverAddress;
            return ajaxSuccess(url);
        }
    }
}
