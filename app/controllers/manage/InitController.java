package controllers.manage;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import controllers.BaseController;
import models.db1.article.B_Article;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Log;
import models.db1.task.schedule.B_Task_User_Statistics_Data;
import models.db1.task.strategy.B_Task_Strategy;
import models.db1.task.strategy.B_Task_Strategy_Info;
import models.db1.task.view.B_Task_View_Tag;
import models.db1.user.B_User_Account;
import models.db1.user.B_User_Info;
import models.db1.user.B_User_Wechat_Info;
import models.db1.user.S_User;
import models.db1.user.coin.B_User_Coin_Log;
import play.db.ebean.Transactional;
import play.mvc.Result;
import util.Common;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2016/12/14.
 */
public class InitController extends BaseController {
    @Transactional
    public static Result dataMigration() throws Exception {
        if (develop) {
            userMigration();
            articleMigration();
            taskMigration();
            taskPropagationMigration();
        }
        return ajaxSuccess("迁移成功");
    }


    public static void userMigration() throws Exception {
        List<SqlRow> sqlRowList = Ebean.getServer("two").createSqlQuery("select " +
                "user.created_time,user.id,user.username,user.state,user.salt,user.password,user.phone," +
                "wechat.openid,wechat.unionid,wechat.subscribe,wechat.nickname,wechat.sex,wechat.language,wechat.city," +
                "wechat.province,wechat.country,wechat.headimgurl,wechat.subscribe_time,wechat.remark,wechat.groupid," +
                "account.coin " +
                " from s_user user " +
                "LEFT JOIN b_user_wechat_info wechat on wechat.owner_id = user.id " +
                "LEFT JOIN b_user_info account on account.owner_id = user.id " +
                "order by user.id").setMaxRows(300000).findList();

//        List<SqlRow> sqlRowList = Ebean.getServer("two").createSqlQuery("select " +
//                "user.id,user.username " +
//                " from s_user user " +
//                "" +
//                "order by user.id").setMaxRows(10).findList();

        for (SqlRow sqlRow : sqlRowList) {
            System.out.println(sqlRow);
            int state = sqlRow.getInteger("state");
            int id = sqlRow.getInteger("id");
            String username = sqlRow.getString("username");
            String salt = sqlRow.getString("salt");
            String password = sqlRow.getString("password");
            String phone = sqlRow.getString("phone");
            Date created_time = sqlRow.getDate("created_time");
            String appOpenid = sqlRow.getString("openid");
            String unionid = sqlRow.getString("unionid");
            Integer subscribe = sqlRow.getInteger("subscribe");
            String nickname = sqlRow.getString("nickname");
            Integer sex = sqlRow.getInteger("sex");
            String language = sqlRow.getString("language");
            String city = sqlRow.getString("city");
            String province = sqlRow.getString("province");
            String country = sqlRow.getString("country");
            String headimgurl = sqlRow.getString("headimgurl");
            Date subscribe_time = sqlRow.getDate("subscribe_time");
            String remark = sqlRow.getString("remark");
            Integer groupid = sqlRow.getInteger("groupid");

            S_User user = null;

            if (state == 1 || state == 0) {
                //平台账号
                user = S_User.migration(username, password, phone, salt, id, created_time);
            } else if (state == 2) {
                user = S_User.migration(null, null, null, salt, id, created_time);
                B_User_Wechat_Info.migration(user, appOpenid, unionid, subscribe, nickname, sex, language, city, province, country, headimgurl, subscribe_time, remark, groupid, created_time);
            } else if (state == 3) {
                user = S_User.migration(null, null, null, salt, id, created_time);
                B_User_Wechat_Info.migration(user, appOpenid, unionid, subscribe, nickname, sex, language, city, province, country, headimgurl, subscribe_time, remark, groupid, created_time);
            } else if (state == 4) {
                user = S_User.migration(username, password, phone, salt, id, created_time);
                B_User_Wechat_Info.migration(user, appOpenid, unionid, subscribe, nickname, sex, language, city, province, country, headimgurl, subscribe_time, remark, groupid, created_time);
            }

            user.refresh();
            Integer coin = sqlRow.getInteger("coin");
            coin = coin < 0 ? 0 : coin;
            user.account.addCoin(coin);
            B_User_Coin_Log.create_SystemMigration(user, coin);


        }
    }


    public static void articleMigration() throws Exception {
        List<SqlRow> sqlRowList = Ebean.getServer("two").createSqlQuery("select " +
                "article.created_time,article.id,article.title,article.article,article.is_commentable,article.is_publish,article.first_picture_in_text," +
                "wechat.openid,user.username,user.id userId " +
                "from b_article article " +
                "left join s_user user on article.owner_id = user.id " +
                "left join b_user_wechat_info wechat on wechat.owner_id = user.id").findList();
        for (SqlRow sqlRow : sqlRowList) {
            System.out.println(sqlRow);
            int id = sqlRow.getInteger("id");
            int userId = sqlRow.getInteger("userId");
            String content = sqlRow.getString("article");
            String title = sqlRow.getString("title");
            Boolean isComment = sqlRow.getBoolean("is_commentable");
            Boolean isPublic = sqlRow.getBoolean("is_publish");
            String coverImg = sqlRow.getString("first_picture_in_text");
            String openid = sqlRow.getString("openid");
            String username = sqlRow.getString("username");
            Date created_time = sqlRow.getDate("created_time");
            S_User user = S_User.findByMigrationid(userId);
            if (user == null) {
                throw new Exception("用户没有找到");
            }
            if (coverImg.length() > 255) {
                coverImg = null;
            }
            B_Article.migration(title, content, isComment, isPublic, coverImg, user, id, created_time);
        }
    }


    public static void taskMigration() throws Exception {
        List<SqlRow> sqlRowList = Ebean.getServer("two").createSqlQuery("select " +
                "task.created_time,task.id,task.owner_id,task.title,task.description,task.img_url,task.end_time,task.type,task.data,task.visible_level,task.state," +
                "one.reward,one.allocation_coin " +
                "from b_task task " +
                "RIGHT JOIN b_task_mode_one one on task.id =one.task_id").findList();
        for (SqlRow sqlRow : sqlRowList) {
            System.out.println(sqlRow);
            int id = sqlRow.getInteger("id");
            int owner_id = sqlRow.getInteger("owner_id");
            S_User user = S_User.findByMigrationid(owner_id);
            String title = sqlRow.getString("title");
            String remark = sqlRow.getString("description");
            String description = sqlRow.getString("description");
            String coverImg = sqlRow.getString("img_url");
            Date endTime = sqlRow.getDate("end_time");
            Date created_time = sqlRow.getDate("created_time");
            String data = sqlRow.getString("data");
            int type = sqlRow.getInteger("type");
            B_Task.DataType dataType = B_Task.DataType.url;
            B_Article article = null;
            if (type == 1) {
                dataType = B_Task.DataType.url;
            } else if (type == 2) {
                int article_id = Integer.valueOf(data.split("article_id=")[1].split("&")[0]);
                article = B_Article.fingByMigrationId(article_id);
                data = null;
                dataType = B_Task.DataType.article;
            }

            int visible_level = sqlRow.getInteger("visible_level");
            B_Task.VisibleType visibleType = B_Task.VisibleType.all;
            if (visible_level == 0) {
                visibleType = B_Task.VisibleType.all;
            } else if (visible_level == 1) {
                visibleType = B_Task.VisibleType.friend;
            }
            B_Task task = B_Task.migtation(user, title, remark, coverImg, endTime, dataType, data, article, visibleType, B_Task.State.finish, id, created_time,description);

            int reward = sqlRow.getInteger("reward");
            int allocation_coin = sqlRow.getInteger("allocation_coin");
            B_Task_Strategy_Info.migtation(reward, allocation_coin, task, created_time);
            B_Task_View_Tag.migration(task);
        }
    }


    public static void taskPropagationMigration() throws Exception {
        List<SqlRow> sqlRowList = Ebean.getServer("two").createSqlQuery("SELECT " +
                "log.created_time,log.executant_id,log.pre_executant_id,log.level,log.allocation_coin," +
                "task.id " +
                "from b_task_mode_one_complete_log log " +
                "LEFT JOIN b_task_mode_one one on one.id = log.one_id  " +
                "LEFT JOIN b_task task on task.id = one.task_id ").findList();
        int count = 0;
        for (SqlRow sqlRow : sqlRowList) {
            count++;
            System.out.println(count + ":" + sqlRow);
            Date created_time = sqlRow.getDate("created_time");
            int allocation_coin = sqlRow.getInteger("allocation_coin");
            int executant_id = sqlRow.getInteger("executant_id");
            int pre_executant_id = sqlRow.getInteger("pre_executant_id");
            int level = sqlRow.getInteger("level");
            int taskId = sqlRow.getInteger("id");

            B_Task task = B_Task.findByMigtationId(taskId);
            S_User preExecutant = getUser(pre_executant_id);
            S_User executant = getUser(executant_id);
            B_Task_Propagation_Log.migration(task, preExecutant, executant, level, created_time);
            B_Task_User_Statistics_Data.migration(executant, task, allocation_coin, created_time);
        }
    }

    private static HashMap<Integer, S_User> userHashMap = new HashMap<>();

    private static S_User getUser(int id) {
        S_User user = userHashMap.get(id);
        if (user == null) {
            user = S_User.findByMigrationid(id);
            userHashMap.put(id, user);
        }
        return user;
    }

}
