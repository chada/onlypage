package controllers;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.util.UrlController;
import models.db1.user.S_User;
import models.db1.wechat.B_Wechat_App;
import play.mvc.Result;
import play.mvc.With;
import security.UserLoginSecuredAction;
import util.Common;
import wechat.bean.thirdPartyPlatform.AuthorizerAccessTokenAndRefreshTokenData;
import wechat.bean.thirdPartyPlatform.WechatAppAccountInfoData;
import wechat.open.ThirdPartyPlatform.ThirdPartyPlatformUtil;
import wechat.open.web.LoginService;

import java.util.List;

import static com.avaje.ebean.Expr.in;


public class WechatController extends BaseController {
    final static String wechatAppAppid = Common.getStringConfig("wechat.app.appid");
    final static String wechatAppAppsecret = Common.getStringConfig("wechat.app.appsecret");
    final static String wechatOpenWebAppid = Common.getStringConfig("wechat.open.web.appid");
    final static String wechatOpenWebAppsecret = Common.getStringConfig("wechat.open.web.appsecret");


    //获取微信开放平台网站登录初始跳转路径
    public static Result getWechatOpenWebLoginUrl() {
        String redirect_uri = Common.getStringConfig("server.address") + "/api/wechat/open/web/getUserInfo";
        String state = "state";
        return ajaxSuccess(LoginService.getUrl(wechatOpenWebAppid, redirect_uri, state));
    }


    public static Result getWechatOpenWebLoginInfo() {
        JSONObject object = new JSONObject();
        String redirect_uri = Common.getStringConfig("server.address") + "/api/wechat/open/web/getUserInfo";
        object.put("appid",wechatOpenWebAppid);
        object.put("scope","snsapi_login");
        object.put("redirect_uri",redirect_uri);
        object.put("state","");
        return ajaxSuccess(object);
    }


    //获取微信第三方平台授权的跳转url地址
    public static Result getThirdPartyPlatformBindWechatAppPageUrl() {
        String result = ThirdPartyPlatformUtil.getComponentLoginUrl();
        if(result==null){
            return ajaxFail("系统数据错误");
        }else{
            return ajaxSuccess(result);
        }
    }

    /*
    * 创建公众号函数（非常重要）
    * 通过auth_code可以获取公众号的一系列信息
    * 进入此函数,代表公众号已经托管
    * */
    public static Result wechatAppComponentLoginPageRedirect() throws Exception {
        String auth_code = getParam("auth_code");
        String expires_in = getParam("expires_in");
        S_User user = currentUser();
        if (user == null) {
            return controllers.SiteController.error("您还没有登录，无法创建");
        }
        AuthorizerAccessTokenAndRefreshTokenData authorizerAccessTokenAndRefreshTokenData = ThirdPartyPlatformUtil.getAuthorizerAccessTokenAndRefreshToken(auth_code);
        WechatAppAccountInfoData wechatAppAccountInfoData = ThirdPartyPlatformUtil.getWechatAppAccountInfo(authorizerAccessTokenAndRefreshTokenData.authorizer_appid);
        B_Wechat_App.Create(user, authorizerAccessTokenAndRefreshTokenData, wechatAppAccountInfoData);
        return redirect(UrlController.WechatAppAfterCreateRedirectUrl);
    }


    //第三方平台托管微信号列表
    @With(UserLoginSecuredAction.class)
    public static Result wechatAppList() {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        S_User user = currentUser();

        PagingList<B_Wechat_App> pagingList = B_Wechat_App.find.where().eq("owner", user).findPagingList(pageSize);
        Page<B_Wechat_App> page = pagingList.getPage(pageNum - 1);
        List<B_Wechat_App> apps = page.getList();

        JSONArray result = new JSONArray();
        for (B_Wechat_App app : apps) {
            JSONObject object = new JSONObject();
            object.put("nick_name", app.nick_name);
            object.put("service_type_info", app.service_type_info);
            object.put("verify_type_info", app.verify_type_info);
            object.put("head_img", app.head_img);
            object.put("id", app.id);
            object.put("isOwner", true);
            object.put("uuid", app.uuid);

            result.add(object);
        }
        return ajaxSuccess(result);
    }



    //第三方平台托管微信号列表
    @With(UserLoginSecuredAction.class)
    public static Result getWechatAppBaseInfo() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        S_User user = currentUser();
        B_Wechat_App app =B_Wechat_App.findByUuid(uuid);

        if(!user.id.equals(app.owner.id)){
            return ajaxFail("你不是该公众号的管理员，没有权限查看公众号信息");
        }
        JSONObject result = new JSONObject();
        result.put("id", app.id);
        result.put("uuid", app.uuid);
        result.put("nick_name", app.nick_name);
        result.put("head_img", app.head_img);
        result.put("service_type_info", app.service_type_info);
        result.put("verify_type_info", app.verify_type_info);
        result.put("user_name", app.user_name);
        result.put("alias", app.alias);
        result.put("qrcode_url", app.qrcode_url);
        result.put("appid", app.appid);
        return ajaxSuccess(result);
    }


    //第三方平台托管微信号列表
    @With(UserLoginSecuredAction.class)
    public static Result getWechatAppMenuInfo() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        S_User user = currentUser();
        B_Wechat_App app =B_Wechat_App.findByUuid(uuid);

        if(!user.id.equals(app.owner.id)){
            return ajaxFail("你不是该公众号的管理员，没有权限查看公众号信息");
        }
        JSONObject result = new JSONObject();
        result.put("id", app.id);
        result.put("uuid", app.uuid);
        result.put("nick_name", app.nick_name);
        result.put("head_img", app.head_img);
        result.put("service_type_info", app.service_type_info);
        result.put("verify_type_info", app.verify_type_info);
        result.put("user_name", app.user_name);
        result.put("alias", app.alias);
        result.put("qrcode_url", app.qrcode_url);
        result.put("appid", app.appid);
        return ajaxSuccess(result);
    }

}
