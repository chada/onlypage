package controllers.plugins;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.BaseController;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Info;
import models.db1.task.B_Task_Propagation_Log;
import play.mvc.Result;
import play.mvc.With;
import security.UserLoginSecuredAction;
import views.html.plugins.onlypageFormSubmit;
import views.html.plugins.pinganCreditCardApplyInfoSubmit;

/**
 * Created by whk on 2017/8/7.
 */
public class OnlypageFormSubmitController extends BaseController {
    public static Result page() throws Exception {
        int taskId=getIntParam("taskId");
        String redirectUrl=getParam("redirectUrl");
//        B_Task_Propagation_Info taskPropagationInfo=B_Task_Propagation_Info.findByPropagationLog(B_Task_Propagation_Log.findByTaskAndExecutant(B_Task.find.where().eq("id", taskId).findUnique(),currentUser()));
//        if(taskPropagationInfo==null) {
            return ok(onlypageFormSubmit.render(taskId, redirectUrl));
//        }else {
//            return redirect(redirectUrl);
//        }
    }
    public static Result pinganCreditCardApplyInfoSubmitPage() throws Exception {
        int taskId=getIntParam("taskId");
        String redirectUrl=getParam("redirectUrl");
        return ok(pinganCreditCardApplyInfoSubmit.render(taskId, redirectUrl));

    }

    @With(UserLoginSecuredAction.class)
    public static Result createPropagationInfo() throws Exception {
        String taskId = getPostParam("taskId");
        String realName = getPostParam("realName");
        String telePhone = getPostParam("telePhone");
        B_Task task =  B_Task.find.where().eq("id", taskId).findUnique();
        if (task == null || task.isLogicDelete()) {
            return ajaxFail("任务不存在");
        }

        B_Task_Propagation_Log propagationLog =  B_Task_Propagation_Log.findByTaskAndExecutant(task,currentUser());
        if (propagationLog == null || propagationLog.isLogicDelete()) {
            return ajaxFail("传播节点不存在");
        }
        B_Task_Propagation_Info taskPropagationInfo=B_Task_Propagation_Info.create(task,propagationLog,realName,telePhone);

        return ajaxSuccess(true);
    }

}
