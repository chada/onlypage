package controllers;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.routes;
import controllers.util.UrlController;
import models.db1.user.S_User;
import models.db1.user.coin.B_User_Coin_In_Log;
import models.db1.user.coin.B_User_Coin_Log;
import models.db1.wechat.B_Wechat_Withdraw;
import play.db.ebean.Transactional;
import play.mvc.Call;
import play.mvc.Result;
import play.mvc.With;
import security.UserLoginSecuredAction;
import util.Common;
import util.cache.CacheUtil;
import wechat.other.webUserAuthorization.UserInfoResult;
import wechat.other.webUserAuthorization.WeChatWebUserAuthorization;
import wechat.pay.WithDrawDepositManage;
import wechat.pay.beans.withdrawDeposit.WithdrawDepositResult;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by Administrator on 2016/12/6.
 */
public class WithdrawDepositController extends BaseController {

    @Transactional
    @With(UserLoginSecuredAction.class)
    public static Result wechatMobile() throws Exception {
        int coin = getIntPostParam("coin");
        if (coin < 100) {
            return ajaxFail("提现金额必须大于1元");
        }
        S_User user = currentUser();
        if (coin > user.account.coin) {
            return ajaxFail("提现金额超出您的账户总额");
        }

        int todayCount = B_Wechat_Withdraw.findTodaySuccessCount(user);
        if (todayCount >= 10) {
            return ajaxFail("提现次数已满，请明天再试");
        }
        if (user.wechatInfo == null || user.wechatInfo.appOpenid == null) {
            return ajaxFail("缺少微信信息，无法提现");
        }


        String nonce_str = Common.randomString(32).toUpperCase();
        String partner_trade_no = B_Wechat_Withdraw.getOnePartnerTradeNo();
        String openid = user.wechatInfo.appOpenid;
        int amount = coin;
        String spbill_create_ip = request().remoteAddress();
        String desc = "唯页平台";
        user.account.reduceCoin(coin);

        B_Wechat_Withdraw withdraw = B_Wechat_Withdraw.create(user, nonce_str, partner_trade_no, openid, amount, desc);

        WithdrawDepositResult result = WithDrawDepositManage.transfers(nonce_str, partner_trade_no, openid, amount, desc, spbill_create_ip);

        if (result.isSuccess()) {
            withdraw.success(result.payment_no, result.payment_time);
            B_User_Coin_Log.create_wehcatWitddrawDeposit(user, amount, withdraw);
            return ajaxSuccess("提现成功");
        } else {
            withdraw.fail(result.getErrorMsg());
            user.account.addCoin(coin);
            return ajaxFail(result.getErrorMsg());
        }


    }


    @With(UserLoginSecuredAction.class)
    public static Result checkCode() {
        S_User user = currentUser();
        JsonNode node = request().body().asJson();
        String checkCode = node.get("checkCode").asText();
        String code = session("pcWithdrawDepositCheckCode_" + user.telephone);
        if (code == null || !code.contentEquals(checkCode)) {
            return ajaxFail("验证码错误");
        } else {
            session("pcWithdrawDepositCheckCodeResult", "true");
            String randomStr = Common.randomString(20);
            session("pcWithdrawDepositCheckQrcode", randomStr);

            Call redirect_url = controllers.routes.WithdrawDepositController.checkQrcode();
            String url = WeChatWebUserAuthorization.getGainCodeUrl_UserInfo(wechatAppAppid, redirect_url, randomStr);
            return ajaxSuccess(url);
        }
    }


    public static Result checkQrcode() throws IOException {
        String code = getParam("code");
        String state = getParam("state");

        UserInfoResult result = WeChatWebUserAuthorization.operate_UserInfo(code, wechatAppAppid, wechatAppAppsecret);
        if (result != null && result.isSuccess()) {
            CacheUtil.set(state, JSON.toJSONString(result), 5 * 60);
            return ok(views.html.util.other.operate_success.render());
        } else {
            return controllers.SiteController.error("微信服务器错误,请重新扫描二维码");
        }
    }

    public static Result getUserInfo() {
        String randomStr = session("pcWithdrawDepositCheckQrcode");
        if (randomStr == null) {
            return ajaxFail("您还没有通过短信验证");
        }
        UserInfoResult userInfoResult = JSON.parseObject((String) CacheUtil.get(randomStr), UserInfoResult.class);
        if (userInfoResult == null || userInfoResult.openid == null) {
            return ajaxFail("您还未扫描二维码");
        }
        JSONObject result = new JSONObject();
        result.put("username", userInfoResult.nickname);
        result.put("avatar", userInfoResult.headimgurl);
        result.put("coin", currentUser().account.coin);
        return ajaxSuccess(result);
    }

    @Transactional
    @With(UserLoginSecuredAction.class)
    public static Result pcOperate() throws Exception {
        String randomStr = session("pcWithdrawDepositCheckQrcode");
        if (randomStr == null) {
            return ajaxFail("您还没有通过短信验证");
        }
        UserInfoResult userInfoResult = JSON.parseObject((String) CacheUtil.get(randomStr), UserInfoResult.class);
        if (userInfoResult == null || userInfoResult.openid == null) {
            return ajaxFail("用户信息数据错误");
        }
        JsonNode node = request().body().asJson();
        int coin = node.get("coin").asInt();
        if (coin < 100) {
            return ajaxFail("提现金额必须大于1元");
        }
        S_User user = currentUser();
        if (coin > user.account.coin) {
            return ajaxFail("提现金额超出您的账户总额");
        }
        int todayCount = B_Wechat_Withdraw.findTodaySuccessCount(user);
        if (todayCount >= 10) {
            return ajaxFail("提现次数已满，请明天再试");
        }


        String nonce_str = Common.randomString(32).toUpperCase();
        String partner_trade_no = B_Wechat_Withdraw.getOnePartnerTradeNo();
        String openid = userInfoResult.openid;
        int amount = coin;
        String spbill_create_ip = request().remoteAddress();
        String desc = "唯页平台";
        user.account.reduceCoin(coin);

        B_Wechat_Withdraw withdraw = B_Wechat_Withdraw.create(user, nonce_str, partner_trade_no, openid, amount, desc);

        WithdrawDepositResult result = WithDrawDepositManage.transfers(nonce_str, partner_trade_no, openid, amount, desc, spbill_create_ip);

        if (result.isSuccess()) {
            withdraw.success(result.payment_no, result.payment_time);
            B_User_Coin_Log.create_wehcatWitddrawDeposit(user, amount, withdraw);
            return ajaxSuccess("提现成功");
        } else {
            withdraw.fail(result.getErrorMsg());
            user.account.addCoin(coin);
            return ajaxFail(result.getErrorMsg());
        }

    }
}
