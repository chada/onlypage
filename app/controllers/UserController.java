package controllers;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.avaje.ebean.Expr;
import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.util.UrlController;
import models.db1.article.B_Article;
import models.db1.task.B_Task;
import models.db1.user.B_User_Info;
import models.db1.user.S_User;
import models.db1.user.coin.B_User_Coin_In_Log;
import models.db1.user.coin.B_User_Coin_Log;
import models.db1.user.tag.B_User_Tag;
import models.db1.user.tag.B_User_Tag_Enum;
import models.db1.wechat.B_Wechat_Pay;
import org.h2.util.StringUtils;
import org.jsoup.helper.StringUtil;
import play.db.ebean.Transactional;
import play.mvc.Result;
import play.mvc.With;
import security.UserLoginSecuredAction;
import util.Common;
import wechat.pay.OrderManage;
import wechat.pay.beans.order.UnifiedOrderResult;


import java.util.ArrayList;
import java.util.List;

public class UserController extends BaseController {
    public static int customizeTagCountLimit = Common.getIntConfig("customizeTagCountLimit", 50);

    //将修改后的个人资料上传到数据库
    public static Result savePersonInfo() {
        JsonNode node = request().body().asJson();
        String nickname = node.get("nickname").asText();
        String avatar = node.get("avatar").asText();
        String remark = node.get("remark").asText();
        S_User owner = currentUser();
        B_User_Info userInfo = B_User_Info.findByOwner(owner);
        if (userInfo == null) {
            userInfo = B_User_Info.create(owner);
            userInfo.avatar = avatar.equals("") ? null : avatar;
            userInfo.nickname = nickname.equals("") ? null : nickname;
            userInfo.remark = remark.equals("") ? null : remark;
            userInfo.save();
        } else {
            userInfo.avatar = avatar.equals("") ? null : avatar;
            userInfo.nickname = nickname.equals("") ? null : nickname;
            userInfo.remark = remark.equals("") ? null : remark;
            userInfo.save();
        }

        return ajaxSuccess("保存成功");
    }

    //获取个人信息：昵称、头像、个性签名、手机号、余额
    public static Result getPersonInfo() {
        JSONObject object = new JSONObject();
        S_User user = currentUser();
        B_User_Info userInfo = B_User_Info.findByOwner(user);
        if (userInfo != null) {
            object.put("nickname", user.getShowName());
            object.put("avatar", user.getShowAvatar());
            object.put("remark", userInfo.remark);
            object.put("telephone", userInfo.owner.telephone);
            object.put("remain", userInfo.owner.account.coin);
        }
        return ajaxSuccess(object);
    }


    //用户账单列表
    public static Result getCoinLogList() {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String tag = node.get("tag").asText();

        ExpressionList expressionList = B_User_Coin_Log.find.where().eq("owner", currentUser()).eq("deleteLogic", false);
        if ("search".equals(tag)) {
            String type = node.get("type").asText();
            String startTime = node.get("createdTime").asText();
            String editedTime = node.get("editedTime").asText();
            B_User_Coin_Log.Type type1 = null;
            if ("in".equals(type)) {
                type1 = B_User_Coin_Log.Type.in;
            }
            if ("out".equals(type)) {
                type1 = B_User_Coin_Log.Type.out;
            }
            if (type1 != null) {
                expressionList = expressionList.eq("type", type1);
            }
            if (!startTime.equals("null")) {
                expressionList = expressionList.gt("createdTime", startTime);
            }
            if (!editedTime.equals("null")) {
                expressionList = expressionList.lt("createdTime", editedTime);
            }

        }
        PagingList<B_User_Coin_Log> pagingList = expressionList.order("createdTime desc").findPagingList(pageSize);
        Page<B_User_Coin_Log> page = pagingList.getPage(pageNum - 1);
        List<B_User_Coin_Log> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_User_Coin_Log info : list) {
            JSONObject object = new JSONObject();
            object.put("type", info.type);
            object.put("remark", "");
            object.put("createdTime", info.createdTime);
            object.put("editedTime", info.editedTime);
            if (info.type==B_User_Coin_Log.Type.out) {
                object.put("state", info.outLog.type);
            } else if (info.type==B_User_Coin_Log.Type.in) {
                object.put("state", info.inLog.type);
            }
            object.put("coin", info.coin);
            object.put("currCoin", info.currCoin);
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    //用户在任务中获取的红包金额列表
    public static Result userGainMoney() {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String tag = node.get("tag").asText();
        ExpressionList expressionList = B_User_Coin_In_Log.find.fetch("log").where().eq("type", B_User_Coin_In_Log.Type.TaskAcquisition).eq("log.owner", currentUser()).eq("deleteLogic", false);
        if ("search".equals(tag)) {
            String title = node.get("title").asText();
            expressionList = expressionList.like("task.title", "%" + title + "%");
        }
        PagingList<B_User_Coin_In_Log> pagingList = expressionList.order("createdTime desc").findPagingList(pageSize);

        Page<B_User_Coin_In_Log> page = pagingList.getPage(pageNum - 1);
        List<B_User_Coin_In_Log> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_User_Coin_In_Log info : list) {
            JSONObject object = new JSONObject();
            object.put("title", info.task.title);
            object.put("editedTime", info.editedTime);
            object.put("coin", info.log.coin);
            object.put("viewUrl", info.task.getViewtUrl());
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    //当前用户在任务中发布的红包列表
    public static Result userSendMoney() {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String tag = node.get("tag").asText();

        PagingList<B_Task> pagingList = null;
        if ("tag".equals(tag)) {
            pagingList = B_Task.find.where().eq("owner", currentUser()).eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);

        } else {
            String title = node.get("title").asText();

            String stateName = node.get("state").asText();
            B_Task.State state = null;
            if ("init".equals(stateName)) {
                state = B_Task.State.init;
            }
            if ("checking".equals(stateName)) {
                state = B_Task.State.checking;
            }
            if ("publish".equals(stateName)) {
                state = B_Task.State.publish;
            }
            if ("finish".equals(stateName)) {
                state = B_Task.State.finish;
            }
            if ("refuse".equals(stateName)) {
                state = B_Task.State.refuse;
            }
            if ("exception".equals(stateName)) {
                state = B_Task.State.exception;
            }

            ExpressionList expressionList = B_Task.find.where().eq("owner", currentUser()).eq("deleteLogic", false);

            if (!title.equals("null")) {

                expressionList = expressionList.like("title", "%" + title + "%");
            }
            if (state != null) {

                expressionList = expressionList.eq("state", state);
            }

            pagingList = expressionList.order("createdTime desc").findPagingList(pageSize);
        }
        Page<B_Task> page = pagingList.getPage(pageNum - 1);
        List<B_Task> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task info : list) {
            JSONObject object = new JSONObject();
            object.put("title", info.title);
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("endTime", info.endTime);
            object.put("coin", info.strategyInfo.coin);
            object.put("hasCost", info.strategyInfo.hasAllocationCoin);
            object.put("state", info.state);

            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    //修改当前用户的手机号码
    @With(UserLoginSecuredAction.class)
    public static Result changeTelephone() {
        JsonNode node = request().body().asJson();
        String phone = node.get("phone").asText();
        String checkCode = node.get("checkCode").asText();
        S_User user = currentUser();
        if (user.telephone != null) {
            String checkOldTelephoneResult = session("checkOldTelephoneResult");
            if (checkOldTelephoneResult == null || !checkOldTelephoneResult.equals("true")) {
                return ajaxFail("旧电话号码未经过验证");
            }
        }

        String changeTelephoneCheckCode = session("changeTelephoneCheckCode_" + phone);
        if (changeTelephoneCheckCode != null && changeTelephoneCheckCode.equals(checkCode)) {
            user.changeTelephone(phone);
            return ajaxSuccess("修改成功");
        } else {
            return ajaxFail("验证码错误");
        }
    }

    @With(UserLoginSecuredAction.class)
    public static Result checkOldTelephone() {
        JsonNode node = request().body().asJson();
        String code = node.get("checkCode").asText();
        String checkCode = session("checkOldTelephone");
        if (code != null && code.equals(checkCode)) {
            session("checkOldTelephoneResult", "true");
            return ajaxSuccess("验证通过");
        } else {
            return ajaxFail("验证失败");
        }
    }

    //获取个人信息：昵称、头像、个性签名、手机号
    @With(UserLoginSecuredAction.class)
    public static Result getUserInfoByUUID() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        JSONObject object = new JSONObject();
        S_User user = S_User.findByUuid(uuid);
        B_User_Info userInfo = B_User_Info.findByOwner(user);
        if (userInfo != null) {
            object.put("nickname", user.getShowName());
            object.put("avatar", user.getShowAvatar());
            object.put("remark", userInfo.remark);
            object.put("telephone", userInfo.owner.telephone);
        }
        return ajaxSuccess(object);
    }

    //根据uuid获取用户发布的红包列表
    public static Result getReleaseTaskListByUserUUID() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String tag = node.get("tag").asText();

        PagingList<B_Task> pagingList = null;
        if ("tag".equals(tag)) {
            pagingList = B_Task.find.where().eq("owner", S_User.findByUuid(uuid)).eq("deleteLogic", false).or(Expr.eq("state",B_Task.State.publish),Expr.eq("state",B_Task.State.finish)).order("createdTime desc").findPagingList(pageSize);
        } else {
            String title = node.get("title").asText();
            String stateName = node.get("state").asText();
            B_Task.State state = null;
            if ("publish".equals(stateName)) {
                state = B_Task.State.publish;
            }
            if ("finish".equals(stateName)) {
                state = B_Task.State.finish;
            }

            ExpressionList expressionList = B_Task.find.where().eq("owner", S_User.findByUuid(uuid)).eq("deleteLogic", false);

            if (!title.equals("null")) {
                expressionList = expressionList.like("title", "%" + title + "%");
            }

            if (state != null) {
                expressionList = expressionList.eq("state", state);
            }else {
                expressionList=expressionList.or(Expr.eq("state",B_Task.State.publish),Expr.eq("state",B_Task.State.finish));
            }

            pagingList = expressionList.order("createdTime desc").findPagingList(pageSize);
        }
        Page<B_Task> page = pagingList.getPage(pageNum - 1);
        List<B_Task> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task info : list) {
            JSONObject object = new JSONObject();
            object.put("title", info.title);
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("endTime", info.endTime);
            object.put("state", info.state);
            object.put("viewUrl", info.getViewtUrl());
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }

    //根据uuid获取用户发布的文章列表
    public static Result getReleaseArticleListByUserUUID() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String tag = node.get("tag").asText();

        PagingList<B_Article> pagingList = null;
        if ("tag".equals(tag)) {
            pagingList = B_Article.find.where().eq("owner", S_User.findByUuid(uuid)).eq("deleteLogic", false).eq("isPublic",true).order("editedTime desc").findPagingList(pageSize);
        } else {
            String title = node.get("title").asText();
            ExpressionList expressionList = B_Task.find.where().eq("owner", S_User.findByUuid(uuid)).eq("deleteLogic", false).eq("isPublic",true);

            if (!title.equals("null")) {
                expressionList = expressionList.like("title", "%" + title + "%");
            }
            pagingList = expressionList.order("editedTime desc").findPagingList(pageSize);
        }
        Page<B_Article> page = pagingList.getPage(pageNum - 1);
        List<B_Article> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Article info : list) {
            JSONObject object = new JSONObject();
            object.put("title", info.title);
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("editedTime", info.editedTime);
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    //获取用户列表
    @With(UserLoginSecuredAction.class)
    public static Result getUserList() {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String queryType = node.get("queryType").asText();

        PagingList<S_User> pagingList = null;
        if ("search".equals(queryType)) {
            String search_username = node.get("search_username").asText();
            String search_userphone = node.get("search_userphone").asText();
            ExpressionList expressionList = S_User.find.where().eq("deleteLogic", false);

            if (!search_username.equals("null")) {
                expressionList = expressionList.or(
                        Expr.or(Expr.like("username", "%" + search_username + "%"),Expr.like("baseInfo.nickname", "%" + search_username + "%")),
                        Expr.or(Expr.like("wechatInfo.nickname", "%" + search_username + "%"),Expr.like("qqInfo.nickname", "%" + search_username + "%"))
                        );
            }
            if (!search_userphone.equals("null")) {
                expressionList = expressionList.like("telephone", "%" + search_userphone + "%");
            }
            pagingList = expressionList.order("createdTime desc").findPagingList(pageSize);
        }else {
            pagingList = S_User.find.where().eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        }

        Page<S_User> page = pagingList.getPage(pageNum - 1);
        List<S_User> userList = page.getList();
        JSONArray array = new JSONArray();
        for (S_User info : userList) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("username", info.getShowName());
            object.put("telephone", info.telephone);
            object.put("avatar", info.getShowAvatar());
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    //------------------------------------------------------------------------
    //--------------------------------标签相关--------------------------------
    //------------------------------------------------------------------------

    //根据uuid获取用户被当前登录用户标记的标签列表
    @With(UserLoginSecuredAction.class)
    public static Result getUserTagEnumListByTaggedUserAndCurrentUser() {
        JsonNode node = request().body().asJson();
        String taggedUserUUID = node.get("taggedUserUUID").asText();

        List<B_User_Tag> userTagList = null;
        userTagList = B_User_Tag.find.where().eq("owner", currentUser()).eq("taggedUser", S_User.findByUuid(taggedUserUUID)).eq("deleteLogic", false).order("createdTime asc").findList();

        JSONArray array = new JSONArray();
        for (B_User_Tag info : userTagList) {
            JSONObject object = new JSONObject();
            object.put("tag", info.tag.tag);
            object.put("id", info.tag.id);
            object.put("color", info.tag.color);
            object.put("uuid", info.tag.uuid);
            array.add(object);
        }
        return ajaxSuccess(array);
    }

    //根据uuid获取用户尚未被当前登录用户标记的标签列表
    @With(UserLoginSecuredAction.class)
    public static Result getUnChosenUserTagEnumListByTaggedUserAndCurrentUser() {
        JsonNode node = request().body().asJson();
        String taggedUserUUID = node.get("taggedUserUUID").asText();

        //某用户被当前用户标记的标签列表
        List<B_User_Tag> chosenUserTagList = null;
        chosenUserTagList = B_User_Tag.find.where().eq("owner", currentUser()).eq("taggedUser", S_User.findByUuid(taggedUserUUID)).eq("deleteLogic", false).order("createdTime asc").findList();
        ArrayList idArray=new ArrayList();
        for(B_User_Tag chosenUserTag:chosenUserTagList){
            idArray.add(chosenUserTag.tag.id);
        }
        List<B_User_Tag_Enum> userTagEnumList = null;
        userTagEnumList = B_User_Tag_Enum.find.where().or(Expr.eq("owner", currentUser()),Expr.eq("tagType", B_User_Tag_Enum.TagEnumType.global))
                .not(Expr.in("id",idArray)).eq("deleteLogic", false).order("createdTime desc").findList();

        JSONArray array = new JSONArray();
        for (B_User_Tag_Enum info : userTagEnumList) {
            JSONObject object = new JSONObject();
            object.put("uuid", info.uuid);
            object.put("id", info.id);
            object.put("tag", info.tag);
            object.put("color", info.color);
            array.add(object);
        }
        return ajaxSuccess(array);
    }

    //获取当前用户的所有tag列表
    @With(UserLoginSecuredAction.class)
    public static Result getUserTagEnumListByCurrentUser() {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String queryType = node.get("queryType").asText();

        PagingList<B_User_Tag_Enum> pagingList = null;
        if ("search".equals(queryType)) {
            String tag = node.get("tagName").asText();
            ExpressionList expressionList = B_User_Tag_Enum.find.where().or(Expr.eq("owner", currentUser()),Expr.eq("tagType", B_User_Tag_Enum.TagEnumType.global))
                    .eq("deleteLogic", false);

            if (!tag.equals("null")) {
                expressionList = expressionList.like("tag", "%" + tag + "%");
            }
            pagingList = expressionList.order("createdTime desc").findPagingList(pageSize);
        }else {
            pagingList = B_User_Tag_Enum.find.where().or(Expr.eq("owner", currentUser()),Expr.eq("tagType", B_User_Tag_Enum.TagEnumType.global))
                    .eq("deleteLogic", false).order("createdTime desc").findPagingList(pageSize);
        }

        Page<B_User_Tag_Enum> page = pagingList.getPage(pageNum - 1);
        List<B_User_Tag_Enum> userTagEnumList = page.getList();
        JSONArray array = new JSONArray();
        for (B_User_Tag_Enum info : userTagEnumList) {
            JSONObject object = new JSONObject();
            object.put("tag", info.tag);
            object.put("id", info.id);
            object.put("tagType", info.tagType);
            object.put("color", info.color);
            object.put("uuid", info.uuid);
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }

    //当前用户给某个用户贴标签
    @With(UserLoginSecuredAction.class)
    public static Result attachTagToUserByCurrentUser() {
        JsonNode node = request().body().asJson();
        String taggedUserUUID = node.get("taggedUserUUID").asText();
        String tagUUID = node.get("tagUUID").asText();
        if(currentUser().uuid.equals(taggedUserUUID)){
            return ajaxFail("不能给自己贴标签");
        }
        if(B_User_Tag.isExistByTaggedUserAndTaggingUser(currentUser(),S_User.findByUuid(taggedUserUUID),B_User_Tag_Enum.findByUuid(tagUUID))){
            return ajaxFail("已贴上该标签");
        }
        B_User_Tag tag=B_User_Tag.create(currentUser(),S_User.findByUuid(taggedUserUUID),B_User_Tag_Enum.findByUuid(tagUUID));
        return ajaxSuccess(tag);
    }

    //当前用户给某个用户删除标签
    @With(UserLoginSecuredAction.class)
    public static Result deleteTagToUserByCurrentUser() {
        JsonNode node = request().body().asJson();
        String taggedUserUUID = node.get("taggedUserUUID").asText();
        String tagUUID = node.get("tagUUID").asText();
        B_User_Tag userTag = B_User_Tag.findByTaggedUserAndTaggingUser(currentUser(),S_User.findByUuid(taggedUserUUID),B_User_Tag_Enum.findByUuid(tagUUID));
        if (userTag == null) {
            return ajaxFail("尚未贴上该标签");
        }
        if (!userTag.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有删除标签的权限");
        }
        userTag.delete(true);
        return ajaxSuccess("删除成功");
    }

    //添加自定义标签
    @With(UserLoginSecuredAction.class)
    public static Result saveCustomizeTagEnum() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        String tag = node.get("tag").asText();
        String color = node.get("color").asText();
        S_User owner = currentUser();
        if(StringUtils.isNullOrEmpty(uuid)){
            boolean isExist= B_User_Tag_Enum.isExistSameNameCustomizeTag(tag,owner);
            if (isExist) {
                return ajaxFail("该标签已存在，请换个名称");
            } else {
                int existTagCount=B_User_Tag_Enum.getCustomizeTagCountByUser(owner);
                if(existTagCount>=customizeTagCountLimit){
                    return ajaxFail("可添加标签数量超过上限（"+customizeTagCountLimit+" 个），请先删除一些闲置标签");
                }else {
                    B_User_Tag_Enum userTagEnum = B_User_Tag_Enum.createCustomize(owner,tag,color);
                    return ajaxSuccess(userTagEnum);
                }
            }
        }else {
            B_User_Tag_Enum userTagEnum=B_User_Tag_Enum.findByUuid(uuid);
            if(userTagEnum==null){
                return ajaxFail("正在编辑的标签不存在，请刷新页面重新编辑");
            }else {
                if(!userTagEnum.owner.id.equals(currentUser().id)){
                    return ajaxFail("您没有修改标签的权限");
                }
                boolean isExist= B_User_Tag_Enum.isExistSameNameCustomizeTag(tag,owner);
                if(tag.equals(userTagEnum.tag)){
                    if(color.equals(userTagEnum.color)){
                        return ajaxFail("没有需要进行修改的内容");
                    }else {
                        userTagEnum.color=color;
                        userTagEnum.saveOrUpdate();
                        return ajaxSuccess(userTagEnum);
                    }
                }else {
                    if(isExist){
                        return ajaxFail("该标签已存在，请换个名称");
                    }else {
                        userTagEnum.tag=tag;
                        userTagEnum.color=color;
                        userTagEnum.saveOrUpdate();
                        return ajaxSuccess(userTagEnum);
                    }
                }
            }
        }

    }

    //删除自定义标签
    @With(UserLoginSecuredAction.class)
    public static Result deleteCustomizeTagEnum() {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();

        B_User_Tag_Enum userTagEnum=B_User_Tag_Enum.findByUuid(uuid);
        if (userTagEnum == null) {
            return ajaxFail("该标签不存在");
        }
        if (!userTagEnum.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有删除标签的权限");
        }
        userTagEnum.delete(true);
        return ajaxSuccess("删除成功");
    }

    //获取当前用户拥有的某个tag的相关人员统计信息
    @With(UserLoginSecuredAction.class)
    public static Result getUserTagListByTagAndCurrentUser() {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        String queryType = node.get("queryType").asText();
        String tagUUID = node.get("tagUUID").asText();

        PagingList<B_User_Tag> pagingList = null;
        if ("search".equals(queryType)) {
            //筛选暂时有点问题-----------暂不使用
            String tag = node.get("userName").asText();
            ExpressionList expressionList = B_User_Tag_Enum.find.where().or(Expr.eq("owner", currentUser()),Expr.eq("tagType", B_User_Tag_Enum.TagEnumType.global))
                    .eq("deleteLogic", false);

            if (!tag.equals("null")) {
                expressionList = expressionList.like("tag", "%" + tag + "%");
            }
            pagingList = expressionList.order("createdTime desc").findPagingList(pageSize);
        }else {
            pagingList = B_User_Tag.find.where().eq("owner", currentUser())
                    .eq("tag", B_User_Tag_Enum.findByUuid(tagUUID)).order("createdTime desc").findPagingList(pageSize);
        }

        Page<B_User_Tag> page = pagingList.getPage(pageNum - 1);
        List<B_User_Tag> userTagEnumList = page.getList();
        JSONArray array = new JSONArray();
        for (B_User_Tag info : userTagEnumList) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("userUUID", info.taggedUser.uuid);
            object.put("userName", info.taggedUser.getShowName());
            object.put("userAvatar", info.taggedUser.getShowAvatar());
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }


    //用户给自己的唯页账号充值
    @With(UserLoginSecuredAction.class)
    @Transactional
    public static Result rechargeAccount() throws Exception {
        JsonNode node = request().body().asJson();
        int coin = node.get("coin").asInt();
        S_User currentUser = currentUser();
        if (coin <= 0) {
            return ajaxFail("充值金额必须大于0元");
        }

        //4%的手续费，就直接用task.check.fee的参数值
        int fee = (int) (coin * Common.getIntConfig("task.check.fee") / 100);
        int total_fee = coin + fee;
        String nonce_str = Common.randomString(32).trim();
        String body = "账号\"" + currentUser.getShowName() + "\"充值费用(包含" + Common.getIntConfig("task.check.fee") + "%微信手续费)";
        String out_trade_no = B_Wechat_Pay.getOneUnUseOutTradeNo();
        String spbill_create_ip = request().remoteAddress();
        String product_id = B_Wechat_Pay.getOneUnUseProductId();
        String notify_url = UrlController.serverAddress + "/api/wechat/pay/userRechargeResult";
        UnifiedOrderResult result = OrderManage.unifiedorderNative(nonce_str, body, out_trade_no, total_fee, spbill_create_ip, product_id, notify_url);
        if (result.isSuccess()) {
            B_Wechat_Pay pay = B_Wechat_Pay.create(currentUser(), product_id, coin, out_trade_no, nonce_str, result.trade_type, result.prepay_id, result.code_url);
            JSONObject object = new JSONObject();
            object.put("url", result.code_url);
            object.put("coin", total_fee);
            return ajaxSuccess(object);
        } else {
            return ajaxFail(result.getErrorMsg());
        }
    }
}
