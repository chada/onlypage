package controllers.qq;


import controllers.BaseController;
import models.db1.user.B_User_Qq_Info;
import models.db1.user.S_User;
import play.mvc.Result;
import qq.beans.QqLoginOpenResult;
import qq.beans.QqLoginResult;
import qq.beans.QqLoginUserResult;
import qq.service.QqLoginService;
import util.Common;

import java.io.IOException;

/**
 * Created by wangh on 2016/11/22.
 * mengdi
 */
public class QqAppController extends BaseController{
    public static Result QqThirdLoginUrl(){
        String appid = Common.getStringConfig("app_ID");
        String redirect_uri= Common.getStringConfig("server.address")+"/api/qq/login";//此处就是配置文件里的redirect_uri
        String state="state";
        System.out.println(redirect_uri);
        return ajaxSuccess(QqLoginService.getUrl(appid, redirect_uri, state));
    }


        public static Result getCode() throws IOException {

        if (!containsParam("code")) {

       } else {
            String code = getParam("code");
            //获取accessToken
            QqLoginResult loginResult = QqLoginService.getAccessToken(code);
            if (loginResult.isSuccess()) {
               //获取openid
                QqLoginOpenResult openId = QqLoginService.getOpenId(loginResult.access_token);
                if(openId.isSuccess()){

                    //获取QQ用户信息
                    QqLoginUserResult userResult = QqLoginService.getUserInfo(loginResult.access_token, openId.openid);
                    if(userResult.isSuccess()){
                        //根据openid查看数据库的B_User_Qq_Info表中是否有此用户信息
                        B_User_Qq_Info info = B_User_Qq_Info.find(openId.openid);
                        S_User user = null;
                        if (userResult.ret != 0) {

                        }else {
                            if (info == null) {
                                user = S_User.tocreateQQUser(userResult, openId.openid);
                            } else {
                                user = info.owner;
                            }
                            loginAsUser(user);
                    }
                }
            }

            }

        }
            return redirect(Common.getStringConfig("server.address"));
    }


}
