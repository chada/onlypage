package controllers;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;
import com.fasterxml.jackson.databind.JsonNode;
import models.db1.task.B_Task;
import models.db1.task.strategy.B_Task_Strategy;
import models.db1.task.strategy.B_Task_Strategy_Info;
import models.db1.task.strategy.B_Task_Strategy_Rule;
import models.db1.task.strategy.B_Task_Strategy_Rule_Condition;
import models.db1.user.S_User;
import org.h2.util.StringUtils;
import org.jsoup.helper.StringUtil;
import play.db.ebean.Transactional;
import play.mvc.*;
import security.UserLoginSecuredAction;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/11/26.
 */
public class StrategyController extends BaseController{

    @With(UserLoginSecuredAction.class)
    public static Result getStrategyInfoByUUID() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task_Strategy strategy = B_Task_Strategy.findByUuid(uuid);

        if (strategy == null || strategy.isLogicDelete()) {
            return ajaxFail("策略不存在");
        }


        JSONObject result = new JSONObject();
        result.put("strategy", strategy);
        return ajaxSuccess(result);
    }

    @With(UserLoginSecuredAction.class)
    public static Result getStrategyInfoById() throws Exception {
        JsonNode node = request().body().asJson();
        String id = node.get("id").asText();
        B_Task_Strategy strategy =  B_Task_Strategy.find.where().eq("id", id).findUnique();
        if (strategy == null || strategy.isLogicDelete()) {
            return ajaxFail("策略不存在");
        }

        JSONObject result = new JSONObject();
        result.put("strategy", strategy);
        return ajaxSuccess(result);
    }

    @Transactional
    @With(UserLoginSecuredAction.class)
    public static Result editStrategy() throws Exception {
        JsonNode node = request().body().asJson();
        Boolean isNew = node.get("isNew").asBoolean();

        String title = node.get("title").asText();
        String remark = node.get("remark").asText();
        String deleteStrategyRuleIdList = node.get("deleteStrategyRuleIdList").asText();
        String deleteStrategyRuleConditionIdList = node.get("deleteStrategyRuleConditionIdList").asText();

        String strategyRuleListStr = node.get("strategyRuleList").asText();
        JSONArray strategyRuleList = JSON.parseArray(strategyRuleListStr);
        B_Task_Strategy taskStrategy =new B_Task_Strategy();
        if (isNew) {
            taskStrategy.owner=currentUser();
            taskStrategy.strategyType= B_Task_Strategy.StrategyType.advancedStrategy;
        } else {
            String uuid = node.get("uuid").asText();
            taskStrategy = B_Task_Strategy.findByUuid(uuid);
            if (taskStrategy == null) {
                return ajaxFail("策略不存在");
            }
            if(!taskStrategy.owner.id.equals(currentUser().id)){
                return ajaxFail("您没有权限修改该策略");
            }

            //删除策略下的规则和条件
            if(!StringUtils.isNullOrEmpty(deleteStrategyRuleConditionIdList)) {
                String[] deleteStrategyRuleConditionIdListArray = deleteStrategyRuleConditionIdList.split(",");
                for (String deleteStrategyRuleConditionIdString : deleteStrategyRuleConditionIdListArray) {
                    int deleteStrategyRuleConditionId = Integer.parseInt(deleteStrategyRuleConditionIdString);
                    B_Task_Strategy_Rule_Condition ruleCondition = B_Task_Strategy_Rule_Condition.find.byId(deleteStrategyRuleConditionId);
                    if (ruleCondition != null) {
                        ruleCondition.delete(true);
                    }
                }
            }

            if(!StringUtils.isNullOrEmpty(deleteStrategyRuleIdList)) {
                String[] deleteStrategyRuleIdListArray = deleteStrategyRuleIdList.split(",");
                for (String deleteStrategyRuleIdString : deleteStrategyRuleIdListArray) {
                    int deleteStrategyRuleId = Integer.parseInt(deleteStrategyRuleIdString);
                    B_Task_Strategy_Rule rule = B_Task_Strategy_Rule.find.byId(deleteStrategyRuleId);
                    if (rule != null) {
                        rule.delete(true);
                    }
                }
            }
        }
        //保存策略
        taskStrategy.title=title;
        taskStrategy.remark=remark;
        taskStrategy.save();


        //保存策略规则和分配条件
        for(Object strategyRuleObj:strategyRuleList){
            JSONObject strategyRuleJSONObj=(JSONObject)strategyRuleObj;
            String ruleIdString=strategyRuleJSONObj.getString("id");
            Integer ruleId=null;
            if(!StringUtils.isNullOrEmpty(ruleIdString)){
                ruleId=Integer.parseInt(ruleIdString);
            }
            B_Task_Strategy_Rule taskStrategyRule=B_Task_Strategy_Rule.createOrUpdate(taskStrategy,ruleId,B_Task_Strategy_Rule.stringConvertTriggerEvent(strategyRuleJSONObj.getString("triggerEvent")),strategyRuleJSONObj.getInteger("readTime"),
                    strategyRuleJSONObj.getInteger("startTriggerNodeLevel"),strategyRuleJSONObj.getInteger("endTriggerNodeLevel"),B_Task_Strategy_Rule.stringConvertRewardUserType(strategyRuleJSONObj.getString("rewardUserType")),
                    strategyRuleJSONObj.getInteger("upRewardUserNum"),B_Task_Strategy_Rule.stringConvertRewardType(strategyRuleJSONObj.getString("rewardType")),strategyRuleJSONObj.getInteger("fixedRewardAmount"),
                    strategyRuleJSONObj.getInteger("minRewardAmount"),strategyRuleJSONObj.getInteger("maxRewardAmount"),strategyRuleJSONObj.getInteger("strategyRuleTriggerNum"));

            JSONArray strategyRuleConditionList =strategyRuleJSONObj.getJSONArray("strategyRuleConditionList");
            for(Object strategyRuleConditionObj:strategyRuleConditionList){
                JSONObject strategyRuleConditionJSONObj=(JSONObject)strategyRuleConditionObj;
                String ruleConditionIdString=strategyRuleConditionJSONObj.getString("id");
                Integer ruleConditionId=null;
                if(!StringUtils.isNullOrEmpty(ruleConditionIdString)){
                    ruleConditionId=Integer.parseInt(ruleConditionIdString);
                }
                B_Task_Strategy_Rule_Condition.createOrUpdate(taskStrategyRule,ruleConditionId,B_Task_Strategy_Rule_Condition.stringConvertConditionNodeType(strategyRuleConditionJSONObj.getString("conditionNodeType")),strategyRuleConditionJSONObj.getInteger("upConditionNodeNum"),
                        B_Task_Strategy_Rule_Condition.stringConvertAllocationConditions(strategyRuleConditionJSONObj.getString("allocationConditions")),strategyRuleConditionJSONObj.getInteger("childnodeNum"),
                        strategyRuleConditionJSONObj.getString("location"),strategyRuleConditionJSONObj.getDouble("longitude"),strategyRuleConditionJSONObj.getDouble("latitude"),strategyRuleConditionJSONObj.getInteger("locationRange"),
                        strategyRuleConditionJSONObj.getString("tag"),B_Task_Strategy_Rule_Condition.stringConvertTagCondition(strategyRuleConditionJSONObj.getString("tagCondition")),strategyRuleConditionJSONObj.getInteger("readingTime"),
                        B_Task_Strategy_Rule_Condition.stringConvertReadingStyle(strategyRuleConditionJSONObj.getString("readingstyle")),strategyRuleConditionJSONObj.getInteger("redpocketquantity"),strategyRuleConditionJSONObj.getInteger("redpocketsum"));
            }
        }


        return ajaxSuccess("创建成功");
    }

    @Transactional
    @With(UserLoginSecuredAction.class)
    public static Result deleteStrategy() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task_Strategy taskStrategy = B_Task_Strategy.findByUuid(uuid);
        for(B_Task_Strategy_Info taskStrategyInfo:taskStrategy.taskStrategyInfoList){
            if(taskStrategyInfo.task.state.equals(B_Task.State.checking)||taskStrategyInfo.task.state.equals(B_Task.State.publish)){
                return ajaxFail("策略尚在被执行中或审核中的任务引用，不能删除");
            }
        }

        if (taskStrategy == null) {
            return ajaxFail("策略不存在");
        }
        if (!taskStrategy.owner.id.equals(currentUser().id)) {
            return ajaxFail("您没有权限删除");
        }
        taskStrategy.delete();
        return ajaxSuccess("删除成功");
    }

    @With(UserLoginSecuredAction.class)
    public static Result editStatus() throws Exception {
        JsonNode node = request().body().asJson();
        String uuid = node.get("uuid").asText();
        B_Task_Strategy taskStrategy = B_Task_Strategy.findByUuid(uuid);
        for(B_Task_Strategy_Info taskStrategyInfo:taskStrategy.taskStrategyInfoList){
            if(taskStrategyInfo.task.state.equals(B_Task.State.checking)||taskStrategyInfo.task.state.equals(B_Task.State.publish)){
                return ajaxFail("策略尚在被执行中或审核中的任务引用，不能修改");
            }
        }
        return ajaxSuccess("");
    }

    @With(UserLoginSecuredAction.class)
    public static Result getStrategyOwnerList() throws Exception {
        JsonNode node = request().body().asJson();
        int pageNum = node.get("pageNum").asInt();
        int pageSize = node.get("pageSize").asInt();
        PagingList<B_Task_Strategy> pagingList = B_Task_Strategy.find.where().eq("owner", currentUser()).eq("deleteLogic", false).eq("strategyType", B_Task_Strategy.StrategyType.advancedStrategy).order("createdTime desc").findPagingList(pageSize);
        Page<B_Task_Strategy> page = pagingList.getPage(pageNum - 1);
        List<B_Task_Strategy> list = page.getList();
        JSONArray array = new JSONArray();
        for (B_Task_Strategy info : list) {
            JSONObject object = new JSONObject();
            object.put("id", info.id);
            object.put("uuid", info.uuid);
            object.put("editedTime", info.editedTime);
            object.put("title", info.title);
            object.put("remark", info.remark);
            object.put("strategyType", info.strategyType);
            array.add(object);
        }
        return paginatorSuccess(array, pagingList.getTotalPageCount());
    }

}
