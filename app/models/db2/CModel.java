package models.db2;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import util.Common;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Administrator on 2016/2/24.
 */
@MappedSuperclass
public class CModel extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id = 0;

    @Version
    public Integer version = 1;

    @Constraints.Required
    public Date createdTime = new Date();

    @Constraints.Required
    public Date editedTime = new Date();

    @Constraints.Required
    public String uuid = null;

    @Constraints.Required
    public Boolean deleteLogic = false;

    @Override
    public void save() {
        if (getRecordUuid() == null) {
            String uuid = UUID.randomUUID().toString();
            setRecordUuid(uuid);
        }
        this.createdTime = new Date();
        super.save();
        this.refresh();
    }

    public void saveOrUpdate() {
        if (this.uuid == null) {
            this.editedTime = new Date();
            this.update();
        } else {
            this.save();
        }
    }

    @Override
    public void delete() {
        this.deleteLogic = true;
        this.saveOrUpdate();
    }

    public void delete(Boolean deleteLogic) {
        if (deleteLogic) {
            super.delete();
        } else {
            this.delete();
        }
    }

    public Boolean isLogicDelete(){
        return this.deleteLogic;
    }


    @Transient
    public void setRecordUuid(String uuid) {
        Common.setPropertyOfObject(this, "uuid", uuid);
    }

    @Transient
    public String getRecordUuid() {
        return (String) Common.getPropertyOfObject(this, "uuid");
    }

}