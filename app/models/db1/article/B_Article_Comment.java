package models.db1.article;

import com.alibaba.fastjson.annotation.JSONField;
import models.db1.CModel;
import models.db1.user.S_User;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Administrator on 2015/1/20.
 * 文章评论
 */
@Entity
public class B_Article_Comment extends CModel {

    @Column(nullable = false)
    @ManyToOne
    public S_User owner;

    @ManyToOne
    public B_Article article;

    //父评论
    @JSONField(serialize = false)
    @ManyToOne
    public B_Article_Comment parent;

    //子评论
    @OneToMany(mappedBy = "parent")
    public List<B_Article_Comment> children;


    //评论内容
    @Column(nullable = false)
    @Length(max = 255)
    public String content;


    public static Finder<Integer, B_Article_Comment> find = new Finder<Integer, B_Article_Comment>(
            Integer.class, B_Article_Comment.class
    );


}
