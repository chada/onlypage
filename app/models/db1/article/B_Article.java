package models.db1.article;

import controllers.util.UrlController;
import models.db1.CModel;
import models.db1.task.B_Task;
import models.db1.user.S_User;
import play.data.validation.Constraints;
import util.Common;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015/1/20.
 */
@Entity
public class B_Article extends CModel {

    @Column(nullable = false)
    @ManyToOne
    public S_User owner;

    //任务标题
    @Constraints.Required
    @Constraints.MaxLength(50)
    public String title;

    @Lob
    public String content;

    public Boolean isComment = true;
    public Boolean isPublic = true;

    //封面图片
    public String coverImg;

    @OneToMany(mappedBy = "article")
    public List<B_Article_Visit_Log> visitLogs;

    @OneToMany(mappedBy = "article")
    public List<B_Task> tasks;


    public int migrationid;

    public static Finder<Integer, B_Article> find = new Finder<Integer, B_Article>(
            Integer.class, B_Article.class
    );


    public static B_Article create(String title, String content, Boolean isComment, Boolean isPublic, String coverImg, S_User user) {
        B_Article article = new B_Article();
        article.owner = user;
        article.title = title;
        article.content = Common.filterEmoji(content);
        article.coverImg = coverImg;
        article.isComment = isComment;
        article.isPublic = isPublic;
        article.save();
        return article;
    }

    public B_Article edit(String title, String content, Boolean isComment, Boolean isPublic, String coverImg) {
        this.title = title;
        this.content = Common.filterEmoji(content);;
        this.coverImg = coverImg;
        this.isComment = isComment;
        this.isPublic = isPublic;
        this.update();
        return this;
    }

    public static B_Article findByUuid(String uuid) {
        return B_Article.find.where().eq("uuid", uuid).findUnique();
    }

    public B_Article changeIsComment(Boolean isComment) {
        this.isComment = isComment;
        this.update();
        return this;
    }

    public B_Article changeIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
        this.update();
        return this;
    }

    public void addVisitLog(S_User user) {
        B_Article_Visit_Log.create(user, this);
    }

    public int getVisitCount() {
        return this.visitLogs.size();
    }

    public String getWechatShareLink() {
        return UrlController.serverAddress + "/api/article/entrance?id=" + this.id;
    }

    public String getPcViewLink() {
        return UrlController.serverAddress + "/#/article/view?id=" + this.id;
    }


    public static B_Article migration(String title, String content, Boolean isComment, Boolean isPublic, String coverImg, S_User user, int migrationid, Date created_time) {
        B_Article article = new B_Article();
        article.owner = user;
        article.title = title;
        article.content = content;
        article.coverImg = coverImg;
        article.isComment = isComment;
        article.isPublic = isPublic;
        article.migrationid = migrationid;
        article.editedTime = created_time;
        article.createdTime = created_time;
        article.save();
        return article;
    }

    public static B_Article fingByMigrationId(int migrationid) {
        return B_Article.find.where().eq("migrationid", migrationid).findUnique();
    }
}
