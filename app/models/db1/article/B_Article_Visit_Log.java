package models.db1.article;

import models.db1.CModel;
import models.db1.task.B_Task;
import models.db1.user.S_User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by Administrator on 2015/1/20.
 * 文章访问记录（一篇文章一个人可以有多条访问记录）
 */
@Entity
public class B_Article_Visit_Log extends CModel {

    @Column(nullable = false)
    @ManyToOne
    public S_User owner;

    @ManyToOne
    public B_Article article;

    public static Finder<Integer, B_Article_Visit_Log> find = new Finder<Integer, B_Article_Visit_Log>(
            Integer.class, B_Article_Visit_Log.class
    );


    public static B_Article_Visit_Log create(S_User owner, B_Article article){
        B_Article_Visit_Log log = new B_Article_Visit_Log();
        log.article = article;
        log.owner = owner;
        log.save();
        return log;
    }


}
