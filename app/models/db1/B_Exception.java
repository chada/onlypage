package models.db1;

import models.db1.user.S_User;
import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2015/1/20.
 * 所有上传的图片的库，防止上传多张同样图片，节省存储空间
 */
@Entity
public class B_Exception extends CModel {

    @Lob
    public String exception;

    public String remark;

    public enum Type {system,taskCheckout}

    public Type type;

    public static Finder<Integer, B_Exception> find = new Finder<Integer, B_Exception>(
            Integer.class, B_Exception.class
    );


    public static B_Exception create(String exception,String remark,Type type){
        B_Exception b_exception = new B_Exception();
        b_exception.exception = exception;
        b_exception.remark = remark;
        b_exception.type = type;
        b_exception.save();
        return b_exception;
    }

}
