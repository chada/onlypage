package models.db1.task;

import beans.task.strategy.StrategyExecuteResult;
import beans.task.strategy.StrategyTriggerEvent;
import com.avaje.ebean.Ebean;
import com.mysql.jdbc.StringUtils;
import controllers.util.UrlController;
import models.db1.CModel;
import models.db1.article.B_Article;
import models.db1.task.promotion.B_Task_Promotion;
import models.db1.task.schedule.B_Task_User_Statistics_Data;
import models.db1.task.strategy.B_Task_Strategy;
import models.db1.task.strategy.B_Task_Strategy_Info;
import models.db1.task.strategy.B_Task_Strategy_Rule;
import models.db1.task.view.B_Task_View_Tag;
import models.db1.user.B_User_Info;
import models.db1.user.S_User;
import models.db1.user.coin.B_User_Coin_In_Log;
import models.db1.user.coin.B_User_Coin_Log;
import models.db1.user.coin.B_User_Coin_Out_Log;
import models.db1.wechat.B_Wechat_Pay;
import models.db1.wechat.B_Wechat_Withdraw;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.Http;
import util.Common;
import util.LockUtil;
import wechat.pay.WithDrawDepositManage;
import wechat.pay.beans.withdrawDeposit.WithdrawDepositResult;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Administrator on 2015/1/20.
 */
@Entity
public class B_Task extends CModel {

    @Column(nullable = false)
    @ManyToOne
    public S_User owner;

    //任务标题
    @Constraints.Required
    public String title;

    //任务结束时间（提前会更改）
    @Constraints.Required
    public Date endTime;

    public String remark;

    //任务描述--仅作备注使用
    public String description;

    //任务类型:1-普通；2-地推模式
    public enum TaskType {
        normal, promotion
    }

    public TaskType taskType;

    //任务的实际数据连接地址
    @Constraints.Required
    public String data;

    public enum DataType {url, article}

    public DataType dataType;

    @ManyToOne
    public B_Article article;

    public String coverImg;

    public enum VisibleType {all, friend, self}

    public VisibleType visibleType;

    @OneToMany(mappedBy = "task")
    public List<B_Task_Propagation_Log> propagationLogs;

    @OneToOne(mappedBy = "task", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public B_Task_Strategy_Info strategyInfo;

    @OneToOne(mappedBy = "task", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    public B_Task_View_Tag viewTag;

    //初始化，审核，发布，结束，拒绝，异常
    public enum State {
        init, checking, publish, finish, refuse, exception
    }

    public State state;

    @OneToMany(mappedBy = "task")
    public List<B_User_Coin_In_Log> inLogs;

    @OneToMany(mappedBy = "task")
    public List<B_User_Coin_Out_Log> outLogs;


    @Column(unique = true)
    private String cloneStr;

    public int migrationid;


    @OneToOne(mappedBy = "task", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public B_Task_Promotion promotion;

    public static Finder<Integer, B_Task> find = new Model.Finder<Integer, B_Task>(
            Integer.class, B_Task.class
    );

    public static B_Task create(S_User owner, String title, String remark, String coverImg, Date endTime, TaskType taskType, DataType dataType, String data, B_Article article, VisibleType visibleType, String description) {
        B_Task task = new B_Task();
        task.owner = owner;
        task.title = title;
        task.remark = remark;
        task.coverImg = coverImg;
        task.endTime = endTime;
        task.dataType = dataType;
        task.taskType = taskType;
        task.data = data;
        task.article = article;
        task.visibleType = visibleType;
        task.state = State.init;
        task.description = description;
        task.save();
        return task;
    }


    public B_Task edit(String title, String remark, String coverImg, Date endTime, TaskType taskType, DataType dataType, String data, B_Article article, VisibleType visibleType, String description) {
        this.title = title;
        this.remark = remark;
        this.coverImg = coverImg;
        this.endTime = endTime;
        this.taskType = taskType;
        this.dataType = dataType;
        this.data = data;
        this.article = article;
        this.visibleType = visibleType;
        this.state = State.init;
        this.description = description;
        this.updateWechatPay(null);
        this.update();
        return this;
    }


    public B_Task edit(String title, String remark, String coverImg, VisibleType visibleType, String description) {
        this.title = title;
        this.remark = remark;
        this.coverImg = coverImg;
        this.visibleType = visibleType;
        this.description = description;
        this.update();
        return this;
    }

    //一定要fetch，不然无法读取到stategy数据
    public static B_Task findByUuid(String uuid) {
//        return B_Task.find.where().eq("uuid", uuid).findUnique();
        return B_Task.find.fetch("strategyInfo").fetch("strategyInfo.strategy").fetch("strategyInfo.strategy.strategyRuleList").where().eq("uuid", uuid).findUnique();
    }

    //查看任务参与人数（传播记录数，一人记一次，未统计一人的多次访问记录）
    public int getParticipantCount() {
        return propagationLogs.size();
    }

    //任务开始url地址（一切以这位起点，可以看成第0层）
    public String getStartUrl() {
        return UrlController.serverAddress + "/api/task/entrance?taskId=" + this.id + "&executantId=0";
    }

    //预览连接
    public String getViewtUrl() {
        String viewUrl = "";
        if (this.dataType == B_Task.DataType.url) {
            viewUrl = this.data;
        } else if (this.dataType == B_Task.DataType.article) {
            viewUrl = this.article.getPcViewLink();
        }
        return viewUrl;
    }

    public String getStartUrl(S_User user) {
        return UrlController.serverAddress + "/api/task/entrance?taskId=" + this.id + "&executantId=" + user.id;
    }


    public void updateWechatPay(B_Wechat_Pay pay) {
        this.strategyInfo.updateWechatPay(pay);
    }

    public void updateRenew(B_Wechat_Pay pay, int coin, int fee) {
        this.strategyInfo.updateRenew(pay, coin, fee);
    }


    public void changeState(State state) {
        this.state = state;
        this.update();
    }

    //任务是否在运行
    public Boolean isRunning() {
        return this.state == State.publish ? true : false;
    }

    //判断是否需要结束任务
    public Boolean needEnd(int coin) {
        if (this.strategyInfo.hasAllocationCoin + coin > this.strategyInfo.coin) {
            return true;
        }
        if (this.endTime.compareTo(new Date()) < 0) {
            return true;
        }
        return false;
    }

    public Boolean needEnd() {
        return needEnd(0);
    }


    public void finish() throws Exception {
        synchronized (LockUtil.getLock("B_Task_Strategy_Info_change_" + this.id)) {
            this.end();
        }
    }

    public void end() throws Exception {
        synchronized (LockUtil.getLock("B_Task_Strategy_Info_end_" + this.id)) {
            if (isRunning()) {
                int remainCoin = this.strategyInfo.coin - this.strategyInfo.hasAllocationCoin;
                if (remainCoin >= 0) {
                    this.changeState(State.finish);
                    if (remainCoin > 0) {
                        this.owner.account.addCoin(this.strategyInfo.coin - this.strategyInfo.hasAllocationCoin);
                        B_User_Coin_Log.create_TaskRemain(this, this.owner, remainCoin);
                        //因为退回给用户了，所以需要将已分配的金额填上此部分（不然再次充值有bug）
                        this.strategyInfo.addHasAllocationCoin(this.strategyInfo.coin - this.strategyInfo.hasAllocationCoin);
                    }
                } else {
                    this.changeState(State.exception);
                }
            }
        }
    }

    public void renew(int coin) {
        synchronized (LockUtil.getLock("B_Task_Strategy_Info_change_" + this.id)) {
            this.strategyInfo.addCoin(coin);
            this.state = State.publish;

            //充值时，结束时间为充值日期的后15天
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_MONTH, 15);
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            this.endTime = c.getTime();
            this.update();
        }
    }

    public void publish() {
        synchronized (LockUtil.getLock("B_Task_Strategy_Info_change_" + this.id)) {
            this.state = State.publish;
            this.update();
        }
    }


    public static void checkout(Integer taskId, final S_User user, final StrategyTriggerEvent strategyTriggerEvent, final String remoteAddress) throws Exception {
//        if (isRunning()) {
        checkoutOperate(taskId, user, strategyTriggerEvent, remoteAddress);
//        }
    }


    public static ExecutorService payPool = Executors.newFixedThreadPool(20);

    //处理函数嵌套事务，外部调用静止使用事务
    private static void checkoutOperate(Integer taskId, S_User user, StrategyTriggerEvent strategyTriggerEvent, final String remoteAddress) throws Exception {

        synchronized (LockUtil.getLock("B_Task_Strategy_Info_change_" + taskId)) {
            B_Task task = B_Task.find.byId(taskId);
            B_Task_Strategy_Info strategy_info = task.strategyInfo;
            if (task.isRunning()) {
                Ebean.beginTransaction();
                try {
                    B_Task_Strategy strategy = task.strategyInfo.strategy;
                    List<StrategyExecuteResult> strategyExecuteResultList = strategy.executeStrategy(task, user, strategyTriggerEvent);
                    for (StrategyExecuteResult strategyExecuteResult : strategyExecuteResultList) {
                        if (strategyExecuteResult.success) {
                            if (strategyExecuteResult.rewardAmount > 0) {

                                if (task.needEnd(strategyExecuteResult.rewardAmount)) {
                                    //已没有更多金额进行分配
                                    task.end();
                                } else {
                                    //个人账户增加
                                    S_User beneficialUser = strategyExecuteResult.beneficialUser;

                                    beneficialUser.account.addCoin(strategyExecuteResult.rewardAmount);
                                    B_User_Coin_Log.create_TaskAcquisition(task, beneficialUser, strategyExecuteResult.rewardAmount);

                                    //统计表记录数据
                                    B_Task_User_Statistics_Data.get(beneficialUser, task).addCoin(strategyExecuteResult.rewardAmount);

                                    //任务总账户标记增加（已分配金额）
                                    strategy_info.addHasAllocationCoin(strategyExecuteResult.rewardAmount);


                                    //打到用户微信账上(如果超过1元，并且用户具有appOpenid)
                                    if (strategyExecuteResult.rewardAmount >= 100 && beneficialUser.wechatInfo.appOpenid != null) {
                                        payPool.execute(new Runnable() {
                                            @Override
                                            public void run() {
                                                String nonce_str = Common.randomString(32).toUpperCase();
                                                String partner_trade_no = B_Wechat_Withdraw.getOnePartnerTradeNo();
                                                String openid = beneficialUser.wechatInfo.appOpenid;
                                                int amount = strategyExecuteResult.rewardAmount;
                                                String spbill_create_ip = remoteAddress;
                                                String desc = strategy_info.autoWithdrawDepositMessage == null ? "唯页红包 " + strategy_info.task.title : strategy_info.autoWithdrawDepositMessage;
                                                desc = desc.length() > 20 ? desc.substring(20) : desc;

                                                B_Wechat_Withdraw withdraw = B_Wechat_Withdraw.create(beneficialUser, nonce_str, partner_trade_no, openid, amount, desc);
                                                WithdrawDepositResult result = WithDrawDepositManage.transfers(nonce_str, partner_trade_no, openid, amount, desc, spbill_create_ip);
                                                if (result.isSuccess()) {
                                                    try {
                                                        beneficialUser.account.reduceCoin(amount);
                                                    } catch (Exception e) {
                                                        //需要处理一下，暂时没想好
//                                                        System.out.println("打钱出现异常");
                                                        e.printStackTrace();
                                                    }
                                                    withdraw.success(result.payment_no, result.payment_time);
                                                    B_User_Coin_Log.create_wehcatWitddrawDeposit(beneficialUser, amount, withdraw);
                                                } else {
                                                    withdraw.fail(result.getErrorMsg());
                                                }
//                                                System.out.println("打钱结束");
                                            }
                                        });

                                    }
                                }
                            }
                            if (strategyExecuteResult.point > 0) {
                                B_Task_User_Statistics_Data.get(strategyExecuteResult.beneficialUser, task).addCredit(strategyExecuteResult.point);
                            }

                            if (strategyExecuteResult.rewardAmount > 0 || strategyExecuteResult.point > 0) {
                                //策略标记
                                B_Task_Strategy.taskStrategyExecuteLogVerifyExistAndCreate(strategyExecuteResult.beneficialUser, strategyExecuteResult.giveBenefitUser, task, strategyExecuteResult.effectiveRule, strategyExecuteResult.rewardAmount);
                            }

                        }
                    }
                    Ebean.commitTransaction();
                } catch (Exception e) {

                } finally {
                    Ebean.endTransaction();
                }
            }

//            System.out.println("执行完成");
        }
    }


    public static B_Task migtation(S_User owner, String title, String remark, String coverImg, Date endTime,
                                   DataType dataType, String data, B_Article article, VisibleType visibleType, State state, int migrationid, Date created_time, String description) {
        B_Task task = new B_Task();
        task.owner = owner;
        task.title = title;
        task.remark = remark;
        task.coverImg = coverImg;
        task.endTime = endTime;
        task.dataType = dataType;
        task.data = data;
        task.article = article;
        task.visibleType = visibleType;
        task.state = state;
        task.migrationid = migrationid;
        task.editedTime = created_time;
        task.createdTime = created_time;
        task.description = description;
        task.save();
        return task;
    }

    public static B_Task findByMigtationId(int migrationid) {
        return B_Task.find.where().eq("migrationid", migrationid).findUnique();
    }


    public String getWechatRechargeOrderInfo() {
        B_User_Coin_Log log = B_User_Coin_Log.findForWechatTaskRecharge(this);
        if (log == null) {
            return null;
        } else {
            return log.inLog.wechatPay.out_trade_no;
        }
    }

    public String getCloneStr() {
        if (this.cloneStr == null) {
            String cloneStr = Common.randomString(20);
            while (B_Task.findByCloneStr(cloneStr) != null) {
                cloneStr = Common.randomString(20);
            }
            this.cloneStr = cloneStr;
            this.update();
        }
        return this.cloneStr;
    }

    public static B_Task findByCloneStr(String cloneStr) {
        return B_Task.find.where().eq("cloneStr", cloneStr).findUnique();
    }

    public B_Task copy(S_User user) {
        B_Task task = B_Task.create(user, this.title, this.remark, this.coverImg, this.endTime, this.taskType, this.dataType, this.data, this.article, this.visibleType, this.description);
        if (task.taskType == B_Task.TaskType.promotion) {
            B_Task_Promotion promotion = B_Task_Promotion.initByTask(task);
        }
        task.strategyInfo = this.strategyInfo.clone(task, user);
        task.viewTag = this.viewTag.clone(task);
        return task;
    }

    public static TaskType stringConvertTaskType(String str) {
        if (StringUtils.isNullOrEmpty(str)) {
            return TaskType.normal;
        }
        for (TaskType taskType : TaskType.values()) {
            if (taskType.toString().equals(str)) {
                return taskType;
            }
        }
        return TaskType.normal;
    }


    public int findPropagationInfoCount() {
        return B_Task_Propagation_Info.find.where().eq("task", this).findRowCount();
    }
}
