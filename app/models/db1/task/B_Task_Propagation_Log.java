package models.db1.task;

import com.alibaba.fastjson.JSONArray;
import com.avaje.ebean.*;
import com.avaje.ebean.Query;
import models.db1.CModel;
import models.db1.task.view.B_Task_View_Tag_Lable;
import models.db1.user.S_User;
import models.db1.user.tag.B_User_Tag;
import models.db1.user.tag.B_User_Tag_Enum;
import play.data.validation.Constraints;
import scala.Int;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015/1/20.
 */
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"executant_id", "task_id"})})
@Entity
public class B_Task_Propagation_Log extends CModel {

    @Column(nullable = false)
    @ManyToOne
    public S_User executant;

    @Column(nullable = false)
    @ManyToOne
    public S_User preExecutant;

    @Constraints.Required
    @ManyToOne
    public B_Task task;

    public int level;


    public static Finder<Integer, B_Task_Propagation_Log> find = new Finder<Integer, B_Task_Propagation_Log>(
            Integer.class, B_Task_Propagation_Log.class
    );

    public static B_Task_Propagation_Log create(B_Task task, S_User preExecutant, S_User executant, int level) {
        B_Task_Propagation_Log log = findByTaskAndExecutant(task, executant);
        if (log == null) {
            log = new B_Task_Propagation_Log();
            log.executant = executant;
            log.preExecutant = preExecutant;
            log.task = task;
            log.level = level;
            log.save();
        }
        return log;
    }


    public static B_Task_Propagation_Log findByTaskAndExecutant(B_Task task, S_User executant) {
        return B_Task_Propagation_Log.find.fetch("executant").fetch("task").fetch("preExecutant").where().eq("task", task).eq("executant", executant).findUnique();
    }

    public static int getCount(B_Task task) {
        return B_Task_Propagation_Log.find.where().eq("task", task).findRowCount();
    }

    /**
     * 获取当前节点的子节点数
     *
     * @return
     */
    public int numberOfChildNodes() {
        return B_Task_Propagation_Log.find.where().eq("task", this.task).eq("preExecutant", this.executant).ne("executant", this.executant).findRowCount();
    }

    /**
     * 获取某个任务某一层级的子节点
     *
     * @return
     */
    public static List<B_Task_Propagation_Log> levelChildNodes(B_Task task, int level) {
        return B_Task_Propagation_Log.find.where().eq("task", task).eq("level", level).eq("deleteLogic", false).findList();
    }

    /**
     * 获取当前节点的所有子节点数量（本身排除在外）
     *
     * @return
     */
    public Integer allChildNodeNum() {
        int currentLogExecutantId = this.executant.id;
        //getAllChildLst()函数返回的是当前节点的所有子节点的executantId字段的拼接值，类似$1,2,3,4,5,6
        //然后再利用MySQL的find_in_set函数去查询表里所有executant_id落在该字符串里的记录，也就是当前节点的所有子节点记录（包含自己）
        String sqlStr ="select count(*) \"count\" from b_task_propagation_log where FIND_IN_SET(executant_id, getAllChildLst(:executantId)) and executant_id != pre_executant_id";
        SqlRow resultSqlRow = Ebean.createSqlQuery(sqlStr).setParameter("executantId", currentLogExecutantId).findUnique();
        Integer count = resultSqlRow.getInteger("count");
        return count;
    }

    /**
     *查询当前log节点的所有子节点（本身排除在外）
     */
    public List<B_Task_Propagation_Log> findAllChildren() {
        int currentLogExecutantId = this.executant.id;
        String sqlStr ="select id from b_task_propagation_log where FIND_IN_SET(executant_id, getAllChildLst(:executantId)) and executant_id != pre_executant_id";
        List<SqlRow> resultSqlRowList = Ebean.createSqlQuery(sqlStr).setParameter("executantId", currentLogExecutantId).findList();
        List<Integer> idList=new ArrayList<>();
        for(SqlRow row:resultSqlRowList){
            idList.add(row.getInteger("id"));
        }
        return B_Task_Propagation_Log.find.where().in("id",idList).findList();
    }

    /**
     *查询当前log节点的level等于某个值的所有子节点
     */
    public List<B_Task_Propagation_Log> findlevelChildren(int levelValue) {
        int currentLogExecutantId = this.executant.id;
        String sqlStr ="select id from b_task_propagation_log where FIND_IN_SET(executant_id, getAllChildLst(:executantId)) and level=:levelValue";
        List<SqlRow> resultSqlRowList = Ebean.createSqlQuery(sqlStr).setParameter("executantId", currentLogExecutantId).setParameter("levelValue",levelValue).findList();
        List<Integer> idList=new ArrayList<>();
        for(SqlRow row:resultSqlRowList){
            idList.add(row.getInteger("id"));
        }
        return B_Task_Propagation_Log.find.where().in("id",idList).findList();
    }

    /**
     * 获取传播的深度，也就是子节点里level的最大值
     * cdp
     * @return
     */
    public static int getPropagationDepth(B_Task task) {
        B_Task_Propagation_Log log = B_Task_Propagation_Log.find.where().eq("task", task).orderBy("level desc").setMaxRows(1).findUnique();
        if(log == null) {
            return 0;
        } else {
            return log.level;
        }
    }

    /**
     * 获取任务的某个层级的所有节点用户的Id列表
     * cdp
     * @return
     */
    public static List<Integer> getTaskLevelUserIds(B_Task task, int level) {
        List<Integer> userIdList = new ArrayList<Integer>();
        List<B_Task_Propagation_Log> logs;
        if(level == 0 ) {
            logs = B_Task_Propagation_Log.find.where().eq("task", task).findList();
        } else {
            logs = B_Task_Propagation_Log.find.where().eq("task", task).eq("level", level).findList();
        }
        for(B_Task_Propagation_Log log : logs) {
            userIdList.add(log.executant.id);
        }
        return userIdList;
    }


    public static List<B_Task_Propagation_Log> findRootByTask(B_Task task){
        return B_Task_Propagation_Log.find.where().eq("task", task).eq("level",1).findList();
    }

    /**
     * 查询当前log节点的所有直属子节点（第一层子节点）
     */
    public static List<B_Task_Propagation_Log> findChildren(B_Task_Propagation_Log log){
        return B_Task_Propagation_Log.find.where().eq("task", log.task).eq("preExecutant",log.executant).ne("executant",log.executant).findList();
    }

    public static B_Task_Propagation_Log findParent(B_Task_Propagation_Log log){
        return B_Task_Propagation_Log.find.where().eq("task", log.task).eq("executant",log.preExecutant).findUnique();
    }

    public static List<B_Task_Propagation_Log> findAllParent(B_Task_Propagation_Log log){
        List<B_Task_Propagation_Log> parents = new ArrayList<B_Task_Propagation_Log>();
        B_Task_Propagation_Log parent =log;
        while(parent.level != 1) {
            parent = B_Task_Propagation_Log.find.where().eq("task", log.task).eq("executant",parent.preExecutant).findUnique();
            parents.add(parent);
        }
        return parents;
    }

    public static B_Task_Propagation_Log findByUuid(String uuid){
        return B_Task_Propagation_Log.find.where().eq("uuid", uuid).findUnique();
    }

    /**
     * 找到某节点的上X层节点
     *
     * @param task         任务
     * @param currentNode  当前节点
     * @param displacement 位移量
     * @return
     */
    public static B_Task_Propagation_Log findSuperiorNodes(B_Task task, B_Task_Propagation_Log currentNode, int displacement) {
        if (displacement < 0) {
            return null;
        } else if (displacement == 0) {
            return currentNode;
        } else {
            if (currentNode.level - displacement > 0) {
                B_Task_Propagation_Log currentRelativeNode = currentNode;
                for (int i = 0; i < displacement; i++) {
                    currentRelativeNode = findByTaskAndExecutant(task, currentRelativeNode.preExecutant);
                    if(currentRelativeNode==null){
                        return null;
                    }
                }
                return currentRelativeNode;
            } else {
                return null;
            }
        }
    }


    public static B_Task_Propagation_Log migration(B_Task task, S_User preExecutant, S_User executant, int level, Date created_time) {
        B_Task_Propagation_Log log = findByTaskAndExecutant(task, executant);
        if (log == null) {
            log = new B_Task_Propagation_Log();
            log.executant = executant;
            log.preExecutant = preExecutant;
            log.task = task;
            log.level = level;
            log.createdTime = created_time;
            log.editedTime = created_time;
            log.save();
        }
        return log;
    }


    public static int getCountBetweenStartTimeAndEndTime(B_Task task,Date startTime ,Date endTime) {
        return B_Task_Propagation_Log.find.where().eq("task", task).gt("createdTime",startTime).le("createdTime",endTime).findRowCount();
    }

    /**
     * 根据标签查询条件来查询符合条件的log列表
     *
     * @param isQueryAll       是否是查询"所有节点"
     * @param queryObjectList  被查询的目标Log列表
     * @param tagQueryList     查询条件数组
     * @param tagQueryList     当前登录用户，标签都是当前用户给节点用户贴上去的
     * @return List            符合查询条件的B_Task_Propagation_Log 列表
     */
    public static List<B_Task_Propagation_Log> getTagQueryLogList(boolean isQueryAll, List<B_Task_Propagation_Log> queryObjectList, JSONArray tagQueryList, S_User currentUser) {
        List<B_Task_Propagation_Log> result = new ArrayList<B_Task_Propagation_Log>();

        for(B_Task_Propagation_Log log : queryObjectList) {
            List<Boolean> flagList = new ArrayList<Boolean>();
            for(int m=0; m<tagQueryList.size();m++) {
                int totalCount = 0;
                int tagQuerylevel = tagQueryList.getJSONObject(m).getIntValue("levelId");
                int tagQueryData = tagQueryList.getJSONObject(m).getIntValue("data");
                int tagQueryTagId = tagQueryList.getJSONObject(m).getIntValue("tagId");
                //当前查询的标签
                B_User_Tag_Enum currentTagEnum = B_User_Tag_Enum.find.byId(tagQueryTagId);
                //当前log节点的子节点
                List<B_Task_Propagation_Log> childLogList = new ArrayList<B_Task_Propagation_Log>();
                if(isQueryAll) {//如果是查询"所有节点"，那就查出当前节点的所有层级的子节点
                    childLogList = log.findAllChildren();
                } else {//如果是查询"某级节点"，那就查出当前节点的某级+1（直属下一级）的子节点
                    childLogList = log.findlevelChildren(tagQuerylevel + 1);
                }
                for(B_Task_Propagation_Log item :childLogList) {
                    boolean hasCurrentTagEnum = B_User_Tag.isExistByTaggedUserAndTaggingUser(currentUser,item.executant,currentTagEnum);
                    if(hasCurrentTagEnum) {
                        totalCount ++;
                    }
                }
                if(totalCount == tagQueryData) {
                    flagList.add(true);
                } else {
                    flagList.add(false);
                }
            }
            if(flagList.contains(false)){
                //只有所有的标签查询都是true，当前的log才是符合查询要求的
                //do nothing
            } else {
                //全部为true的话，当前的log可以添加到targetLogList
                result.add(log);
            }
        }
        return result;
    }

    /**
     * 获取当前节点的子节点所填写的信息数量
     * @return
     */
    public Integer getPropagationInfoCount(){
        return B_Task_Propagation_Info.find.fetch("executantPropagationLog").where().eq("task",this.task).eq("executantPropagationLog.preExecutant",this.executant).ne("executantPropagationLog.executant", this.executant).eq("deleteLogic", false).findRowCount();
    }

}
