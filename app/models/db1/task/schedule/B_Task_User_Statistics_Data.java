package models.db1.task.schedule;

import beans.task.strategy.StrategyExecuteResult;
import com.avaje.ebean.Ebean;
import controllers.util.UrlController;
import models.db1.CModel;
import models.db1.article.B_Article;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Log;
import models.db1.task.strategy.B_Task_Strategy;
import models.db1.task.strategy.B_Task_Strategy_Info;
import models.db1.task.strategy.B_Task_Strategy_Rule;
import models.db1.task.strategy.B_Task_Strategy_Rule_Condition;
import models.db1.task.view.B_Task_View_Tag;
import models.db1.user.S_User;
import models.db1.user.coin.B_User_Coin_In_Log;
import models.db1.user.coin.B_User_Coin_Log;
import models.db1.user.coin.B_User_Coin_Out_Log;
import models.db1.wechat.B_Wechat_Pay;
import models.db1.wechat.B_Wechat_Withdraw;
import play.data.validation.Constraints;
import play.mvc.Http;
import util.Common;
import util.LockUtil;
import wechat.pay.WithDrawDepositManage;
import wechat.pay.beans.withdrawDeposit.WithdrawDepositResult;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015/1/20.
 * 任务用户的统计数据
 */
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"owner_id", "task_id"})})
@Entity
public class B_Task_User_Statistics_Data extends CModel {

    @Column(nullable = false)
    @ManyToOne
    public S_User owner;

    @Constraints.Required
    @ManyToOne
    public B_Task task;

    //此次任务用户所获得所有金钱
    public int coin;

    //此次任务用户所获得所有积分
    public int credit;

    //此次任务用户所阅读的时间
    public int readTime;

    //此次任务用户阅读时所在的经度
    public Double longitude;
    //此次任务用户阅读时所在的纬度
    public Double latitude;

    //此次任务用户阅读时所在的纬度
    public Boolean hasShareToFriendCircle = false;

    //用户是否在此次任务中静止发送红包
//    public Boolean isStoped = false;

    //此次任务用户的阅读方式
    public B_Task_Strategy_Rule_Condition.ReadingStyle readingStyle;

    public static Finder<Integer, B_Task_User_Statistics_Data> find = new Finder<Integer, B_Task_User_Statistics_Data>(
            Integer.class, B_Task_User_Statistics_Data.class
    );

    public static B_Task_User_Statistics_Data create(S_User owner, B_Task task) {
        B_Task_User_Statistics_Data data = findByOwnerAndTask(owner, task);
        if (data == null) {
            data = new B_Task_User_Statistics_Data();
            data.owner = owner;
            data.task = task;
            data.save();
        }
        return data;
    }

    public static B_Task_User_Statistics_Data migration(S_User owner, B_Task task, int coin, Date time) {
        B_Task_User_Statistics_Data data = findByOwnerAndTask(owner, task);
        if (data == null) {
            data = new B_Task_User_Statistics_Data();
            data.owner = owner;
            data.task = task;
            data.coin = coin;
            data.createdTime = time;
            data.editedTime = time;
            data.save();
        }
        return data;
    }

    public static B_Task_User_Statistics_Data get(S_User owner, B_Task task) {
        return create(owner, task);
    }

    public static B_Task_User_Statistics_Data findByOwnerAndTask(S_User owner, B_Task task) {
        return B_Task_User_Statistics_Data.find.where().eq("owner", owner).eq("task", task).findUnique();
    }

    public void addCoin(int coin) {
        this.coin += coin;
        this.update();
    }

    public void addCredit(int credit) {
        this.credit += credit;
        this.update();
    }

    public void updateLocationCoordinate(Double longitude,Double latitude){
        this.longitude=longitude;
        this.latitude=latitude;
        this.update();
    }

    public void updateShareToFriendCircleState(Boolean hasShareToFriendCircle){
        this.hasShareToFriendCircle=hasShareToFriendCircle;
        this.update();
    }

    //获取金钱排名前row位
    public static List<B_Task_User_Statistics_Data> findForCoinTop(B_Task task, int row) {
        return B_Task_User_Statistics_Data.find.where().eq("task", task).order("coin desc").setMaxRows(row).findList();
    }


    //获取自己金钱排名
    public static int getSelfCoinBillBoard(B_Task task, S_User user) {
        B_Task_User_Statistics_Data self = B_Task_User_Statistics_Data.get(user, task);
        return B_Task_User_Statistics_Data.find.where().gt("coin", self.coin).findRowCount() + 1;
    }

    //获取积分排名前row位
    public static List<B_Task_User_Statistics_Data> findForCreditTop(B_Task task, int row) {
        return B_Task_User_Statistics_Data.find.where().eq("task", task).order("credit desc").setMaxRows(row).findList();
    }

    //获取自己金钱排名
    public static int getSelfCreditBillBoard(B_Task task, S_User user) {
        B_Task_User_Statistics_Data self = B_Task_User_Statistics_Data.get(user, task);
        return B_Task_User_Statistics_Data.find.where().gt("credit", self.credit).findRowCount() + 1;
    }

    //获取阅读时间
    public static int getUserReadTimeByOwnerAndTask(S_User owner, B_Task task) {
        B_Task_User_Statistics_Data taskUserStatisticsData = findByOwnerAndTask(owner, task);
        if (taskUserStatisticsData == null) {
            return 0;
        } else {
            return taskUserStatisticsData.readTime;
        }
    }

    //获取阅读方式
    public static B_Task_Strategy_Rule_Condition.ReadingStyle getUserReadStyleByOwnerAndTask(S_User owner, B_Task task) {
        B_Task_User_Statistics_Data taskUserStatisticsData = findByOwnerAndTask(owner, task);
        if (taskUserStatisticsData == null) {
            return null;
        } else {
            return taskUserStatisticsData.readingStyle;
        }
    }

}
