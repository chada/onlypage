package models.db1.task.strategy;

import models.db1.CModel;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Log;
import models.db1.user.S_User;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/11/22.
 */
@Entity
public class B_Task_Strategy_Execute_Log extends CModel {

    //受益者
    @ManyToOne
    public S_User beneficialUser;

    //给予利益者
    @ManyToOne
    public S_User giveBenefitUser;

    @ManyToOne
    public B_Task task;

    @ManyToOne
    public B_Task_Strategy_Rule taskStrategyRule;

    public int settleRewardAmount = 0;


    public static Finder<Integer, B_Task_Strategy_Execute_Log> find = new Model.Finder<Integer, B_Task_Strategy_Execute_Log>(
            Integer.class, B_Task_Strategy_Execute_Log.class
    );

    /**
     * 获取奖励日志记录
     *
     * @param task           任务
     * @param beneficialUser 用户
     * @param rule           规则
     * @return
     */
    public static B_Task_Strategy_Execute_Log findByTaskAndUserAndRule(B_Task task, S_User beneficialUser, B_Task_Strategy_Rule rule) {
        return B_Task_Strategy_Execute_Log.find.where().eq("task", task).eq("beneficialUser", beneficialUser).eq("taskStrategyRule", rule).findUnique();
    }

    /**
     * 检测是否存在用户在规则下获奖记录
     *
     * @param task            任务
     * @param beneficialUser  受益用户
     * @param giveBenefitUser 给予利益用户
     * @param rule            规则
     * @return
     */
    public static boolean isExistFindByTaskAndUserAndRule(B_Task task, S_User beneficialUser, S_User giveBenefitUser, B_Task_Strategy_Rule rule) {
        return B_Task_Strategy_Execute_Log.find.where().eq("task", task).eq("beneficialUser", beneficialUser).eq("giveBenefitUser", giveBenefitUser).eq("taskStrategyRule", rule).findRowCount() > 0 ? true : false;
    }

    /**
     * 用户在规则下获奖记录条数
     *
     * @param task            任务
     * @param beneficialUser  受益用户
     * @param giveBenefitUser 给予利益用户
     * @param rule            规则
     * @return
     */
    public static int hasExistNumFindByTaskAndUserAndRule(B_Task task, S_User beneficialUser, S_User giveBenefitUser, B_Task_Strategy_Rule rule) {
        return B_Task_Strategy_Execute_Log.find.where().eq("task", task).eq("beneficialUser", beneficialUser).eq("giveBenefitUser", giveBenefitUser).eq("taskStrategyRule", rule).findRowCount();
    }


    /**
     * 创建策略规则奖励日志（检测是否存在）
     *
     * @param beneficialUser     收益用户
     * @param task               任务
     * @param taskStrategyRule   规则
     * @param settleRewardAmount 实际获取奖励数值（不区分积分和金钱）
     * @return
     */
    public static B_Task_Strategy_Execute_Log verifyExistAndCreate(S_User beneficialUser, S_User giveBenefitUser, B_Task task, B_Task_Strategy_Rule taskStrategyRule, int settleRewardAmount) {
        if (!isExistFindByTaskAndUserAndRule(task, beneficialUser, giveBenefitUser, taskStrategyRule)) {
            B_Task_Strategy_Execute_Log log = new B_Task_Strategy_Execute_Log();
            log.beneficialUser = beneficialUser;
            log.taskStrategyRule = taskStrategyRule;
            log.giveBenefitUser = giveBenefitUser;
            log.task = task;
            log.settleRewardAmount = settleRewardAmount;
            log.save();
            return log;
        } else {
            return null;
        }
    }


    /**
     * 根据给予利益用户获取发放奖励日志数量（用户发出的奖励个数）
     * @param giveBenefitUser 给予利益者
     * @param task 任务
     * @param taskStrategyRule 策略规则
     * @param includeSelf 是否包括自己发给自己
     * @return
     */
    public static Integer getLogNumByGiveBenefitUser(S_User giveBenefitUser,B_Task task,B_Task_Strategy_Rule taskStrategyRule,boolean includeSelf){
        if(includeSelf){
            return B_Task_Strategy_Execute_Log.find.where().eq("giveBenefitUser",giveBenefitUser).eq("task",task).eq("taskStrategyRule",taskStrategyRule).findRowCount();
        }else {
            return B_Task_Strategy_Execute_Log.find.where().eq("giveBenefitUser",giveBenefitUser).ne("beneficialUser",giveBenefitUser).eq("task",task).eq("taskStrategyRule",taskStrategyRule).findRowCount();
        }
    }

    /**
     * 根据获益用户获取获得奖励日志数量（用户获得的奖励个数）
     * @param beneficialUser 给予利益者
     * @param task 任务
     * @param taskStrategyRule 策略规则
     * @param includeSelf 是否包括自己发给自己
     * @return
     */
    public static Integer getLogNumByBeneficialUser(S_User beneficialUser,B_Task task,B_Task_Strategy_Rule taskStrategyRule,boolean includeSelf){
        if(includeSelf){
            return B_Task_Strategy_Execute_Log.find.where().eq("beneficialUser",beneficialUser).eq("task",task).eq("taskStrategyRule",taskStrategyRule).findRowCount();
        }else {
            return B_Task_Strategy_Execute_Log.find.where().eq("beneficialUser",beneficialUser).ne("giveBenefitUser",beneficialUser).eq("task",task).eq("taskStrategyRule",taskStrategyRule).findRowCount();
        }
    }

    /**
     * 根据给予利益用户获取发放奖励总额（用户发出的奖励总额）
     * @param giveBenefitUser 给予利益者
     * @param task 任务
     * @param taskStrategyRule 策略规则
     * @param includeSelf 是否包括自己发给自己
     * @return
     */
    public static Integer getRewarAmountSumByGiveBeneficialUser(S_User giveBenefitUser,B_Task task,B_Task_Strategy_Rule taskStrategyRule,boolean includeSelf){
        List<B_Task_Strategy_Execute_Log> taskStrategyExecuteLogList=new ArrayList<>();
        if(includeSelf){
            taskStrategyExecuteLogList=  B_Task_Strategy_Execute_Log.find.where().eq("giveBenefitUser",giveBenefitUser).eq("task",task).eq("taskStrategyRule",taskStrategyRule).findList();
        }else {
            taskStrategyExecuteLogList= B_Task_Strategy_Execute_Log.find.where().eq("giveBenefitUser",giveBenefitUser).ne("beneficialUser",giveBenefitUser).eq("task",task).eq("taskStrategyRule",taskStrategyRule).findList();
        }
        Integer result=0;
        for (B_Task_Strategy_Execute_Log log:taskStrategyExecuteLogList){
            result+=log.settleRewardAmount;
        }
        return result;
    }

    /**
     * 根据受益用户获取获得奖励总额（用户获得的奖励总额）
     * @param beneficialUser 给予利益者
     * @param task 任务
     * @param taskStrategyRule 策略规则
     * @param includeSelf 是否包括自己发给自己
     * @return
     */
    public static Integer getRewarAmountSumByBeneficialUser(S_User beneficialUser,B_Task task,B_Task_Strategy_Rule taskStrategyRule,boolean includeSelf){
        List<B_Task_Strategy_Execute_Log> taskStrategyExecuteLogList=new ArrayList<>();
        if(includeSelf){
            taskStrategyExecuteLogList=  B_Task_Strategy_Execute_Log.find.where().eq("beneficialUser",beneficialUser).eq("task",task).eq("taskStrategyRule",taskStrategyRule).findList();
        }else {
            taskStrategyExecuteLogList= B_Task_Strategy_Execute_Log.find.where().eq("beneficialUser",beneficialUser).ne("giveBenefitUser",beneficialUser).eq("task",task).eq("taskStrategyRule",taskStrategyRule).findList();
        }
        Integer result=0;
        for (B_Task_Strategy_Execute_Log log:taskStrategyExecuteLogList){
            result+=log.settleRewardAmount;
        }
        return result;
    }
}
