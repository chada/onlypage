package models.db1.task.strategy;

import com.alibaba.fastjson.annotation.JSONField;
import com.mysql.jdbc.StringUtils;
import com.ning.org.jboss.netty.util.internal.StringUtil;
import models.db1.CModel;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Log;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Administrator on 2016/11/23.
 */
@Entity
public class B_Task_Strategy_Rule_Condition extends CModel {
    public enum ConditionNodeType{self,up}
    public enum AllocationConditions{childnode,location,tag,readingtime,readingstyle,redpocketquantity,redpocketsum,sharetofriendcircle}
    public enum ReadingStyle{normal,QRcode}
    public enum TagCondition{include,exclude}

    @ManyToOne
    @JSONField(serialize = false)
    @Column(nullable = false)
    public B_Task_Strategy_Rule strategyRule;

    //--------------------------------------------------------
    //条件触发节点类型
    @Column(nullable = false)
    public ConditionNodeType conditionNodeType;

    //上X级节点—conditionNodeType为up时填
    public Integer upConditionNodeNum;



    //分配条件
    @Column(nullable = false)
    public AllocationConditions allocationConditions;

    //子节点数限定值
    public Integer childnodeNum;

    //地理位置限定值
    public String location;

    //经度
    public Double longitude;
    //纬度
    public Double latitude;
    //有效范围
    public Integer locationRange;

    //标签
    public String tag;
    //标签状况
    public TagCondition tagCondition;

    //阅读时间
    public Integer readingTime;

    //阅读方式
    public ReadingStyle readingstyle;

    //红包数量限制(<=)
    public Integer redpocketquantity;

    //红包金额限制(<=)
    public Integer redpocketsum;


    public static Finder<Integer, B_Task_Strategy_Rule_Condition> find = new Model.Finder<Integer, B_Task_Strategy_Rule_Condition>(
            Integer.class, B_Task_Strategy_Rule_Condition.class
    );

    public static List<B_Task_Strategy_Rule_Condition> findByTaskStrategyRule(B_Task_Strategy_Rule taskStrategyRule) {
        return B_Task_Strategy_Rule_Condition.find.where().eq("strategyRule", taskStrategyRule).findList();
    }

    /**
     * 创建策略规则分配条件
     * @param strategyRule 策略规则
     * @param conditionNodeType 条件触发节点类型
     * @param upConditionNodeNum 上级触发节点位移量
     * @param allocationConditions 分配条件类型
     * @param childnodeNum 子节点数量
     * @param location 地址
     * @param tag 标签
     * @param tagCondition 标签情况
     * @param readingTime 阅读时间
     * @param readingstyle 阅读方式
     * @return
     */
    public static B_Task_Strategy_Rule_Condition create(B_Task_Strategy_Rule strategyRule,ConditionNodeType conditionNodeType,Integer upConditionNodeNum,AllocationConditions allocationConditions,Integer childnodeNum,
                                                        String location,Double longitude,Double latitude,Integer locationRange, String tag,TagCondition tagCondition,Integer readingTime,ReadingStyle readingstyle,Integer redpocketquantity,Integer redpocketsum){
        B_Task_Strategy_Rule_Condition taskStrategyRuleCondition=new B_Task_Strategy_Rule_Condition();
        taskStrategyRuleCondition.strategyRule=strategyRule;
        taskStrategyRuleCondition.conditionNodeType=conditionNodeType;
        taskStrategyRuleCondition.upConditionNodeNum=upConditionNodeNum;
        taskStrategyRuleCondition.allocationConditions=allocationConditions;
        taskStrategyRuleCondition.childnodeNum=childnodeNum;
        taskStrategyRuleCondition.location=location;
        taskStrategyRuleCondition.longitude=longitude;
        taskStrategyRuleCondition.latitude=latitude;
        taskStrategyRuleCondition.locationRange=locationRange;
        taskStrategyRuleCondition.tag=tag;
        taskStrategyRuleCondition.tagCondition=tagCondition;
        taskStrategyRuleCondition.readingTime=readingTime;
        taskStrategyRuleCondition.readingstyle=readingstyle;
        taskStrategyRuleCondition.redpocketquantity=redpocketquantity;
        taskStrategyRuleCondition.redpocketsum=redpocketsum;
        taskStrategyRuleCondition.save();

        return taskStrategyRuleCondition;
    }

    public static B_Task_Strategy_Rule_Condition createOrUpdate(B_Task_Strategy_Rule strategyRule,Integer conditionId,ConditionNodeType conditionNodeType,Integer upConditionNodeNum,AllocationConditions allocationConditions,Integer childnodeNum,
                                                        String location,Double longitude,Double latitude,Integer locationRange, String tag,TagCondition tagCondition,Integer readingTime,ReadingStyle readingstyle,Integer redpocketquantity,Integer redpocketsum){
        B_Task_Strategy_Rule_Condition taskStrategyRuleCondition=new B_Task_Strategy_Rule_Condition();
        if(conditionId!=null) {
            taskStrategyRuleCondition=B_Task_Strategy_Rule_Condition.find.byId(conditionId);
            if(taskStrategyRuleCondition==null){
                return null;
            }
        }
        taskStrategyRuleCondition.strategyRule=strategyRule;
        taskStrategyRuleCondition.conditionNodeType=conditionNodeType;
        taskStrategyRuleCondition.upConditionNodeNum=upConditionNodeNum;
        taskStrategyRuleCondition.allocationConditions=allocationConditions;
        taskStrategyRuleCondition.childnodeNum=childnodeNum;
        taskStrategyRuleCondition.location=location;
        taskStrategyRuleCondition.longitude=longitude;
        taskStrategyRuleCondition.latitude=latitude;
        taskStrategyRuleCondition.locationRange=locationRange;
        taskStrategyRuleCondition.tag=tag;
        taskStrategyRuleCondition.tagCondition=tagCondition;
        taskStrategyRuleCondition.readingTime=readingTime;
        taskStrategyRuleCondition.readingstyle=readingstyle;
        taskStrategyRuleCondition.redpocketquantity=redpocketquantity;
        taskStrategyRuleCondition.redpocketsum=redpocketsum;
        taskStrategyRuleCondition.save();

        return taskStrategyRuleCondition;
    }


    /**
     * string转换成条件触发节点类型ConditionNodeType
     * @param str
     * @return
     */
    public static ConditionNodeType stringConvertConditionNodeType(String str){
        if(StringUtils.isNullOrEmpty(str)){
            return null;
        }
        for (ConditionNodeType conditionNodeType:ConditionNodeType.values()){
            if(conditionNodeType.toString().equals(str)){
                return conditionNodeType;
            }
        }
        return null;
    }


    /**
     * string转换成分配条件类型AllocationConditions
     * @param str
     * @return
     */
    public static AllocationConditions stringConvertAllocationConditions(String str){
        if(StringUtils.isNullOrEmpty(str)){
            return null;
        }
        for (AllocationConditions allocationConditions:AllocationConditions.values()){
            if(allocationConditions.toString().equals(str)){
                return allocationConditions;
            }
        }
        return null;
    }


    /**
     * string转换成阅读方式类型ReadingStyle
     * @param str
     * @return
     */
    public static ReadingStyle stringConvertReadingStyle(String str){
        if(StringUtils.isNullOrEmpty(str)){
            return null;
        }
        for (ReadingStyle readingStyle:ReadingStyle.values()){
            if(readingStyle.toString().equals(str)){
                return readingStyle;
            }
        }
        return null;
    }

    /**
     * string转换成标签状况类型TagCondition
     * @param str
     * @return
     */
    public static TagCondition stringConvertTagCondition(String str){
        if(StringUtils.isNullOrEmpty(str)){
            return null;
        }
        for (TagCondition tagCondition:TagCondition.values()){
            if(tagCondition.toString().equals(str)){
                return tagCondition;
            }
        }
        return null;
    }

    public B_Task_Propagation_Log getReactNode(B_Task task, B_Task_Propagation_Log node){
        B_Task_Propagation_Log reactNode=new B_Task_Propagation_Log();
        if(this.conditionNodeType.equals(B_Task_Strategy_Rule_Condition.ConditionNodeType.self)){
            reactNode=node;
        }else if(this.conditionNodeType.equals(B_Task_Strategy_Rule_Condition.ConditionNodeType.up)){
            reactNode=B_Task_Propagation_Log.findSuperiorNodes(task,node,this.upConditionNodeNum);//获取当前节点上X层节点的子节点
        }
        return reactNode;
    }
}
