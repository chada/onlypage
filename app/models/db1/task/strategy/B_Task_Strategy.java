package models.db1.task.strategy;

import beans.task.strategy.DefaultStrategyOne;
import beans.task.strategy.StrategyExecuteResult;
import com.alibaba.fastjson.JSONObject;
import beans.task.strategy.StrategyTriggerEvent;
import com.alibaba.fastjson.annotation.JSONField;
import models.db1.CModel;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Log;
import models.db1.task.promotion.B_Task_Promotion;
import models.db1.user.S_User;
import play.db.ebean.Model;
import scala.Int;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/11/21.
 */
@Entity
public class B_Task_Strategy extends CModel {
    public enum StrategyType {advancedStrategy, defaultStrategyOne}

    @Column(nullable = false)
    @ManyToOne
    @JSONField(serialize = false)
    public S_User owner;

    @Column(nullable = false)
    public String title;

    @Column(nullable = false)
    public StrategyType strategyType;

    public String remark;

    @OneToMany(mappedBy = "strategy", cascade = CascadeType.ALL)
    public List<B_Task_Strategy_Rule> strategyRuleList;


    @OneToMany(mappedBy = "strategy")
    @JoinColumn(name = "strategy_id")
    @JSONField(serialize = false)
    public List<B_Task_Strategy_Info> taskStrategyInfoList;


    public static Finder<Integer, B_Task_Strategy> find = new Model.Finder<Integer, B_Task_Strategy>(
            Integer.class, B_Task_Strategy.class
    );

    public B_Task_Strategy copy(S_User owner){
        B_Task_Strategy copiedStrategy = this;
        //创建策略
        B_Task_Strategy newStrategy=create(owner,copiedStrategy.title,copiedStrategy.strategyType,copiedStrategy.remark);

        //创建规则及分配条件
        for(B_Task_Strategy_Rule copiedTaskStrategyRule:copiedStrategy.strategyRuleList){
            //创建规则
            B_Task_Strategy_Rule newTaskStrategyRule=B_Task_Strategy_Rule.create(newStrategy,copiedTaskStrategyRule.triggerEvent,copiedTaskStrategyRule.readTime,
                    copiedTaskStrategyRule.startTriggerNodeLevel,copiedTaskStrategyRule.endTriggerNodeLevel,copiedTaskStrategyRule.rewardUserType,copiedTaskStrategyRule.upRewardUserNum,copiedTaskStrategyRule.rewardType,
                    copiedTaskStrategyRule.fixedRewardAmount,copiedTaskStrategyRule.minRewardAmount,copiedTaskStrategyRule.maxRewardAmount,copiedTaskStrategyRule.strategyRuleTriggerNum);

            //创建分配条件
            for(B_Task_Strategy_Rule_Condition copiedTaskStrategyRuleCondition:copiedTaskStrategyRule.strategyRuleConditionList){
                B_Task_Strategy_Rule_Condition newTaskStrategyRuleCondition=B_Task_Strategy_Rule_Condition.create(newTaskStrategyRule,copiedTaskStrategyRuleCondition.conditionNodeType,copiedTaskStrategyRuleCondition.upConditionNodeNum,
                        copiedTaskStrategyRuleCondition.allocationConditions,copiedTaskStrategyRuleCondition.childnodeNum,
                        copiedTaskStrategyRuleCondition.location,copiedTaskStrategyRuleCondition.longitude,copiedTaskStrategyRuleCondition.latitude,copiedTaskStrategyRuleCondition.locationRange,
                        copiedTaskStrategyRuleCondition.tag,copiedTaskStrategyRuleCondition.tagCondition,
                        copiedTaskStrategyRuleCondition.readingTime,copiedTaskStrategyRuleCondition.readingstyle,copiedTaskStrategyRuleCondition.redpocketquantity,copiedTaskStrategyRuleCondition.redpocketsum);
            }
        }

        return newStrategy;
    }

    /**
     * 创建策略
     * @param owner 拥有者
     * @param title 策略名称
     * @param strategyType 策略类型
     * @param remark 备注
     * @return
     */
    public static B_Task_Strategy create(S_User owner,String title,StrategyType strategyType,String remark){
        B_Task_Strategy strategy=new B_Task_Strategy();
        strategy.owner=owner;
        strategy.title=title;
        strategy.strategyType=strategyType;
        strategy.remark=remark;
        strategy.save();
        return strategy;
    }


    public static B_Task_Strategy findByUuid(String uuid) {
        return B_Task_Strategy.find.where().eq("uuid", uuid).findUnique();
    }


    /**
     * 获取触发时间（动作）列表
     *
     * @return
     */
    public List<StrategyTriggerEvent> acquireTriggerEventList() {
        List<StrategyTriggerEvent> strategyTriggerEventList = new ArrayList<>();
        for (B_Task_Strategy_Rule strategyRule : this.strategyRuleList) {
            StrategyTriggerEvent triggerEvent = new StrategyTriggerEvent(strategyRule.triggerEvent, strategyRule.readTime);
            boolean existFlag = false;
            for (StrategyTriggerEvent event : strategyTriggerEventList) {
                if (event.isSameAs(triggerEvent)) {
                    existFlag = true;
                    break;
                }
            }

            if (!existFlag) {
                strategyTriggerEventList.add(triggerEvent);
            }
        }
        return strategyTriggerEventList;
    }

    /**
     * 是否含有地址分配条件
     *
     * @return
     */
    public boolean existLocationRuleCondition() {
        boolean existFlag = false;
        for (B_Task_Strategy_Rule strategyRule : this.strategyRuleList) {
            for (B_Task_Strategy_Rule_Condition ruleCondition : strategyRule.strategyRuleConditionList) {
                if (ruleCondition.allocationConditions.equals(B_Task_Strategy_Rule_Condition.AllocationConditions.location)) {
                    existFlag = true;
                    break;
                }
            }
            if(existFlag){
                break;
            }
        }
        return existFlag;
    }

    /**
     * 策略执行
     *
     * @param task         任务
     * @param currentUser  当前用户
     * @param strategyTriggerEvent 触发时间（动作）对象
     * @return 用户及相应奖励列表
     */
    public List<StrategyExecuteResult> executeStrategy(B_Task task, S_User currentUser, StrategyTriggerEvent strategyTriggerEvent) {
        List<StrategyExecuteResult> strategyExecuteResultList = new ArrayList<>();
        //对地推任务特殊处理
        //需要任务信息在完全填写完成及用户确认后才能进行策略结算
        if(task.taskType== B_Task.TaskType.promotion&&task.promotion!=null&&task.promotion.promotionProcess!= B_Task_Promotion.Process.complete){
            return strategyExecuteResultList;
        }
        B_Task_Propagation_Log currentUserNode = B_Task_Propagation_Log.findByTaskAndExecutant(task, currentUser);//获取当前用户节点
        if(currentUserNode==null){//若用户没有参与任务，则直接返回空List
            return strategyExecuteResultList;
        }

        //遍历策略规则
        for (B_Task_Strategy_Rule strategyRule : this.strategyRuleList) {
            if (strategyRule.triggerEvent.equals(strategyTriggerEvent.triggerEvent)) {//匹配触发动作相应的规则
                if(strategyTriggerEvent.triggerEvent.equals(B_Task_Strategy_Rule.TriggerEvent.readtime)){//阅读时间规则的话需要判断时间是否正确
                    if(strategyRule.readTime>strategyTriggerEvent.readTime){
                        continue;
                    }
                }
                StrategyExecuteResult result = new StrategyExecuteResult();//规则结果

                B_Task_Propagation_Log triggerNode = currentUserNode;//触发节点—默认为当前用户节点
//                boolean hasSelfTypeChildNodeCondition = strategyRule.hasSelfTypeChildNodeCondition();//是否有self子节点条件
                boolean ruleValidityFlag = true;//规则有效性标记
//                if (hasSelfTypeChildNodeCondition&&B_Task_Strategy_Rule.TriggerEvent.enterpage.equals(strategyTriggerEvent.triggerEvent)) {
//                   /*
//                     当进入页面时，有self子节点条件时，相对的来说就是父节点的字节点数（也就是兄弟节点的数量）
//                     这个条件其实是当前节点的父节点分配条件，而不是当前节点的分配条件
//                     奖励对象也应当为父节点用户
//                     即对象全部转换成当前节点的父节点
//                    */
//                    triggerNode = B_Task_Propagation_Log.findSuperiorNodes(task, triggerNode,1);//当前用户节点的父节点
//                    if(triggerNode==null){//不存在父节点，则跳过当前循环
//                        continue;
//                    }
//                }


                //奖励用户节点
                B_Task_Propagation_Log rewardNode = new B_Task_Propagation_Log();
                //给予奖励用户
                S_User giveRewardUser = new S_User();
                if (strategyRule.rewardUserType.equals(B_Task_Strategy_Rule.RewardUserType.self)) {//自身节点
                    rewardNode = triggerNode;
                    giveRewardUser=triggerNode.preExecutant;//受益用户为自身时，则给予者就是上级节点
                } else if (strategyRule.rewardUserType.equals(B_Task_Strategy_Rule.RewardUserType.up)) {//上X层节点
                    rewardNode = B_Task_Propagation_Log.findSuperiorNodes(task, triggerNode, strategyRule.upRewardUserNum);//获取当前节点上X层节点的子节点
                    if (rewardNode == null) {//上X层节点为空则直接跳过当前循环
                        continue;
                    }
                    giveRewardUser=triggerNode.executant;//受益用户为他人时，则给予者就是当前触发用户
                }

                if (triggerNode.level >= strategyRule.startTriggerNodeLevel && (strategyRule.endTriggerNodeLevel == null ? true : (triggerNode.level <= strategyRule.endTriggerNodeLevel))) {
                    boolean hasGetRuleReward = B_Task_Strategy_Execute_Log.isExistFindByTaskAndUserAndRule(task, rewardNode.executant,giveRewardUser, strategyRule);//是否获取过该规则奖励
                    if (!hasGetRuleReward) {
                        ruleValidityFlag = strategyRule.ruleConditionValidity(task, triggerNode);
                    } else {
                        continue;
                    }
                } else {
                    continue;
                }


                if (ruleValidityFlag) {
                    result.beneficialUser = rewardNode.executant;
                    result.giveBenefitUser = giveRewardUser;
                    result.effectiveRule = strategyRule;

                    //设置奖励值
                    int rewardAmount = 0;
                    if (strategyRule.rewardType.equals(B_Task_Strategy_Rule.RewardType.points)) {
                        rewardAmount = strategyRule.fixedRewardAmount;
                        result.point = rewardAmount;
                    } else if (strategyRule.rewardType.equals(B_Task_Strategy_Rule.RewardType.fixedamount)) {
                        rewardAmount = strategyRule.fixedRewardAmount;
                        result.rewardAmount = rewardAmount;
                    } else if (strategyRule.rewardType.equals(B_Task_Strategy_Rule.RewardType.randomamount)) {
                        rewardAmount = strategyRule.generateRandomCoin();
                        result.rewardAmount = rewardAmount;
                    }
                    result.success = true;
                    strategyExecuteResultList.add(result);
                }
            }
        }


        return strategyExecuteResultList;
    }


    /**
     * 创建策略规则奖励日志
     *
     * @param beneficialUser     获益用户
     * @param giveBenefitUser    给予利益用户
     * @param task
     * @param taskStrategyRule
     * @param settleRewardAmount
     * @return
     */
    public static B_Task_Strategy_Execute_Log taskStrategyExecuteLogVerifyExistAndCreate(S_User beneficialUser,S_User giveBenefitUser, B_Task task, B_Task_Strategy_Rule taskStrategyRule, int settleRewardAmount) {
        return B_Task_Strategy_Execute_Log.verifyExistAndCreate(beneficialUser,giveBenefitUser, task, taskStrategyRule, settleRewardAmount);
    }


    public static B_Task_Strategy create(JSONObject strategy) {
        if (strategy.getString("type").equals("defaultStrategyOne")) {
            JSONObject defaultStrategyOneData = strategy.getJSONObject("defaultStrategyOneData");
            JSONObject type = defaultStrategyOneData.getJSONObject("type");
            B_Task_Strategy_Rule.RewardType rewardType = null;
            if (type.getString("value").equals("fixedamount")) {
                rewardType = B_Task_Strategy_Rule.RewardType.fixedamount;
            } else if (type.getString("value").equals("randomamount")) {
                rewardType = B_Task_Strategy_Rule.RewardType.randomamount;
            }
            return DefaultStrategyOne.createDefaultStrategyOne(
                    defaultStrategyOneData.getInteger("startTriggerNodeLevel"),
                    defaultStrategyOneData.getInteger("endTriggerNodeLevel"),
                    rewardType,
                    defaultStrategyOneData.getInteger("fixedRewardAmount"),
                    defaultStrategyOneData.getInteger("minRewardAmount"),
                    defaultStrategyOneData.getInteger("maxRewardAmount")
            );
        }
        return null;
    }


    public B_Task_Strategy edit(JSONObject strategy) {
        if (strategy.getString("type").equals("defaultStrategyOne")) {
            JSONObject defaultStrategyOneData = strategy.getJSONObject("defaultStrategyOneData");
            JSONObject type = defaultStrategyOneData.getJSONObject("type");
            B_Task_Strategy_Rule.RewardType rewardType = null;
            if (type.getString("value").equals("fixedamount")) {
                rewardType = B_Task_Strategy_Rule.RewardType.fixedamount;
            } else if (type.getString("value").equals("randomamount")) {
                rewardType = B_Task_Strategy_Rule.RewardType.randomamount;
            }
            this.editDefaultStrategyOne(
                    defaultStrategyOneData.getInteger("startTriggerNodeLevel"),
                    defaultStrategyOneData.getInteger("endTriggerNodeLevel"),
                    rewardType,
                    defaultStrategyOneData.getInteger("fixedRewardAmount"),
                    defaultStrategyOneData.getInteger("minRewardAmount"),
                    defaultStrategyOneData.getInteger("maxRewardAmount")
            );
            return this;
        }
        return null;
    }


    public B_Task_Strategy editDefaultStrategyOne(Integer startTriggerNodeLevel, Integer endTriggerNodeLevel, B_Task_Strategy_Rule.RewardType rewardType,
                                                  Integer fixedRewardAmount, Integer minRewardAmount, Integer maxRewardAmount) {
        //获取strategy
        if (this == null) {
            return null;
        }
        //获取rule
        if (this.strategyRuleList.size() > 1 || this.strategyRuleList.size() < 1) {
            return null;
        }
        B_Task_Strategy_Rule taskStrategyRule = this.strategyRuleList.get(0);

        //更新rule并保存
        taskStrategyRule.startTriggerNodeLevel = startTriggerNodeLevel;
        taskStrategyRule.endTriggerNodeLevel = endTriggerNodeLevel;
        taskStrategyRule.rewardType = rewardType;
        taskStrategyRule.fixedRewardAmount = fixedRewardAmount;
        taskStrategyRule.minRewardAmount = minRewardAmount;
        taskStrategyRule.maxRewardAmount = maxRewardAmount;
        taskStrategyRule.update();
        return this;
    }

}
