package models.db1.task.strategy;

import beans.task.strategy.DefaultStrategyOne;
import com.alibaba.fastjson.JSONObject;
import models.db1.CModel;
import models.db1.task.B_Task;
import models.db1.user.S_User;
import models.db1.wechat.B_Wechat_Pay;
import util.Common;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2016/11/21.
 */
@Entity
public class B_Task_Strategy_Info extends CModel {

    @OneToOne(fetch = FetchType.EAGER)
    public B_Task task;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public B_Task_Strategy strategy;

    //手续费
    public int fee;

    //总分配金额
    public int coin;

    //已分配的金额
    public int hasAllocationCoin;

    //总积分
    public int credit;

    //已分配积分
    public int hasAllocationcredit;

    //任务系统自动提现时，打款语
    public String autoWithdrawDepositMessage;

    //任务订单
    @OneToOne(fetch = FetchType.EAGER)
    public B_Wechat_Pay pay;

    @OneToMany
    public List<B_Task_Strategy_Renew> renewList;

    public static Finder<Integer, B_Task_Strategy_Info> find = new Finder<Integer, B_Task_Strategy_Info>(
            Integer.class, B_Task_Strategy_Info.class
    );


    public static B_Task_Strategy_Info create(int coin, String autoWithdrawDepositMessage, JSONObject strategy, B_Task task) throws Exception {
        B_Task_Strategy_Info info = new B_Task_Strategy_Info();
        info.coin = coin;
        info.task = task;
        info.autoWithdrawDepositMessage = autoWithdrawDepositMessage;
        info.fee = (int) (coin * Common.getIntConfig("task.check.fee") / 100);

        if (strategy.getString("type").equals("defaultStrategyOne")) {
            info.strategy = B_Task_Strategy.create(strategy);
        } else {
            String uuid = strategy.getString("uuid");
            B_Task_Strategy strategy_result = B_Task_Strategy.findByUuid(uuid);
            if (strategy_result == null) {
                throw new Exception("策略不存在");
            }
            info.strategy = strategy_result;
        }

        info.save();
        return info;
    }

    public B_Task_Strategy_Info clone(B_Task task, S_User user){
        B_Task_Strategy_Info info = new B_Task_Strategy_Info();
        info.task = task;
        info.coin = this.coin;
        info.autoWithdrawDepositMessage = this.autoWithdrawDepositMessage;
        info.fee =this.fee;
        info.strategy = this.strategy.copy(user);
        info.save();
        return info;
    }


    public void changeAutoWithdrawDepositMessage(String autoWithdrawDepositMessage){
        this.autoWithdrawDepositMessage = autoWithdrawDepositMessage;
        this.update();
    }

    public B_Task_Strategy_Info edit(int coin, String autoWithdrawDepositMessage, JSONObject strategy) throws Exception {
        this.coin = coin;
        this.autoWithdrawDepositMessage = autoWithdrawDepositMessage;
        this.fee = (int) (coin * Common.getIntConfig("task.check.fee") / 100);

        if (strategy.getString("type").equals("defaultStrategyOne")) {
            if (this.strategy.strategyType.equals(B_Task_Strategy.StrategyType.defaultStrategyOne)) {
                this.strategy = this.strategy.edit(strategy);
            } else {
                this.strategy = B_Task_Strategy.create(strategy);
            }
        } else {
            String uuid = strategy.getString("uuid");
            B_Task_Strategy strategy_result = B_Task_Strategy.findByUuid(uuid);
            if (strategy_result == null) {
                throw new Exception("策略不存在");
            }
            if (this.strategy.strategyType.equals(B_Task_Strategy.StrategyType.defaultStrategyOne)) {
                this.strategy.delete();
            }
            this.strategy = strategy_result;
        }
        this.update();
        return this;
    }


    public void updateWechatPay(B_Wechat_Pay pay) {
        this.pay = pay;
        this.update();
    }

    public void updateRenew(B_Wechat_Pay pay, int coin, int fee) {
        B_Task_Strategy_Renew.create(pay, this, coin, fee);
    }


    public static B_Task_Strategy_Info findByWechatPay(B_Wechat_Pay pay) {
        return B_Task_Strategy_Info.find.where().eq("pay", pay).findUnique();
    }


    public void addHasAllocationCoin(int coin) {
        this.hasAllocationCoin += coin;
        this.update();
    }

    public void addCoin(int coin) {
        this.coin += coin;
        this.update();
    }


    public static B_Task_Strategy_Info migtation(int coin, int hasAllocationCoin, B_Task task, Date created_time) {
        B_Task_Strategy_Info info = new B_Task_Strategy_Info();
        info.coin = coin;
        info.task = task;
        info.hasAllocationCoin = hasAllocationCoin;
        info.fee = (int) (coin * Common.getIntConfig("task.check.fee") / 100);
        info.strategy = null;
        info.createdTime = created_time;
        info.editedTime = created_time;
        info.save();
        return info;
    }
}
