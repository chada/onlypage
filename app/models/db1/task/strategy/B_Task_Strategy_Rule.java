package models.db1.task.strategy;

import com.alibaba.fastjson.annotation.JSONField;
import com.mysql.jdbc.StringUtils;
import models.db1.CModel;
import models.db1.task.B_Task;
import models.db1.task.B_Task_Propagation_Log;
import models.db1.task.schedule.B_Task_User_Statistics_Data;
import models.db1.user.tag.B_User_Tag;
import models.db1.user.tag.B_User_Tag_Enum;
import play.db.ebean.Model;
import util.LocationCoordinateUtil;

import javax.persistence.*;
import java.util.List;
import java.util.Random;

/**
 * Created by Administrator on 2016/11/22.
 */
@Entity
public class B_Task_Strategy_Rule  extends CModel {
    public enum TriggerEvent{enterpage,readtime,repost,infosubmit,purchase,sharetofriendcircle}
    public enum RewardUserType{self,up}
    public enum RewardType{fixedamount,randomamount,points}

    @ManyToOne
    @JSONField(serialize = false)
    @Column(nullable = false)
    public B_Task_Strategy strategy;

    //触发次数
    public int strategyRuleTriggerNum=1;

    //-----------------------------触发时间（动作）-----------------------------------

    @Column(nullable = false)
    public TriggerEvent triggerEvent;

    //阅读时间
    public Integer readTime;

    //-----------------------------分配条件-----------------------------------
    @OneToMany(mappedBy = "strategyRule", cascade = CascadeType.ALL)
    public List<B_Task_Strategy_Rule_Condition> strategyRuleConditionList;


    //-----------------------------触发对象有效层级-----------------------------------
    //触发节点
    @Column(nullable = false)
    public Integer startTriggerNodeLevel;

    public Integer endTriggerNodeLevel; // null即为无穷大


    //-----------------------------奖励目标对象-----------------------------------
    //奖励目标类型
    @Column(nullable = false)
    public RewardUserType rewardUserType;

    //上X级节点—rewardUserType为up时填
    public Integer upRewardUserNum;


    //-----------------------------奖励-----------------------------------

    //奖励类型
    @Column(nullable = false)
    public RewardType rewardType;


    //固定金额、积分数量
    public Integer fixedRewardAmount=0;

    //随机金额
    public Integer minRewardAmount=0;//最小值
    public Integer maxRewardAmount=0;//最大值




    public static Finder<Integer, B_Task_Strategy_Rule> find = new Model.Finder<Integer, B_Task_Strategy_Rule>(
            Integer.class, B_Task_Strategy_Rule.class
    );


    public static List<B_Task_Strategy_Rule> findByTaskStrategy(B_Task_Strategy taskStrategy) {
        return B_Task_Strategy_Rule.find.where().eq("strategy", taskStrategy).findList();
    }

    /**
     * 创建策略规则
     * @param strategy  策略
     * @param triggerEvent 触发事件
     * @param readTime 阅读时间
     * @param startTriggerNodeLevel 开始节点层次
     * @param endTriggerNodeLevel 结束节点层次
     * @param rewardUserType 奖励用户类型
     * @param upRewardUserNum 上级奖励用户位移量
     * @param rewardType 奖励类型
     * @param fixedRewardAmount 固定金额、积分
     * @param minRewardAmount 最小值
     * @param maxRewardAmount 最大值
     * @return
     */
    public static B_Task_Strategy_Rule create(B_Task_Strategy strategy,TriggerEvent triggerEvent,Integer readTime,Integer startTriggerNodeLevel,Integer endTriggerNodeLevel,
                                              RewardUserType rewardUserType,Integer upRewardUserNum,RewardType rewardType,Integer fixedRewardAmount,Integer minRewardAmount,Integer maxRewardAmount,Integer strategyRuleTriggerNum){
        B_Task_Strategy_Rule taskStrategyRule=new B_Task_Strategy_Rule();
        taskStrategyRule.strategy=strategy;
        taskStrategyRule.triggerEvent=triggerEvent;
        taskStrategyRule.readTime=readTime;
        taskStrategyRule.startTriggerNodeLevel=startTriggerNodeLevel;
        taskStrategyRule.endTriggerNodeLevel=endTriggerNodeLevel;
        taskStrategyRule.rewardUserType=rewardUserType;
        taskStrategyRule.upRewardUserNum=upRewardUserNum;
        taskStrategyRule.rewardType=rewardType;
        taskStrategyRule.fixedRewardAmount=fixedRewardAmount;
        taskStrategyRule.minRewardAmount=minRewardAmount;
        taskStrategyRule.maxRewardAmount=maxRewardAmount;
        taskStrategyRule.strategyRuleTriggerNum=strategyRuleTriggerNum;
        taskStrategyRule.save();

        return taskStrategyRule;
    }


    public static B_Task_Strategy_Rule createOrUpdate(B_Task_Strategy strategy,Integer ruleId,TriggerEvent triggerEvent,Integer readTime,Integer startTriggerNodeLevel,Integer endTriggerNodeLevel,
                                              RewardUserType rewardUserType,Integer upRewardUserNum,RewardType rewardType,Integer fixedRewardAmount,Integer minRewardAmount,Integer maxRewardAmount,Integer strategyRuleTriggerNum){
        B_Task_Strategy_Rule taskStrategyRule = new B_Task_Strategy_Rule();
        if(ruleId!=null) {
            taskStrategyRule=B_Task_Strategy_Rule.find.byId(ruleId);
            if(taskStrategyRule==null){
                return null;
            }
        }
        taskStrategyRule.strategy=strategy;
        taskStrategyRule.triggerEvent=triggerEvent;
        taskStrategyRule.readTime=readTime;
        taskStrategyRule.startTriggerNodeLevel=startTriggerNodeLevel;
        taskStrategyRule.endTriggerNodeLevel=endTriggerNodeLevel;
        taskStrategyRule.rewardUserType=rewardUserType;
        taskStrategyRule.upRewardUserNum=upRewardUserNum;
        taskStrategyRule.rewardType=rewardType;
        taskStrategyRule.fixedRewardAmount=fixedRewardAmount;
        taskStrategyRule.minRewardAmount=minRewardAmount;
        taskStrategyRule.maxRewardAmount=maxRewardAmount;
        taskStrategyRule.strategyRuleTriggerNum=strategyRuleTriggerNum;
        taskStrategyRule.saveOrUpdate();

        return taskStrategyRule;
    }

    //是否含有当前节点层数的子节点数量条件
    public boolean hasSelfTypeChildNodeCondition(){
        for(B_Task_Strategy_Rule_Condition condition:this.strategyRuleConditionList){
            if(condition.allocationConditions.equals(B_Task_Strategy_Rule_Condition.AllocationConditions.childnode)&&condition.conditionNodeType.equals(B_Task_Strategy_Rule_Condition.ConditionNodeType.self)){
                return true;
            }
        }
        return false;
    }

    /**
     * 规则有效性验证
     * @param task
     * @param triggerNode
     * @return
     */
    public boolean ruleConditionValidity(B_Task task,B_Task_Propagation_Log triggerNode){
        boolean ruleValidityFlag=true;
        boolean hasLocationCondition=false;//是否含有地理位置标识
        boolean ruleValidityLocationFlag=false;//地理位置判定标识
        for (B_Task_Strategy_Rule_Condition strategyRuleCondition : this.strategyRuleConditionList) {
            switch (strategyRuleCondition.allocationConditions){
                case childnode:{//子节点数量条件
                    B_Task_Propagation_Log reactNode=strategyRuleCondition.getReactNode(task,triggerNode);
                    if(reactNode==null){
                        return false;
                    }
                    int childNodeNum=reactNode.numberOfChildNodes();//获取子节点数
                    if(childNodeNum>=strategyRuleCondition.childnodeNum){

                    }else {
                        ruleValidityFlag = false;
                    }
                    break;
                }
                case location: {//地址条件——多地点定位，"或"操作
                    hasLocationCondition=true;//存在地理位置分配条件
                    if(ruleValidityLocationFlag){//如果已经是true则说明已经符合条件，不需要判断下面的地点了
                        break;
                    }
                    B_Task_Propagation_Log reactNode=strategyRuleCondition.getReactNode(task,triggerNode);
                    B_Task_User_Statistics_Data taskUserStatisticsData=B_Task_User_Statistics_Data.findByOwnerAndTask(reactNode.executant,task);
                    if(taskUserStatisticsData==null||(taskUserStatisticsData.latitude==null)||(taskUserStatisticsData.longitude==null)){

                    }else {
                        Double distance= LocationCoordinateUtil.distanceOfTwoCoordinate(taskUserStatisticsData.latitude,taskUserStatisticsData.longitude,
                                strategyRuleCondition.latitude,strategyRuleCondition.longitude);
                        if (distance<=strategyRuleCondition.locationRange) {
                            ruleValidityLocationFlag=true;
                        } else {

                        }
                    }
                    break;
                }
                case tag: {//标签条件
                    B_Task_Propagation_Log reactNode=strategyRuleCondition.getReactNode(task,triggerNode);
                    boolean isExistTag= B_User_Tag.isExistByTaggedUserAndTaggingUser(task.owner,reactNode.executant, B_User_Tag_Enum.findByTagName(strategyRuleCondition.tag,task.owner));
                    if (strategyRuleCondition.tagCondition.equals(B_Task_Strategy_Rule_Condition.TagCondition.include) ? isExistTag : (!isExistTag)) {

                    } else {
                        ruleValidityFlag = false;
                    }
                    break;
                }
                case readingtime: {//阅读时间条件
                    B_Task_Propagation_Log reactNode=strategyRuleCondition.getReactNode(task,triggerNode);
                    if(reactNode==null){
                        return false;
                    }
                    int readingTime= B_Task_User_Statistics_Data.getUserReadTimeByOwnerAndTask(reactNode.executant,task);
                    if (readingTime >= strategyRuleCondition.readingTime) {

                    } else {
                        ruleValidityFlag = false;
                    }
                    break;
                }
                case readingstyle: {//阅读方式条件
                    B_Task_Propagation_Log reactNode=strategyRuleCondition.getReactNode(task,triggerNode);
                    if(reactNode==null){
                        return false;
                    }
                    B_Task_Strategy_Rule_Condition.ReadingStyle readingStyle=B_Task_User_Statistics_Data.getUserReadStyleByOwnerAndTask(reactNode.executant,task);
                    if (strategyRuleCondition.readingstyle.equals(readingStyle)) {

                    } else {
                        ruleValidityFlag = false;
                    }
                    break;
                }
                case redpocketquantity:{//红包数量限制
                    Integer redpocketquantity=0;
                    if (this.rewardUserType.equals(B_Task_Strategy_Rule.RewardUserType.self)) {
                        //受益者为自身节点时，则发放者为上级用户（第一级则发放者为自身）
                        //需要查询上级节点发放的奖励个数
                        redpocketquantity=B_Task_Strategy_Execute_Log.getLogNumByGiveBenefitUser(triggerNode.preExecutant,task,this,false);
                    } else if (this.rewardUserType.equals(B_Task_Strategy_Rule.RewardUserType.up)) {//上X层节点
                        //受益者为上级节点时，则发放者为当前触发用户
                        //需要查询受益节点获得的奖励个数
                        B_Task_Propagation_Log rewardNode=this.getRewardUserNode(task,triggerNode);
                        if(rewardNode==null){
                            return false;
                        }
                        redpocketquantity=B_Task_Strategy_Execute_Log.getLogNumByBeneficialUser(rewardNode.executant,task,this,false);
                    }

                    if (redpocketquantity<strategyRuleCondition.redpocketquantity) {

                    } else {
                        ruleValidityFlag = false;
                    }
                    break;
                }
                case redpocketsum:{//红包金额限制
                    Integer redpocketsum=0;
                    if (this.rewardUserType.equals(B_Task_Strategy_Rule.RewardUserType.self)) {
                        //受益者为自身节点时，则发放者为上级用户（第一级则发放者为自身）
                        //需要查询上级节点发放的奖励金额
                        redpocketsum=B_Task_Strategy_Execute_Log.getRewarAmountSumByGiveBeneficialUser(triggerNode.preExecutant,task,this,false);
                    } else if (this.rewardUserType.equals(B_Task_Strategy_Rule.RewardUserType.up)) {//上X层节点
                        //受益者为上级节点时，则发放者为当前触发用户
                        //需要查询受益节点获得的奖励个数
                        B_Task_Propagation_Log rewardNode=this.getRewardUserNode(task,triggerNode);
                        if(rewardNode==null){
                            return false;
                        }
                        redpocketsum=B_Task_Strategy_Execute_Log.getRewarAmountSumByBeneficialUser(rewardNode.executant,task,this,false);
                    }

                    if (redpocketsum<strategyRuleCondition.redpocketsum) {

                    } else {
                        ruleValidityFlag = false;
                    }
                    break;
                }
                case sharetofriendcircle: {//地址条件——暂未用到
                    B_Task_Propagation_Log reactNode=strategyRuleCondition.getReactNode(task,triggerNode);
                    B_Task_User_Statistics_Data taskUserStatisticsData=B_Task_User_Statistics_Data.findByOwnerAndTask(reactNode.executant,task);
                    if(taskUserStatisticsData.hasShareToFriendCircle){

                    }else {
                        ruleValidityFlag = false;
                    }
                    break;
                }
            }

            if(!ruleValidityFlag){
                break;
            }
        }

        ruleValidityFlag=hasLocationCondition?(ruleValidityFlag&&ruleValidityLocationFlag):ruleValidityFlag;//合并多地点结果和其他结果
        return ruleValidityFlag;
    }


    /**
     * 生成范围在[min,max]中的随机金额---暂时21次随机取最小
     * @return min-max中的随机值
     */
    public int generateRandomCoin(){
        int max=this.maxRewardAmount;
        int min=this.minRewardAmount;

        int resultCoin=0;
        int resultCoin1=0;
        int resultCoin2=0;
        int resultCoin3=0;
        int resultCoin4=0;
        int resultCoin5=0;
        int resultCoin6=0;
        int resultCoin7=0;
        int tempResultCoin1=0;
        int tempResultCoin2=0;
        int tempResultCoin3=0;

        int randomCoin1=0;
        int randomCoin2=0;
        int randomCoin3=0;

        int randomCoin4=0;
        int randomCoin5=0;
        int randomCoin6=0;

        int randomCoin7=0;
        int randomCoin8=0;
        int randomCoin9=0;

        int randomCoin10=0;
        int randomCoin11=0;
        int randomCoin12=0;

        int randomCoin13=0;
        int randomCoin14=0;
        int randomCoin15=0;

        int randomCoin16=0;
        int randomCoin17=0;
        int randomCoin18=0;

        int randomCoin19=0;
        int randomCoin20=0;
        int randomCoin21=0;

        Random random1=new Random();
        randomCoin1=min+random1.nextInt(max-min+1);
        Random random2=new Random();
        randomCoin2=min+random2.nextInt(max-min+1);
        Random random3=new Random();
        randomCoin3=min+random3.nextInt(max-min+1);

        Random random4=new Random();
        randomCoin4=min+random4.nextInt(max-min+1);
        Random random5=new Random();
        randomCoin5=min+random5.nextInt(max-min+1);
        Random random6=new Random();
        randomCoin6=min+random6.nextInt(max-min+1);

        Random random7=new Random();
        randomCoin7=min+random7.nextInt(max-min+1);
        Random random8=new Random();
        randomCoin8=min+random8.nextInt(max-min+1);
        Random random9=new Random();
        randomCoin9=min+random9.nextInt(max-min+1);

        Random random10=new Random();
        randomCoin10=min+random10.nextInt(max-min+1);
        Random random11=new Random();
        randomCoin11=min+random11.nextInt(max-min+1);
        Random random12=new Random();
        randomCoin12=min+random12.nextInt(max-min+1);

        Random random13=new Random();
        randomCoin13=min+random13.nextInt(max-min+1);
        Random random14=new Random();
        randomCoin14=min+random14.nextInt(max-min+1);
        Random random15=new Random();
        randomCoin15=min+random15.nextInt(max-min+1);

        Random random16=new Random();
        randomCoin16=min+random16.nextInt(max-min+1);
        Random random17=new Random();
        randomCoin17=min+random17.nextInt(max-min+1);
        Random random18=new Random();
        randomCoin18=min+random18.nextInt(max-min+1);

        Random random19=new Random();
        randomCoin19=min+random19.nextInt(max-min+1);
        Random random20=new Random();
        randomCoin20=min+random20.nextInt(max-min+1);
        Random random21=new Random();
        randomCoin21=min+random21.nextInt(max-min+1);

        resultCoin1=randomCoin1>randomCoin2?(randomCoin2>randomCoin3?randomCoin3:randomCoin2):(randomCoin1>randomCoin3?randomCoin3:randomCoin1);
        resultCoin2=randomCoin4>randomCoin5?(randomCoin5>randomCoin6?randomCoin6:randomCoin5):(randomCoin4>randomCoin6?randomCoin6:randomCoin4);
        resultCoin3=randomCoin7>randomCoin8?(randomCoin8>randomCoin9?randomCoin9:randomCoin8):(randomCoin7>randomCoin9?randomCoin9:randomCoin7);
        resultCoin4=randomCoin10>randomCoin11?(randomCoin11>randomCoin12?randomCoin12:randomCoin11):(randomCoin10>randomCoin12?randomCoin12:randomCoin10);

        resultCoin5=randomCoin13>randomCoin14?(randomCoin14>randomCoin15?randomCoin15:randomCoin14):(randomCoin13>randomCoin15?randomCoin15:randomCoin13);
        resultCoin6=randomCoin16>randomCoin17?(randomCoin17>randomCoin18?randomCoin18:randomCoin17):(randomCoin16>randomCoin18?randomCoin18:randomCoin16);
        resultCoin7=randomCoin19>randomCoin20?(randomCoin20>randomCoin21?randomCoin21:randomCoin20):(randomCoin19>randomCoin21?randomCoin21:randomCoin19);

        tempResultCoin1=resultCoin1>resultCoin2?(resultCoin2>resultCoin3?resultCoin3:resultCoin2):(resultCoin1>resultCoin3?resultCoin3:resultCoin1);
        tempResultCoin2=resultCoin4>resultCoin5?(resultCoin5>resultCoin6?resultCoin6:resultCoin5):(resultCoin4>resultCoin6?resultCoin6:resultCoin4);
        tempResultCoin3=resultCoin7;

        resultCoin=tempResultCoin1>tempResultCoin2?(tempResultCoin2>tempResultCoin3?tempResultCoin3:tempResultCoin2):(tempResultCoin1>tempResultCoin3?tempResultCoin3:tempResultCoin1);

        return resultCoin;
    }


    /**
     * string转换成奖励类型RewardType
     * @param str
     * @return
     */
    public static RewardType stringConvertRewardType(String str){
        if(StringUtils.isNullOrEmpty(str)){
            return null;
        }
        for (RewardType rewardType:RewardType.values()){
            if(rewardType.toString().equals(str)){
                return rewardType;
            }
        }
        return null;
    }


    /**
     * string转换成触发事件TriggerEvent
     * @param str
     * @return
     */
    public static TriggerEvent stringConvertTriggerEvent(String str){
        if(StringUtils.isNullOrEmpty(str)){
            return null;
        }
        for (TriggerEvent triggerEvent:TriggerEvent.values()){
            if(triggerEvent.toString().equals(str)){
                return triggerEvent;
            }
        }
        return null;
    }

    /**
     * string转换成奖励用户类型RewardUserType
     * @param str
     * @return
     */
    public static RewardUserType stringConvertRewardUserType(String str){
        if(StringUtils.isNullOrEmpty(str)){
            return null;
        }
        for (RewardUserType rewardUserType:RewardUserType.values()){
            if(rewardUserType.toString().equals(str)){
                return rewardUserType;
            }
        }
        return null;
    }

    public B_Task_Propagation_Log getRewardUserNode(B_Task task,B_Task_Propagation_Log triggerNode){
        B_Task_Propagation_Log rewardNode = new B_Task_Propagation_Log();
        if (this.rewardUserType.equals(B_Task_Strategy_Rule.RewardUserType.self)) {//自身节点
            rewardNode = triggerNode;
        } else if (this.rewardUserType.equals(B_Task_Strategy_Rule.RewardUserType.up)) {//上X层节点
            rewardNode = B_Task_Propagation_Log.findSuperiorNodes(task, triggerNode, this.upRewardUserNum);//获取当前节点上X层节点的子节点
        }
        return rewardNode;
    }
}
