package models.db1.task.strategy;

import models.db1.CModel;
import models.db1.wechat.B_Wechat_Pay;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * Created by Administrator on 2015/1/20.
 * 任务充值表
 */
@Entity
public class B_Task_Strategy_Renew extends CModel {

    @Constraints.Required
    @ManyToOne
    public B_Task_Strategy_Info info;


    @Constraints.Required
    @OneToOne(fetch = FetchType.EAGER)
    public B_Wechat_Pay wechatPay;

    //充值金额
    public int coin;

    //手续费
    public int fee;

    public enum State {init, success, fail}

    public State state;


    public static Finder<Integer, B_Task_Strategy_Renew> find = new Finder<Integer, B_Task_Strategy_Renew>(
            Integer.class, B_Task_Strategy_Renew.class
    );


    public static B_Task_Strategy_Renew create(B_Wechat_Pay wechatPay, B_Task_Strategy_Info info, int coin, int fee) {
        B_Task_Strategy_Renew renew = new B_Task_Strategy_Renew();
        renew.wechatPay = wechatPay;
        renew.info = info;
        renew.coin = coin;
        renew.fee = fee;
        renew.state = State.init;
        renew.save();
        return renew;
    }

    public static B_Task_Strategy_Renew findByWechatPay(B_Wechat_Pay pay) {
        return B_Task_Strategy_Renew.find.where().eq("wechatPay", pay).findUnique();
    }


}
