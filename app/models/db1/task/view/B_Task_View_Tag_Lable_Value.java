package models.db1.task.view;

import models.db1.CModel;
import models.db1.user.S_User;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by Administrator on 2015/1/20.
 */
@Entity
public class B_Task_View_Tag_Lable_Value extends CModel {

    @ManyToOne
    public B_Task_View_Tag_Lable_Info info;

    @ManyToOne
    public B_Task_View_Tag_Lable_Submit submit;

    //实际值
    public String value;

    public static Finder<Integer, B_Task_View_Tag_Lable_Value> find = new Finder<Integer, B_Task_View_Tag_Lable_Value>(
            Integer.class, B_Task_View_Tag_Lable_Value.class
    );

    public static B_Task_View_Tag_Lable_Value create(B_Task_View_Tag_Lable_Info info, B_Task_View_Tag_Lable_Submit submit, String value) {
        B_Task_View_Tag_Lable_Value lable_value = new B_Task_View_Tag_Lable_Value();
        lable_value.info = info;
        lable_value.submit = submit;
        lable_value.value = value;
        lable_value.save();
        return lable_value;
    }


}
