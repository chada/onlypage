package models.db1.task.view;

import models.db1.CModel;
import models.db1.user.S_User;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by Administrator on 2015/1/20.
 */
@Entity
public class B_Task_View_Tag_Lable_Submit extends CModel {

    @ManyToOne
    public B_Task_View_Tag_Lable lable;

    @OneToMany(mappedBy = "submit")
    public List<B_Task_View_Tag_Lable_Value> valueList;

    @ManyToOne
    public S_User owner;

    public static Finder<Integer, B_Task_View_Tag_Lable_Submit> find = new Finder<Integer, B_Task_View_Tag_Lable_Submit>(
            Integer.class, B_Task_View_Tag_Lable_Submit.class
    );

    public static B_Task_View_Tag_Lable_Submit create(B_Task_View_Tag_Lable lable,S_User owner){
        B_Task_View_Tag_Lable_Submit submit = new B_Task_View_Tag_Lable_Submit();
        submit.lable = lable;
        submit.owner = owner;
        submit.save();
        return submit;
    }


}
