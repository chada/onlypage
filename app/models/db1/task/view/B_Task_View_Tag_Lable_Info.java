package models.db1.task.view;

import models.db1.CModel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by Administrator on 2015/1/20.
 * 任务手机端展示的tag
 */
@Entity
public class B_Task_View_Tag_Lable_Info extends CModel {

    @ManyToOne(cascade = CascadeType.REMOVE)
    public B_Task_View_Tag_Lable lable;

    //自定义列显示名
    public String rowName;

    public enum Type {phone, text, file}

    public Type type;

    @OneToMany(mappedBy = "info")
    List<B_Task_View_Tag_Lable_Value> valueList;

    public static Finder<Integer, B_Task_View_Tag_Lable_Info> find = new Finder<Integer, B_Task_View_Tag_Lable_Info>(
            Integer.class, B_Task_View_Tag_Lable_Info.class
    );


    public static B_Task_View_Tag_Lable_Info create(String rowName, String type, B_Task_View_Tag_Lable lable) {
        B_Task_View_Tag_Lable_Info info = new B_Task_View_Tag_Lable_Info();
        info.rowName = rowName;
        if (type.equals("phone")) {
            info.type = Type.phone;
        } else {
            info.type = Type.text;
        }
        info.lable = lable;
        info.save();
        return info;
    }


    public B_Task_View_Tag_Lable_Info edit(String rowName) {
        if(!this.rowName.equals(rowName)){
            this.rowName = rowName;
            this.update();
        }
        return this;
    }

    public B_Task_View_Tag_Lable_Info clone (B_Task_View_Tag_Lable lable){
        B_Task_View_Tag_Lable_Info info = new B_Task_View_Tag_Lable_Info();
        info.lable = lable;
        info.rowName = this.rowName;
        info.type = this.type;
        info.save();
        return info;
    }

}
