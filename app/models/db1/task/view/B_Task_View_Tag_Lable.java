package models.db1.task.view;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import models.db1.CModel;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/20.
 * 任务手机端展示的tag
 */
@Entity
public class B_Task_View_Tag_Lable extends CModel {

    @OneToOne(fetch = FetchType.EAGER)
    public B_Task_View_Tag viewTag;

    //标题
    public String title;

    //描述
    public String remark;

    @OneToMany(mappedBy = "lable", cascade = CascadeType.REMOVE)
    private List<B_Task_View_Tag_Lable_Info> infoList;

    @OneToMany(mappedBy = "lable", cascade = CascadeType.REMOVE)
    public List<B_Task_View_Tag_Lable_Submit> submitList;


    public static Finder<Integer, B_Task_View_Tag_Lable> find = new Finder<Integer, B_Task_View_Tag_Lable>(
            Integer.class, B_Task_View_Tag_Lable.class
    );


    public static B_Task_View_Tag_Lable create(JSONObject object, B_Task_View_Tag viewTag) {
//        System.out.println(object);
        B_Task_View_Tag_Lable lable = new B_Task_View_Tag_Lable();
        lable.title = object.getString("title");
        lable.remark = object.getString("remark");
        lable.viewTag = viewTag;
        lable.save();
        JSONArray infoList = object.getJSONArray("infoList");
        for (Object temp : infoList) {
            JSONObject info = (JSONObject) temp;
            B_Task_View_Tag_Lable_Info.create(info.getString("rowName"), info.getString("type"), lable);
        }
        return lable;
    }


    public B_Task_View_Tag_Lable edit(JSONObject object) {
        this.title = object.getString("title");
        this.remark = object.getString("remark");
        this.update();
        List<B_Task_View_Tag_Lable_Info> old = this.infoList;
        JSONArray now = object.getJSONArray("infoList");
        List<B_Task_View_Tag_Lable_Info> deleteList = new ArrayList<>();

        //删除旧的
        for (B_Task_View_Tag_Lable_Info oldInfo : old) {
            Boolean has = false;
            String rowName = null;
            for (Object temp : now) {
                JSONObject info = (JSONObject) temp;
                if (info.containsKey("id")) {
                    if (info.getInteger("id").equals(oldInfo.id)) {
                        has = true;
                        rowName = info.getString("rowName");
                    }
                }
            }
            if (has) {
                oldInfo.edit(rowName);
            } else {
                deleteList.add(oldInfo);
            }
        }
        for (B_Task_View_Tag_Lable_Info info : deleteList) {
            info.delete();
        }

        //添加新的
        for (Object temp : now) {
            JSONObject info = (JSONObject) temp;
            if (!info.containsKey("id")) {
                B_Task_View_Tag_Lable_Info.create(info.getString("rowName"), info.getString("type"), this);
            }
        }

        return this;
    }

    public B_Task_View_Tag_Lable clone(B_Task_View_Tag viewTag) {
        B_Task_View_Tag_Lable lable = new B_Task_View_Tag_Lable();
        lable.viewTag = viewTag;
        lable.title = this.title;
        lable.remark = this.remark;
        lable.save();
        for (B_Task_View_Tag_Lable_Info info : this.infoList) {
            info.clone(lable);
        }
        return lable;
    }


    public List<B_Task_View_Tag_Lable_Info> getInfoList(){
        List<B_Task_View_Tag_Lable_Info> infoList = new ArrayList<>();
        for(B_Task_View_Tag_Lable_Info info:this.infoList){
            if(info.deleteLogic){
                continue;
            }
            infoList.add(info);
        }
        return infoList;
    }

}
