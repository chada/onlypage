package models.db1.task.view;

import com.alibaba.fastjson.JSONObject;
import models.db1.CModel;
import models.db1.task.B_Task;

import javax.persistence.*;

/**
 * Created by Administrator on 2015/1/20.
 * 任务手机端展示的tag
 */
@Entity
public class B_Task_View_Tag extends CModel {

    @OneToOne(fetch = FetchType.EAGER)
    public B_Task task;

    public Boolean isShow;
    public Boolean isShowRedPackage;
    public Boolean isShowRedPackageBillBoard;
    public Boolean isShowCreditBillBoard;
    public Boolean isShowUserInput;

    @OneToOne(mappedBy = "viewTag", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    public B_Task_View_Tag_Lable lable;


    public static Finder<Integer, B_Task_View_Tag> find = new Finder<Integer, B_Task_View_Tag>(
            Integer.class, B_Task_View_Tag.class
    );

    public static B_Task_View_Tag create(JSONObject object, B_Task task) {
        B_Task_View_Tag viewTag = new B_Task_View_Tag();
        viewTag.isShow = object.getBoolean("isShow");

        if (viewTag.isShow) {
            viewTag.isShowRedPackage = object.getBoolean("isShowRedPackage");
            viewTag.isShowRedPackageBillBoard = object.getBoolean("isShowRedPackageBillBoard");
            viewTag.isShowCreditBillBoard = object.getBoolean("isShowCreditBillBoard");
            viewTag.isShowUserInput = object.getBoolean("isShowUserInput");
        } else {
            viewTag.isShowRedPackage = false;
            viewTag.isShowRedPackageBillBoard = false;
            viewTag.isShowCreditBillBoard = false;
            viewTag.isShowUserInput = false;
        }
        viewTag.task = task;
        viewTag.save();

        if (viewTag.isShowUserInput) {
            B_Task_View_Tag_Lable.create(object.getJSONObject("tagLable"), viewTag);
        }
        return viewTag;
    }

    public B_Task_View_Tag clone(B_Task task) {
        B_Task_View_Tag viewTag = new B_Task_View_Tag();
        viewTag.task = task;
        viewTag.isShow = this.isShow;
        viewTag.isShowCreditBillBoard = this.isShowCreditBillBoard;
        viewTag.isShowRedPackage = this.isShowRedPackage;
        viewTag.isShowRedPackageBillBoard = this.isShowRedPackageBillBoard;
        viewTag.isShowUserInput = this.isShowUserInput;
        viewTag.save();
        if (viewTag.isShowUserInput) {
            viewTag.lable = this.lable.clone(viewTag);
        }
        viewTag.update();
        return viewTag;
    }


    public B_Task_View_Tag edit(JSONObject object) {
        this.isShow = object.getBoolean("isShow");
        if (this.isShow) {
            this.isShowRedPackage = object.getBoolean("isShowRedPackage");
            this.isShowRedPackageBillBoard = object.getBoolean("isShowRedPackageBillBoard");
            this.isShowCreditBillBoard = object.getBoolean("isShowCreditBillBoard");
            this.isShowUserInput = object.getBoolean("isShowUserInput");
        } else {
            this.isShowRedPackage = false;
            this.isShowRedPackageBillBoard = false;
            this.isShowCreditBillBoard = false;
            this.isShowUserInput = false;
        }
        this.save();
        if (this.isShowUserInput) {

            if (this.lable != null) {
                this.lable.edit(object.getJSONObject("tagLable"));
            } else {
                B_Task_View_Tag_Lable.create(object.getJSONObject("tagLable"), this);
            }
        }
        return this;
    }


    public static B_Task_View_Tag migration(B_Task task) {
        B_Task_View_Tag viewTag = new B_Task_View_Tag();
        viewTag.isShowRedPackage = false;
        viewTag.isShowRedPackageBillBoard = false;
        viewTag.isShowCreditBillBoard = false;
        viewTag.isShowUserInput = false;
        viewTag.isShow = false;
        viewTag.task = task;
        viewTag.save();
        return viewTag;
    }

}
