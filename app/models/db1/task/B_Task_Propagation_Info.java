package models.db1.task;

import models.db1.CModel;
import models.db1.user.S_User;
import play.data.validation.Constraints;

import javax.persistence.*;

/**
 * Created by whk on 2017/8/7.
 */
@Entity
public class B_Task_Propagation_Info  extends CModel {
    @Column(nullable = false)
    @ManyToOne
    public B_Task_Propagation_Log executantPropagationLog;

    @Constraints.Required
    @ManyToOne
    public B_Task task;

    public String realName;

    public String telePhone;

    public static Finder<Integer, B_Task_Propagation_Info> find = new Finder<Integer, B_Task_Propagation_Info>(
            Integer.class, B_Task_Propagation_Info.class
    );

    public static B_Task_Propagation_Info findByPropagationLog(B_Task_Propagation_Log propagationLog) {
        return B_Task_Propagation_Info.find.where().eq("executantPropagationLog", propagationLog).findUnique();
    }

    public static B_Task_Propagation_Info create(B_Task task, B_Task_Propagation_Log propagationLog, String realName, String telePhone) {
//        B_Task_Propagation_Info info = findByPropagationLog(propagationLog);
//        if (info == null) {
        B_Task_Propagation_Info info =new B_Task_Propagation_Info();
            info = new B_Task_Propagation_Info();
            info.realName = realName;
            info.telePhone = telePhone;
            info.executantPropagationLog = propagationLog;
            info.task = task;
            info.save();
//        }
        return info;
    }
}
