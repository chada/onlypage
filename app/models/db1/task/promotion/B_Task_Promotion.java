package models.db1.task.promotion;

import models.db1.CModel;
import models.db1.task.B_Task;
import models.db1.user.S_User;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Created by whk on 2017/8/21.
 */
@Entity
public class B_Task_Promotion extends CModel {

    @Column(nullable = false)
    @ManyToOne
    public S_User task_owner;

    @OneToOne(fetch = FetchType.EAGER)
    public B_Task task;

    //地推人员
    @Column(nullable = false)
    @ManyToOne
    public S_User groundPromoter;

    //推广者：商家、商户、个人等
    @Column(nullable = false)
    @ManyToOne
    public S_User promoter;

    public String promoterName;
    public String promoterContactName;
    public String promoterPhone;
    public String promoterLocation;
    public Double promoterLatitude;
    public Double promoterLongitude;



    public enum Process {init,groundPromoterBind,infoSet,promoterBind,complete}
    public Process promotionProcess;

    public static Model.Finder<Integer, B_Task_Promotion> find = new Model.Finder<Integer, B_Task_Promotion>(
            Integer.class, B_Task_Promotion.class
    );

    public static B_Task_Promotion findByTask(B_Task task) {
        return B_Task_Promotion.find.where().eq("task", task).findUnique();
    }

    public static B_Task_Promotion initByTask(B_Task task){
        B_Task_Promotion taskPromotion = findByTask(task);
        if(taskPromotion==null){
            taskPromotion = new B_Task_Promotion();
            taskPromotion.task_owner = task.owner;
            taskPromotion.task = task;
            taskPromotion.promotionProcess = Process.init;
            taskPromotion.save();
        }

        return taskPromotion;
    }



    public B_Task_Promotion clone(B_Task task){
        B_Task_Promotion info = new B_Task_Promotion();
        info.task_owner = task.owner;
        info.task = task;
        info.promotionProcess = Process.init;
        info.save();
        return info;
    }

}
