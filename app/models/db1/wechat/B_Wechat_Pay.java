package models.db1.wechat;

import com.alibaba.fastjson.annotation.JSONField;
import models.db1.CModel;
import models.db1.task.strategy.B_Task_Strategy_Info;
import models.db1.task.strategy.B_Task_Strategy_Renew;
import models.db1.user.S_User;
import util.Common;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2016/11/19.
 * 微信支付
 */
@Entity
public class B_Wechat_Pay extends CModel {

    @Column(nullable = false)
    @ManyToOne
    @JSONField(serialize = false)
    public S_User owner;

    @Column(unique = true)
    public String product_id;

    public int total_fee;

    //商户订单号
    @Column(unique = true)
    public String out_trade_no;

    //支付二维码
    public String code_url;

    public String prepay_id;

    public String trade_type;

    //商户退款单号
    @Column(unique = true)
    public String out_refund_no;

    public String openid;

    public String bank_type;
    public String transaction_id;
    public String time_end;
    public String nonce_str;


    //初始化，成功，失败，退款
    public enum State {
        Init, Success, Fail, Refuse
    }

    public State state;


    @OneToOne(mappedBy = "pay", fetch = FetchType.LAZY)
    public B_Task_Strategy_Info strategy_info;

    @OneToOne(mappedBy = "wechatPay", fetch = FetchType.LAZY)
    public B_Task_Strategy_Renew strategy_renew;


    public static Finder<Integer, B_Wechat_Pay> find = new Finder<Integer, B_Wechat_Pay>(
            Integer.class, B_Wechat_Pay.class
    );

    public static B_Wechat_Pay create(S_User owner, String product_id, int total_fee, String out_trade_no, String nonce_str, String trade_type, String prepay_id, String code_url) {
        B_Wechat_Pay pay = new B_Wechat_Pay();
        pay.owner = owner;
        pay.product_id = product_id;
        pay.total_fee = total_fee;
        pay.out_trade_no = out_trade_no;
        pay.nonce_str = nonce_str;
        pay.code_url = code_url;
        pay.trade_type = trade_type;
        pay.prepay_id = prepay_id;
        pay.state = State.Init;
        pay.save();
        return pay;
    }

    public B_Wechat_Pay updateInfo(String openid, String bank_type, String transaction_id, String time_end) {
        this.openid = openid;
        this.bank_type = bank_type;
        this.transaction_id = transaction_id;
        this.time_end = time_end;
        this.state = State.Success;
        this.update();
        return this;
    }


    public Boolean isInitAndUseful() {
        long diff = (new Date().getTime() - this.createdTime.getTime()) / 1000;
        if (this.state == State.Init && diff < 60 * 60 * 2) {
            return true;
        }
        return false;
    }

    public Boolean isNativeTradeType() {
        return "NATIVE".equals(this.trade_type) ? true : false;
    }

    public Boolean isJsapiTradeType() {
        return "JSAPI".equals(this.trade_type) ? true : false;
    }


    public static String getOneUnUseOutTradeNo() {
        String out_trade_no = Common.randomString(32).trim();
        while (findByOutTradeNo(out_trade_no) != null) {
            out_trade_no = Common.randomString(32).trim();
        }
        return out_trade_no;
    }


    public static String getOneUnUseProductId() {
        String product_id = Common.randomString(32).trim();
        while (findByProductId(product_id) != null) {
            product_id = Common.randomString(32).trim();
        }
        return product_id;
    }


    public static B_Wechat_Pay findByOutTradeNo(String out_trade_no) {
        return B_Wechat_Pay.find.where().eq("out_trade_no", out_trade_no).findUnique();
    }


    public static B_Wechat_Pay findByProductId(String product_id) {
        return B_Wechat_Pay.find.where().eq("product_id", product_id).findUnique();
    }

    public static B_Wechat_Pay findByProductIdAndOutTradeNo(String product_id, String out_trade_no) {
        return B_Wechat_Pay.find.where().eq("product_id", product_id).eq("out_trade_no", out_trade_no).findUnique();
    }
}
