package models.db1.wechat;

import com.alibaba.fastjson.annotation.JSONField;
import models.db1.CModel;
import models.db1.user.S_User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.UUID;

/**
 * Created by Administrator on 2016/11/19.
 * 微信提现
 */
@Entity
public class B_Wechat_Withdraw extends CModel {

    @Column(nullable = false)
    @ManyToOne
    @JSONField(serialize = false)
    public S_User owner;

    public String nonce_str;

    @Column(unique = true)
    public String partner_trade_no;

    public String openid;

    public int amount;

    //原desc
    public String remark;

    public String payment_no;

    public String payment_time;

    //初始化，成功，失败
    public enum State {
        init, success, fail
    }

    public State state;
    public String errorMsg;


    public static Finder<Integer, B_Wechat_Withdraw> find = new Finder<Integer, B_Wechat_Withdraw>(
            Integer.class, B_Wechat_Withdraw.class
    );

    public static int findTodaySuccessCount(S_User user) {
        Calendar c1 = new GregorianCalendar();
        c1.set(Calendar.HOUR_OF_DAY, 0);
        c1.set(Calendar.MINUTE, 0);
        c1.set(Calendar.SECOND, 0);
//        System.out.println(c1.getTime().toLocaleString());
        return B_Wechat_Withdraw.find.where().ge("createdTime", c1.getTime()).eq("state", State.success).eq("owner",user).findRowCount();
    }

    public static B_Wechat_Withdraw findByPartnerTradeNo(String partner_trade_no) {
        return B_Wechat_Withdraw.find.where().eq("partner_trade_no", partner_trade_no).findUnique();
    }


    public static B_Wechat_Withdraw create(S_User owner,String nonce_str, String partner_trade_no, String openid, int amount,String remark) {
        B_Wechat_Withdraw withdraw = new B_Wechat_Withdraw();
        withdraw.owner = owner;
        withdraw.nonce_str = nonce_str;
        withdraw.partner_trade_no = partner_trade_no;
        withdraw.openid = openid;
        withdraw.amount = amount;
        withdraw.remark = remark;
        withdraw.state = State.init;
        withdraw.save();
        return withdraw;
    }

    public B_Wechat_Withdraw success(String payment_no, String payment_time) {
        this.payment_no = payment_no;
        this.payment_time = payment_time;
        this.state = State.success;
        this.update();
        return this;
    }


    public B_Wechat_Withdraw fail(String errorMsg) {
        this.errorMsg = errorMsg;
        this.state = State.fail;
        this.update();
        return this;
    }

    public static String getOnePartnerTradeNo() {
        String partner_trade_no = UUID.randomUUID().toString().toUpperCase().replaceAll("-", "");
        if (findByPartnerTradeNo(partner_trade_no) != null) {
            partner_trade_no = UUID.randomUUID().toString().toUpperCase().replaceAll("-", "");
        }
        return partner_trade_no;
    }

}
