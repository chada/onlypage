package models.db1.wechat;

import models.db1.CModel;
import models.db1.user.S_User;
import play.data.validation.Constraints;
import wechat.bean.thirdPartyPlatform.AuthorizerAccessTokenAndRefreshTokenData;
import wechat.bean.thirdPartyPlatform.AuthorizerRefreshTokenData;
import wechat.bean.thirdPartyPlatform.WechatAppAccountInfoData;
import wechat.open.ThirdPartyPlatform.ThirdPartyPlatformUtil;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2015/1/20.
 * 公众号第三方托管
 */
@Entity
public class B_Wechat_App extends CModel {

    @ManyToOne
    @Column(nullable = false)
    public S_User owner;

    //授权方昵称
    public String nick_name;
    //授权方头像
    public String head_img;
    //授权方公众号类型，0代表订阅号，1代表由历史老帐号升级后的订阅号，2代表服务号
    public int service_type_info;
    //授权方认证类型，-1代表未认证，0代表微信认证，1代表新浪微博认证，2代表腾讯微博认证，3代表已资质认证通过但还未通过名称认证，4代表已资质认证通过、还未通过名称认证，但通过了新浪微博认证，5代表已资质认证通过、还未通过名称认证，但通过了腾讯微博认证
    public int verify_type_info;
    //授权方公众号的原始ID
    public String user_name;
    //授权方公众号所设置的微信号，可能为空
    public String alias;
    //二维码图片的URL，开发者最好自行也进行保存
    public String qrcode_url;
    //授权方appid
    @Constraints.Required
    @Column(unique = true, nullable = false)
    public String appid;
    /*
    * 公众号授权给开发者的权限集列表（请注意，当出现用户已经将消息与菜单权限集授权给了某个第三方，再授权给另一个第三方时，由于该权限集是互斥的，后一个第三方的授权将去除此权限集，开发者可以在返回的func_info信息中验证这一点，避免信息遗漏），1到13分别代表：
    * 消息与菜单权限集
    * 用户管理权限集
    * 帐号管理权限集
    * 网页授权权限集
    * 微信小店权限集
    * 多客服权限集
    * 业务通知权限集
    * 微信卡券权限集
    * 微信扫一扫权限集
    * 微信连WIFI权限集
    * 素材管理权限集
    * 摇一摇周边权限集
    * 微信门店权限集
    * */
    public String func_info;
    //公众号是否具备API权限
    public Boolean apiAuthority;
    //授权方令牌（在授权的公众号具备API权限时，才有此返回值）
    private String authorizer_access_token;
    //有效期（在授权的公众号具备API权限时，才有此返回值）
    public String expires_in;
    //刷新令牌（在授权的公众号具备API权限时，才有此返回值），刷新令牌主要用于公众号第三方平台获取和刷新已授权用户的access_token，只会在授权时刻提供，请妥善保存。 一旦丢失，只能让用户重新授权，才能再次拿到新的刷新令牌
    public String authorizer_refresh_token;


    public static Finder<Integer, B_Wechat_App> find = new Finder<Integer, B_Wechat_App>(
            Integer.class, B_Wechat_App.class
    );

    private B_Wechat_App() {

    }


    public static B_Wechat_App findByUuid(String uuid) {
        return B_Wechat_App.find.where().eq("uuid", uuid).findUnique();
    }

    public static B_Wechat_App FindByAppId(String appId) {
        if (appId == null) {
            return null;
        } else {
            return B_Wechat_App.find.where().eq("appid", appId).findUnique();
        }
    }


    public static B_Wechat_App FindByUserName(String user_name) {
        return B_Wechat_App.find.where().eq("user_name", user_name).findUnique();
    }

    //创建托管公众号
    public static B_Wechat_App Create(S_User owner, AuthorizerAccessTokenAndRefreshTokenData authorizerAccessTokenAndRefreshTokenData, WechatAppAccountInfoData wechatAppAccountInfoData) {
        B_Wechat_App app = FindByAppId(authorizerAccessTokenAndRefreshTokenData.authorizer_appid);
        if (app == null) {
            app = new B_Wechat_App();
        }
        app.owner = owner;
        app.nick_name = wechatAppAccountInfoData.nick_name;
        app.head_img = wechatAppAccountInfoData.head_img;
        app.service_type_info = wechatAppAccountInfoData.service_type_info;
        app.verify_type_info = wechatAppAccountInfoData.verify_type_info;
        app.user_name = wechatAppAccountInfoData.user_name;
        app.alias = wechatAppAccountInfoData.alias;
        app.qrcode_url = wechatAppAccountInfoData.qrcode_url;
        app.appid = wechatAppAccountInfoData.authorizer_appid;
        app.func_info = wechatAppAccountInfoData.func_info.toJSONString();

        app.apiAuthority = authorizerAccessTokenAndRefreshTokenData.apiAuthority;
        app.authorizer_access_token = authorizerAccessTokenAndRefreshTokenData.authorizer_access_token;
        app.expires_in = authorizerAccessTokenAndRefreshTokenData.expires_in;
        app.authorizer_refresh_token = authorizerAccessTokenAndRefreshTokenData.authorizer_refresh_token;
        app.saveOrUpdate();
        return app;
    }


    //获取托管公众号的access_token，若token失效则进行更新
    public String  getAccessToken(){
        long pre = this.editedTime.getTime();
        long curr = new Date().getTime();
        //加上60s，时间误差值
        int time = (int)((curr - pre) / 1000)+60;
        if(time>=Integer.valueOf(expires_in)){
            try {
                AuthorizerRefreshTokenData authorizerRefreshTokenData = ThirdPartyPlatformUtil.authorizerRefreshToken(this.appid, this.authorizer_refresh_token);
                if(authorizerRefreshTokenData==null){
                    return null;
                }
                this.authorizer_access_token = authorizerRefreshTokenData.authorizer_access_token;
                this.authorizer_refresh_token = authorizerRefreshTokenData.authorizer_refresh_token;
                this.expires_in = authorizerRefreshTokenData.expires_in;
                this.editedTime = new Date();
                this.saveOrUpdate();
                return  authorizerRefreshTokenData.authorizer_access_token;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }else{
            return this.authorizer_access_token;
        }
    }
}
