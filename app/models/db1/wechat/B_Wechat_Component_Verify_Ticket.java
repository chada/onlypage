package models.db1.wechat;

import models.db1.CModel;
import play.data.validation.Constraints;

import javax.persistence.Entity;

/**
 * Created by whk on 2017/10/9.
 * 用于存放component_verify_ticket
 * 在公众号第三方平台创建审核通过后，微信服务器会向其“授权事件接收URL”每隔10分钟定时推送component_verify_ticket。
 */
@Entity
public class B_Wechat_Component_Verify_Ticket extends CModel {

    //第三方平台appId
    @Constraints.Required
    public String appId;

    //ticket获取时间
    @Constraints.Required
    public String createTime;

    //ticket消息类型
    @Constraints.Required
    public String infoType;

    //component_verify_ticket
    @Constraints.Required
    public String componentVerifyTicket;

    public static Finder<Integer, B_Wechat_Component_Verify_Ticket> find = new Finder<Integer, B_Wechat_Component_Verify_Ticket>(
            Integer.class, B_Wechat_Component_Verify_Ticket.class
    );

    private B_Wechat_Component_Verify_Ticket(){

    }


    public B_Wechat_Component_Verify_Ticket( String appId, String createTime, String infoType, String componentVerifyTicket){
        B_Wechat_Component_Verify_Ticket component_verify_ticket = B_Wechat_Component_Verify_Ticket.FindByAppId(appId);
        if(component_verify_ticket==null){
            component_verify_ticket = new B_Wechat_Component_Verify_Ticket();
            component_verify_ticket.appId  = appId;
        }else {
        }
        component_verify_ticket.createTime  = createTime;
        component_verify_ticket.infoType  = infoType;
        component_verify_ticket.componentVerifyTicket  = componentVerifyTicket;
        component_verify_ticket.saveOrUpdate();
    }


    public static B_Wechat_Component_Verify_Ticket FindByAppId(String appId){
        return B_Wechat_Component_Verify_Ticket.find.where().eq("appId",appId).setMaxRows(1).findUnique();
    }
}
