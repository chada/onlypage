package models.db1;

import play.data.validation.Constraints;

import javax.persistence.*;

/**
 * Created by whk on 2017/8/10.
 */
@Entity
public class B_Customization_Html extends CModel  {
    //页面标题
    @Constraints.Required
    public String title;

    //页面名称
    @Constraints.Required
    public String fileName;

    @Lob
    public String html;

    public int visitCount=0;

    public static Finder<Integer, B_Customization_Html> find = new Finder<Integer, B_Customization_Html>(
            Integer.class, B_Customization_Html.class
    );

    public static B_Customization_Html findByFileName(String fileName){
        return B_Customization_Html.find.where().eq("fileName", fileName).eq("deleteLogic", false).findUnique();
    }
}
