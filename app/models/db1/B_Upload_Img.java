package models.db1;

import models.db1.article.B_Article_Visit_Log;
import models.db1.user.S_User;
import org.apache.commons.codec.digest.DigestUtils;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.io.*;
import java.util.List;

/**
 * Created by Administrator on 2015/1/20.
 * 所有上传的图片的库，防止上传多张同样图片，节省存储空间
 */
@Entity
public class B_Upload_Img extends CModel {

    @Column(nullable = false)
    @ManyToOne
    public S_User owner;

    @Column(unique = true)
    public String md5;

    @Column(unique = true)
    public String url;

    public static Finder<Integer, B_Upload_Img> find = new Finder<Integer, B_Upload_Img>(
            Integer.class, B_Upload_Img.class
    );

    public static B_Upload_Img create(File file,String url,S_User user) throws IOException {
        InputStream is = new FileInputStream(file);
        String md5 = DigestUtils.md5Hex(is);
        B_Upload_Img img = new B_Upload_Img();
        img.md5 = md5;
        img.owner = user;
        img.url = url;
        img.save();
        return img;
    }


    public static B_Upload_Img findByFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);
        return findByInputstream(is);
    }


    public static B_Upload_Img findByInputstream(InputStream is) throws IOException {
        String md5 = DigestUtils.md5Hex(is);
        return findByMd5(md5);
    }


    public static B_Upload_Img findByMd5(String md5) {
        return B_Upload_Img.find.where().eq("md5", md5).findUnique();
    }

}
