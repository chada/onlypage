package models.db1.user;

import com.alibaba.fastjson.JSON;
import models.db1.CModel;
import wechat.open.web.beans.LoginUserResult;
import wechat.other.webUserAuthorization.UserInfoResult;

import javax.persistence.*;
import java.util.Date;

@Entity
public class B_User_Wechat_Info extends CModel {


    @OneToOne(fetch = FetchType.LAZY)
    public S_User owner;

    @Column(unique = true)
    public String appOpenid;

    @Column(unique = true)
    public String webOpenid;

    @Column(unique = true)
    public String unionid;

    public int subscribe;
    public String nickname;
    public int sex;
    public String language;
    public String city;
    public String province;
    public String country;
    public String headimgurl;
    public Date subscribe_time;
    public String remark;
    public int groupid;


    public static Finder<Integer, B_User_Wechat_Info> find = new Finder<Integer, B_User_Wechat_Info>(
            Integer.class, B_User_Wechat_Info.class
    );


    public static B_User_Wechat_Info create(LoginUserResult info, S_User user) {
        B_User_Wechat_Info wechat_info = JSON.parseObject(JSON.toJSONString(info), B_User_Wechat_Info.class);
        wechat_info.webOpenid = info.openid;
        wechat_info.owner = user;
        wechat_info.save();
        return wechat_info;
    }


    public static B_User_Wechat_Info create(UserInfoResult info, S_User user) {
        B_User_Wechat_Info wechat_info = JSON.parseObject(JSON.toJSONString(info), B_User_Wechat_Info.class);
        wechat_info.appOpenid = info.openid;
        wechat_info.owner = user;
        wechat_info.save();
        return wechat_info;
    }


    public B_User_Wechat_Info updateInfo(UserInfoResult info) {
        this.nickname = info.nickname;
        this.appOpenid = info.openid;
        this.sex = Integer.valueOf(info.sex);
        this.province = info.province;
        this.city = info.city;
        this.country = info.country;
        this.headimgurl = info.headimgurl;
        this.update();
        return this;
    }

    public B_User_Wechat_Info updateInfo(LoginUserResult info) {
        this.nickname = info.nickname;
        this.webOpenid = info.openid;
        this.sex = Integer.valueOf(info.sex);
        this.province = info.province;
        this.city = info.city;
        this.country = info.country;
        this.headimgurl = info.headimgurl;
        this.update();
        return this;
    }

    public static B_User_Wechat_Info findByWebOpenid(String webOpenid) {

        return B_User_Wechat_Info.find.where().eq("webOpenid", webOpenid).findUnique();
    }


    public static B_User_Wechat_Info migration(S_User user, String appOpenid, String unionid, int subscribe,
                                               String nickname,
                                               int sex,
                                               String language,
                                               String city,
                                               String province,
                                               String country,
                                               String headimgurl,
                                               Date subscribe_time,
                                               String remark,
                                               int groupid, Date created_time) {
        B_User_Wechat_Info wechat_info = new B_User_Wechat_Info();
        wechat_info.owner = user;
        wechat_info.appOpenid = appOpenid;
        wechat_info.unionid = unionid;
        wechat_info.subscribe = subscribe;
        wechat_info.nickname = nickname;
        wechat_info.sex = sex;
        wechat_info.language = language;
        wechat_info.city = city;
        wechat_info.province = province;
        wechat_info.country = country;
        wechat_info.headimgurl = headimgurl;
        wechat_info.subscribe_time = subscribe_time;
        wechat_info.remark = remark;
        wechat_info.groupid = groupid;
        wechat_info.createdTime = created_time;
        wechat_info.editedTime = created_time;
        wechat_info.save();
        return wechat_info;
    }
}
