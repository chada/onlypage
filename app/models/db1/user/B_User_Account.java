package models.db1.user;

import com.alibaba.fastjson.annotation.JSONField;
import models.db1.CModel;
import scala.Int;
import util.LockUtil;


import javax.persistence.*;

@Entity
public class B_User_Account extends CModel {


    @OneToOne(fetch = FetchType.LAZY)
    public S_User owner;

    public int coin;

    public int taskCount;


    public static Finder<Integer, B_User_Account> find = new Finder<Integer, B_User_Account>(
            Integer.class, B_User_Account.class
    );


    public static B_User_Account create(S_User owner) {
        B_User_Account account = new B_User_Account();
        account.owner = owner;
        account.coin = 0;
        account.taskCount = 0;
        account.save();
        return account;
    }


    public void addCoin(int coin) throws Exception {
        synchronized (LockUtil.getLock("B_User_Account" + this.id)) {
            this.coin += coin;
            if (this.coin < 0) {
                throw new Exception("账户金额不能小于0");
            } else {
                this.update();
            }
        }
    }


    public void reduceCoin(int coin) throws Exception {
        synchronized (LockUtil.getLock("B_User_Account" + this.id)) {
            this.coin -= coin;
            if (this.coin < 0) {
                throw new Exception("账户金额不能小于0");
            } else {
                this.update();
            }
        }
    }


    public static void test(final S_User user, final LockUtil lock) {
        synchronized (lock) {
            System.out.println("开始1-" + user.id);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("结束1-" + user.id);
        }

    }


}
