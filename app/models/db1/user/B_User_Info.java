package models.db1.user;

import models.db1.CModel;
import play.data.validation.Constraints;
import util.LockUtil;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class B_User_Info extends CModel {


    public String nickname;

    public String avatar;

    public String remark;//个性签名

    @OneToOne(fetch = FetchType.LAZY)
    public S_User owner;

    public static Finder<Integer, B_User_Info> find = new Finder<Integer, B_User_Info>(
            Integer.class, B_User_Info.class
    );


    public static B_User_Info create(S_User owner) {
        B_User_Info info = new B_User_Info();
        info.owner = owner;
        info.save();
        return info;
    }

    public  static  B_User_Info findByOwner(S_User owner){
        B_User_Info userInfo = B_User_Info.find.where().eq("owner_id",owner.id).findUnique();
        return userInfo;
    }
}
