package models.db1.user;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import models.db1.CModel;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import qq.beans.QqLoginUserResult;
import util.Common;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by wangh on 2016/11/22.
 */
@Entity
public class B_User_Qq_Info extends CModel {


    @OneToOne(fetch = FetchType.LAZY)
    public S_User owner;

    public int ret;
    public String openid;
    public String nickname;
    public int gender;
    public String city;
    public String province;
    public String msg;
    public String figureurl_qq_1;
    public String figureurl_qq_2;

    public static Finder<Integer, B_User_Qq_Info> find = new Model.Finder<Integer, B_User_Qq_Info>(
            Integer.class, B_User_Qq_Info.class
    );


    public static B_User_Qq_Info find(String openid){
        return B_User_Qq_Info.find.where().eq("openid",openid).findUnique();
    }

    public static B_User_Qq_Info create(QqLoginUserResult result,S_User user,String openid){
        B_User_Qq_Info qqInfo = null;
        qqInfo = JSON.parseObject(JSON.toJSONString(result), B_User_Qq_Info.class);//json.parseObject(参数1，参数2)，参数1是一个json字符串，参数2是一个类，目的是将json字符串转换成对象
        qqInfo.openid = openid; //由于获取的用户信息中并不包含openid，在这里将传过来的openid添加到表中
        qqInfo.owner = user;
        qqInfo.save();
        return qqInfo;
    }


}
