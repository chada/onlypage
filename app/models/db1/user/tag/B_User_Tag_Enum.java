package models.db1.user.tag;

import com.avaje.ebean.Expr;
import models.db1.CModel;
import models.db1.user.S_User;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by whk on 2017/7/7.
 */
@Entity
public class B_User_Tag_Enum extends CModel {
    public enum TagEnumType{global,customize}

    public TagEnumType tagType;

    @ManyToOne
    public S_User owner;

    public String tag;

    //颜色样式
    public String color;

    @OneToMany(mappedBy = "tag", cascade = CascadeType.ALL)
    public List<B_User_Tag> userTagList;

    public static Finder<Integer, B_User_Tag_Enum> find = new Finder<Integer, B_User_Tag_Enum>(
            Integer.class, B_User_Tag_Enum.class
    );

    public static B_User_Tag_Enum createCustomize(S_User user, String tag, String color){
        B_User_Tag_Enum userTagEnum=new B_User_Tag_Enum();
        userTagEnum.owner=user;
        userTagEnum.tag=tag;
        userTagEnum.color=color;
        userTagEnum.tagType=TagEnumType.customize;
        userTagEnum.save();
        return userTagEnum;
    }


    public static B_User_Tag_Enum createGlobal(String tag, String color){
        B_User_Tag_Enum userTagEnum=new B_User_Tag_Enum();
        userTagEnum.tag=tag;
        userTagEnum.color=color;
        userTagEnum.tagType=TagEnumType.global;
        userTagEnum.save();
        return userTagEnum;
    }

    public static B_User_Tag_Enum findByUuid(String uuid) {
        return B_User_Tag_Enum.find.where().eq("uuid", uuid).findUnique();
    }

    public static B_User_Tag_Enum findByTagName(String tag,S_User user) {
        return B_User_Tag_Enum.find.where().eq("tag", tag)
                .or(Expr.eq("owner", user),Expr.eq("tagType", B_User_Tag_Enum.TagEnumType.global)).findUnique();
    }

    public static int getCustomizeTagCountByUser(S_User user) {
        return B_User_Tag_Enum.find.where().eq("owner", user).eq("tagType", TagEnumType.customize).findRowCount();
    }

    public static boolean isExistSameNameCustomizeTag(String tag,S_User user) {
        return B_User_Tag_Enum.find.where().eq("tag", tag)
                .or(Expr.eq("owner", user),Expr.eq("tagType", B_User_Tag_Enum.TagEnumType.global)).findRowCount()>0?true:false;
    }
}
