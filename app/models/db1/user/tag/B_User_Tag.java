package models.db1.user.tag;

import models.db1.CModel;
import models.db1.user.S_User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by whk on 2017/7/7.
 */
@Entity
public class B_User_Tag  extends CModel {
    @Column(nullable = false)
    @ManyToOne
    public S_User owner;


    @Column(nullable = false)
    @ManyToOne
    public S_User taggedUser;

    @Column(nullable = false)
    @ManyToOne
    public B_User_Tag_Enum tag;

    public static Finder<Integer, B_User_Tag> find = new Finder<Integer, B_User_Tag>(
            Integer.class, B_User_Tag.class
    );

    public static B_User_Tag create(S_User taggingUser,S_User taggedUser,B_User_Tag_Enum tag){
        B_User_Tag userTag=new B_User_Tag();
        userTag.owner=taggingUser;
        userTag.taggedUser=taggedUser;
        userTag.tag=tag;
        userTag.save();
        return userTag;
    }

    public static B_User_Tag findByTaggedUserAndTaggingUser(S_User taggingUser,S_User taggedUser,B_User_Tag_Enum tag){
        return B_User_Tag.find.where().eq("owner", taggingUser).eq("taggedUser", taggedUser).eq("tag", tag).findUnique();
    }

    public static boolean isExistByTaggedUserAndTaggingUser(S_User taggingUser,S_User taggedUser,B_User_Tag_Enum tag){
        return B_User_Tag.find.where().eq("owner", taggingUser).eq("taggedUser", taggedUser).eq("tag", tag).findRowCount()>0?true:false;
    }

    public static List<B_User_Tag> findTagListByTaggedUserAndTaggingUser(S_User taggingUser,S_User taggedUser){
        return B_User_Tag.find.where().eq("owner", taggingUser).eq("taggedUser", taggedUser).findList();
    }
}
