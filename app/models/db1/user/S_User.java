package models.db1.user;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.avaje.ebean.Ebean;
import models.db1.CModel;
import play.data.validation.Constraints;
import qq.beans.QqLoginUserResult;
import util.Common;
import wechat.open.web.beans.LoginUserResult;
import wechat.other.webUserAuthorization.UserInfoResult;


import javax.persistence.*;

import java.util.Date;

import static play.mvc.Controller.session;

@Entity
public class S_User extends CModel {


    @Constraints.Required
    @Column(unique = true)
    public String username;


    @Column(unique = true)
    public String telephone;

    @Constraints.Required
    @JSONField(serialize = false)
    public String password;

    @Constraints.Required
    @Column(nullable = false)
    @JSONField(serialize = false)
    public String salt;


    @OneToOne(mappedBy = "owner", cascade = CascadeType.ALL)
    public B_User_Account account;

//    @OneToOne
//    public B_User_Account account;


    @OneToOne(mappedBy = "owner", cascade = CascadeType.ALL)
    public B_User_Wechat_Info wechatInfo;


    //QQ账号的详细信息
    @OneToOne(mappedBy = "owner", cascade = CascadeType.ALL)
    public B_User_Qq_Info qqInfo;


    @OneToOne(mappedBy = "owner", cascade = CascadeType.ALL)
    public B_User_Info baseInfo;


    public int migrationid;

    public static Finder<Integer, S_User> find = new Finder<Integer, S_User>(
            Integer.class, S_User.class
    );


    //将QQ第三方登录获取的用户信息存到数据库B_User_Qq_Info表
    public static synchronized S_User tocreateQQUser(QqLoginUserResult result, String openid) {
        String username = result.nickname;
        String salt = Common.randomString(10);
        String password = encryptPassword(openid, salt);
        String telephone = null;
        S_User user = regist(username, password, telephone);
        B_User_Qq_Info info = B_User_Qq_Info.create(result, user, openid);
        user.qqInfo = info;
//        user.openid = openid;
        user.save();
        return user;

    }


    public static S_User regist(String username, String password, String telephone) {
        S_User user = new S_User();
        String salt = Common.randomString(10);
        user.username = username;
        user.salt = salt;
        user.password = password == null ? null : encryptPassword(password, salt);
        user.telephone = telephone;
        user.save();
        user.account = B_User_Account.create(user);
        user.baseInfo = B_User_Info.create(user);
        return user;
    }

    //通过网站应用获取
    public static S_User regist(LoginUserResult loginUserResult) {
        S_User user = regist(null, null, null);
        B_User_Wechat_Info info = B_User_Wechat_Info.create(loginUserResult, user);
        user.wechatInfo = info;
        user.saveOrUpdate();
        return user;
    }

    //通过网页授权获取
    public static S_User regist(UserInfoResult userInfoResult) {
        S_User user = regist(null, null, null);
        B_User_Wechat_Info info = B_User_Wechat_Info.create(userInfoResult, user);
        user.wechatInfo = info;
        user.saveOrUpdate();
        return user;
    }

    public S_User updateWechatInfo(UserInfoResult userInfoResult) {
        this.wechatInfo.updateInfo(userInfoResult);
        return this;
    }

    public S_User updateWechatInfo(LoginUserResult loginUserResult) {
        this.wechatInfo.updateInfo(loginUserResult);
        return this;
    }

    //是否缺少微信的详细数据
    public Boolean isMissingWechatDetailedData() {
        if (this.wechatInfo == null) {
            return true;
        }
        if (this.wechatInfo.nickname == null) {
            return true;
        }
        return false;
    }


    public static S_User findByTelephone(String telephone) {
        return S_User.find.where().eq("telephone", telephone).findUnique();
    }

    public static S_User findByUsername(String username) {
        return S_User.find.where().eq("username", username).findUnique();
    }

    public static S_User findByUuid(String uuid) {
        return S_User.find.where().eq("uuid", uuid).findUnique();
    }

    public static S_User findByTelphoneAndPassword(String telephone, String password) {
        S_User user = S_User.findByTelephone(telephone);
        if (user != null) {
            if (user.password.equals(encryptPassword(password, user.salt))) {
                return user;
            }
        }
        return null;
    }


    public static S_User findByUsernameAndPassword(String username, String password) {
        S_User user = S_User.findByUsername(username);
        if (user != null) {
            if (user.password.equals(encryptPassword(password, user.salt))) {
                return user;
            }
        }
        return null;
    }


    public static String encryptPassword(String password, String salt) {
        if (salt == null) {
            salt = "";
        }
        return Common.base64encode(Common.base64encode(password) + salt);
    }


    public String getShowName() {
        if (this.baseInfo.nickname != null) {
            return this.baseInfo.nickname;
        }
        if (this.wechatInfo != null && this.wechatInfo.nickname != null) {
            return this.wechatInfo.nickname;
        }

        if (this.qqInfo != null && this.qqInfo.nickname != null) {
            return this.qqInfo.nickname;
        }

        if (this.username != null) {
            return this.username;
        }

        return "用户";
    }


    public String getShowAvatar() {
        if (this.baseInfo.avatar != null) {
            return this.baseInfo.avatar;
        }
        if (this.wechatInfo != null && this.wechatInfo.headimgurl != null) {
            return this.wechatInfo.headimgurl;
        }

        if (this.qqInfo != null && this.qqInfo.figureurl_qq_1 != null) {
            return this.qqInfo.figureurl_qq_1;
        }

        return "http://oh526npw0.bkt.clouddn.com/avatar.png";
    }


    public static S_User findByAppOpenId(String openid) {
        return S_User.find.where().eq("wechatInfo.appOpenid", openid).findUnique();
    }

    public static S_User findByWebOpenId(String openid) {
        return S_User.find.where().eq("wechatInfo.webOpenid", openid).findUnique();
    }

    public static S_User findByUnionid(String openid) {
        return S_User.find.where().eq("wechatInfo.unionid", openid).findUnique();
    }

    public void updateWebOpenId(String openid) {
        B_User_Wechat_Info info = this.wechatInfo;
        info.webOpenid = openid;
        info.update();
    }


    public void updateAppOpenId(String openid) {
        B_User_Wechat_Info info = this.wechatInfo;
        info.appOpenid = openid;
        info.update();
    }

    public void changeTelephone(String telephone) {
        this.telephone = telephone;
        this.update();
    }


    public static S_User migration(String username, String password, String telephone, String salt, int migrationid, Date created_time) {

        if (telephone != null && S_User.findByTelephone(telephone) == null) {
            telephone = null;
        }
        S_User user = new S_User();
        user.username = username;
        user.salt = salt;
        user.password = password;
        user.telephone = telephone;
        user.migrationid = migrationid;
        user.createdTime = created_time;
        user.editedTime = created_time;
        user.save();
        B_User_Account.create(user);
        B_User_Info.create(user);
        return user;
    }

    public static S_User findByMigrationid(int migrationid) {
        return S_User.find.where().eq("migrationid", migrationid).findUnique();
    }
}
