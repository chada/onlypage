package models.db1.user.coin;

import com.alibaba.fastjson.annotation.JSONField;
import models.db1.CModel;
import models.db1.task.B_Task;
import models.db1.user.S_User;
import models.db1.wechat.B_Wechat_Pay;
import models.db1.wechat.B_Wechat_Withdraw;

import javax.persistence.*;

//用户账号增加
@Entity
public class B_User_Coin_Out_Log extends CModel {

    @OneToOne(fetch = FetchType.LAZY)
    public B_User_Coin_Log log;

    //系统，提现，任务审核花费,任务续费，打赏
    public enum Type {
        System, WithdrawDeposit, TaskCheck,TaskRenew,DaShang
    }

    public Type type;


    public enum WithdrawType {
        wechat, zhifubao
    }

    public WithdrawType withdrawType;

    @OneToOne(fetch = FetchType.LAZY)
    public B_Wechat_Withdraw wechatWithdraw;

    @ManyToOne
    @JSONField(serialize = false)
    public B_Task task;

    public String remark;


    public static Finder<Integer, B_User_Coin_Out_Log> find = new Finder<Integer, B_User_Coin_Out_Log>(
            Integer.class, B_User_Coin_Out_Log.class
    );


    public static B_User_Coin_Out_Log create(B_User_Coin_Log log, B_User_Coin_Out_Log.Type type, B_User_Coin_Out_Log.WithdrawType withdrawType,B_Wechat_Withdraw wechatWithdraw, B_Task task, String remark) {
        B_User_Coin_Out_Log out_log = new B_User_Coin_Out_Log();
        out_log.task = task;
        out_log.withdrawType = withdrawType;
        out_log.type = type;
        out_log.log = log;
        out_log.remark = remark;
        out_log.wechatWithdraw = wechatWithdraw;
        out_log.save();
        return out_log;
    }

    public static B_User_Coin_Out_Log create(B_User_Coin_Log log, B_User_Coin_Out_Log.Type type, B_Task task, String remark) {
        B_User_Coin_Out_Log out_log = new B_User_Coin_Out_Log();
        out_log.task = task;
        out_log.type = type;
        out_log.log = log;
        out_log.remark = remark;
        out_log.save();
        return out_log;
    }
}
