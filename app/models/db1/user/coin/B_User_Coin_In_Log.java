package models.db1.user.coin;

import com.alibaba.fastjson.annotation.JSONField;
import models.db1.CModel;
import models.db1.article.B_Article;
import models.db1.task.B_Task;
import models.db1.user.S_User;
import models.db1.wechat.B_Wechat_Pay;

import javax.persistence.*;

//用户账号增加
@Entity
public class B_User_Coin_In_Log extends CModel {

    @OneToOne(fetch = FetchType.LAZY)
    public B_User_Coin_Log log;

    //系统迁移，任务充值,任务剩余，任务获取,任务续费，打赏用户，用户账号充值
    public enum Type {
        System, SystemMigration, TaskRecharge, TaskRemain, TaskAcquisition,TaskRenew,DaShang,UserRechargeAccount
    }

    public Type type;


    public enum PayType {
        wechat, zhifubao
    }

    public PayType payType;

    @OneToOne(fetch = FetchType.LAZY)
    public B_Wechat_Pay wechatPay;

    @ManyToOne
    @JSONField(serialize = false)
    public B_Task task;

    public String remark;

    public static Finder<Integer, B_User_Coin_In_Log> find = new Finder<Integer, B_User_Coin_In_Log>(
            Integer.class, B_User_Coin_In_Log.class
    );


    public static B_User_Coin_In_Log create(B_User_Coin_Log log, Type type, PayType payType, B_Wechat_Pay wechatPay,B_Task task, String remark) {
        B_User_Coin_In_Log in_log = new B_User_Coin_In_Log();
        in_log.task = task;
        in_log.payType = payType;
        in_log.type = type;
        in_log.log = log;
        in_log.remark = remark;
        in_log.wechatPay = wechatPay;
        in_log.save();
        return in_log;
    }

    //被打赏者的In记录，类型是DaShang，不涉及到微信支付，打赏是用唯页平台里的账户余额来打赏别人的
    public static B_User_Coin_In_Log createRewarded(B_User_Coin_Log log, B_User_Coin_In_Log.Type type, B_Task task, String remark) {
        B_User_Coin_In_Log in_log = new B_User_Coin_In_Log();
        in_log.task = task;
        in_log.type = type;
        in_log.log = log;
        in_log.remark = remark;
        in_log.save();
        return in_log;
    }

}
