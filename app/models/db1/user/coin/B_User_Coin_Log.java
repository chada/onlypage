package models.db1.user.coin;

import models.db1.CModel;
import models.db1.task.B_Task;
import models.db1.task.strategy.B_Task_Strategy_Info;
import models.db1.user.S_User;
import models.db1.wechat.B_Wechat_Pay;
import models.db1.wechat.B_Wechat_Withdraw;

import javax.persistence.*;
import java.util.Date;

//用户所有金钱变动记录
@Entity
public class B_User_Coin_Log extends CModel {


    @Column(nullable = false)
    @ManyToOne
    public S_User owner;

    public enum Type {
        in, out
    }

    public Type type;

    public int coin;

    public int currCoin;

    @OneToOne(mappedBy = "log", cascade = CascadeType.ALL)
    public B_User_Coin_In_Log inLog;

    @OneToOne(mappedBy = "log", cascade = CascadeType.ALL)
    public B_User_Coin_Out_Log outLog;

    public static Finder<Integer, B_User_Coin_Log> find = new Finder<Integer, B_User_Coin_Log>(
            Integer.class, B_User_Coin_Log.class
    );

    //任务审核充值
    public static void create_taskCommit(B_Task task, int coin, B_Wechat_Pay wechatPay) {
        B_User_Coin_Log log_in = new B_User_Coin_Log();
        log_in.type = Type.in;
        log_in.coin = coin;
        log_in.owner = task.owner;
        log_in.save();
        B_User_Coin_In_Log in_log = B_User_Coin_In_Log.create(log_in, B_User_Coin_In_Log.Type.TaskRecharge, B_User_Coin_In_Log.PayType.wechat, wechatPay, task, null);
    }

    public static void create_taskCheck(B_Task task, int coin) {
        B_User_Coin_Log log_out = new B_User_Coin_Log();
        log_out.type = Type.out;
        log_out.coin = coin;
        log_out.owner = task.owner;
        log_out.save();
        B_User_Coin_Out_Log out_log = B_User_Coin_Out_Log.create(log_out, B_User_Coin_Out_Log.Type.TaskCheck, B_User_Coin_Out_Log.WithdrawType.wechat, null, task, null);
    }

    //微信提现
    public static void create_wehcatWitddrawDeposit(S_User owner, int coin, B_Wechat_Withdraw withdraw) {
        B_User_Coin_Log log_out = new B_User_Coin_Log();
        log_out.type = Type.out;
        log_out.coin = coin;
        log_out.owner = owner;
        log_out.save();
        B_User_Coin_Out_Log out_log = B_User_Coin_Out_Log.create(log_out, B_User_Coin_Out_Log.Type.WithdrawDeposit, B_User_Coin_Out_Log.WithdrawType.wechat, withdraw, null, null);
    }

    //打赏别人
    public static void create_wechatReward(S_User owner, int coin) {
        B_User_Coin_Log log_out = new B_User_Coin_Log();
        log_out.type = Type.out;
        log_out.coin = coin;
        log_out.owner = owner;
        log_out.save();
        //打赏者的Out记录，类型是DaShang，不涉及到提现
        B_User_Coin_Out_Log out_log = B_User_Coin_Out_Log.create(log_out, B_User_Coin_Out_Log.Type.DaShang,null, null);
    }

    //被别人打赏
    public static void create_wechatRewarded(S_User owner, int coin) {
        B_User_Coin_Log log_in = new B_User_Coin_Log();
        log_in.type = Type.in;
        log_in.coin = coin;
        log_in.owner = owner;
        log_in.save();
        B_User_Coin_In_Log in_log = B_User_Coin_In_Log.createRewarded(log_in, B_User_Coin_In_Log.Type.DaShang,null, null);
    }

    //被别人打赏
    public static void create_wechatRewardedWithDraw(S_User owner, int coin) {
        B_User_Coin_Log log_out = new B_User_Coin_Log();
        log_out.type = Type.out;
        log_out.coin = coin;
        log_out.owner = owner;
        log_out.save();
        ////被打赏者的Out记录，类型是withdraw，因为是自动打赏到用户的微信钱包里，相当于用户自己提现了一次
        B_User_Coin_Out_Log out_log = B_User_Coin_Out_Log.create(log_out, B_User_Coin_Out_Log.Type.WithdrawDeposit,null, null);
    }

    //任务获取
    public static void create_TaskAcquisition(B_Task task, S_User owner, int coin) {
        B_User_Coin_Log log_in = new B_User_Coin_Log();
        log_in.type = Type.in;
        log_in.coin = coin;
        log_in.owner = owner;
        log_in.save();
        B_User_Coin_In_Log in_log = B_User_Coin_In_Log.create(log_in, B_User_Coin_In_Log.Type.TaskAcquisition, null, null, task, null);
    }


    //任务剩余
    public static void create_TaskRemain(B_Task task, S_User owner, int coin) {
        B_User_Coin_Log log_in = new B_User_Coin_Log();
        log_in.type = Type.in;
        log_in.coin = coin;
        log_in.owner = owner;
        log_in.save();
        B_User_Coin_In_Log in_log = B_User_Coin_In_Log.create(log_in, B_User_Coin_In_Log.Type.TaskRemain, null, null, task, null);
    }


    public static void create_SystemMigration(S_User owner, int coin) {
        B_User_Coin_Log log_in = new B_User_Coin_Log();
        log_in.type = Type.in;
        log_in.coin = coin;
        log_in.owner = owner;
        log_in.save();
        B_User_Coin_In_Log in_log = B_User_Coin_In_Log.create(log_in, B_User_Coin_In_Log.Type.SystemMigration, null, null, null, null);
    }

    //任务充值
    public static void create_taskRenewIn(B_Task task, int coin, B_Wechat_Pay wechatPay) {
        B_User_Coin_Log log_in = new B_User_Coin_Log();
        log_in.type = Type.in;
        log_in.coin = coin;
        log_in.owner = task.owner;
        log_in.save();
        B_User_Coin_In_Log in_log = B_User_Coin_In_Log.create(log_in, B_User_Coin_In_Log.Type.TaskRenew, B_User_Coin_In_Log.PayType.wechat, wechatPay, task, null);
    }
    public static void create_taskRenewOut(B_Task task, int coin) {
        B_User_Coin_Log log_out = new B_User_Coin_Log();
        log_out.type = Type.out;
        log_out.coin = coin;
        log_out.owner = task.owner;
        log_out.save();
        B_User_Coin_Out_Log out_log = B_User_Coin_Out_Log.create(log_out, B_User_Coin_Out_Log.Type.TaskRenew, B_User_Coin_Out_Log.WithdrawType.wechat, null, task, null);
    }

    //用户账号充值
    public static void create_userRechargeIn(S_User rechargeUser, int coin, B_Wechat_Pay wechatPay) {
        B_User_Coin_Log log_in = new B_User_Coin_Log();
        log_in.type = Type.in;
        log_in.coin = coin;
        log_in.owner = rechargeUser;
        log_in.save();
        B_User_Coin_In_Log in_log = B_User_Coin_In_Log.create(log_in, B_User_Coin_In_Log.Type.UserRechargeAccount, B_User_Coin_In_Log.PayType.wechat, wechatPay, null, null);
    }


    @Override
    public void save() {
        this.currCoin = this.owner.account.coin;
        super.save();
    }


    public static B_User_Coin_Log findForWechatTaskRecharge(B_Task task) {
        return B_User_Coin_Log.find.fetch("inLog").fetch("inLog.wechatPay").where().eq("inLog.task", task).eq("inLog.type", B_User_Coin_In_Log.Type.TaskRecharge).findUnique();
    }
}
