package services;

import beans.util.sms.SendSmsResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import controllers.BaseController;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.*;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.protocol.HttpContext;
import play.Logger;
import util.Common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * 短息服务--luosimao
 * Created by Administrator on 2015/3/4.
 */
public class LuosimaoService {
    public static String key = Common.getStringConfig("luosimao.service.key");

    public static SendSmsResult SendSMSTimely(String mobiles, String msg) throws Exception {

        if(BaseController.develop){
            System.out.println(mobiles+":"+msg);
            return SendSmsResult.createTest();
        }

        LuosimaoService l = new LuosimaoService();
        SendSmsResult result = l.send(mobiles, msg);
        return result;
//        System.out.println(msg);
//        return true;
    }

    private SendSmsResult send(String mobiles, String msg) throws Exception {
        HttpClient client = new HttpClient();
        GetMethod get = new GetMethod();
        get.setURI(new URI("http://sms-api.luosimao.com/v1/http_get/send/json?key=" + key + "&mobile=" + mobiles + "&message=" + msg));
        client.executeMethod(get);
        String result = get.getResponseBodyAsString();
        JSONObject jsonObject = JSON.parseObject(result);
        return SendSmsResult.create(jsonObject);
//        System.out.println(jsonObject);
//        String error = jsonObject.getString("error");
//        String message = jsonObject.getString("msg");
//        if (error.equals("0")) {
//            return true;
//        }
//        Logger.info("错误代码:" + error + "短信发送服务错误：" + message);
//        return false;
    }


    private String status() throws Exception {

        DefaultHttpClient client = new DefaultHttpClient();

        client.addRequestInterceptor(new HttpRequestInterceptor() {
            @Override
            public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
                request.addHeader("Accept-Encoding", "gzip");
                request.addHeader("Authorization", "Basic " + new Base64().encodeToString("api:3c8fc929b80b444f1a3c7e8cdfb9395f".getBytes("utf-8")));
            }
        });

        client.getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 30000);
        client.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, 30000);

        HttpGet request = new HttpGet("https://sms-api.luosimao.com/v1/status.json");

        ByteArrayOutputStream bos = null;
        InputStream bis = null;
        byte[] buf = new byte[10240];

        String content = null;
        try {
            HttpResponse response = client.execute(request);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                bis = response.getEntity().getContent();
                Header[] gzip = response.getHeaders("Content-Encoding");

                bos = new ByteArrayOutputStream();
                int count;
                while ((count = bis.read(buf)) != -1) {
                    bos.write(buf, 0, count);
                }
                bis.close();

                if (gzip.length > 0 && gzip[0].getValue().equalsIgnoreCase("gzip")) {
                    GZIPInputStream gzin = new GZIPInputStream(new ByteArrayInputStream(bos.toByteArray()));
                    StringBuffer sb = new StringBuffer();
                    int size;
                    while ((size = gzin.read(buf)) != -1) {
                        sb.append(new String(buf, 0, size, "utf-8"));
                    }
                    gzin.close();
                    bos.close();

                    content = sb.toString();
                } else {
                    content = bos.toString();
                }

//                System.out.println(content);
            } else {
//                System.out.println("error code is " + response.getStatusLine().getStatusCode());
            }
            return content;

        } finally {
            if (bis != null) {
                try {
                    bis.close();// 最后要关闭BufferedReader
                } catch (Exception e) {
                }
            }
        }
    }
}
