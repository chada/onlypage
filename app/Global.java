import beans.task.TaskConsumer;
import com.avaje.ebean.Ebean;
import controllers.api.TaskController;
import models.db1.B_Exception;
import models.db1.task.B_Task;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.mvc.Action;
import play.mvc.Http;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import views.html.error.error_pc;

import static play.mvc.Results.internalServerError;

/**
 * Created by Administrator on 2016/3/2.
 */
public class Global extends GlobalSettings {
    @Override
    public Action onRequest(Http.Request request, Method method) {
        return super.onRequest(request, method);
    }

    @Override
    public void onStart(Application app) {
        Logger.error("项目启动");
        runEveryDay();
    }


    @Override
    public void onStop(Application app) {
        TaskController.pool.shutdown();
        Logger.info("项目关闭");
    }

    @Override
    public play.libs.F.Promise<play.mvc.SimpleResult> onHandlerNotFound(Http.RequestHeader requestHeader) {
//        Logger.info(requestHeader.host()+requestHeader.uri());
//        return super.onHandlerNotFound(requestHeader);
        return play.libs.F.Promise.<play.mvc.SimpleResult>pure(internalServerError(
                error_pc.render("你访问的页面不存在")
        ));
    }


    public static int count = 0;

    //每天零点开始进行运算
    private static void runEveryDay() {
        Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd '00:00:01'");
        Date startTime = null;
        try {
            startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sdf.format(calendar.getTime()));
        } catch (ParseException e) {
            B_Exception.create(e.getMessage(), "Global-runEveryDay计算startTime异常", B_Exception.Type.system);
            e.printStackTrace();
        }
        long daySpan = 24 * 60 * 60 * 1000;
        Timer t = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                try {
                    taskCheckout();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        t.scheduleAtFixedRate(task, startTime, daySpan);
    }


    private static void taskCheckout() throws Exception {
        Date now = new Date();
        List<B_Task> taskList = B_Task.find.where().le("endTime", now).findList();
        for (B_Task task : taskList) {
            Ebean.beginTransaction();
            try {
                if (task.needEnd()) {
                    task.finish();
                }
                Ebean.commitTransaction();
            } catch (Exception e) {

            } finally {
                Ebean.endTransaction();
            }
        }
    }

}
