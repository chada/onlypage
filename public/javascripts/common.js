/**
 * Created by Administrator on 2016/4/12.
 */

//显示提示信息
function showPromptMsg_Success(text) {
    if (typeof(toastr) == "undefined") {
        stopPromptMsg()
        $("#promptMsg_Success").show()
        $("#promptMsg_Success").html(text).fadeOut(3000);
    } else {
        toastr.success(text)
    }
}

function showPromptMsg_Info(text) {
    if (typeof(toastr) == "undefined") {
        stopPromptMsg()
        $("#promptMsg_Info").show()
        $("#promptMsg_Info").html(text).fadeOut(3000);
    } else {
        toastr.info(text)
    }
}

function showPromptMsg_Warning(text) {
    if (typeof(toastr) == "undefined") {
        stopPromptMsg()
        $("#promptMsg_Warning").show();
        $("#promptMsg_Warning").html(text).fadeOut(3000);
    } else {
        toastr.warning(text)
    }
}

function showPromptMsg_Danger(text) {
    if (typeof(toastr) == "undefined") {
        stopPromptMsg()
        $("#promptMsg_Danger").show()
        $("#promptMsg_Danger").html(text).fadeOut(3000);
    } else {
        toastr.error(text)
    }
}

function stopPromptMsg() {
    $("#promptMsg_Success").stop(true, true);
    $("#promptMsg_Info").stop(true, true);
    $("#promptMsg_Warning").stop(true, true);
    $("#promptMsg_Danger").stop(true, true);
}


//显示loading加载标志
function loadingStart() {
    $("#loading").show()
    $("#loadingCover").show()

}

function loadingEnd() {
    $("#loading").hide()
    $("#loadingCover").hide()
}


function checkURL(URL) {
    var str = URL;
    str = str.match(/http(s):\/\/.+/);
    if (str == null) {
        return false;
    } else {
        return true;
    }
}
function getPic(content) {
    var imgReg = /<img.*?(?:>|\/>)/gi;
    //匹配src属性
    var srcReg = /src=[\'\"]?([^\'\"]*)[\'\"]?/i;
    var arr = content.match(imgReg);
    if(arr!=null&&arr.length>0){
        return arr[0].match(srcReg)[1];
    }
}

function loadjs(src,func){
    //判断这个js文件存在直接执行回调
    var scripts = document.getElementsByTagName('script') ;
    for(i in scripts)
        if(scripts[i].src == src)
            return func() ;
    if(typeof func != 'function')
    {
        console.log('param 2 is not a function!!') ;
        return false ;
    }
    var script = document.createElement('script') ;
    script.type ='text/javascript' ;
    script.src = src ;
    var head = document.getElementsByTagName('head').item(0);
    head.appendChild(script);

    script.onload = function(){
        func();
    }
}


function isEmpty(str) {
    //注：str == '' 时，如果str=true也会成立。
    if (str == null || str == undefined || str === '' || str.length == 0)
        return true;
    else
        return false;
}

function isInteger(obj) {
    return Math.floor(obj) == obj
}

function setCurrentUserCookie(userName,userUUID) {
    var user= userName+','+userUUID;
    $.cookie('currentUser', user,{ path: '/' });
}

function deleteCurrentUserCookie() {
    $.cookie('currentUser', null,{ path: '/' });
}

function getCurrentUserName() {
    if(isEmpty($.cookie('currentUser'))){
        return null;
    }
    return $.cookie('currentUser').split(',')[0];
}

function getCurrentUserUUID() {
    if(isEmpty($.cookie('currentUser'))){
        return null;
    }
    return $.cookie('currentUser').split(',')[1];
}

function confirmSwal(title, text, callback) {
    swal({
        title: title,
        text: text,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "确认",
        cancelButtonText: "取消",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function (isConfirm) {
        callback(isConfirm)  //isConfirm 是布尔值
    });
}

//自定义任务页面，链接跳转前可以进行传播节点的详细内容填写
//填写完成后跳转至指定回调页面；
//若没有获取到任务信息则直接跳转
function propagationInfoSubmitAndRedirect(redirectUrl) {
    var taskId=$(parent.window.document).find("#iframe").attr("taskId");
    if(!isEmpty(taskId)) {
        window.open("/plugins/onlypageFormSubmit/page?taskId=" + taskId + "&redirectUrl=" + encodeURIComponent(redirectUrl))
    }else {
        window.location.href =redirectUrl;
    }
}

//自定义任务页面，链接跳转前可以进行传播节点的详细内容填写
//填写完成后跳转至指定回调页面；
//若没有获取到任务信息则直接跳转
function pinganPropagationInfoSubmitAndRedirect(redirectUrl) {
    var taskId=$(parent.window.document).find("#iframe").attr("taskId");
    if(!isEmpty(taskId)) {
        window.open("/plugins/onlypageFormSubmit/pinganCreditCardApplyInfoSubmitPage?taskId=" + taskId + "&redirectUrl=" + encodeURIComponent(redirectUrl))
    }else {
        window.location.href =redirectUrl;
    }
}