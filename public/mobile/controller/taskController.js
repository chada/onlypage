var taskController = angular.module('taskController', []);

taskController.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider.state('layout.taskList', {
            url: "/task/list",
            templateUrl: '/assets/mobile/html/task/list.html',
            controller: 'taskListCtrl'
        }).state('layout.taskOwnerList', {
            url: "/task/ownerList",
            templateUrl: '/assets/mobile/html/task/ownerList.html',
            controller: 'taskOwnerListCtrl'
        }).state('layout.taskGainList', {
            url: "/task/gainList",
            templateUrl: '/assets/mobile/html/task/gainList.html',
            controller: 'taskGainListCtrl'
        }).state('layout.taskParticipateList', {
            url: "/task/participateList",
            templateUrl: '/assets/mobile/html/task/participateList.html',
            controller: 'taskParticipateListCtrl'
        }).state('layout.groundPromoterList', {
            url: "/task/groundPromoterList",
            templateUrl: '/assets/mobile/html/task/groundPromoterList.html',
            controller: 'taskGroundPromoterListCtrl'
        }).state('layout.promoterList', {
            url: "/task/promoterList",
            templateUrl: '/assets/mobile/html/task/promoterList.html',
            controller: 'taskPromoterListCtrl'
        }).state('layout.createArticle', {
            url: "/task/createArticle",
            templateUrl: '/assets/mobile/html/task/createArticle.html',
            controller: 'taskCreateArticleCtrl'
        }).state('layout.propagationInfoList', {
            url: "/task/propagationInfoList?:taskUUID",
            templateUrl: '/assets/mobile/html/task/propagation/taskPropagationInfoList.html',
            controller: 'propagationInfoListCtrl'
        }).state('layout.myChildPropagationList', {
            url: "/task/myChildPropagationList?:taskUUID",
            templateUrl: '/assets/mobile/html/task/propagation/myChildPropagationList.html',
            controller: 'myChildPropagationListCtrl'
        }).state('layout.myChildPropagationInfoList', {
            url: "/task/myChildPropagationInfoList?:taskUUID",
            templateUrl: '/assets/mobile/html/task/propagation/myChildPropagationInfoList.html',
            controller: 'myChildPropagationInfoListCtrl'
        }).state('layout.taskCreate', {
            url: "/task/create",
            templateUrl: '/assets/mobile/html/task/edit.html',
            controller: "taskEditCtrl"
        }).state('layout.taskEdit', {
            url: "/task/edit?:uuid",
            templateUrl: 'assets/mobile/html/task/edit.html',
            controller: "taskEditCtrl"
        })
    }]);


taskController.controller('taskListCtrl', function ($scope, $http, $rootScope, $timeout, $stateParams, $state) {
    $scope.list = []
    $scope.pageSize = 10;
    var canReqData = false;//由于第一次分页控件会加载一次，判断屏幕低端控件也会加载一次，所以需要禁止其中的一个
    $scope.getList = function (data) {
        // console.log(data)
        $.each(data, function (index, ele) {
            $scope.list.push(ele)

        })
        if (data.length < $scope.pageSize) {
            canReqData = false;
        } else {
            canReqData = true;
        }
    }

    $scope.reqData = function () {
        if (canReqData) {
            canReqData = false;
            $scope.getListOperate.nextPage()
        }
    }

    $scope.showTaskStartUrl = function (info) {
        window.location.href = info.shareUrl
    }

})

//我发布的任务
taskController.controller('taskOwnerListCtrl', function ($scope, $http, $rootScope, $timeout, $stateParams, $state) {
    $scope.list = []
    $scope.pageSize = 10;
    var canReqData = false;//由于第一次分页控件会加载一次，判断屏幕低端控件也会加载一次，所以需要禁止其中的一个
    $scope.getList = function (data) {
        // console.log(data)
        $.each(data, function (index, ele) {
            $scope.list.push(ele)

        })
        if (data.length < $scope.pageSize) {
            canReqData = false;
        } else {
            canReqData = true;
        }
    }

    $scope.reqData = function () {
        if (canReqData) {
            canReqData = false;
            $scope.getListOperate.nextPage()
        }
    }

    $scope.commitCheck = function (info) {
        $scope.currTask = info;
        if (info.coin == 0) {
            $scope.recharge(info)
        }else{
            $("#taskCommitCheckModal").modal("show")
        }


    }


    $scope.payTypeList = [{name: "审核"}, {name: "充值"}]

    $scope.recharge = function (info) {
        loadingStart()
        $http({
            url: '/mobile/task/commitCheck/recharge/info',
            data: {
                uuid: info.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                if (info.coin == 0) {
                    info.state= "publish"
                    showPromptMsg_Success(data.data)
                } else {
                    $scope.rechargeInfo = data.data
                    $scope.payType = $scope.payTypeList[0]
                    if (typeof WeixinJSBridge == "undefined") {
                        if (document.addEventListener) {
                            document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
                        } else if (document.attachEvent) {
                            document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
                            document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
                        }
                    } else {
                        onBridgeReady();
                    }
                }
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }


    function onBridgeReady() {
        WeixinJSBridge.invoke(
            'getBrandWCPayRequest', {
                "appId": $scope.rechargeInfo.appId,     //公众号名称，由商户传入
                "timeStamp": $scope.rechargeInfo.timeStamp, //时间戳，自1970年以来的秒数
                "nonceStr": $scope.rechargeInfo.nonceStr, //随机串
                "package": $scope.rechargeInfo.package,
                "signType": $scope.rechargeInfo.signType, //微信签名方式:
                "paySign": $scope.rechargeInfo.paySign //微信签名
            },
            function (res) {
                if (res.err_msg == "get_brand_wcpay_request:ok") {
                    if ($scope.payType == $scope.payTypeList[0]) {
                        $("#taskCommitCheckModal").modal("hide")
                    } else if ($scope.payType == $scope.payTypeList[1]) {
                        $("#taskRenewModal").modal("hide")
                        $scope.currTask.coin += ($scope.renewMoney * 100)
                    }

                    $scope.currTask.state = "publish"
                    $scope.$apply()
                }
            }
        );
    }


    $scope.delete = function (info) {
        commonComfirm({
            title: "提醒",
            message: "确定删除此任务？",
            operate: function (reselt) {
                if (reselt) {
                    loadingStart()
                    $http({
                        url: '/mobile/task/delete',
                        data: {
                            uuid: info.uuid
                        },
                        method: 'POST'
                    }).success(function (data) {
                        if (data.success) {
                            $scope.list = []
                            $scope.getListOperate.init()
                            showPromptMsg_Success(data.data)
                        } else {
                            showPromptMsg_Warning(data.data)
                        }

                    }).finally(function () {
                        loadingEnd()
                    })
                } else {

                }
            }
        })
    }

    $scope.edit=function (info) {
        $state.go("layout.taskEdit",{uuid:info.uuid});
    };

    $scope.checkout = function (task) {
        commonComfirm({
            title: "提醒",
            message: "确定强制结算此任务？",
            operate: function (reselt) {
                if (reselt) {
                    loadingStart()
                    $http({
                        url: '/mobile/task/checkout',
                        data: {
                            uuid: task.uuid
                        },
                        method: 'POST'
                    }).success(function (data) {
                        if (data.success) {
                            $scope.list = []
                            $scope.getListOperate.init()
                            showPromptMsg_Success(data.data)
                        } else {
                            showPromptMsg_Warning(data.data)
                        }


                    }).finally(function () {
                        loadingEnd()
                    })
                } else {

                }
            }
        })
    }

    $scope.showTaskStartUrl = function (info) {
        window.location.href = info.shareUrl
    }


    $scope.showRenewModal = function (info) {
        $scope.currTask = info;
        $("#taskRenewModal").modal("show")
    }


    $scope.renew = function (task) {

        if ($scope.renewMoney == undefined || $scope.renewMoney == "" || $scope.renewMoney < 0) {
            showPromptMsg_Warning("请输入不小于0元的充值金额")
            return false;
            l
        }

        loadingStart()
        $http({
            url: '/mobile/task/renew',
            data: {
                uuid: task.uuid,
                coin: $scope.renewMoney * 100,
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.payType = $scope.payTypeList[1]
                $scope.rechargeInfo = data.data
                if (typeof WeixinJSBridge == "undefined") {
                    if (document.addEventListener) {
                        document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
                    } else if (document.attachEvent) {
                        document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
                        document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
                    }
                } else {
                    onBridgeReady();
                }
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }

})

//我的获益
taskController.controller('taskGainListCtrl', function ($scope, $http, $rootScope, $timeout, $stateParams, $state) {
    $scope.list = []
    $scope.pageSize = 10;
    var canReqData = false;//由于第一次分页控件会加载一次，判断屏幕低端控件也会加载一次，所以需要禁止其中的一个
    $scope.getList = function (data) {

        $.each(data, function (index, ele) {
            $scope.list.push(ele)

        })
        if (data.length < $scope.pageSize) {
            canReqData = false;
        } else {
            canReqData = true;
        }
    }

    $scope.reqData = function () {
        if (canReqData) {
            canReqData = false;
            $scope.getListOperate.nextPage()
        }
    }

    $scope.showTaskStartUrl = function (info) {
        window.location.href = info.shareUrl
    }


})

//我的代言(我参与的任务)
taskController.controller('taskParticipateListCtrl', function ($scope, $http, $rootScope, $timeout, $stateParams, $state) {
    $scope.list = []
    $scope.pageSize = 10;
    var canReqData = false;//由于第一次分页控件会加载一次，判断屏幕低端控件也会加载一次，所以需要禁止其中的一个
    $scope.getList = function (data) {

        $.each(data, function (index, ele) {
            $scope.list.push(ele)

        })
        if (data.length < $scope.pageSize) {
            canReqData = false;
        } else {
            canReqData = true;
        }
    }

    $scope.reqData = function () {
        if (canReqData) {
            canReqData = false;
            $scope.getListOperate.nextPage()
        }
    }

    //分享
    $scope.showTaskStartUrl = function (info) {
        window.location.href = info.shareUrl
    }

    //参与伙伴
    $scope.showChildPropagationList = function (info) {
        $state.go("layout.myChildPropagationList",{taskUUID:info.uuid})
    }

    //查看填写（子节点填写信息）
    $scope.showChildPropagationInfoList = function (info) {
        $state.go("layout.myChildPropagationInfoList",{taskUUID:info.uuid})
    }

})

//我是地推人员
taskController.controller('taskGroundPromoterListCtrl', function ($scope, $http, $rootScope, $timeout, $stateParams, $state) {
    $scope.list = []
    $scope.pageSize = 10;
    var canReqData = false;//由于第一次分页控件会加载一次，判断屏幕低端控件也会加载一次，所以需要禁止其中的一个
    $scope.getList = function (data) {

        $.each(data, function (index, ele) {
            $scope.list.push(ele)

        })
        if (data.length < $scope.pageSize) {
            canReqData = false;
        } else {
            canReqData = true;
        }
    }

    $scope.reqData = function () {
        if (canReqData) {
            canReqData = false;
            $scope.getListOperate.nextPage()
        }
    }

    $scope.showTaskStartUrl = function (info) {
        window.location.href = info.shareUrl
    }


})

//我是商家
taskController.controller('taskPromoterListCtrl', function ($scope, $http, $rootScope, $timeout, $stateParams, $state) {
    $scope.list = []
    $scope.pageSize = 10;
    var canReqData = false;//由于第一次分页控件会加载一次，判断屏幕低端控件也会加载一次，所以需要禁止其中的一个
    $scope.getList = function (data) {

        $.each(data, function (index, ele) {
            $scope.list.push(ele)

        })
        if (data.length < $scope.pageSize) {
            canReqData = false;
        } else {
            canReqData = true;
        }
    }

    $scope.reqData = function () {
        if (canReqData) {
            canReqData = false;
            $scope.getListOperate.nextPage()
        }
    }

    $scope.showTaskStartUrl = function (info) {
        window.location.href = info.shareUrl
    }

    $scope.showPropagationInfoList = function (info) {
        $state.go("layout.propagationInfoList",{taskUUID:info.uuid})
    }

})

//创建文章
taskController.controller('taskCreateArticleCtrl', function ($scope, $http, $rootScope, $timeout, $stateParams, $state) {
    $scope.isNew = true;
    $scope.isComment = true;
    $scope.isPublic = true;

    var ue = UE.getEditor('container', {
        //initialFrameHeight: ($(window).height() - 230) < 300 ? 300 : $(window).height() - 530,
        initialFrameHeight: 250,
        toolbars: [
            [
                'undo', //撤销
                'redo', //重做
                'fontsize', //字号
                'fontfamily', //字体
                'blockquote', //引用
                'horizontal', //分隔线
                'removeformat', //清除格式
                'formatmatch', //格式刷
                'link', //超链接
                'emotion', //表情
                'bold', //加粗
                'italic', //斜体
                'underline', //下划线
                'forecolor', //字体颜色
                'backcolor', //背景色
                'indent', //首行缩进
                'justifyleft', //居左对齐
                'justifyright', //居右对齐
                'justifycenter', //居中对齐
                'justifyjustify', //两端对齐
                'rowspacingtop', //段前距
                'rowspacingbottom', //段后距
                'lineheight', //行间距
                'insertorderedlist', //有序列表
                'insertunorderedlist', //无序列表
                'imagenone', //默认
                'imageleft', //左浮动
                'imageright', //右浮动
                'imagecenter', //居中
                'simpleupload' //单图上传
            ]
        ]
    });

    //是否允许开关
    $scope.isCommentChange=function (status) {
        if($scope.isComment!=status) {
            $scope.isComment = !$scope.isComment;
        }
    };

    //是否公开开关
    $scope.isPublicChange=function (status) {
        if($scope.isPublic!=status) {
            $scope.isPublic = !$scope.isPublic;
        }
    };

    $scope.save = function () {
        if ($scope.title == undefined || $scope.title == "") {
            showPromptMsg_Warning("请输入标题");
            return
        }

        var html = ue.getContent();

        if (html == undefined || html == "") {
            showPromptMsg_Warning("请输入正文");
            return
        }

        var coverImg = getPic(html);

        loadingStart();
        $http({
            url: '/mobile/article/edit',
            data: {
                title: $scope.title,
                content: html,
                isComment: $scope.isComment,
                isPublic: $scope.isPublic,
                coverImg: coverImg,
                isNew: $scope.isNew
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data);
                $state.go("layout.personalCenter");
            } else {
                showPromptMsg_Warning(data.data);
                $state.go("layout.personalCenter");
            }
        }).finally(function () {
            loadingEnd()
        });
    }
});

//任务节点填写信息
taskController.controller('propagationInfoListCtrl', function ($scope, $http, $rootScope, $timeout, $stateParams, $state) {
    $scope.taskUUID = isEmpty($stateParams.taskUUID) ? 0 : $stateParams.taskUUID;

    $scope.list = []
    $scope.pageSize = 10;
    $scope.getPropagationInfoListData={
        uuid:$scope.taskUUID
    }
    var canReqData = false;//由于第一次分页控件会加载一次，判断屏幕低端控件也会加载一次，所以需要禁止其中的一个
    $scope.getList = function (data) {

        $.each(data, function (index, ele) {
            $scope.list.push(ele)

        })
        if (data.length < $scope.pageSize) {
            canReqData = false;
        } else {
            canReqData = true;
        }
    }

    $scope.reqData = function () {
        if (canReqData) {
            canReqData = false;
            $scope.getListOperate.nextPage()
        }
    }


})

//我在任务中的子节点
taskController.controller('myChildPropagationListCtrl', function ($scope, $http, $rootScope, $timeout, $stateParams, $state) {
    $scope.taskUUID = isEmpty($stateParams.taskUUID) ? 0 : $stateParams.taskUUID;

    $scope.list = []
    $scope.pageSize = 10;
    $scope.getPropagationInfoListData={
        uuid:$scope.taskUUID
    }
    var canReqData = false;//由于第一次分页控件会加载一次，判断屏幕低端控件也会加载一次，所以需要禁止其中的一个
    $scope.getList = function (data) {

        $.each(data, function (index, ele) {
            $scope.list.push(ele)

        })
        if (data.length < $scope.pageSize) {
            canReqData = false;
        } else {
            canReqData = true;
        }
    }

    $scope.reqData = function () {
        if (canReqData) {
            canReqData = false;
            $scope.getListOperate.nextPage()
        }
    }


})

//我在任务中的子节点填写信息
taskController.controller('myChildPropagationInfoListCtrl', function ($scope, $http, $rootScope, $timeout, $stateParams, $state) {
    $scope.taskUUID = isEmpty($stateParams.taskUUID) ? 0 : $stateParams.taskUUID;

    $scope.list = []
    $scope.pageSize = 10;
    $scope.getPropagationInfoListData={
        uuid:$scope.taskUUID
    }
    var canReqData = false;//由于第一次分页控件会加载一次，判断屏幕低端控件也会加载一次，所以需要禁止其中的一个
    $scope.getList = function (data) {

        $.each(data, function (index, ele) {
            $scope.list.push(ele)

        })
        if (data.length < $scope.pageSize) {
            canReqData = false;
        } else {
            canReqData = true;
        }
    }

    $scope.reqData = function () {
        if (canReqData) {
            canReqData = false;
            $scope.getListOperate.nextPage()
        }
    }


})



taskController.controller('taskEditCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload,$state) {

    $scope.isNew = $stateParams.uuid == undefined ? true : false

    //任务可见级别
    $scope.visibleTypeList = [
        {name: '自己可见', value: "self"},
        {name: '好友可见', value: "friend"},
        {name: '全部可见', value: "all"}
    ];

    //任务内容类型
    $scope.dataTypeList = [
        {name: '自定义链接', value: "url"},
        {name: '唯页文章', value: "article"}
    ];

    //策略类型
    $scope.strategyTypeList = [
        {name: '高级策略', value: 'advancedStrategy',},
        {name: '默认规则一', value: 'defaultStrategyOne',},
    ];

    //策略奖励类型
    $scope.strategyRewardTypeList = [
        {name: "固定金额", value: 'fixedamount'},
        {name: "随机金额", value: 'randomamount'}
    ];

    //获取编辑时的任务信息
    $scope.getTaskEditInfo = function () {
        loadingStart();
        $http({
            url: '/mobile/task/edit/info',
            data: {
                uuid: $stateParams.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                var info = data.data;
                $scope.task=info.task;
                $scope.strategy=info.strategy;
                $scope.taskCoin=info.taskCoin / 100;

                if ($scope.strategy.strategyType == $scope.strategyTypeList[1].value) {
                    $scope.selectStrategy($scope.strategyTypeList[1], '默认策略一');
                    $scope.defaultStrategyOneDataInit = info.strategy.defaultStrategyOne;
                    $scope.defaultStrategyOneDataInit.fixedRewardAmount = $scope.defaultStrategyOneDataInit.fixedRewardAmount / 100
                    $scope.defaultStrategyOneDataInit.minRewardAmount = $scope.defaultStrategyOneDataInit.minRewardAmount / 100
                    $scope.defaultStrategyOneDataInit.maxRewardAmount = $scope.defaultStrategyOneDataInit.maxRewardAmount / 100
                } else if (info.strategy.strategyType == $scope.strategyTypeList[0].value) {
                    $scope.selectStrategy($scope.strategyTypeList[0], info.strategy.advancedStrategy.title, info.strategy.advancedStrategy.uuid)
                }


            } else {
                showPromptMsg_Warning(data.data)
                $state.go("console.taskOwnerList")
            }
        }).finally(function () {
            loadingEnd()
        })
    }

    //选择策略
    $scope.selectStrategy = function (type, title, uuid) {
        $scope.strategy={
            type: type.value,
            title: title,
            uuid: uuid,
            defaultStrategyOneData: {
                startTriggerNodeLevel: 1,
                type:$scope.strategyRewardTypeList[0]
            }
        };
        $("#strategyListModal").modal("hide");
    };


    var nowDate=new Date();

    if ($scope.isNew) {
        nowDate.setDate(nowDate.getDate()+15);//默认设置截止日期为15天
        $scope.task={
            endTime: nowDate.Format("yyyy-MM-dd")
        };

        $scope.selectStrategy($scope.strategyTypeList[1],'默认规则 一');//设置默认规则一
        $scope.showAdvanceInfoStatus=false;//高级策略启用与否
    } else {
        $scope.getTaskEditInfo();
        $scope.showAdvanceInfoStatus=true;
    }

    $scope.getStrategyList = function (data) {
        $scope.strategyList = data
    };

    //选择文章模态框显示
    $scope.clickArticleInput = function () {
        if ($scope.task.dataType == 'article') {
            $("#articleListModal").modal("show")
        } else {

        }
    };

    //改变任务内容类型
    $scope.changeTaskType = function (hide) {
        if ($scope.task.dataType == 'article') {
            if (!hide) {
                $("#articleListModal").modal("show")
            }
        }
    };

    //文章列表
    $scope.getArticleList = function (data) {
        $scope.articleList = data
    };

    //选择文章
    $scope.selectArticle = function (article) {
        $scope.task.article = article;
    };

    //高级设置开关
    $scope.showAdvanceInfoStatusChange=function (status) {
        if($scope.showAdvanceInfoStatus!=status) {
            $scope.showAdvanceInfoStatus = !$scope.showAdvanceInfoStatus;

            if($scope.showAdvanceInfoStatus){
                if(isEmpty($scope.strategy.defaultStrategyOneData.endTriggerNodeLevel)&&
                    isEmpty($scope.strategy.defaultStrategyOneData.fixedRewardAmount)&&
                    isEmpty($scope.strategy.defaultStrategyOneData.maxRewardAmount)&&
                    isEmpty($scope.strategy.defaultStrategyOneData.minRewardAmount)){//未设定任何默认策略
                    $scope.strategy={};
                }
            }else {
                if(isEmpty($scope.strategy)||($scope.strategy.type!=$scope.strategyTypeList[1].value)){
                    $scope.selectStrategy($scope.strategyTypeList[1],'默认规则 一');
                }
            }
        }

    };

    //上传任务封面图案
    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if (!file.name.match(/.jpg|.gif|.png|.bmp/i)) {
                    showPromptMsg_Warning('图片格式无效！目前只支持jpg gif png bmp格式，请重新上传');
                    loadingEnd();
                    return false;
                }
            }
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                loadingStart()
                $upload.upload({
                    url: 'upload/image/url',
                    data: {tag: 'taskCoverImg'},
                    file: file
                }).progress(function (evt) {
                }).success(function (data, status, headers, config) {
                    $scope.task.coverImg = data.data
                }).finally(function () {
                    loadingEnd()
                });
            }
        }
    };



    $scope.save = function () {

/******************************表单验证条件***********************************/

        if (isEmpty($scope.task.coverImg)) {
            showPromptMsg_Warning("请上传封面图片");
            return false;
        }
        if (isEmpty($scope.task.title)) {
            showPromptMsg_Warning("请输入标题");
            return false;
        }
        if (isEmpty($scope.task.remark)) {
            showPromptMsg_Warning("请输入描述");
            return false;
        }

        if ($scope.task.dataType == 'article') {
            if (isEmpty($scope.task.article)) {
                showPromptMsg_Warning("请输入广告页面地址(选择文章)");
                return false;
            }
        } else if ($scope.task.dataType == 'url') {
            if (isEmpty($scope.task.data)) {
                showPromptMsg_Warning("请输入广告页面地址");
                return false;
            }
        }

        if (isEmpty($scope.taskCoin)) {
            showPromptMsg_Warning("请输入红包金额");
            return false;
        }
        if ($scope.taskCoin < 0) {
            showPromptMsg_Warning("红包金额不能小于0")
            return false;
        }

        if($scope.showAdvanceInfoStatus){//启用高级策略时

        }

        if ($scope.strategy.type == undefined || $scope.strategy.type == "") {
            showPromptMsg_Warning("请选择任务策略")
            return false;
        }

        if ($scope.strategy.type == $scope.strategyTypeList[1].value) {
            var result = $scope.defaultStrategyOneCheck();
            if (!result.result) {
                showPromptMsg_Warning((result.msg));
                return false
            }
        }

/******************************end***********************************/


/******************************数据处理***********************************/

        $scope.task.endTime+=" 23:59:59";

        if($scope.strategy.type == $scope.strategyTypeList[1].value) {
            $scope.strategy.defaultStrategyOneData.fixedRewardAmount = $scope.strategy.defaultStrategyOneData.fixedRewardAmount * 100;
            $scope.strategy.defaultStrategyOneData.minRewardAmount = $scope.strategy.defaultStrategyOneData.minRewardAmount * 100;
            $scope.strategy.defaultStrategyOneData.maxRewardAmount = $scope.strategy.defaultStrategyOneData.maxRewardAmount * 100;
        }
        if($scope.showAdvanceInfoStatus){

        }

/******************************end***********************************/
        loadingStart();
        $http({
            url: '/mobile/task/edit',
            data: {
                isNew: $scope.isNew,
                uuid: $stateParams.uuid,
                taskCoin: $scope.taskCoin * 100,
                task: JSON.stringify($scope.task),
                strategy: JSON.stringify($scope.strategy),
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
                $state.go("layout.taskOwnerList")
                // $location.path("/console/task/owner/list")
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }

});


taskController.directive('directiveMobileTaskEditStrategyOne', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/mobile/html/directives/task/edit/defaultStrategyOne.html',
        controller: 'directiveMobileTaskEditStrategyOneCtrl',
        scope: {
            info: "=info",
            init: "=init",
            check: "=check",
            typelist: "=strategyrewardtypelist"
        }
    }
})

taskController.controller('directiveMobileTaskEditStrategyOneCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload) {

    $scope.$watch("init", function (newData) {
        if (newData == undefined || newData == "") {

        } else {
            $scope.info.startTriggerNodeLevel = newData.startTriggerNodeLevel
            $scope.info.endTriggerNodeLevel = newData.endTriggerNodeLevel
            $.each($scope.typelist, function (index, ele) {
                if (ele.value == newData.rewardType) {
                    $scope.info.type = ele;
                }
            })

            $scope.info.fixedRewardAmount = newData.fixedRewardAmount
            $scope.info.minRewardAmount = newData.minRewardAmount
            $scope.info.maxRewardAmount = newData.maxRewardAmount
        }
    },true)

    $scope.check = function () {
        if (isEmpty($scope.info.startTriggerNodeLevel)) {
            // return returnMsg(false, "请输入策略起始层次")
            $scope.info.startTriggerNodeLevel = 1
        }
        if (!isEmpty($scope.info.endTriggerNodeLevel)&&($scope.info.endTriggerNodeLevel < $scope.info.startTriggerNodeLevel)) {
            return returnMsg(false, "策略起始层次不能大于策略截止层次")
        }

        if (isEmpty($scope.info.type)) {
            return returnMsg(false, "请选择红包类型 ")
        }
        if ($scope.info.type == $scope.typelist[0]) {
            if (isEmpty($scope.info.fixedRewardAmount)) {
                return returnMsg(false, "请输入固定金额 ")
            }
            if ($scope.info.fixedRewardAmount < 0) {
                return returnMsg(false, "固定金额不能小于0")
            }
        }


        if ($scope.info.type == $scope.typelist[1]) {
            if (isEmpty($scope.info.minRewardAmount)) {
                return returnMsg(false, "请输入随机金额下限 ")
            }
            if ($scope.info.minRewardAmount < 0) {
                return returnMsg(false, "随机金额下限不能小于0")
            }

            if (isEmpty($scope.info.maxRewardAmount)) {
                return returnMsg(false, "请输入随机金额上限 ")
            }
            if ($scope.info.maxRewardAmount < 0) {
                return returnMsg(false, "随机金额上线不能小于0")
            }

            if ($scope.info.maxRewardAmount < $scope.info.minRewardAmount) {
                return returnMsg(false, "随机金额下限不能小于随机金额上线")
            }
        }

        return returnMsg(true, "ok")
    }

    function returnMsg(result, msg) {
        var object = {
            result: result,
            msg: msg
        }
        return object;
    }

})