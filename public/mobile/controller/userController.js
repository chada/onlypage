/**
 * Created by whk on 2017/9/7.
 */
var userController = angular.module('userController', []);

userController.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider.state('layout.personalCenter', {
            url: "/user/personalCenter",
            templateUrl: '/assets/mobile/html/user/personalCenter.html',
            controller: 'personalCenterCtrl'
        })
    }]);

taskController.controller('personalCenterCtrl', function ($scope, $http, $rootScope, $timeout, $stateParams, $state) {

    $scope.getUserInfo = function () {
        $http({
            url: '/mobile/user/info',
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.userInfo = data.data
                // console.log(   $scope.userInfo )
            } else {
                showPromptMsg_Warning(data.data)
            }

        }).finally(function () {

        })
    }

    $scope.getUserStatisticInfo = function () {
        $http({
            url: '/mobile/user/getUserStatisticInfo',
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.userStatisticInfo = data.data
            } else {
                showPromptMsg_Warning(data.data)
            }

        }).finally(function () {

        })
    }

    $scope.getUserInfo();
    $scope.getUserStatisticInfo();



    //我发布的
    $scope.taskOwnerList = function () {
        $state.go("layout.taskOwnerList")
    }

    //我获益的
    $scope.taskGainList = function () {
        $state.go("layout.taskGainList")
    }

    //我代言的（参与的）
    $scope.taskParticipateList = function () {
        $state.go("layout.taskParticipateList")
    }

    //我是地推人员
    $scope.groundPromoterList = function () {
        $state.go("layout.groundPromoterList")
    }

    //我是商家
    $scope.promoterList = function () {
        $state.go("layout.promoterList")
    }

    //创建文章
    $scope.createArticle = function() {
        $state.go("layout.createArticle")
    }


})