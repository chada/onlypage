var mSharpApp = angular.module('mSharpApp', [
    'commonDirectives',
    'commonFilters',
    'ui.router',
    'infinite-scroll',
    'taskController',
    'userController',
    'angularFileUpload'
]);

mSharpApp.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.when("", "/");
        $urlRouterProvider.when("/", "/layout/task/list");

        $stateProvider.state('layout', {
            url: "/layout",
            templateUrl: '/assets/mobile/html/index.html'
        }).state('layout.test', {
            url: "/test",
            templateUrl: '/assets/mobile/html/test.html'
        })
    }]);


mSharpApp.directive('headLayout', function () {
    return {
        restrict: 'EA',
        scope: true,
        templateUrl: '/assets/mobile/html/directives/head/headLayout.html',
        controller: 'directiveHeadTaskController'
    };
})


mSharpApp.controller('directiveHeadTaskController', function ($scope, $http, $location, $rootScope, $timeout, $stateParams) {
    $scope.getUserInfo = function () {
        $http({
            url: '/mobile/user/info',
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.userInfo = data.data
                // console.log(   $scope.userInfo )
            } else {
                showPromptMsg_Warning(data.data)
            }

        }).finally(function () {

        })
    }
    $scope.getUserInfo()
})


mSharpApp.directive('footLayout', function () {
    return {
        restrict: 'EA',
        scope: true,
        templateUrl: '/assets/mobile/html/directives/foot/foot.html',
        controller: 'directiveFootController'
    };
})


mSharpApp.controller('directiveFootController', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $state) {

    $scope.taskList = function () {
        $state.go("layout.taskList")
    }

    $scope.taskOwnerList = function () {
        $state.go("layout.taskOwnerList")
    }

    $scope.create = function () {
        $state.go("layout.taskCreate")
    }

    $scope.personalCenter=function () {
        $state.go("layout.personalCenter")
    }
})


mSharpApp.directive('onlypageAgreement', function () {
    return {
        restrict: 'EA',
        scope: true,
        templateUrl: 'assets/angular/html/directives/agreement.html',
    };
})



//时间选择器
mSharpApp.directive('dateSelect', function () {
    return {
        restrict: 'EA',
        scope: {
            clicktarget: "@", //触发目标
            result: "=", //选择结果
            name: "@", //区别每个控件（防止一个页面有多个控件，数据混乱）,
            maxdate:"="
        },
        templateUrl: 'assets/mobile/html/directives/util/mobiscrollDateSelect.html',
        controller: 'directivesDateSelectCtrl',
    };
})

mSharpApp.controller('directivesDateSelectCtrl', function ($scope, $http, $location, $rootScope, $timeout) {

    $($scope.clicktarget).on("click", function () {
        $("#" + $scope.name).click()
    })

    $scope.init = function () {
        var scrollNowDate=new Date();
        var maxDate=scrollNowDate.setDate(scrollNowDate.getDate()+$scope.maxdate);
        $('#' + $scope.name).mobiscroll().date({
            theme: 'android-holo-light',     // Specify theme like: theme: 'ios' or omit setting to use default
            mode: 'scroller',       // Specify scroller mode like: mode: 'mixed' or omit setting to use default
            display: 'bottom', // Specify display mode like: display: 'bottom' or omit setting to use default
            lang: 'zh',       // Specify language like: lang: 'pl' or omit setting to use default
            maxDate:new Date(maxDate),
            minDate:new Date(),
            onSelect: function (valueText, inst) {
                //console.log(valueText.replace(/\//g,"/"))
                var d = new Date(valueText).Format("yyyy-MM-dd");
                $scope.result = d
                $scope.$apply()
            },
        })
        //隐藏原有的数据显示
        $("#" + $scope.name).hide()
    }
})