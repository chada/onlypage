/**
 * Created by Administrator on 2016/12/22.
 */
var taskController = angular.module('taskController', []);
taskController.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider.state('manage.taskList', {
            url: "/task/list",
            templateUrl: 'assets/manage/html/task/list.html',
            controller: "taskListCtrl"
        })
    }]);

taskController.controller('taskListCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload) {
    $scope.getList = function (data) {
        $scope.list = data
    }

    $scope.getOrderInfo = function (uuid) {
        loadingStart()
        $http({
            url: "manage/task/orderInfo",
            data: {
                uuid: uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.orderInfo = data.data
                $("#orderInfoModal").modal("show")
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }
})