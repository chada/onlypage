/**
 * Created by Administrator on 2016/12/22.
 */
var userController = angular.module('userController', []);
userController.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider.state('manage.userList', {
            url: "/user/list",
            templateUrl: 'assets/manage/html/user/list.html',
            controller: "userListCtrl"
        })
    }]);

userController.controller('userListCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload) {
    $scope.getList = function (data) {
        $scope.list = data
    }

    $scope.login = function (uuid) {
        loadingStart()
        $http({
            url: "manage/user/login",
            data: {
                uuid: uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success("登录成功");
                window.open(data.data)
                // window.open("http://localhost:9000")
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }
})