var mSharpApp = angular.module('mSharpApp', [
    'commonDirectives',
    'commonFilters',
    'angularFileUpload',
    'ui.router',
    'frapontillo.bootstrap-switch',
    'taskController',
    'userController'
]);

mSharpApp.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.when("", "/");
        $urlRouterProvider.when("/", "/manage");

        $stateProvider.state('manage', {
            url: "/manage",
            templateUrl: 'assets/manage/html/index.html',
            controller: 'indexCtrl'
        }).state('login', {
            url: "/site/login",
            templateUrl: 'assets/manage/html/site/login.html',
            controller: 'loginCtrl'
        }).state('empty', {
            url: "/empty",
            templateUrl: 'assets/manage/html/empty.html',
            // controller: 'loginCtrl'
        })
    }]);


mSharpApp.controller('indexCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload, $state, $interval) {
})

mSharpApp.controller('loginCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload, $state, $interval) {


    $scope.sendSMSBtnValue = "获取验证码"
    $scope.sendSms = function () {
        $scope.setSendSMSBtnDisabled()
        if ($scope.tel == undefined || $scope.tel == "") {
            showPromptMsg_Warning("请输入手机号码")
            $scope.setSendSMSBtnEnabled()
            return false;
        }
        loadingStart()
        $http({
            url: 'manage/sendLoginSms',
            data: {
                tel: $scope.tel,
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.startTiming()
                showPromptMsg_Success(data.data)
            } else {
                $scope.setSendSMSBtnEnabled()
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }


    //按钮禁止使用
    $scope.setSendSMSBtnDisabled = function () {
        $("#sendSMSBtn").attr("disabled", "disabled");
        $scope.sendSMSBtnValue = "正在发送"
    }
    //按钮可以使用
    $scope.setSendSMSBtnEnabled = function () {
        $('#sendSMSBtn').removeAttr("disabled");
        $scope.sendSMSBtnValue = "重新发送"
    }


    $scope.startTiming = function () {
        var time = 60;
        $interval(function (count) {
            $scope.sendSMSBtnValue = time + "s";
            time--
            if (time == 0) {
                $scope.setSendSMSBtnEnabled()
                $scope.sendSMSBtnValue = "重新发送"
            }
        }, 1000, 60);
    }


    $scope.login = function () {
        if ($scope.tel == undefined || $scope.tel == "") {
            showPromptMsg_Warning("请输入手机号码")
            return false;
        }
        if ($scope.sms == undefined || $scope.sms == "") {
            showPromptMsg_Warning("请输入短信验证码")
            return false;
        }

        loadingStart()
        $http({
            url: 'manage/login',
            data: {
                tel: $scope.tel,
                sms: $scope.sms,
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $rootScope.loginInfo.login = true
                $state.go("manage.taskList")
                showPromptMsg_Success(data.data)
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }
})


mSharpApp.directive('headFront', function () {
    return {
        restrict: 'EA',
        scope: true,
        templateUrl: 'assets/manage/html/directives/head.html',
        controller: 'directiveHeadController'
    };
})

mSharpApp.controller('directiveHeadController', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload, $state, $interval) {
})


mSharpApp.directive('menu', function () {
    return {
        restrict: 'EA',
        templateUrl: 'assets/manage/html/directives/menu.html',
        // controller: 'directiveSearchMyBusinessCtrl',
    };
})


mSharpApp.directive('loadingChangeRoute', ['$rootScope', '$http', '$state',
    function ($rootScope, $http, $state) {
        return {
            link: function (scope, element) {
                //检查路径，如果在范围内，则无需进行登录即可查看
                function checkUrl(name) {

                    var list = ["login", "empty"]

                    var flag = false;
                    $.each(list, function (index, ele) {
                        if (ele == name) {
                            flag = true
                        }
                    })
                    return flag
                }

                function operate(info, toState, toParams, event) {
                    if (info.login) {

                    } else {
                        //没有登录，只能通过允许路径，否则去登录注册页面
                        if (!checkUrl(toState.name)) {
                            $state.go('empty')
                            event.preventDefault();

                        }
                    }
                }


                $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

                    if (!$rootScope.loginInfo) {
                        $http({
                            url: 'manage/site/getLoginInfo',
                            method: 'POST'
                        }).success(function (data) {
                            if (data.success) {
                                $rootScope.loginInfo = data.data;
                                operate($rootScope.loginInfo, toState, toParams, event)
                            } else {
                                showPromptMsg_Warning(data.data)
                            }
                        })
                    } else {
                        operate($rootScope.loginInfo, toState, toParams, event)
                    }
                    loadingStart()
                })


                $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                    $(".modal-backdrop").hide()
                    loadingEnd()
                })
            }
        };
    }]
);