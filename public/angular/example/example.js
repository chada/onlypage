/**
 * Created by Administrator on 2016/4/22.
 */
var exampleController = angular.module('exampleController', []);

exampleController.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.when('/example/bootstrap', {
            templateUrl: 'assets/angular/example/html/bootstrap_table.html',
            controller: 'exampleBootstrapTableCtrl'
        }).when('/example/pullAndPush', {
            templateUrl: 'assets/angular/example/html/pullAndPush.html',
            controller: 'examplePullAndPushCtrl'
        })
    }]);


exampleController.controller('exampleBootstrapTableCtrl', function ($scope, $http, $location, $rootScope, $timeout, $routeParams) {
    paginatorReq("example/bootstrap/data", "exampleBootstrapTableCtrl", 10, 3, $scope, $http, function (data) {
        console.log(data.data)
        $scope.dataList = data.data
    })
})
exampleController.controller('examplePullAndPushCtrl', function ($scope, $http, $location, $rootScope, $timeout, $routeParams) {
    $scope.dataList =[1,2,3,4,5,6,7]
    var num = 8
    $scope.getNextData = function(){
        $scope.dataList.push(num++)
        $scope.dataList.push(num++)
        $scope.dataList.push(num++)
        $scope.dataList.push(num++)
        $scope.dataList.push(num++)
        $scope.dataList.push(num++)
        console.log("next")
    }

})
