/**
 * Created by Administrator on 2016/4/11.
 */

var commonDirectives = angular.module('commonDirectives', []);

commonDirectives.directive('afterRepeatDomCreate', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$eval(attr.afterRepeatDomCreate)
                });
            }
        }
    };
});


commonDirectives.directive('afterDomCreate', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            $timeout(function () {
                scope.$eval(attr.afterDomCreate)
            });
        }
    };
});

commonDirectives.directive('loading', function () {
    return {
        restrict: 'E',
        templateUrl: '/assets/angular/html/directives/loading.html'
    };
})
//路由切换loading
// commonDirectives.directive('loadingChangeRoute', ['$rootScope', '$http', '$state',
//     function ($rootScope, $http, $state) {
//         return {
//             link: function (scope, element) {
//
//
//                 $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
//
//
//                 })
//
//
//                 $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
//                     $(".modal-backdrop").hide()
//                 })
//             }
//         };
//     }]
// );


commonDirectives.directive('backgroundImage', function () {
    return function (scope, element, attrs) {
        element.css({
            'background-image': 'url(' + attrs.backgroundImage + ')',
        });
    };
});

commonDirectives.directive('confirm', function () {
    return {
        restrict: 'E',  // used E because of element
        templateUrl: '/assets/angular/html/directives/confirm.html'
    };
});

commonDirectives.directive('paginatorTable', function () {
    return {
        restrict: 'E',  // used E because of element
        templateUrl: '/assets/angular/html/directives/paginator/paginatorTable.html',
        controller: 'paginatorCtrl',
        scope: {
            tag: '@tag',//tag
            url: '@url',//请求地址
            data: '=data',//请求参数
            pageSize: '@pagesize',//每页显示数据
            pageLength: '@pagelength',//页面显示数目
            callback: '&callback',  //数据获取后的回调操作
            operate: '=operate', //定义操作
            autoGetData: '@autogetdata' //是否自动获取数据（页面初始加载）
        }
    };
});

commonDirectives.directive('paginator', function () {
    return {
        restrict: 'E',  // used E because of element
        controller: 'paginatorCtrl',
        scope: {
            tag: '@tag',//tag
            url: '@url',//请求地址
            data: '=data',//请求参数
            pageSize: '@pagesize',//每页显示数据
            pageLength: '@pagelength',//页面显示数目
            callback: '&callback',  //数据获取后的回调操作
            operate: '=operate',//定义操作
            nextTargetId: '@nexttargetid',//单独绑定加载下一次按钮对象
            autoGetData: '@autogetdata' //是否自动获取数据（页面初始加载）
        }
    };
});

commonDirectives.controller('paginatorCtrl', function ($scope, $http, $location, $rootScope,$timeout) {
    // if (sessionStorage.getItem($scope.tag) == "" || sessionStorage.getItem($scope.tag) == null) {
    //     sessionStorage.setItem($scope.tag, 1);
    // }
    sessionStorage.setItem($scope.tag, 1);

    $scope.reqData = function () {
        loadingStart()
        $http({
            url: $scope.url,
            data: $.extend({
                pageNum: sessionStorage.getItem($scope.tag),
                pageSize: $scope.pageSize,
            },$scope.data),

            method: "POST"
        }).success(function (data) {
            if (data.success) {
                $scope.pageCount = data.data.PageCount
                $scope.pageList = []

                var currP = parseInt(sessionStorage.getItem($scope.tag));
                var len = parseInt($scope.pageLength);
                var pageSum = parseInt($scope.pageCount);

                if (currP > 1) {
                    if (currP - 1 + len <= pageSum) {
                        for (var i = currP - 1; i < currP - 1 + len; i++) {
                            var page = {
                                id: i
                            }
                            $scope.pageList.push(page);
                        }
                    }
                    else {
                        var initPosition = pageSum - len > 0 ? pageSum - len + 1 : 1
                        for (var i = initPosition; i <= pageSum; i++) {
                            var page = {
                                id: i
                            }
                            $scope.pageList.push(page);
                        }
                    }
                }
                else {
                    if (currP + len <= pageSum) {
                        for (var i = currP; i < currP + len; i++) {
                            var page = {
                                id: i
                            }
                            $scope.pageList.push(page);
                        }
                    }
                    else {
                        for (var i = 1; i <= pageSum; i++) {
                            var page = {
                                id: i
                            }
                            $scope.pageList.push(page);
                        }
                    }
                }

                $scope.currPage = currP;
                $scope.page = page;
                $scope.callback(data.data)
            } else {
                showPromptMsg_Warning(data.data)
            }


        }).finally(function () {
            //load结束，不管成功与否
            loadingEnd();
        })
    }
    if(isEmpty($scope.autoGetData)){
        $scope.autoGetData=true;
    }
    if($scope.autoGetData!='false') {
        $scope.reqData()
    }

    //页面切换
    $scope.change = function (num) {
        sessionStorage.setItem($scope.tag, num)
        $scope.currPage = num;
        $scope.reqData()
    }
    
    $scope.inputChange = function (e,page) {
        var keycode = window.event ? e.keyCode : e.which;//获取按键编码
        if (keycode == 13) {
            if(!isInteger(page)){
                showPromptMsg_Warning("请输入正确页码")
                return false
            }

            if(page<1){
                showPromptMsg_Warning("只能从第1页开始查看")
                return false
            }

            if(page>$scope.pageCount){
                showPromptMsg_Warning("只能查看到第"+$scope.pageCount+"页")
                return false
            }

            $scope.change(page)
        }

    }

    $scope.operate = {}
    $scope.operate.refresh = function () {
        $scope.reqData()
    }

    $scope.operate.init = function () {
        $timeout(function(){
            sessionStorage.setItem($scope.tag, 1);
            $scope.reqData()
        },200)
    }

    $scope.operate.changeUrl = function (url) {
        $scope.url = url;
    }


    $scope.operate.changeData = function (data) {
        $scope.data = data;
    }

    $scope.operate.nextPage = function (data) {
        sessionStorage.setItem($scope.tag, parseInt(sessionStorage.getItem($scope.tag)) + 1)
        $scope.reqData()
    }


    //下一页进行绑定
    if ($scope.nextTargetId) {

        $("#" + $scope.nextTargetId).on("click", function () {
            $scope.operate.nextPage()
        })
    }
})


commonDirectives.directive('colorPicker', function () {
    return {
        restrict: 'E',
        templateUrl: '/assets/angular/html/directives/common/colorPicker.html',
        controller: 'colorPickerCtrl',
        scope: {
            colorPickerId:'@colorPickerId',
            colorModel: '=colorModel',//ng-model绑定
            colorMap: '=colorMap',//颜色展示Map
            colorPickerSelectedShowWidth:'@colorPickerSelectedShowWidth',//选择后的显示宽度
            colorPickerSelectedShowHeight:'@colorPickerSelectedShowHeight',//选择后的显示高度
            colorPickerOptionWidth:'@colorPickerOptionWidth',//选项宽度
            colorPickerOptionHeight:'@colorPickerOptionHeight'//选项高度
        }
    };
});

commonDirectives.controller('colorPickerCtrl', function ($scope, $http, $location, $rootScope,$timeout) {
    //初始化展示宽度、高度
    if(isEmpty($scope.colorPickerSelectedShowWidth)){
        $scope.colorPickerSelectedShowWidth="100px";
    }
    if(isEmpty($scope.colorPickerSelectedShowHeight)){
        $scope.colorPickerSelectedShowHeight="20px";
    }
    if(isEmpty($scope.colorPickerOptionWidth)){
        $scope.colorPickerOptionWidth="100px";
    }
    if(isEmpty($scope.colorPickerOptionHeight)){
        $scope.colorPickerOptionHeight="20px";
    }

    //初始化选择颜色
    // if(isEmpty($scope.colorModel)){
    //     $scope.colorModel=$scope.colorMap[0];
    // }

    $scope.selectedShowStyle={
        "width" : $scope.colorPickerSelectedShowWidth,
        "height" : $scope.colorPickerSelectedShowHeight,
        "background-color":$scope.colorModel.value
    };
    $scope.optionShowStyle={
        "width" : $scope.colorPickerOptionWidth,
        "height" : $scope.colorPickerOptionHeight
    };

    $scope.showSelectOptionList=function(){
        $("#"+$scope.colorPickerId).find('.ulDiv').toggleClass('ulShow');
    };

    $scope.selectColorOption=function(color){
        $scope.colorModel=color;
        $('.ulDiv').removeClass('ulShow');
    };

    //监听ng-model动态，改变展示选中颜色
    $scope.$watch('colorModel',function (newvalue,oldvalue) {
        $scope.selectedShowStyle['background-color']=newvalue.value;
    },true)
});

