var consoleController = angular.module('consoleController', []);

consoleController.config(['$stateProvider','$urlRouterProvider',
    function ($stateProvider,$urlRouterProvider) {
        $stateProvider.state('console', {
            url: "/console",
            templateUrl: 'assets/angular/html/console/layout.html',
            controller:"consoleIndexCtrl"
        })
    }]);


consoleController.controller('consoleIndexCtrl', function ($scope, $http, $location, $rootScope, $timeout,$stateParams) {

})


consoleController.directive('menuConsole', function () {
    return {
        restrict: 'EA',
        templateUrl: 'assets/angular/html/directives/menu/console.html',
        // controller: 'directiveSearchMyBusinessCtrl',
    };
})