var strategyController = angular.module('strategyController', []);

strategyController.config(['$stateProvider','$urlRouterProvider',
    function ($stateProvider,$urlRouterProvider) {
        $stateProvider.state('console.strategyIndex', {
            url: "/strategy/index",
            templateUrl: 'assets/angular/html/strategy/index.html',
            controller:"strategyIndexCtrl"
        }).state('console.strategyOwnerList', {
            url: "/strategy/owner/list",
            templateUrl: 'assets/angular/html/strategy/list.html',
            controller:"strategyOwnerListCtrl"
        }).state('console.strategyCreate', {
            url: "/strategy/create",
            templateUrl: 'assets/angular/html/strategy/edit.html',
            controller:"strategyEditCtrl"
        }).state('console.strategyEdit', {
            url: "/strategy/edit?:uuid",
            templateUrl: 'assets/angular/html/strategy/edit.html',
            controller:"strategyEditCtrl"
        }).state('console.strategyPreview', {
            url: "/strategy/preview?:id",
            templateUrl: 'assets/angular/html/strategy/preview.html',
            controller:"strategyPreviewCtrl"
        })
    }]);


strategyController.controller('strategyIndexCtrl', function ($scope, $http, $location, $rootScope, $timeout,$stateParams) {

});

strategyController.controller('strategyOwnerListCtrl', function ($scope, $http, $location, $rootScope, $timeout,$stateParams,$state) {
    $scope.getList = function (data) {
        $scope.list = data
    };

    $scope.delete = function (strategy) {
        commonComfirm({
            title: "提醒",
            message: "确定删除此策略？",
            operate: function (result) {
                if (result) {
                    loadingStart()
                    $http({
                        url: '/strategy/deleteStrategy',
                        data: {
                            uuid: strategy.uuid
                        },
                        method: 'POST'
                    }).success(function (data) {
                        if (data.success) {
                            $scope.getListOperate.refresh()
                            showPromptMsg_Success(data.data)
                        } else {
                            showPromptMsg_Warning(data.data)
                        }


                    }).finally(function () {
                        loadingEnd()
                    })
                } else {

                }
            }
        })
    };


    $scope.edit=function (strategy) {
        loadingStart();
        $http({
            url: '/strategy/editStatus',
            data: {
                uuid: strategy.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $state.go("console.strategyEdit",{uuid:strategy.uuid});
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }

});

strategyController.controller('strategyEditCtrl', function ($scope, $http, $location, $rootScope, $timeout,$stateParams,$state) {
    $scope.isNew = isEmpty($stateParams.uuid)? true : false;

    //策略规则相关
    $scope.strategyTriggerEventList=[
        {name: '进入页面', value: "enterpage"},
        {name: '阅读时间', value: "readtime"},
        {name: '转发', value: "repost"},
        {name: '信息提交', value: "infosubmit"},
        {name: '购买', value: "purchase"},
        {name: '分享到朋友圈', value: "sharetofriendcircle"}
    ];
    $scope.strategyRewardTypeList=[
        {name: '固定金额', value: "fixedamount"},
        {name: '随机金额', value: "randomamount"},
        {name: '积分', value: "points"}
    ];
    $scope.strategyRewardUserTypeList=[
        {name: '自身', value: "self"},
        {name: '上级', value: "up"}
    ];

    //分配条件相关
    $scope.strategyConditionNodeTypeList=[
        {name: '自身', value: "self"},
        {name: '上级', value: "up"}
    ];
    $scope.strategyConditionAllocationConditionsList=[
        {name: '子节点数量', value: "childnode"},
        {name: '地址', value: "location"},
        {name: '标签', value: "tag"},
        {name: '阅读时间', value: "readingtime"},
        {name: '阅读方式', value: "readingstyle"},
        {name: '红包数量限制', value: "redpocketquantity"},
        {name: '红包金额限制', value: "redpocketsum"},
        {name: '分享到朋友圈', value: "sharetofriendcircle"}
    ];
    $scope.strategyConditionTagConditionList=[
        {name: '包含', value: "include"},
        {name: '不包含', value: "exclude"}
    ];
    $scope.strategyConditionReadingStyleList=[
        {name: '普通', value: "normal"},
        {name: '二维码', value: "QRcode"}
    ];

    //设置策略规则列表
    $scope.setStrategyRuleList=function (data) {
        $scope.strategyRuleList=isEmpty(data)?[]:data;
    };

    $scope.deleteStrategyRuleIdList=[];
    $scope.deleteStrategyRuleConditionIdList=[];

    //获取策略信息
    $scope.getStrategyInfo = function () {
        loadingStart();
        $http({
            url: 'strategy/getStrategyInfoByUUID',
            data: {
                uuid: $stateParams.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.strategy = data.data.strategy;
                //金额处理
                $.each($scope.strategy.strategyRuleList, function (index, ele) {
                    if (ele.rewardType != $scope.strategyRewardTypeList[2].value) {//积分不需要处理
                        ele.fixedRewardAmount=ele.fixedRewardAmount/100;
                        ele.minRewardAmount=ele.minRewardAmount/100;
                        ele.maxRewardAmount=ele.maxRewardAmount/100;
                    }
                    $.each(ele.strategyRuleConditionList, function (index, condition) {
                        if (ele.rewardType != $scope.strategyRewardTypeList[2].value) {
                            condition.redpocketsum=condition.redpocketsum/100;
                        }
                    });
                });
                $scope.setStrategyRuleList($scope.strategy.strategyRuleList);
            } else {
                showPromptMsg_Warning(data.data);
                $state.go("console.strategyOwnerList");
            }
        }).finally(function () {
            loadingEnd()
        })
    }

    if ( $scope.isNew) {
        $scope.setStrategyRuleList();
    } else {
        $scope.getStrategyInfo()
    }


    /*******************************************************************
    /****************************规则相关函数***************************
    /******************************************************************/

    //创建策略规则
    $scope.addStrategyRule= function () {
        $scope.strategyRule={triggerEvent:$scope.strategyTriggerEventList[0].value,rewardType:$scope.strategyRewardTypeList[0].value,rewardUserType:$scope.strategyRewardUserTypeList[0].value};
        $("#strategyRuleCreatedModal").modal("show")
    };

    //编辑策略规则
    $scope.editStrategyRule=function (item) {
        $scope.strategyRule= $scope.strategyRuleList[$scope.strategyRuleList.indexOf(item)];
        $scope.strategyRuleIndex=$scope.strategyRuleList.indexOf(item);
        $("#strategyRuleCreatedModal").modal("show")
    };

    //删除策略规则
    $scope.deleteStrategyRule=function (item) {
        $scope.strategyRuleList.splice($scope.strategyRuleList.indexOf(item),1);
        if(!isEmpty(item.id)) {
            $scope.deleteStrategyRuleIdList.push(item.id)
        }
    };

    //保存策略规则
    $scope.saveStrategyRule=function () {
        if($scope.strategyRule.triggerEvent=="readtime"&&isEmpty($scope.strategyRule.readTime)){
            showPromptMsg_Warning("请填写阅读时间");
            return false;
        }

        if(isEmpty($scope.strategyRule.startTriggerNodeLevel)){
            showPromptMsg_Warning("请输入开始节点层级");
            return false;
        }

        if((!isEmpty($scope.strategyRule.endTriggerNodeLevel))&&($scope.strategyRule.endTriggerNodeLevel<$scope.strategyRule.startTriggerNodeLevel)){
            showPromptMsg_Warning("开始节点层级必须小于结束节点层级");
            return false;
        }

        if($scope.strategyRule.rewardType=="randomamount"){
            if(isEmpty($scope.strategyRule.minRewardAmount)){
                showPromptMsg_Warning("请填写奖励最小值");
                return false;
            }
            if(isEmpty($scope.strategyRule.maxRewardAmount)){
                showPromptMsg_Warning("请填写奖励最大值");
                return false;
            }
        }else {
            if(isEmpty($scope.strategyRule.fixedRewardAmount)){
                showPromptMsg_Warning("请填写固定奖励值");
                return false;
            }
        }

        if($scope.strategyRule.rewardUserType=="up"&&isEmpty($scope.strategyRule.upRewardUserNum)){
            showPromptMsg_Warning("请填写奖励类型-上X级节点");
            return false;
        }

        if(isEmpty($scope.strategyRuleIndex)){
            $scope.strategyRule.strategyRuleConditionList=[];
            $scope.strategyRuleList.push($scope.strategyRule);
        }else {
            $scope.strategyRuleList.splice($scope.strategyRuleIndex,1,$scope.strategyRule);
            $scope.strategyRuleIndex=undefined;
        }

        $("#strategyRuleCreatedModal").modal("hide");
    };


    /*******************************************************************
    /****************************规则分配条件相关函数*******************
    /******************************************************************/

    //添加策略规则分配条件
    $scope.addStrategyRuleCondition=function (ruleItem) {
        $scope.strategyRuleConditionRuleIndex=$scope.strategyRuleList.indexOf(ruleItem);
        $scope.strategyRuleCondition={
            conditionNodeType:$scope.strategyConditionNodeTypeList[0].value,
            allocationConditions:$scope.strategyConditionAllocationConditionsList[0].value,
            tagCondition:$scope.strategyConditionTagConditionList[0].value,
            readingstyle:$scope.strategyConditionReadingStyleList[0].value};
        $("#strategyRuleConditionCreatedModal").modal("show")
    };

    //修改策略规则分配条件---暂未使用
    $scope.editStrategyRuleCondition=function (ruleItem,conditionItem) {
        $scope.strategyRuleConditionRuleIndex=$scope.strategyRuleList.indexOf(ruleItem);
        $scope.strategyRuleConditionConditionIndex=$scope.strategyRuleList[$scope.strategyRuleConditionRuleIndex].strategyRuleConditionList.indexOf(conditionItem);
        $scope.strategyRuleCondition=$scope.strategyRuleList[$scope.strategyRuleConditionRuleIndex].strategyRuleConditionList[$scope.strategyRuleConditionConditionIndex];
        $("#strategyRuleConditionCreatedModal").modal("show")
    };

    //删除策略规则分配条件
    $scope.deleteStrategyRuleCondition=function (ruleItem,conditionItem) {
        var ruleIndex=$scope.strategyRuleList.indexOf(ruleItem);
        var conditionIndex=$scope.strategyRuleList[$scope.strategyRuleList.indexOf(ruleItem)].strategyRuleConditionList.indexOf(conditionItem);
        $scope.strategyRuleList[ruleIndex].strategyRuleConditionList.splice(conditionIndex,1);
        if(!isEmpty(conditionItem.id)) {
            $scope.deleteStrategyRuleConditionIdList.push(conditionItem.id)
        }
        console.log($scope.deleteStrategyRuleConditionIdList)
    };

    //保存策略规则分配条件
    $scope.saveStrategyRuleCondition=function () {

        if($scope.strategyRuleCondition.conditionNodeType=="up"&&isEmpty($scope.strategyRuleCondition.upConditionNodeNum)){
            showPromptMsg_Warning("请填写节点触发类型-上X级节点");
            return false;
        }

        switch ($scope.strategyRuleCondition.allocationConditions){
            case "childnode":
                if(isEmpty($scope.strategyRuleCondition.childnodeNum)){
                    showPromptMsg_Warning("请填写子节点个数");
                    return false;
                }
                break;
            case "location":
                if(isEmpty($scope.strategyRuleCondition.longitude)){
                    showPromptMsg_Warning("请填写经度");
                    return false;
                }
                if(isEmpty($scope.strategyRuleCondition.latitude)){
                    showPromptMsg_Warning("请填写纬度");
                    return false;
                }
                if(isEmpty($scope.strategyRuleCondition.locationRange)){
                    showPromptMsg_Warning("请填写范围");
                    return false;
                }
                break;
            case "tag":
                if(isEmpty($scope.strategyRuleCondition.tag)){
                    showPromptMsg_Warning("请填写标签");
                    return false;
                }
                break;
            case "readingtime":
                if(isEmpty($scope.strategyRuleCondition.readingTime)){
                    showPromptMsg_Warning("请填写阅读时间");
                    return false;
                }
                break;
            case "readingstyle":
                break;
        }





        if(isEmpty($scope.strategyRuleConditionConditionIndex)){
            $scope.strategyRuleList[$scope.strategyRuleConditionRuleIndex].strategyRuleConditionList.push($scope.strategyRuleCondition);
        }else {
            $scope.strategyRuleList[$scope.strategyRuleConditionRuleIndex].strategyRuleConditionList.splice($scope.strategyRuleConditionConditionIndex,1,$scope.strategyRuleCondition);
            $scope.strategyRuleConditionRuleIndex=undefined;
            $scope.strategyRuleConditionConditionIndex=undefined;
        }
        $("#strategyRuleConditionCreatedModal").modal("hide");
    }

    //显示标签选择模态框
    $scope.showChooseUserTagEnumModal=function () {
        $("#chooseUserTagEnumModal").modal("show")
    };

    //获取tagEnum列表的请求参数数据
    $scope.userTagEnumListData = {
        queryType: "normal"
    };
    //设置标签列表数据（列表请求返回回调函数）
    $scope.getMyUserTagEnumList = function (data) {
        $scope.userTagEnumList = data;
    };

    //标签列表筛选提交---暂未使用
    $scope.myUserTagEnumListConfirm = function () {
        $scope.userTagEnumListData = {
            queryType: "search",
            tagName: $scope.search_tag == undefined ? null : $scope.search_tag
        };
        $scope.myUserTagEnumListOperate.changeData($scope.userTagEnumListData);
        $scope.myUserTagEnumListOperate.init();
        $('#searchMyUserTagListModal').modal('hide');
    };

    //标签选择
    $scope.chooseTagEnum=function (tagEnum) {
        $scope.strategyRuleCondition.tag=tagEnum.tag;
        $("#chooseUserTagEnumModal").modal("hide")
    };

    $scope.save = function () {
        if (isEmpty($scope.strategy.title)) {
            showPromptMsg_Warning("请输入标题");
            return
        }

        if (isEmpty($scope.strategy.remark)) {
            showPromptMsg_Warning("请输入说明");
            return
        }

        if(isEmpty($scope.strategyRuleList)){
            showPromptMsg_Warning("请添加规则");
            return
        }

        /*****************数据处理******************/
        $.each($scope.strategyRuleList, function (index, ele) {
            if (ele.rewardType != $scope.strategyRewardTypeList[2].value) {
                ele.fixedRewardAmount=ele.fixedRewardAmount*100;
                ele.minRewardAmount=ele.minRewardAmount*100;
                ele.maxRewardAmount=ele.maxRewardAmount*100;
            }
            $.each(ele.strategyRuleConditionList, function (index, condition) {
                if (ele.rewardType != $scope.strategyRewardTypeList[2].value) {
                    condition.redpocketsum=condition.redpocketsum*100;
                }
            });
        });


        loadingStart();
        $http({
            url: 'strategy/editStrategy',
            data: {
                title: $scope.strategy.title,
                remark: $scope.strategy.remark,
                strategyRuleList: JSON.stringify($scope.strategyRuleList),
                deleteStrategyRuleIdList:$scope.deleteStrategyRuleIdList.join(","),
                deleteStrategyRuleConditionIdList:$scope.deleteStrategyRuleConditionIdList.join(","),
                isNew:  $scope.isNew,
                uuid: $stateParams.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
                $state.go("console.strategyOwnerList")
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })

    }
});

strategyController.controller('strategyPreviewCtrl', function ($scope, $http, $location, $rootScope, $timeout,$stateParams,$state) {
    if(isEmpty($stateParams.id)){
        showPromptMsg_Warning("uuid参数错误");
        $state.go("console.strategyOwnerList");
    }

    //策略规则相关
    $scope.strategyTriggerEventList=[
        {name: '进入页面', value: "enterpage"},
        {name: '阅读时间', value: "readtime"},
        {name: '转发', value: "repost"},
        {name: '信息提交', value: "infosubmit"},
        {name: '购买', value: "purchase"}
    ];
    $scope.strategyRewardTypeList=[
        {name: '固定金额', value: "fixedamount"},
        {name: '随机金额', value: "randomamount"},
        {name: '积分', value: "points"}
    ];
    $scope.strategyRewardUserTypeList=[
        {name: '自身', value: "self"},
        {name: '上级', value: "up"}
    ];

    //分配条件相关
    $scope.strategyConditionNodeTypeList=[
        {name: '自身', value: "self"},
        {name: '上级', value: "up"}
    ];
    $scope.strategyConditionAllocationConditionsList=[
        {name: '子节点数量', value: "childnode"},
        // {name: '地址', value: "location"},
        // {name: '标签', value: "tag"},
        {name: '阅读时间', value: "readingtime"},
        {name: '阅读方式', value: "readingstyle"},
        {name: '红包数量限制', value: "redpocketquantity"},
        {name: '红包金额限制', value: "redpocketsum"},
        {name: '分享到朋友圈', value: "sharetofriendcircle"}
    ];
    $scope.strategyConditionTagConditionList=[
        {name: '包含', value: "include"},
        {name: '不包含', value: "exclude"}
    ];
    $scope.strategyConditionReadingStyleList=[
        {name: '普通', value: "normal"},
        {name: '二维码', value: "QRcode"}
    ];

    //设置策略规则列表
    $scope.setStrategyRuleList=function (data) {
        $scope.strategyRuleList=isEmpty(data)?[]:data;
    };

    //获取策略信息
    $scope.getStrategyInfo = function () {
        loadingStart();
        $http({
            url: 'strategy/getStrategyInfoById',
            data: {
                id: $stateParams.id
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.strategy = data.data.strategy;
                //金额处理
                $.each($scope.strategy.strategyRuleList, function (index, ele) {
                    if (ele.rewardType != $scope.strategyRewardTypeList[2].value) {//积分不需要处理
                        ele.fixedRewardAmount=ele.fixedRewardAmount/100;
                        ele.minRewardAmount=ele.minRewardAmount/100;
                        ele.maxRewardAmount=ele.maxRewardAmount/100;
                    }
                    $.each(ele.strategyRuleConditionList, function (index, condition) {
                        if (ele.rewardType != $scope.strategyRewardTypeList[2].value) {
                            condition.redpocketsum=condition.redpocketsum/100;
                        }
                    });
                });
                $scope.setStrategyRuleList($scope.strategy.strategyRuleList);
            } else {
                showPromptMsg_Warning(data.data);
                $state.go("console.strategyOwnerList");
            }
        }).finally(function () {
            loadingEnd()
        })
    };

    $scope.getStrategyInfo()

});
