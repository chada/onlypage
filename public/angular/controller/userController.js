/**
 * Created by Administrator on 2017/1/3.
 */
var userController = angular.module('userController', []);

userController.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider.state('console.userPersonal', {
            url: "/user/personal",
            templateUrl: 'assets/angular/html/user/personal.html',
            controller: "userPersonCtrl"
        }).state('console.userInfoEdit', {
            url: "/userInfo/edit?:avatar:signature:nickname",
            templateUrl: 'assets/angular/html/user/personalEdit.html',
            controller: "userPersonEditCtrl"
        }).state('console.userAccountList', {
            url: "/user/accountList",
            templateUrl: 'assets/angular/html/user/accountList.html',
            controller: "userAccountListCtrl"
        }).state('console.userInfo', {
            url: "/userInfo?:uuid",
            templateUrl: 'assets/angular/html/user/userInfo.html',
            controller: "userInfoCtrl"
        }).state('console.userTagEnumStatisticalInfo', {
            url: "/userTagEnumStatisticalInfo?:tagUUID:tagName",
            templateUrl: 'assets/angular/html/user/userTagEnumStatisticalInfo.html',
            controller: "userTagEnumStatisticalInfoCtrl"
        })
    }]);


userController.directive('directiveWithdrawDeposit', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/user/withdrawDeposit.html',
        controller: 'directiveWithdrawDepositCtrl',
        scope: {
            telephone: '=telephone',
            target: '@target'
        }
    }

});

userController.controller('directiveWithdrawDepositCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $interval) {
    $("#" + $scope.target).on("click", function () {
        if ($scope.telephone) {
            $scope.getBaseInfo()
        } else {
            showPromptMsg_Warning("请先完善你的手机号码信息")
        }
    })


    $scope.getBaseInfo = function () {
        $("#checkTelephoneModal").modal("show")
    }

    $scope.check = function () {
        loadingStart();
        $http({
            url: 'withdrawDeposit/checkCode',
            data: {
                checkCode: $scope.checkCode
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $("#checkTelephoneModal").modal("hide")

                $("#linkUrl").empty()
                $("#linkUrl").qrcode({width: 210, height: 210, correctLevel: 0, text: data.data});
                $("#shareQrcode").modal("show")
            } else {
                showPromptMsg_Warning(data.data);
            }
        }).finally(function () {
            loadingEnd();
        });
    }


    $scope.getUserInfo = function () {
        loadingStart();
        $http({
            url: 'withdrawDeposit/getUserInfo',
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $("#shareQrcode").modal("hide")
                $scope.userInfo = data.data
                $("#withdrawDepositModal").modal("show")
                // console.log($scope.userInfo)
            } else {
                showPromptMsg_Warning(data.data);
            }
        }).finally(function () {
            loadingEnd();
        });
    }

    $scope.withdrawDeposit = function () {
        if ($scope.withdrawDepositMoney == undefined || $scope.withdrawDepositMoney == "") {
            showPromptMsg_Warning("请输入提现金额")
            return false;
        }
        if ($scope.withdrawDepositMoney <= 0) {
            showPromptMsg_Warning("提现金额必须大于0元")
            return false;
        }
        loadingStart();
        $http({
            url: 'withdrawDeposit/pcOperate',
            data: {
                coin: $scope.withdrawDepositMoney * 100
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $("#withdrawDepositModal").modal("hide")
                showPromptMsg_Success(data.data)
            } else {
                showPromptMsg_Warning(data.data);
            }
        }).finally(function () {
            loadingEnd();
        });

    }
})


userController.directive('directiveEditTelephone', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/user/editTelphone.html',
        controller: 'directiveEditTelephoneCtrl',
        scope: {
            oldtelphone: '=oldtelphone',
            target: '@target'
        }
    }

});

userController.controller('directiveEditTelephoneCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $interval) {


    $("#" + $scope.target).on("click", function () {
        if ($scope.oldtelphone) {
            $("#checkOldTelephoneModal").modal("show")
        } else {
            $("#newTelephoneModal").modal("show")
        }
    })

    $scope.showChangeTelephoneModal = function () {
        console.log($scope.oldTelphone)
        $("#checkOldTelephoneModal").modal("show")
    }


    $scope.checkOldTelephone = function () {
        loadingStart();
        $http({
            url: 'user/checkOldTelephone',
            data: {
                checkCode: $scope.checkCode
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $("#checkOldTelephoneModal").modal("hide")
                $("#newTelephoneModal").modal("show")
                showPromptMsg_Success(data.data);
            } else {
                showPromptMsg_Warning(data.data);
            }
        }).finally(function () {
            loadingEnd();
        });
    }


    $scope.changeTelephone = function () {
        loadingStart();
        $http({
            url: 'user/changeTelephone',
            data: {
                checkCode: $scope.newPhoneCheckCode,
                phone: $scope.newPhone
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.oldtelphone = $scope.newPhone
                $("#newTelephoneModal").modal("hide")
                showPromptMsg_Success(data.data);
            } else {
                showPromptMsg_Warning(data.data);
            }
        }).finally(function () {
            loadingEnd();
        });
    }
})

userController.controller('userPersonCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $interval, $state) {
    //获取当前用户的信息
    $http({
        url: 'user/getPersonalInfo',
        method: 'POST'
    }).success(function (data) {
        if (data.success) {
            $scope.userInfo = data.data;
        } else {
            showPromptMsg_Warning(data.data);
        }
    })


    //点击账单按钮
    $scope.tolist = function () {
        $state.go("console.userAccountList");//跳到查询记录的页面
    }


    $scope.tab = "gain";
    $scope.myGain = function () {
        $scope.tab = "gain";
    }
    $scope.mySend = function () {

        $scope.tab = "send";
    }
    $scope.myTag = function () {

        $scope.tab = "tag";
    }

    $scope.edit = function () {
        $state.go('console.userInfoEdit', {
            avatar: $scope.userInfo.avatar,
            signature: $scope.userInfo.signature,
            nickname: $scope.userInfo.nickname
        });
    };


})

userController.controller('userPersonEditCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $state, $upload) {
    $scope.visible = true;
    $scope.toggle = function () {
        $scope.visible = !$scope.visible;
    }

    $scope.isEye = false;
    $scope.canShow = function () {
        $scope.isEye = !$scope.isEye;
    }

    //上传头像
    $scope.uploadAvatar = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if (!file.name.match(/.jpg|.gif|.png|.bmp/i)) {
                    showPromptMsg_Warning('图片格式无效！目前只支持jpg gif png bmp格式，请重新上传');
                    loadingEnd();
                    return false;
                }
            }
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                loadingStart()
                $upload.upload({
                    url: 'upload/image/url',
                    data: {tag: 'user'},
                    file: file
                }).progress(function (evt) {
                }).success(function (data, status, headers, config) {
                    $scope.avatar = data.data
                }).finally(function () {
                    loadingEnd()
                });
            }
        }
    };
    $scope.avatar = $stateParams.avatar;
    $scope.nickname = $stateParams.nickname;
    $scope.signature = $stateParams.signature;

    //修改后保存
    $scope.save = function () {

        loadingStart();
        $http({
            url: 'user/personalInfoEdit',
            data: {
                nickname: $scope.nickname == undefined ? "" : $scope.nickname,
                avatar: $scope.avatar == undefined ? "" : $scope.avatar,
                remark: $scope.signature == undefined ? "" : $scope.signature
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data);
                $state.go('console.userPersonal');
            } else {
                showPromptMsg_Warning(data.data);
            }
        }).finally(function () {
            loadingEnd();
        });
    };

})

userController.controller('userAccountListCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $state) {

    $scope.types = [
        {name: '全部', value: ''},
        {name: '收入', value: 'in'},
        {name: '支出', value: 'out'}
    ]

    $scope.getAccountList = function (data) {
        $scope.logs = data;
    }

    $scope.return = function () {
        $state.go('console.userPersonal');
    }

    $scope.data = {
        tag: "list_account"
    }
    //模态框点击确定
    $scope.confirm = function () {
        // console.log($scope.createdTime)
        // console.log($scope.type)
        $scope.data = {
            tag: "search",
            type: $scope.type.value == undefined ? "null" : $scope.type.value,
            createdTime: $scope.createdTime == undefined ? "null" : $scope.createdTime,
            editedTime: $scope.editedTime == undefined ? "null" : $scope.editedTime
        }
        $scope.getAccountOperate.changeData($scope.data);
        $scope.getAccountOperate.init();
        $('#searchAccountModal').modal('hide');

    }
    //模态框隐藏时，重置模态框内容
    $('#searchAccountModal').on('hide.bs.modal', function () {
        $('select').get(0).selectedIndex = 0;
        $('input').removeAttr("readonly");
        $(':input').each(function () {
            var type = this.type;
            if (type == "text") {
                this.value = "";
            }
        })

        $('input').attr("readonly", "readonly");
    })


    //日期时间选择器
    $('#startTime').datetimepicker({
        dateFormat: 'yy-mm-dd',
        showSecond: true, //显示秒
        timeFormat: 'hh:ii:ss',//格式化时间
        stepHour: 1,//设置步长
        stepMinute: 1,
        stepSecond: 1,
        autoclose: true
    }).on('hide', function (event) {
        if (event && event.preventDefault) {
            event.preventDefault(); //支持DOM标准的浏览器
        } else {
            window.event.returnValue = false;//IE
        }
        event.stopPropagation();
    });

    $('#editedTime').datetimepicker({
        dateFormat: 'yy-mm-dd',
        showSecond: true, //显示秒
        timeFormat: 'hh:ii:ss',//格式化时间
        stepHour: 1,//设置步长
        stepMinute: 1,
        stepSecond: 1,
        autoclose: true
    }).on('hide', function (event) {
        if (event && event.preventDefault) {
            event.preventDefault(); //支持DOM标准的浏览器
        } else {
            window.event.returnValue = false;//IE
        }
        event.stopPropagation();
    });

});

userController.controller('userInfoCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $interval, $state) {
    $scope.uuid = $stateParams.uuid;
    $scope.userTagList=[];

    if(getCurrentUserUUID()==$scope.uuid){
        $state.go("console.userPersonal")
    }


    //获取当前用户的信息
    $http({
        url: 'user/getUserInfo',
        data: {
            uuid: $scope.uuid
        },
        method: 'POST'
    }).success(function (data) {
        if (data.success) {
            $scope.userInfo = data.data;
        } else {
            showPromptMsg_Warning(data.data);
        }
    });


    //获取登录用户给当前用户的标签
    $http({
        url: 'user/getUserTagListByTaggedUserAndCurrentUser',
        data: {
            taggedUserUUID: $scope.uuid
        },
        method: 'POST'
    }).success(function (data) {
        if (data.success) {
            $scope.userTagList = data.data;
        } else {
            showPromptMsg_Warning(data.data);
        }
    });


    $scope.tab = "task";
    $scope.getTaskList = function () {
        $scope.tab = "task";
    };
    $scope.getArticleList = function () {
        $scope.tab = "article";
    }

});

userController.controller('userTagEnumStatisticalInfoCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $interval, $state) {
    $scope.tagUUID = $stateParams.tagUUID;
    $scope.tagName = $stateParams.tagName;
    $scope.userTagList=[];

    //获取列表的请求参数数据
    $scope.userTagListQueryData = {
        queryType: "normal",
        tagUUID:$scope.tagUUID
    };

    //设置标签列表数据（列表请求返回回调函数）
    $scope.getUserTagList = function (data) {
        $scope.userTagList = data;
    };

});

/* 领取的红包*/
userController.directive('directiveUserGainMoney', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/user/gain.html',
        controller: 'directiveUserGainMoneyCtrl'
    }
})

userController.controller('directiveUserGainMoneyCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams) {

    $scope.gainData = {
        tag: "tag"
    }
    $scope.getArticleList = function (data) {
        $scope.articles = data;
    }

    $scope.gainModal = function () {

    }

    $scope.preview = function (url) {
        window.open(url)
    }

    $scope.gainConfirm = function () {
        $scope.gainData = {
            tag: "search",
            title: $scope.title == undefined ? null : $scope.title
        }
        $scope.gainOperate.changeData($scope.gainData);
        $scope.gainOperate.init();
        $('#searchGainModal').modal('hide');

    };
    $('#searchGainModal').on('hide.bs.modal', function () {
        $scope.title = "";
        // console.log(333)
    })

})
/* 发布的红包*/

userController.directive('directiveUserSendMoney', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/user/send.html',
        controller: 'directiveUserSendMoneyCtrl'
    }
})

userController.controller('directiveUserSendMoneyCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams) {

    $scope.states = [
        {name: '全部', value: ''},
        {name: '未审核', value: 'init'},
        {name: '审核中', value: 'checking'},
        {name: '发布中', value: 'publish'},
        {name: '已结束', value: 'finish'},
        {name: '审核不通过', value: 'refuse'},
        {name: '异常', value: 'exception'}
    ]

    //$scope.url = '/user/userSendMoney';
    //$scope.tag = "send"
    $scope.getList = function (data) {
        $scope.tasklist = data;
        //  console.log(data)
    }

    $scope.sendData = {
        tag: "tag"
    }

    $scope.confirm = function () {
        $scope.sendData = {
            tag: "search",
            title: $scope.search_title == undefined ? null : $scope.search_title,
            state: $scope.state.value == undefined ? null : $scope.state.value
        }
        $scope.operate.changeData($scope.sendData)

        //var title = $scope.search_title == undefined ? null : $scope.search_title;
        //var state = $scope.search_state == undefined ? null : $scope.search_state;
        //$scope.tag = "search";
        //$scope.url = '/user/userSendMoney?title=' + title + '&state=' + state;
        //console.log('进来了么')
        //$scope.operate.changeUrl($scope.url)
        $scope.operate.init();
        $('#searchSendModal').modal('hide');

    }
    $('#searchSendModal').on('hide.bs.modal', function () {
        $scope.state = $scope.states[0];
        $scope.search_title = "";
    })

    $scope.sendModal = function () {

    }
})

/* 用户标签列表*/
userController.directive('directiveMyUserTagList', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/user/myUserTagList.html',
        controller: 'directiveMyUserTagListCtrl'
    }
});

userController.controller('directiveMyUserTagListCtrl', function ($scope, $http, $location, $rootScope, $timeout,$state, $stateParams) {
    $scope.tagShowColorSelectMap=[
        {name: '灰色', value: '#777'},
        {name: '蓝色', value: '#337ab7'},
        {name: '红色', value: '#d9534f'},
        {name: '绿色', value: '#5cb85c'},
        {name: '蓝色2', value: '#5bc0de'},
        {name: '黄色', value: '#f0ad4e'}
    ];

    $scope.tagTypeQueryMap = [
        {name: '全部', value: ''},
        {name: '全局', value: 'global'},
        {name: '自定义', value: 'customize'}
    ];

    //获取列表的请求参数数据
    $scope.myUserTagListData = {
        queryType: "normal"
    };
    //设置标签列表数据（列表请求返回回调函数）
    $scope.getMyUserTagList = function (data) {
        $scope.myUserTagList = data;
    };

    //标签列表筛选提交
    $scope.myUserTagListConfirm = function () {
        $scope.myUserTagListData = {
            queryType: "search",
            tagName: $scope.search_tag == undefined ? null : $scope.search_tag
        };
        $scope.myUserTagListOperate.changeData($scope.myUserTagListData);
        $scope.myUserTagListOperate.init();
        $('#searchMyUserTagListModal').modal('hide');

    };

    //隐藏筛选Modal时清空筛选条件
    $('#searchMyUserTagListModal').on('hide.bs.modal', function () {
        $scope.search_tag = "";
    });

    $scope.myUserTagListModal = function () {

    };


    //-------------------------------创建标签-----------------------------------
    //标签信息
    $scope.tagInfo={
        color:$scope.tagShowColorSelectMap[0]
    };

    //点击添加标签按钮
    $scope.tagCreateModal = function () {
        //初始化标签信息
        $scope.tagInfo={
            color:$scope.tagShowColorSelectMap[0]
        };
    };

    //保存标签
    $scope.submitCreateTag=function () {
        console.log($scope.tagInfo);
        if(isEmpty($scope.tagInfo.tag)){
            showPromptMsg_Warning("名称不能为空");
            return false;
        }
        if(isEmpty($scope.tagInfo.color.value)){
            showPromptMsg_Warning("颜色不能为空");
            return false;
        }

        loadingStart();

        $http({
            url: 'user/addOrUpdateCustomizeTag',
            data: {
                uuid:isEmpty($scope.tagInfo.uuid)? "" : $scope.tagInfo.uuid,
                tag: isEmpty($scope.tagInfo.tag)? "" : $scope.tagInfo.tag,
                color: isEmpty($scope.tagInfo.color.value)? "" : $scope.tagInfo.color.value
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                if(isEmpty($scope.tagInfo.uuid)) {
                    showPromptMsg_Success("添加成功");
                    $scope.myUserTagList.push(data.data);
                }else {
                    showPromptMsg_Success("编辑成功");
                    $scope.myUserTagListOperate.init();
                }

                $('#tagCreateModal').modal('hide');

                //重置tag创建信息
                $scope.tagInfo={
                    color:$scope.tagShowColorSelectMap[0]
                };

            } else {
                showPromptMsg_Warning(data.data);
            }
        }).finally(function () {
            loadingEnd();
        });
    };

    //编辑标签按钮
    $scope.editCustomizeTag = function (tag) {
        //初始化标签信息
        $scope.tagInfo={
            tag:tag.tag,
            color:{name: '', value: tag.color},
            uuid:tag.uuid
        };
        $("#tagCreateModal").modal("show");
    }

    //删除标签
    $scope.deleteCustomizeTag=function (tag) {
        confirmSwal('删除警告', '删除标签会同时删除该标签相关的用户列表，确定要删除？', function (isConfirm) {
            if (isConfirm) {

                loadingStart();

                $http({
                    url: 'user/deleteCustomizeTag',
                    data: {
                        uuid:isEmpty(tag.uuid)? "" : tag.uuid
                    },
                    method: 'POST'
                }).success(function (data) {
                    if (data.success) {
                        $scope.myUserTagList.splice($scope.myUserTagList.indexOf(tag),1);
                        showPromptMsg_Success("删除成功");
                    } else {
                        showPromptMsg_Warning(data.data);
                    }
                }).finally(function () {
                    loadingEnd();
                });
            }
        });
    }

    //-------------------------------查看相关用户-----------------------------------
    $scope.showRelatedUserByTag=function (tag) {
        $state.go('console.userTagEnumStatisticalInfo', {
            tagUUID: tag.uuid,
            tagName: tag.tag
        });
    }

});

/* 根据uuid获取用户发布的红包*/
userController.directive('directiveUserReleaseTask', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/user/userReleaseTaskList.html',
        controller: 'directiveUserReleaseTaskCtrl'
    }
})

userController.controller('directiveUserReleaseTaskCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams) {

    $scope.states = [
        {name: '全部', value: ''},
        {name: '发布中', value: 'publish'},
        {name: '已结束', value: 'finish'}
    ];

    $scope.getUserTaskList = function (data) {
        $scope.taskList = data;
    };

    $scope.userTaskListData = {
        tag: "tag",
        uuid:$scope.uuid
    };

    $scope.userTaskListConfirm = function () {
        $scope.userTaskListData = {
            tag: "search",
            uuid:$scope.uuid,
            title: $scope.search_title == undefined ? null : $scope.search_title,
            state: $scope.state.value == undefined ? null : $scope.state.value
        };
        $scope.userTaskListOperate.changeData($scope.userTaskListData)

        $scope.userTaskListOperate.init();
        $('#searchUserTaskListModal').modal('hide');

    };
    $('#searchUserTaskListModal').on('hide.bs.modal', function () {
        $scope.state = $scope.states[0];
        $scope.search_title = "";
    });

    $scope.previewTask = function (url) {
        window.open(url)
    };

    $scope.searchUserTaskListModal = function () {

    }
});

/* 根据uuid获取用户发布的文章*/
userController.directive('directiveUserReleaseArticle', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/user/userReleaseArticleList.html',
        controller: 'directiveUserReleaseArticleCtrl'
    }
})

userController.controller('directiveUserReleaseArticleCtrl', function ($scope, $http, $location, $rootScope, $timeout,$state, $stateParams) {
    $scope.getUserArticleList = function (data) {
        $scope.articleList = data;
    };

    $scope.userArticleListData = {
        tag: "tag",
        uuid:$scope.uuid
    };

    $scope.userArticleListConfirm = function () {
        $scope.userArticleListData = {
            tag: "search",
            uuid:$scope.uuid,
            title: $scope.articleTitle == undefined ? null : $scope.articleTitle
        };
        $scope.userArticleListOperate.changeData($scope.userArticleListData)

        $scope.userArticleListOperate.init();
        $('#searchUserArticleListModal').modal('hide');

    };
    $('#searchUserArticleListModal').on('hide.bs.modal', function () {
        $scope.articleTitle = "";
    });

    $scope.previewArticle = function (articleId) {
        window.open( $state.href('articleView', {id:articleId}))
    };

    $scope.searchUserArticleListModal = function () {

    }
});

/* 编辑用户标签*/
userController.directive('directiveTagUser', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/user/tagUser.html',
        controller: 'directiveTagUserCtrl',
        scope: {
            target: '@target',
            tagUserModalChosenTagList: '=tagusermodalchosentaglist'
        }
    }
});

userController.controller('directiveTagUserCtrl', function ($scope, $http, $location, $rootScope, $timeout,$state, $stateParams) {

    $scope.useruuid = $stateParams.uuid;
    $scope.tagUserModalUnChosenTagList=[];

    $("#" + $scope.target).on("click", function () {
        $("#tagUserModal").modal("show")
    });


    //获取当前登录用户可选择的标签列表
    $http({
        url: 'user/getUnChosenUserTagListByTaggedUserAndCurrentUser',
        data: {
            taggedUserUUID: $scope.useruuid
        },
        method: 'POST'
    }).success(function (data) {
        if (data.success) {
            $scope.tagUserModalUnChosenTagList = data.data;
        } else {
            showPromptMsg_Warning(data.data);
        }
    });

    //贴标签
    $scope.attachTag=function(tag){
        loadingStart();
        $http({
            url: 'user/attachTagToUserByCurrentUser',
            data: {
                taggedUserUUID: $scope.useruuid,
                tagUUID: tag.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.tagUserModalUnChosenTagList.splice($scope.tagUserModalUnChosenTagList.indexOf(tag),1);
                $scope.tagUserModalChosenTagList.push(tag);
            } else {
                showPromptMsg_Warning(data.data);
            }
        }).finally(function () {
            loadingEnd();
        });
    };

    //删除标签
    $scope.deleteTag=function(tag){
        loadingStart();
        $http({
            url: 'user/deleteTagToUserByCurrentUser',
            data: {
                taggedUserUUID: $scope.useruuid,
                tagUUID: tag.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.tagUserModalChosenTagList.splice($scope.tagUserModalChosenTagList.indexOf(tag),1);
                $scope.tagUserModalUnChosenTagList.push(tag);
            } else {
                showPromptMsg_Warning(data.data);
            }
        }).finally(function () {
            loadingEnd();
        });
    };



});