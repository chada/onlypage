var testController = angular.module('testController', []);

testController.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider.state('console.test', {
            url: "/test/index",
            templateUrl: 'assets/angular/html/test/test.html',
            controller: "testIndexCtrl"
        }).state('console.strategyTest', {
            url: "/strategyTest/index",
            templateUrl: 'assets/angular/html/test/testStrategyIndex.html',
            controller: "testStrategyIndexCtrl"
        })
    }]);


testController.controller('testIndexCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams) {


    $scope.addUser = function () {
        loadingStart();
        $http({
            url: 'test/addUser',
            data: {},
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }


    $scope.taskTest = function () {
        loadingStart();
        $http({
            url: 'test/taskTest',
            data: {},
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }
});

testController.controller('testStrategyIndexCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams) {

    /***************************默认策略1生成**************************/
    //奖励类型
    $scope.rewardTypeList = [
        {name: '固定金额', value: "fixedamount"},
        {name: '随机金额', value: "randomamount"},
        {name: '积分', value: "points"}
    ];
    $scope.defaultStrategyOne = {};

    $scope.createDefaultStrategyOne = function () {
        //表单信息验证
        if (isEmpty($scope.defaultStrategyOne.startTriggerNodeLevel)) {
            showPromptMsg_Warning("请输入开始节点层级");
            return false;
        }

        if ((!isEmpty($scope.defaultStrategyOne.endTriggerNodeLevel)) && ($scope.defaultStrategyOne.endTriggerNodeLevel < $scope.defaultStrategyOne.startTriggerNodeLevel)) {
            showPromptMsg_Warning("开始节点层级必须小于结束节点层级");
            return false;
        }
        if ($scope.defaultStrategyOne.rewardType == "randomamount") {
            if (isEmpty($scope.defaultStrategyOne.minRewardAmount)) {
                showPromptMsg_Warning("请输入最小值");
                return false;
            }
            if (isEmpty($scope.defaultStrategyOne.maxRewardAmount)) {
                showPromptMsg_Warning("请输入最大值");
                return false;
            }
        } else {
            if (isEmpty($scope.defaultStrategyOne.fixedRewardAmount)) {
                showPromptMsg_Warning("请输入固定奖励值");
                return false;
            }
        }

        loadingStart();

        //如果填了很多奖励的数量，根据奖励类型来进行相应的处理(其他类型的值清空)
        if ($scope.rewardType == "randomamount") {
            $scope.defaultStrategyOne.fixedRewardAmount = undefined;
        } else {
            $scope.defaultStrategyOne.minRewardAmount = undefined;
            $scope.defaultStrategyOne.maxRewardAmount = undefined;
        }

        $http({
            url: 'test/strategy/entrance',
            data: {
                strategyTestType: "createDefaultStrategyOne",
                startTriggerNodeLevel: $scope.defaultStrategyOne.startTriggerNodeLevel,
                endTriggerNodeLevel: $scope.defaultStrategyOne.endTriggerNodeLevel,
                rewardType: $scope.defaultStrategyOne.rewardType,
                fixedRewardAmount: $scope.defaultStrategyOne.fixedRewardAmount,
                minRewardAmount: $scope.defaultStrategyOne.minRewardAmount,
                maxRewardAmount: $scope.defaultStrategyOne.maxRewardAmount
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }


    /***************************策略触发模拟**************************/
    //触发事件
    $scope.triggerEventList = [
        {name: '进入页面', value: "enterpage"},
        {name: '阅读时间', value: "readtime"},
        {name: '转发', value: "repost"},
        {name: '信息提交', value: "infosubmit"},
        {name: '购买', value: "purchase"}
    ];

    $scope.simulationTriggerEventFormData = {};

    $scope.simulationTriggerEventSubmit = function () {
        $http({
            url: 'test/strategy/entrance',
            data: {
                strategyTestType: "triggerEventSimulation",
                userId: $scope.simulationTriggerEventFormData.userId,
                taskId: $scope.simulationTriggerEventFormData.taskId,
                strategyId: $scope.simulationTriggerEventFormData.strategyId,
                triggerEvent: $scope.simulationTriggerEventFormData.triggerEvent,
                readTime: $scope.simulationTriggerEventFormData.readTime
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }


    /***************************策略复制**************************/
    $scope.strategyCopy = {};

    $scope.strategyCopySubmit = function () {

        //表单信息验证
        if (isEmpty($scope.strategyCopy.uuid)) {
            showPromptMsg_Warning("请输入uuid");
            return false;
        }

        $http({
            url: 'test/strategy/entrance',
            data: {
                strategyTestType: "strategyCopy",
                uuid: $scope.strategyCopy.uuid,
                userId: $scope.strategyCopy.userId
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }

});
