var siteController = angular.module('siteController', []);

siteController.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider.state('loginAndRegist', {
            url: "/loginAndRegist",
            templateUrl: 'assets/angular/html/site/loginAndRegist.html',
            controller: "loginAndRegistCtrl"
        }).state('loginAndRegistWithParam', {
            url: "/loginAndRegistWithParam?:name:param",
            templateUrl: 'assets/angular/html/site/loginAndRegist.html',
            controller: "loginAndRegistCtrl"
        })
    }]);


siteController.controller('loginAndRegistCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $state) {
    $scope.name = $stateParams.name

    $scope.param = $stateParams.param == undefined ? "" : JSON.parse($stateParams.param)

    $scope.tag = "login"
    $scope.changeForRegist = function () {
        $scope.tag = "regist"
    }
    $scope.changeForLogin = function () {
        $scope.tag = "login"
    }

    $scope.wechatLogin = function () {
        $scope.getWechatOpenWebLoginUrl()
    }

    $scope.regist = function () {
        loadingStart()
        if ($scope.registUsername == undefined || $scope.registUsername == "") {
            showPromptMsg_Warning("请输入用户名")
            loadingEnd()
            return false
        }
        if ($scope.registPassword == undefined || $scope.registPassword == "") {
            showPromptMsg_Warning("请输入密码")
            loadingEnd()
            return false
        }
        if ($scope.registPassword.length < 8 || $scope.registPassword.length > 32) {
            showPromptMsg_Warning("请输入8~32位密码")
            loadingEnd()
            return false
        }
        if ($scope.registTelphone == undefined || $scope.registTelphone == "") {
            showPromptMsg_Warning("请输入手机号")
            loadingEnd()
            return false
        }

        if ($scope.registTelphone.length != 11) {
            showPromptMsg_Warning("请输入正确的手机号")
            loadingEnd()
            return false
        }
        if ($scope.telephoneCheckCode == undefined || $scope.telephoneCheckCode == "") {
            showPromptMsg_Warning("请输入短信验证码")
            loadingEnd()
            return false
        }

        $http({
            url: "site/regist",
            data: {
                username: $scope.registUsername,
                password: $scope.registPassword,
                telphone: $scope.registTelphone,
                telephoneCheckCode: $scope.telephoneCheckCode,
            },
            method: "POST"
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
                $scope.setLoginInfo()

            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd();
        })
    }


    $scope.setLoginInfo = function () {
        $http({
            url: 'site/getLoginInfo',
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $rootScope.loginInfo = data.data;
                setCurrentUserCookie($rootScope.loginInfo.username,$rootScope.loginInfo.uuid);
                if ($scope.name == undefined) {
                    $location.path("/")
                } else {
                    $state.go($scope.name, $scope.param)
                }
            } else {
                showPromptMsg_Warning(data.data)
            }
        })
    }

    $scope.login = function () {
        loadingStart()
        if ($scope.loginUsernameAndTelphone == undefined || $scope.loginUsernameAndTelphone == "") {
            showPromptMsg_Warning("请输入用户名或者手机号")
            loadingEnd()
            return false
        }
        if ($scope.loginPassword == undefined || $scope.loginPassword == "") {
            showPromptMsg_Warning("请输入密码")
            loadingEnd()
            return false
        }

        $http({
            url: "site/login",
            data: {
                usernameAndTelphone: $scope.loginUsernameAndTelphone,
                password: $scope.loginPassword
            },
            method: "POST"
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
                $scope.setLoginInfo()
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd();
        })
    }


    $scope.getWechatOpenWebLoginUrl = function () {
        loadingStart()
        $http({
            url: 'wechat/open/web/getLoginUrl',
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                window.location.href = data.data
            } else {
                showPromptMsg_Warning(data.data)
                loadingEnd()
            }
        }).finally(function () {

        })
    }


    $scope.getQQOpenWebLoginUrl = function () {
        $http({
            url: 'qq/getLoginUrl',
            method: 'GET'
        }).success(function (data) {
            if (data.success) {
                window.location.href = data.data;
                location.log(data.data);
            } else {
                showPromptMsg_Warning(data.data);
            }
        })
    }
});



