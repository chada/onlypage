var taskController = angular.module('taskController', []);

taskController.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider.state('console.taskCreate', {
            url: "/task/create",
            templateUrl: 'assets/angular/html/task/edit.html',
            controller: "taskEditCtrl"
        }).state('console.taskEdit', {
            url: "/task/edit?:uuid",
            templateUrl: 'assets/angular/html/task/edit.html',
            controller: "taskEditCtrl"
        }).state('console.taskOwnerList', {
            url: "/task/owner/list",
            templateUrl: 'assets/angular/html/task/list.html',
            controller: "taskOwnerListCtrl"
        }).state('front.taskList', {
            url: "/task/list",
            templateUrl: 'assets/angular/html/task/frontList.html',
            controller: "taskFrontListCtrl"
        }).state('console.taskStatisticalInfo', {
            url: "/task/statisticalInfo?:uuid",
            templateUrl: 'assets/angular/html/task/statisticalInfo.html',
            controller: "taskStatisticalInfoCtrl"
        }).state('console.taskViewTagUserInput', {
            url: "/task/viewTagUserInput?:uuid",
            templateUrl: 'assets/angular/html/task/viewTagUserInput.html',
            controller: "taskViewTagUserInputCtrl"
        })
    }]);

taskController.controller('taskViewTagUserInputCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload) {
    $scope.uuid = $stateParams.uuid
    $scope.requestData = {
        uuid: $scope.uuid
    }

    $scope.getList = function (data) {
        $scope.info = data
    }
})

taskController.controller('taskStatisticalInfoCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload) {
    $scope.uuid = $stateParams.uuid

    $scope.getTaskBaseInfo = function () {
        $http({
            url: 'task/statistical/baseInfo',
            data: {
                uuid: $scope.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.baseInfo = data.data;
                // console.log($scope.baseInfo)
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }
    $scope.getTaskBaseInfo();
    $scope.view = function (url) {
        window.open(url)
    }


    $scope.getAccepterListRequestData = {uuid: $scope.uuid}
    $scope.getAccepterList = function (data) {
        $scope.accepterList = data
    }


    $scope.timeLineTagList = [
        {name: '今日数据', value: "today"},
        {name: '昨日数据', value: "yesterday"},
        {name: '最近15天数据', value: "last15"},
        {name: '最近30天数据', value: "last30"}
    ]
    $scope.timeLineTag = $scope.timeLineTagList[0]

    $scope.$watch("timeLineTag", function () {
        $scope.getTimeLineDate()
    })
    $scope.getTimeLineDate = function () {
        $http({
            url: 'task/statistical/timeLine/info',
            data: {
                uuid: $scope.uuid,
                tag: $scope.timeLineTag.value
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                // var width = (data.data.length - 1) * 100 + 250 + "px"
                // $("#timeLine_completer").css("width", width)
                var xAxis_data = []
                var series_data = []
                for (var i = 0; i < data.data.length; i++) {
                    var temp = data.data[i]
                    if ($scope.timeLineTag.value == "today" || $scope.timeLineTag.value == "yesterday") {
                        xAxis_data.push(new Date(temp.startDate).Format("h时"))
                    } else {
                        xAxis_data.push(new Date(temp.startDate).Format("M月d日"))
                    }
                    series_data.push(temp.visitCount)
                }
                var timeLine_completer = echarts.init(document.getElementById('timeLine_completer'));
                var timeLine_completer_option = {
                    tooltip: {
                        trigger: 'axis'
                    },
                    toolbox: {
                        show: false,
                        x: '100',
                        feature: {
                            mark: {show: true},
                            magicType: {show: true, type: ['line', 'bar']},
                            restore: {show: true},
                            saveAsImage: {show: true}
                        }
                    },
                    grid: {
                        top: '10px',
                        left: '10px',
                        right: '40px',
                        bottom: '10px',
                        containLabel: true
                    },
                    calculable: true,
                    xAxis: [
                        {
                            type: 'category',
                            boundaryGap: false,
                            data: xAxis_data
                        }
                    ],

                    yAxis: [
                        {
                            type: 'value'
                        }
                    ],
                    series: [
                        {
                            name: '访问量',
                            type: 'line',
                            stack: '总量',
                            itemStyle: {normal: {areaStyle: {type: 'default'}}},
                            data: series_data
                        }
                    ]
                };


                timeLine_completer.setOption(timeLine_completer_option);
            } else {

            }
        })
    }
    // $scope.getTimeLineDate()

    // ---------------------节点填写信息------------------
    $scope.getPropagationInfoListData = {uuid: $scope.uuid}
    $scope.getPropagationInfoList = function (data) {
        $scope.propagationInfoList = data
    }

    $scope.getPropagationInfoDetailData={
        id:0
    };

    //设置节点填写详细列表数据（列表请求返回回调函数）
    $scope.getPropagationInfoDetailList = function (data) {
        $scope.propagationInfoDetailList = data;
    };

    $scope.showPropagationInfoDetailModal=function (propagationId) {
        $scope.getPropagationInfoDetailData={
            id:propagationId
        };
        $scope.getPropagationInfoDetailListOperate.changeData($scope.getPropagationInfoDetailData);
        $scope.getPropagationInfoDetailListOperate.init();
        $('#paopagationInfoDetailListModal').modal('show');
    }


})

taskController.controller('taskFrontListCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $state, $upload) {
    $scope.list = []
    $scope.getList = function (data) {
        if (data.length == 0) {
            $("#taskNextPage").hide()
        } else {
            $("#taskNextPage").show()
        }
        $.each(data, function (index, ele) {
            $scope.list.push(ele)
        })
    }


    $scope.showTaskStartUrl = function (url) {
        $("#linkUrl").empty()
        $("#linkUrl").qrcode({width: 210, height: 210, correctLevel: 0, text: url});
        $("#shareQrcode").modal("show")
    }

    $scope.view = function (url) {
        window.open(url)
    }

    $scope.viewUserInfo = function (uuid) {
        $state.go('console.userInfo', {
            uuid: uuid
        });
    }
})

taskController.controller('taskOwnerListCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload, $state) {
    $scope.tab = "normal";
    //切换至普通模式列表
    $scope.normalTask = function () {
        $scope.tab = "normal";
    }
    //切换至地推模式列表
    $scope.promotionTask = function () {
        $scope.tab = "promotion";
    }

    /******************************普通模式列表数据获取*************************************/
    //获取列表的请求参数数据
    $scope.myNormalTaskListData = {
        queryType: "search",
        search_taskType:'normal',
        search_taskName:null
    };

    //任务列表筛选提交
    $scope.myNormalTaskListSearchConfirm = function () {
        $scope.myTaskListData = {
            queryType: "search",
            search_taskType:'normal',
            search_taskName: isEmpty($scope.search_taskName)? null : $scope.search_taskName
        };
        $scope.getNormalListOperate.changeData($scope.myTaskListData);
        $scope.getNormalListOperate.init();
        $('#searchMyNormalTaskListModal').modal('hide');

    };

    //任务列表获取返回回调函数
    $scope.getList = function (data) {
        $scope.list = data
    }


    $scope.commitCheck = function (task) {
        loadingStart()
        $http({
            url: 'task/commit/check',
            data: {
                id: task.id
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.currTask = task;
                if ($scope.currTask.coin == 0) {
                    task.state = "publish"
                    showPromptMsg_Success(data.data)
                } else {
                    $scope.payInfo = data.data
                    // console.log($scope.payInfo)
                    $("#linkUrl").empty()
                    $("#linkUrl").qrcode({width: 210, height: 210, correctLevel: 0, text: $scope.payInfo.url});
                    $("#taskCommitCheckModal").modal("show")
                }
            } else {
                showPromptMsg_Warning(data.data)
            }


        }).finally(function () {
            loadingEnd()
        })
    }

    $scope.delete = function (task) {


        commonComfirm({
            title: "提醒",
            message: "确定删除此任务？",
            operate: function (reselt) {
                if (reselt) {
                    loadingStart()
                    $http({
                        url: 'task/delete',
                        data: {
                            uuid: task.uuid
                        },
                        method: 'POST'
                    }).success(function (data) {
                        if (data.success) {
                            $scope.getListOperate.refresh()
                            showPromptMsg_Success(data.data)
                        } else {
                            showPromptMsg_Warning(data.data)
                        }


                    }).finally(function () {
                        loadingEnd()
                    })
                } else {

                }
            }
        })


    }


    $scope.showTaskStartUrl = function (url) {
        $("#shareUrl").empty()
        $("#shareUrl").qrcode({width: 210, height: 210, correctLevel: 0, text: url});
        $("#shareQrcode").modal("show")
    }

    $scope.refresh = function () {
        $scope.getListOperate.refresh()
        $("#taskCommitCheckModal").modal("hide")
    }


    $scope.checkout = function (task) {
        commonComfirm({
            title: "提醒",
            message: "确定强制结算此任务？",
            operate: function (reselt) {
                if (reselt) {
                    loadingStart()
                    $http({
                        url: 'task/checkout',
                        data: {
                            uuid: task.uuid
                        },
                        method: 'POST'
                    }).success(function (data) {
                        if (data.success) {
                            $scope.getListOperate.refresh()
                            showPromptMsg_Success(data.data)
                        } else {
                            showPromptMsg_Warning(data.data)
                        }


                    }).finally(function () {
                        loadingEnd()
                    })
                } else {

                }
            }
        })
    }

    $scope.renew = function (task) {
        $scope.renewTask = task;
        $("#taskRenewModal").modal("show")
    }

    $scope.submitRenewMoney = function () {
        if ($scope.renewMoney <= 0) {
            showPromptMsg_Warning("充值金额必须大于0元")
            return false;
        }
        if ($scope.renewTask == undefined) {
            showPromptMsg_Warning("请选择一个具体的任务")
            return false;
        }

        loadingStart()
        $http({
            url: 'task/renew',
            data: {
                id: $scope.renewTask.id,
                coin: $scope.renewMoney * 100
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.renewInfo = data.data
                // console.log($scope.payInfo)
                $("#renewLinkUrl").empty()
                $("#renewLinkUrl").qrcode({width: 210, height: 210, correctLevel: 0, text: $scope.renewInfo.url});
                $("#taskRenewModal").modal("hide")
                $("#taskRenewQrcodeModal").modal("show")
            } else {
                showPromptMsg_Warning(data.data)
            }


        }).finally(function () {
            loadingEnd()
        })
    }

    $scope.renewRefresh = function () {
        $scope.getListOperate.refresh()
        $("#taskRenewQrcodeModal").modal("hide")
    }


    $scope.edit = function (info) {
        if (info.state == "init") {
            $state.go("console.taskEdit", {uuid: info.uuid})
        } else if (info.state == "publish") {
            $scope.editTask = info
            $scope.getEditInfo(info.uuid)
        }
    }

    $scope.visibleTypeList = [
        {name: '全部可见', value: "all"},
        {name: '好友可见', value: "friend"},
        {name: '自己可见', value: "self"}
    ];

    $scope.getEditInfo = function (uuid) {
        loadingStart();
        $http({
            url: 'task/edit/running/info',
            data: {
                uuid: uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.isShowAdvanceSet = false
                // console.log(data.data)
                $scope.editInfo = data.data

                $.each($scope.visibleTypeList, function (index, ele) {
                    if (ele.value == $scope.editInfo.visibleType) {
                        $scope.editInfo.visibleType = ele
                        return
                    }
                })

                if ($scope.editInfo.viewTag.isShowUserInput) {
                    $scope.tagInfoCustomList = $scope.editInfo.viewTag.tagLable.infoList
                    $scope.tagInfoCustomTitle = $scope.editInfo.viewTag.tagLable.title
                    $scope.tagInfoCustomRemark = $scope.editInfo.viewTag.tagLable.remark
                }

                $("#taskEditModal").modal("show")
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }


    $scope.editSave = function () {
        if ($scope.editInfo.coverImg == undefined || $scope.editInfo.coverImg == "") {
            showPromptMsg_Warning("请上传封面图片")
            return false;
        }
        if ($scope.editInfo.title == undefined || $scope.editInfo.title == "") {
            showPromptMsg_Warning("请输入标题")
            return false;
        }
        if ($scope.editInfo.remark == undefined || $scope.editInfo.remark == "") {
            showPromptMsg_Warning("请输入描述")
            return false;
        }


        if ($scope.editInfo.viewTag.isShow) {
            if ($scope.editInfo.viewTag.isShowUserInput) {
                var tagInfoCustomListData = [];
                $.each($scope.tagInfoCustomList, function (index, ele) {
                    if (ele.checked) {
                        if (ele.rowName == undefined || ele.rowName == "") {
                            showPromptMsg_Warning("请输入自定义属性名")
                            return false
                        } else {
                            tagInfoCustomListData.push({
                                rowName: ele.rowName,
                                type: ele.type,
                                id: ele.id
                            })
                        }
                    }
                })
                if (tagInfoCustomListData.length == 0) {
                    showPromptMsg_Warning("请至少选择一项自定义属性")
                    return false
                }
                if (tagInfoCustomListData.length > 10) {
                    showPromptMsg_Warning("至多选择10条自定义属性")
                    return false
                }

                if ($scope.tagInfoCustomTitle == undefined || $scope.tagInfoCustomTitle == "") {
                    showPromptMsg_Warning("请输入自定义标题")
                    return false
                }

                if ($scope.tagInfoCustomRemark == undefined || $scope.tagInfoCustomRemark == "") {
                    showPromptMsg_Warning("请输入自定义描述")
                    return false
                }
                var tagLable = {
                    title: $scope.tagInfoCustomTitle,
                    remark: $scope.tagInfoCustomRemark,
                    infoList: tagInfoCustomListData
                }
                $scope.editInfo.viewTag.tagLable = tagLable;
            }

        }


        // console.log($scope.editInfo)
        loadingStart();
        $http({
            url: 'task/edit/running/save',
            data: {
                data: JSON.stringify($scope.editInfo)
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
                $scope.getListOperate.refresh()
                $("#taskEditModal").modal("hide")
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }

    //上传任务封面图案
    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if (!file.name.match(/.jpg|.gif|.png|.bmp/i)) {
                    showPromptMsg_Warning('图片格式无效！目前只支持jpg gif png bmp格式，请重新上传');
                    loadingEnd();
                    return false;
                }
            }
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                loadingStart()
                $upload.upload({
                    url: 'upload/image/url',
                    data: {tag: 'taskCoverImg'},
                    file: file
                }).progress(function (evt) {
                }).success(function (data, status, headers, config) {
                    $scope.editInfo.coverImg = data.data
                }).finally(function () {
                    loadingEnd()
                });
            }
        }
    };

    $scope.getCloneStr = function (info) {
        if (info.cloneStr == undefined) {
            loadingStart();
            $http({
                url: 'task/cloneStr',
                data: {
                    uuid: info.uuid
                },
                method: 'POST'
            }).success(function (data) {
                if (data.success) {
                    info.cloneStr = data.data
                    $scope.showCloneStrModal(info)
                } else {
                    showPromptMsg_Warning(data.data)
                }
            }).finally(function () {
                loadingEnd()
            })
        } else {
            $scope.showCloneStrModal(info)
        }
    }

    $scope.showCloneStrModal = function (info) {
        $scope.currCloneTask = info
        $("#showCloneStrModal").modal("show")
    }

    $scope.taskCopy = function (cloneStr) {
        if (cloneStr == undefined || cloneStr == "") {
            showPromptMsg_Warning("请输入复制码")
            return false
        }
        loadingStart();
        $http({
            url: 'task/copy',
            data: {
                cloneStr: cloneStr
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
                $scope.getListOperate.refresh()
                $("#showCloneStrModal").modal("hide")
                $("#showCloneModal").modal("hide")
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }


    $scope.deleteTagInfoCustom = function (index) {
        $scope.tagInfoCustomList.splice(index, 1)
    }
    $scope.addTagInfoCustom = function () {
        if ($scope.tagInfoCustomList.length > 10) {
            showPromptMsg_Warning("最多允许10项")
            return false
        }
        $scope.tagInfoCustomList.push(
            {rowName: "", edit: true, checked: true, type: 'text'}
        )
    }
})

taskController.controller('taskEditCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload, $state) {

    $scope.isNew = $stateParams.uuid == undefined ? true : false
    $scope.isShowAdvanceSet = false
    //任务可见级别
    $scope.visibleTypeList = [
        {name: '全部可见', value: "all"},
        {name: '好友可见', value: "friend"},
        {name: '自己可见', value: "self"}
    ];
    $scope.taskTypeList= [
        {name: '普通模式', value: "normal"},
        {name: '地推模式', value: "promotion"}
    ];

    $scope.tagInfoCustomList = [
        {rowName: "姓名", edit: true, checked: false, type: 'text'},
        {rowName: "电话", edit: false, checked: false, type: 'phone'},
        {rowName: "邮箱", edit: true, checked: false, type: 'text'},
        // {row: "attachment", rowName: " 上传附件（只支持jpg,png,pdf,最大4M）", edit: false, checked: false},
    ]
    $scope.deleteTagInfoCustom = function (index) {
        $scope.tagInfoCustomList.splice(index, 1)
    }
    $scope.addTagInfoCustom = function () {
        if ($scope.tagInfoCustomList.length > 10) {
            showPromptMsg_Warning("最多允许10项")
            return false
        }
        $scope.tagInfoCustomList.push(
            {rowName: "", edit: true, checked: true, type: 'text'}
        )
    }
    //任务内容类型
    $scope.dataTypeList = [
        {name: '自定义连接 ', value: "url"},
        {name: '唯页文章 ', value: "article"},
    ]


    //任务类型
    $scope.changeDataType = function (info, hide) {
        $scope.dataType = info
        $scope.dataShowStr = ""
        $scope.data = ""
        if (info.value == 'article') {
            $("#data").attr("readonly", "readonly")
            if (!hide) {
                $("#articleListModal").modal("show")
            }
        } else {
            $("#data").removeAttr("readonly")
        }
    }

    $scope.clickDataInput = function () {
        if ($scope.dataType.value == 'article') {
            $("#articleListModal").modal("show")
        } else {

        }
    }

    $scope.getArticleList = function (data) {
        $scope.articleList = data
    }
    $scope.selectArticle = function (article) {
        $scope.data = article.id
        $scope.dataShowStr = "文章：" + article.title

    }


    $scope.getStrategyList = function (data) {
        $scope.strategyList = data
    }


    $scope.strategyTypeList = [
        {name: '高级策略', value: 'advancedStrategy',},
        {name: '默认规则一', value: 'defaultStrategyOne',},
    ]

    $scope.selectStrategy = function (type, title, uuid) {
        $scope.strategy = {
            type: type.value,
            title: title,
            uuid: uuid,
            defaultStrategyOneData: {
                startTriggerNodeLevel: 1,
            }
        }
        $("#strategyListModal").modal("hide")
    }


    $scope.getEditInfo = function () {
        loadingStart();
        $http({
            url: "task/edit/info",
            data: {
                uuid: $stateParams.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                var info = data.data
                // console.log(info)
                $scope.coverImg = info.coverImg
                $scope.title = info.title
                $scope.remark = info.remark
                $scope.description = info.description
                $.each($scope.dataTypeList, function (index, ele) {
                    if (ele.value == info.dataType) {
                        $scope.changeDataType(ele, true)
                        if (info.dataType == "url") {
                            $scope.dataShowStr = info.data
                        } else {
                            $scope.selectArticle(info.article)
                        }

                    }
                })

                $.each($scope.visibleTypeList, function (index, ele) {
                    if (ele.value == info.visibleType) {
                        $scope.visibleType = ele
                    }
                })

                $.each($scope.taskTypeList, function (index, ele) {
                    if (ele.value == info.taskType) {
                        $scope.taskType = ele
                    }
                })

                //任务类型暂时不能进行修改
                // $(".taskType").attr("disabled",true);

                $scope.endTime = info.endTime
                $scope.coin = info.coin / 100
                $scope.autoWithdrawDepositMessage = info.autoWithdrawDepositMessage

                if (info.strategyType == $scope.strategyTypeList[1].value) {
                    $scope.selectStrategy($scope.strategyTypeList[1], '默认策略一', 0)
                    $scope.defaultStrategyOneDataInit = info.defaultStrategyOne
                    $scope.defaultStrategyOneDataInit.fixedRewardAmount = $scope.defaultStrategyOneDataInit.fixedRewardAmount / 100
                    $scope.defaultStrategyOneDataInit.minRewardAmount = $scope.defaultStrategyOneDataInit.minRewardAmount / 100
                    $scope.defaultStrategyOneDataInit.maxRewardAmount = $scope.defaultStrategyOneDataInit.maxRewardAmount / 100
                } else if (info.strategyType == $scope.strategyTypeList[0].value) {
                    $scope.selectStrategy($scope.strategyTypeList[0], info.advancedStrategy.title, info.advancedStrategy.uuid)
                }


                $scope.viewTag = info.tag

                if ($scope.viewTag.isShowUserInput) {
                    $scope.tagInfoCustomList = info.tagLable.infoList
                    $scope.tagInfoCustomTitle = info.tagLable.title
                    $scope.tagInfoCustomRemark = info.tagLable.remark
                }

            } else {
                showPromptMsg_Warning(data.data)
                $state.go("console.taskOwnerList")
            }
        }).finally(function () {
            loadingEnd()
        })
    }


    $scope.isNew = $stateParams.uuid == undefined ? true : false
    if ($scope.isNew) {
        $scope.dataType = $scope.dataTypeList[0]
        //任务显示tag
        $scope.viewTag = {
            isShow: false,
            isShowRedPackage: false,
            isShowRedPackageBillBoard: false,
            isShowCreditBillBoard: false,
            isShowUserInput: false
        }
    } else {
        $scope.getEditInfo()
    }


    $scope.save = function () {

        if ($scope.coverImg == undefined || $scope.coverImg == "") {
            showPromptMsg_Warning("请上传封面图片")
            return false;
        }
        if ($scope.title == undefined || $scope.title == "") {
            showPromptMsg_Warning("请输入标题")
            return false;
        }
        if ($scope.remark == undefined || $scope.remark == "") {
            showPromptMsg_Warning("请输入描述")
            return false;
        }

        if ($scope.dataType.value == 'article') {
            if ($scope.data == undefined || $scope.data == "") {
                showPromptMsg_Warning("请输入广告页面地址(选择文章)")
                return false;
            }
        } else if ($scope.dataType.value == 'url') {
            if ($scope.dataShowStr == undefined || $scope.dataShowStr == "") {
                showPromptMsg_Warning("请输入广告页面地址")
                return false;
            }
            $scope.data = $scope.dataShowStr
        }

        if ($scope.endTime == undefined || $scope.endTime === "") {
            showPromptMsg_Warning("请选择红包截止时间")
            return false
        }

        if ($scope.coin == undefined) {
            showPromptMsg_Warning("请输入红包金额")
            return false;
        }
        if ($scope.coin < 0) {
            showPromptMsg_Warning("红包金额不能小于0")
            return false;
        }


        if ($scope.strategy.type == undefined || $scope.strategy.type == "") {
            showPromptMsg_Warning("请选择红包发放规则")
            return false;
        }

        if ($scope.strategy.type == $scope.strategyTypeList[1].value) {
            var result = $scope.defaultStrategyOneCheck()
            if (!result.result) {
                showPromptMsg_Warning((result.msg))
                return false
            }
        }

        if ($scope.viewTag.isShow) {
            if ($scope.viewTag.isShowUserInput) {
                var tagInfoCustomListData = [];
                $.each($scope.tagInfoCustomList, function (index, ele) {
                    if (ele.checked) {
                        if (ele.rowName == undefined || ele.rowName == "") {
                            showPromptMsg_Warning("请输入自定义属性名")
                            return false
                        } else {
                            tagInfoCustomListData.push({
                                rowName: ele.rowName,
                                type: ele.type,
                                id: ele.id
                            })
                        }
                    }
                })
                if (tagInfoCustomListData.length == 0) {
                    showPromptMsg_Warning("请至少选择一项自定义属性")
                    return false
                }
                if (tagInfoCustomListData.length > 10) {
                    showPromptMsg_Warning("至多选择10条自定义属性")
                    return false
                }

                if ($scope.tagInfoCustomTitle == undefined || $scope.tagInfoCustomTitle == "") {
                    showPromptMsg_Warning("请输入自定义标题")
                    return false
                }

                if ($scope.tagInfoCustomRemark == undefined || $scope.tagInfoCustomRemark == "") {
                    showPromptMsg_Warning("请输入自定义描述")
                    return false
                }
                var tagLable = {
                    title: $scope.tagInfoCustomTitle,
                    remark: $scope.tagInfoCustomRemark,
                    infoList: tagInfoCustomListData
                }
                $scope.viewTag.tagLable = tagLable;
            }

        }


        //默认策略一 金额数值需要*100
        $scope.strategy.defaultStrategyOneData.fixedRewardAmount = $scope.strategy.defaultStrategyOneData.fixedRewardAmount * 100
        $scope.strategy.defaultStrategyOneData.minRewardAmount = $scope.strategy.defaultStrategyOneData.minRewardAmount * 100
        $scope.strategy.defaultStrategyOneData.maxRewardAmount = $scope.strategy.defaultStrategyOneData.maxRewardAmount * 100


        loadingStart();
        $http({
            url: 'task/edit',
            data: {
                isNew: $scope.isNew,
                uuid: $stateParams.uuid,
                coverImg: $scope.coverImg,
                title: $scope.title,
                remark: $scope.remark,
                description:$scope.description ? $scope.description : "" ,
                endTime: $scope.endTime + " 23:59:59",
                dataType: $scope.dataType.value,
                data: $scope.data,
                visibleType: $scope.visibleType.value,
                taskType: $scope.taskType.value,
                coin: $scope.coin * 100,
                autoWithdrawDepositMessage: $scope.autoWithdrawDepositMessage,
                strategy: JSON.stringify($scope.strategy),
                viewTag: JSON.stringify($scope.viewTag)
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
                $state.go("console.taskOwnerList")
                // $location.path("/console/task/owner/list")
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }


    //上传任务封面图案
    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if (!file.name.match(/.jpg|.gif|.png|.bmp/i)) {
                    showPromptMsg_Warning('图片格式无效！目前只支持jpg gif png bmp格式，请重新上传');
                    loadingEnd();
                    return false;
                }
            }
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                loadingStart()
                $upload.upload({
                    url: 'upload/image/url',
                    data: {tag: 'taskCoverImg'},
                    file: file
                }).progress(function (evt) {
                }).success(function (data, status, headers, config) {
                    $scope.coverImg = data.data
                }).finally(function () {
                    loadingEnd()
                });
            }
        }
    };


    var now = new Date()
    $('#endtimepicker').datetimepicker({
        format: 'yyyy-mm-dd',
        startDate: now,
        endDate: new Date(now.valueOf() + 15 * 24 * 3600 * 1000),
        maxView: 2,
        minView: 2,
        language: "zh-CN",
        autoclose: true
    }).on('changeDate', function (ev) {

    });
})

taskController.directive('directiveTaskEditStrategyOne', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/task/edit/strategyOne.html',
        controller: 'directiveTaskEditStrategyOneCtrl',
        scope: {
            info: "=info",
            init: "=init",
            check: "=check"
        }
    }
})

taskController.controller('directiveTaskEditStrategyOneCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload) {
    $scope.typeList = [
        {name: "固定金额", value: 'fixedamount'},
        {name: "随机金额", value: 'randomamount'}
    ]

    $scope.$watch("init", function (newData) {
        // console.log(newData)
        if (newData == undefined || newData == "") {
        } else {
            $scope.info.startTriggerNodeLevel = newData.startTriggerNodeLevel
            $scope.info.endTriggerNodeLevel = newData.endTriggerNodeLevel
            $.each($scope.typeList, function (index, ele) {
                if (ele.value == newData.rewardType) {
                    $scope.info.type = ele;
                }
            })

            $scope.info.fixedRewardAmount = newData.fixedRewardAmount
            $scope.info.minRewardAmount = newData.minRewardAmount
            $scope.info.maxRewardAmount = newData.maxRewardAmount
        }
    })

    $scope.check = function () {
        if ($scope.info.startTriggerNodeLevel == undefined || $scope.info.startTriggerNodeLevel == "") {
            $scope.info.startTriggerNodeLevel = 1
            // return returnMsg(false, "请输入策略起始层次")
        }
        // if ($scope.info.endTriggerNodeLevel == undefined || $scope.info.endTriggerNodeLevel == "") {
        //     return returnMsg(false, "请输入策略截止层次")
        // }

        if ($scope.info.endTriggerNodeLevel < $scope.info.startTriggerNodeLevel) {
            return returnMsg(false, "策略起始层次不能大于策略截止层次")
        }

        if ($scope.info.type == undefined || $scope.info.type == "") {
            return returnMsg(false, "请选择红包类型 ")
        }
        if ($scope.info.type == $scope.typeList[0]) {
            if ($scope.info.fixedRewardAmount == undefined || $scope.info.fixedRewardAmount == "") {
                return returnMsg(false, "请输入固定金额 ")
            }
            if ($scope.info.fixedRewardAmount < 0) {
                return returnMsg(false, "固定金额不能小于0")
            }
        }


        if ($scope.info.type == $scope.typeList[1]) {
            if ($scope.info.minRewardAmount == undefined || $scope.info.minRewardAmount == "") {
                return returnMsg(false, "请输入随机金额下限 ")
            }
            if ($scope.info.minRewardAmount < 0) {
                return returnMsg(false, "随机金额下限不能小于0")
            }

            if ($scope.info.maxRewardAmount == undefined || $scope.info.minRewardAmount == "") {
                return returnMsg(false, "请输入随机金额上限 ")
            }
            if ($scope.info.maxRewardAmount < 0) {
                return returnMsg(false, "随机金额上线不能小于0")
            }

            if ($scope.info.maxRewardAmount < $scope.info.minRewardAmount) {
                return returnMsg(false, "随机金额下限不能小于随机金额上线")
            }
        }

        return returnMsg(true, "ok")
    }

    function returnMsg(result, msg) {
        var object = {
            result: result,
            msg: msg
        }
        return object;
    }

})

taskController.directive('directiveTaskPropagation', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/task/propagation.html',
        controller: 'directiveTaskPropagationCtrl',
        scope: {
            uuid: "@uuid"
        }
    }
})

taskController.controller('directiveTaskPropagationCtrl', function ($scope, $http, $location, $rootScope, $timeout, $stateParams, $upload) {
    // console.log($scope.uuid)
    $scope.data = [
        {username: "任务", uuid: '0', open: false}
    ]

    $scope.getData = function (info) {
        loadingStart()
        $http({
            url: 'task/statistical/propagationInfo',
            data: {
                uuid: $scope.uuid,
                propagation: info.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                info.children = data.data
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }

    $scope.showOrHideChildren = function (info) {
        if (!info.open && info.children == undefined) {
            $scope.getData(info)
        }
        info.open = !info.open
    }
    $scope.showOrHideChildren($scope.data[0]);
})

taskController.directive('directiveTaskReward', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/task/reward.html',
        controller: 'directiveTaskRewardCtrl',
        scope: {
            uuid: '@uuid'
        }
    }
});

taskController.controller('directiveTaskRewardCtrl', function ($scope, $http) {
    //三个查询DIV块的总对象
    $scope.info = {
        childrenCount: {
            levelList: [],
            level: '',
            op: '',
            data: '',
            logicOp: ''
        },
        salesCount: {
            levelList: [],
            level: '',
            op: '',
            data: '',
            logicOp: ''
        },
        tagList: []
    };

    $scope.rewardUserList = [];

    //标签查询Div块的下拉选项单独处理
    $scope.tagLevelList = []
    $scope.tagList = []
    $scope.levelMax;

    //获取传播的深度，Level的最大值
    $scope.propagationDepth = function () {
        loadingStart();
        $http({
            url: 'task/statistical/propagationDepth',
            data: {
                uuid: $scope.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                console.log("MaxLevel="+data.data.propagationDepth);
                if (data.data.propagationDepth == 0 || data.data.propagationDepth == 1) {
                    //深度为0或1，表示无人参与或无人阅读，打赏模块还需要显示吗？
                } else {//深度>=2
                    $scope.levelMax = data.data.propagationDepth;
                    //前两个模块查询的层级是针对子层级来讲的，所以最小从二层节点用户开始
                    for (var i = 2; i <= data.data.propagationDepth; i++) {
                        //给子节点和销售量模块层级下拉选项赋值
                        $scope.info.childrenCount.levelList.push({
                            value: i,
                            name: i + "级节点"
                        });
                        $scope.info.salesCount.levelList.push({
                            value: i,
                            name: i + "级节点"
                        });
                    }
                    //给子节点和销售量模块层级下拉选项赋值ALL选项
                    $scope.info.childrenCount.levelList.push({
                        value: -1,
                        name: "所有节点"
                    });
                    $scope.info.salesCount.levelList.push({
                        value: -1,
                        name: "所有节点"
                    });
                    //标签查询模块的层级是针对父层级来讲的，所以最小从一层节点用户开始
                    for (var i = 1; i <= data.data.propagationDepth - 1; i++) {
                        $scope.tagLevelList.push({
                            value: i,
                            name: i + "级节点"
                        })
                    }
                    $scope.tagLevelList.push({
                        value: -1,
                        name: "所有节点"
                    });
                    $scope.getCurrentUserAllTags();
                }
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd();
        });
    }

    $scope.propagationDepth();

    //标签查询Div块新增按钮
    $scope.addTagQuery = function () {
        $scope.info.tagList.push({
            levelList: $scope.tagLevelList,
            level:'',
            data: '',
            tagList: $scope.tagList,
            tag:'',
            logicOp: ''
        })
    }

    //标签当前查询Div块删除按钮
    $scope.deleteCurrentTagQuery = function (index) {
        $scope.info.tagList.splice(index,1)
    }

    //获取当前用户自己创建的标签和系统公共可见的标签
    $scope.getCurrentUserAllTags = function () {
        loadingStart();
        $http({
            url: 'user/getUserTagEnumListByCurrentUser',
            data: {
                pageNum: 1,
                pageSize: 1000,
                queryType: ""
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                for (var i = 0; i < data.data.data.length; i++) {
                    var item = data.data.data[i];
                    $scope.tagList.push({
                        name: item.tag,
                        value: item.id
                    })
                }
                //首次进入页面，调用一下标签查询的新增函数
                $scope.addTagQuery();
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd();
        });
    }

    //根据查询条件向后台查询结果
    $scope.queryRewardUserList = function (queryBody) {
        loadingStart();
        $http({
            url: 'task/statistical/queryRewardUserList',
            data: {
                uuid: $scope.uuid,
                queryBody: queryBody
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.rewardUserList = data.data;
                if(data.data.length == 0) {
                    showPromptMsg_Warning("没有查询到符合条件的结果");
                }
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd();
        });
    }

    //查询按钮
    $scope.queryPropagationNodeUser = function () {
        var queryBody = {
            childrenCount: {
                level: $scope.info.childrenCount.level,
                op: $scope.info.childrenCount.op,
                data: $scope.info.childrenCount.data,
                //logicOp: $scope.info.childrenCount.logicOp
                logicOp: "and"
            },
            salesCount: {
                level: $scope.info.salesCount.level,
                op: $scope.info.salesCount.op,
                data: $scope.info.salesCount.data,
                //logicOp: $scope.info.salesCount.logicOp
                logicOp: "and"
            },
            tagList: []
        }
        for(var i=0;i<$scope.info.tagList.length; i++) {
            queryBody.tagList.push({
                level: $scope.info.tagList[i].level ,
                data: $scope.info.tagList[i].data,
                tag: $scope.info.tagList[i].tag,
                //logicOp: $scope.info.tagList[i].logicOp
                logicOp: "and"
            });
        }
        $scope.queryRewardUserList(queryBody);
    }

    //打赏按钮
    $scope.rewardNodeUser = function(currentRewardUser) {
        $scope.currentRewardUser = currentRewardUser;
        $("#taskRewardModal").modal("show");
    }

    //打赏某个节点用户
    $scope.submitRewardMoney = function () {
        if ($scope.rewardMoney <= 0) {
            showPromptMsg_Warning("打赏金额必须大于0元");
            return false;
        }
        loadingStart();
        $http({
            url: 'task/rewardNodeUser',
            data: {
                uuid: $scope.uuid,
                coin: $scope.rewardMoney * 100,
                desc: $scope.rewardDesc,
                rewardUserUUID: $scope.currentRewardUser.userUUID
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $("#taskRewardModal").modal("hide");
                showPromptMsg_Success(data.data);
            } else {
                if(data.data.errorType == "NotEnoughMoney") {//余额不足，需要充值
                    showPromptMsg_Warning(data.data.msg);
                    //隐藏打赏块，显示充值块
                    $("#taskRewardModal").modal("hide");
                    $("#userRechargeModal").modal("show");
                } else {//其他的fail情况
                    showPromptMsg_Warning(data.data);
                }
            }
        }).finally(function () {
            loadingEnd();
        })
    }

    //余额不足，账号充值
    $scope.submitRechargeMoney = function () {
        if ($scope.rechargeMoney <= 0) {
            showPromptMsg_Warning("充值金额必须大于0元")
            return false;
        }
        loadingStart();
        $http({
            url: 'user/rechargeAccount',
            data: {
                coin: $scope.rechargeMoney * 100
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.rechargeInfo = data.data;
                $("#rechargeLinkUrl").empty();
                $("#rechargeLinkUrl").qrcode({width: 210, height: 210, correctLevel: 0, text: $scope.rechargeInfo.url});
                $("#userRechargeModal").modal("hide");
                $("#userRechargeQrcodeModal").modal("show");
            } else {
                showPromptMsg_Warning(data.data);
            }
        }).finally(function () {
            loadingEnd();
        })
    }

    $scope.rechargeRefresh = function () {
        $("#userRechargeQrcodeModal").modal("hide");
    }


    $scope.data = [
        {username: "", uuid: '0', open: false}
    ];

    //查询子层节点数据
    $scope.getData = function (info) {
        loadingStart();
        $http({
            url: 'task/statistical/propagationInfo',
            data: {
                uuid: $scope.uuid,
                propagation: info.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                info.children = data.data
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }

    //查询父层节点数据，只调用一次
    $scope.getPropagationData = function(info) {
        loadingStart();
        $http({
            url: 'task/statistical/nodeUserpropagationInfo',
            data: {
                uuid: $scope.uuid,
                propagation: info.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                //先给当前info节点正常赋值children
                info.children = data.data.childrenNode;
                //下面开始把 父-当前节点-子 拼接起来
                var parentNodes = data.data.parentNode;
                var length = data.data.parentNode.length;
                if(length == 0) {
                    //如果是一级用户，已经到头了，它的父就是任务
                    $scope.data = [{
                        username: "任务",
                        uuid: 0,
                        open: true,
                        children: new Array(info)
                    }];
                } else {
                    for(var i=0; i<length; i++) {
                        if(i != length-1) {//没到父的最后一个的话，它的子是后一个父，如此循环
                            parentNodes[i].children = new Array(parentNodes[i+1]);
                        } else {//到父的最后的一个的话，它的子就是当前的info节点了
                            parentNodes[i].children = new Array(info);
                        }
                    }
                    //用拼接后的层级数据重置$scope.data
                    $scope.data = new Array(parentNodes[0]);
                }
            } else {
                showPromptMsg_Warning(data.data);
            }
        }).finally(function () {
            loadingEnd();
        })
    };

    $scope.showOrHideChildren = function (info) {
        if (!info.open && info.children == undefined) {
            $scope.getData(info);
        }
        info.open = !info.open;
    };

    $scope.queryRewardUserPropagationData = function(info) {
        $scope.getPropagationData(info);
        info.open = !info.open;
    }

    //查看打赏用户的子父树形节点
    $scope.viewNodeUserTree = function(currentNodeUser) {
        //每次查看子父节点，重置一下$scope.data
        $scope.data = [{username: "", uuid: '0', open: false}];
        //首次请求使用当前节点用户的信息
        $scope.data[0].username = currentNodeUser.username;
        $scope.data[0].uuid = currentNodeUser.uuid;
        $scope.data[0].avatar = currentNodeUser.avatar;
        //查看子父节点按钮，调用请求父层数据的函数，只调用一次，请求子层数据的话，还是用邵锦杰之前的getData函数和后台
        $scope.queryRewardUserPropagationData($scope.data[0]);
        $("#rewardUserPropagationModal").modal("show");
    }
});



taskController.directive('directiveMyPromotionTaskList', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/task/promotion/myPromotionTaskList.html',
        controller: 'directiveMyPromotionTaskListCtrl',
        scope: {}
    }
});

taskController.controller('directiveMyPromotionTaskListCtrl', function ($scope, $http, $location, $rootScope, $timeout,$state, $stateParams) {

    /******************************地推模式列表数据获取*************************************/
    //获取列表的请求参数数据
    $scope.myPromotionTaskListData = {
        queryType: "normal"
    };

    //任务列表筛选提交
    $scope.myPromotionTaskListSearchConfirm = function () {
        $scope.myPromotionTaskListData = {
            queryType: "search",
            search_taskName: isEmpty($scope.promotion_search_taskName)? null : $scope.promotion_search_taskName
        };
        $scope.getPromotionListOperate.changeData($scope.myTaskListData);
        $scope.getPromotionListOperate.init();
        $('#searchMyPromotionTaskListModal').modal('hide');

    };

    //任务列表获取返回回调函数
    $scope.getPromotionList = function (data) {
        $scope.promotionTaskList = data
    };

    //刷新列表
    $scope.promotionTaskListRefresh = function () {
        $scope.getPromotionListOperate.refresh()
    }


//------------------------------------操作函数------------------------------------/

//------------------任务审核----------------------------------
    $scope.promotionTaskCommitCheck = function (task) {
        loadingStart()
        $http({
            url: 'task/commit/check',
            data: {
                id: task.id
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.currTask = task;
                if ($scope.currTask.coin == 0) {
                    task.state = "publish"
                    showPromptMsg_Success(data.data)
                } else {
                    $scope.payInfo = data.data

                    $("#promotionLinkUrl").empty()
                    $("#promotionLinkUrl").qrcode({width: 210, height: 210, correctLevel: 0, text: $scope.payInfo.url});
                    $("#promotionTaskCommitCheckModal").modal("show")
                }
            } else {
                showPromptMsg_Warning(data.data)
            }


        }).finally(function () {
            loadingEnd()
        })
    }

    $scope.promotionRefresh=function () {
        $scope.promotionTaskListRefresh()
        $("#promotionTaskCommitCheckModal").modal("hide")
    }


//------------------任务删除----------------------------------
    $scope.promotionDelete = function (task) {
        commonComfirm({
            title: "提醒",
            message: "确定删除此地推任务？",
            operate: function (reselt) {
                if (reselt) {
                    loadingStart()
                    $http({
                        url: 'task/delete',
                        data: {
                            uuid: task.uuid
                        },
                        method: 'POST'
                    }).success(function (data) {
                        if (data.success) {
                            $scope.promotionTaskListRefresh()
                            showPromptMsg_Success(data.data)
                        } else {
                            showPromptMsg_Warning(data.data)
                        }


                    }).finally(function () {
                        loadingEnd()
                    })
                } else {

                }
            }
        })


    }


//------------------任务分享二维码----------------------------------
    $scope.showPromotionTaskStartUrl = function (url) {
        $("#promotionShareUrl").empty()
        $("#promotionShareUrl").qrcode({width: 210, height: 210, correctLevel: 0, text: url});
        $("#promotionShareQrcode").modal("show")
    }


//------------------强制红包结束----------------------------------
    $scope.promotionCheckout = function (task) {
        commonComfirm({
            title: "提醒",
            message: "确定强制结算此地推任务？",
            operate: function (reselt) {
                if (reselt) {
                    loadingStart()
                    $http({
                        url: 'task/checkout',
                        data: {
                            uuid: task.uuid
                        },
                        method: 'POST'
                    }).success(function (data) {
                        if (data.success) {
                            $scope.promotionTaskListRefresh()
                            showPromptMsg_Success(data.data)
                        } else {
                            showPromptMsg_Warning(data.data)
                        }


                    }).finally(function () {
                        loadingEnd()
                    })
                } else {

                }
            }
        })
    }


//------------------续费----------------------------------
    $scope.promotionRenew = function (task) {
        $scope.renewTask = task;
        $("#promotionTaskRenewModal").modal("show")
    }

    $scope.submitPromotionRenewMoney = function () {
        if ($scope.renewMoney <= 0) {
            showPromptMsg_Warning("充值金额必须大于0元")
            return false;
        }
        if ($scope.renewTask == undefined) {
            showPromptMsg_Warning("请选择一个具体的任务")
            return false;
        }

        loadingStart()
        $http({
            url: 'task/renew',
            data: {
                id: $scope.renewTask.id,
                coin: $scope.renewMoney * 100
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.renewInfo = data.data
                // console.log($scope.payInfo)
                $("#promotionRenewLinkUrl").empty()
                $("#promotionRenewLinkUrl").qrcode({width: 210, height: 210, correctLevel: 0, text: $scope.renewInfo.url});
                $("#promotionTaskRenewModal").modal("hide")
                $("#promotionTaskRenewQrcodeModal").modal("show")
            } else {
                showPromptMsg_Warning(data.data)
            }


        }).finally(function () {
            loadingEnd()
        })
    }

    $scope.promotionRenewRefresh = function () {
        $scope.promotionTaskListRefresh()
        $("#promotionTaskRenewQrcodeModal").modal("hide")
    }


//------------------编辑----------------------------------
    $scope.promotionEdit = function (info) {
        if (info.state == "init") {
            $state.go("console.taskEdit", {uuid: info.uuid})
        } else if (info.state == "publish") {
            $scope.editTask = info
            $scope.getEditInfo(info.uuid)
        }
    }

    $scope.visibleTypeList = [
        {name: '全部可见', value: "all"},
        {name: '好友可见', value: "friend"},
        {name: '自己可见', value: "self"}
    ];

    $scope.getEditInfo = function (uuid) {
        loadingStart();
        $http({
            url: 'task/edit/running/info',
            data: {
                uuid: uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.isShowAdvanceSet = false
                // console.log(data.data)
                $scope.editInfo = data.data

                $.each($scope.visibleTypeList, function (index, ele) {
                    if (ele.value == $scope.editInfo.visibleType) {
                        $scope.editInfo.visibleType = ele
                        return
                    }
                })

                if ($scope.editInfo.viewTag.isShowUserInput) {
                    $scope.tagInfoCustomList = $scope.editInfo.viewTag.tagLable.infoList
                    $scope.tagInfoCustomTitle = $scope.editInfo.viewTag.tagLable.title
                    $scope.tagInfoCustomRemark = $scope.editInfo.viewTag.tagLable.remark
                }

                $("#promotionTaskEditModal").modal("show")
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }

    $scope.promotionEditSave = function () {
        if ($scope.editInfo.coverImg == undefined || $scope.editInfo.coverImg == "") {
            showPromptMsg_Warning("请上传封面图片")
            return false;
        }
        if ($scope.editInfo.title == undefined || $scope.editInfo.title == "") {
            showPromptMsg_Warning("请输入标题")
            return false;
        }
        if ($scope.editInfo.remark == undefined || $scope.editInfo.remark == "") {
            showPromptMsg_Warning("请输入描述")
            return false;
        }


        if ($scope.editInfo.viewTag.isShow) {
            if ($scope.editInfo.viewTag.isShowUserInput) {
                var tagInfoCustomListData = [];
                $.each($scope.tagInfoCustomList, function (index, ele) {
                    if (ele.checked) {
                        if (ele.rowName == undefined || ele.rowName == "") {
                            showPromptMsg_Warning("请输入自定义属性名")
                            return false
                        } else {
                            tagInfoCustomListData.push({
                                rowName: ele.rowName,
                                type: ele.type,
                                id: ele.id
                            })
                        }
                    }
                })
                if (tagInfoCustomListData.length == 0) {
                    showPromptMsg_Warning("请至少选择一项自定义属性")
                    return false
                }
                if (tagInfoCustomListData.length > 10) {
                    showPromptMsg_Warning("至多选择10条自定义属性")
                    return false
                }

                if ($scope.tagInfoCustomTitle == undefined || $scope.tagInfoCustomTitle == "") {
                    showPromptMsg_Warning("请输入自定义标题")
                    return false
                }

                if ($scope.tagInfoCustomRemark == undefined || $scope.tagInfoCustomRemark == "") {
                    showPromptMsg_Warning("请输入自定义描述")
                    return false
                }
                var tagLable = {
                    title: $scope.tagInfoCustomTitle,
                    remark: $scope.tagInfoCustomRemark,
                    infoList: tagInfoCustomListData
                }
                $scope.editInfo.viewTag.tagLable = tagLable;
            }

        }


        // console.log($scope.editInfo)
        loadingStart();
        $http({
            url: 'task/edit/running/save',
            data: {
                data: JSON.stringify($scope.editInfo)
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
                $scope.promotionTaskListRefresh()
                $("#promotionTaskEditModal").modal("hide")
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }

    //上传任务封面图案
    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if (!file.name.match(/.jpg|.gif|.png|.bmp/i)) {
                    showPromptMsg_Warning('图片格式无效！目前只支持jpg gif png bmp格式，请重新上传');
                    loadingEnd();
                    return false;
                }
            }
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                loadingStart()
                $upload.upload({
                    url: 'upload/image/url',
                    data: {tag: 'taskCoverImg'},
                    file: file
                }).progress(function (evt) {
                }).success(function (data, status, headers, config) {
                    $scope.editInfo.coverImg = data.data
                }).finally(function () {
                    loadingEnd()
                });
            }
        }
    };

    $scope.deleteTagInfoCustom = function (index) {
        $scope.tagInfoCustomList.splice(index, 1)
    }

    $scope.addTagInfoCustom = function () {
        if ($scope.tagInfoCustomList.length > 10) {
            showPromptMsg_Warning("最多允许10项")
            return false
        }
        $scope.tagInfoCustomList.push(
            {rowName: "", edit: true, checked: true, type: 'text'}
        )
    }


//------------------拷贝红包----------------------------------
    $scope.getPromotionCloneStr = function (info) {
        if (info.cloneStr == undefined) {
            loadingStart();
            $http({
                url: 'task/cloneStr',
                data: {
                    uuid: info.uuid
                },
                method: 'POST'
            }).success(function (data) {
                if (data.success) {
                    info.cloneStr = data.data
                    $scope.showPromotionCloneStrModal(info)
                } else {
                    showPromptMsg_Warning(data.data)
                }
            }).finally(function () {
                loadingEnd()
            })
        } else {
            $scope.showPromotionCloneStrModal(info)
        }
    }

    $scope.showPromotionCloneStrModal = function (info) {
        $scope.currCloneTask = info
        $("#showPromotionCloneStrModal").modal("show")
    }

    $scope.promotionTaskCopy = function (cloneStr) {
        if (cloneStr == undefined || cloneStr == "") {
            showPromptMsg_Warning("请输入复制码")
            return false
        }
        loadingStart();
        $http({
            url: 'task/copy',
            data: {
                cloneStr: cloneStr
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
                $scope.promotionTaskListRefresh()
                $("#showPromotionCloneStrModal").modal("hide")
                $("#showPromotionCloneModal").modal("hide")
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }


//------------------任务分配----------------------------------
    $scope.userList =[];
    $scope.selectedUser=null;
    $scope.selectedTask=null;

    //任务分配按钮
    $scope.groundPromoterBind=function (task) {
        $scope.selectedTask=task;
        $("#groundPromoterBindModal").find(".modal-title").html($scope.selectedTask.title+"——分配用户选择")
        $("#groundPromoterBindModal").modal("show")
    };

    //获取用户列表的请求参数数据
    $scope.userListQueryData = {
        queryType: "search"
    };

    //用户列表获取返回回调函数
    $scope.getUserList = function (data) {
        $scope.userList = data;
        $scope.selectedUser=null;
    };

    //用户列表筛选提交
    $scope.userListSearchConfirm = function () {
        $scope.userListQueryData = {
            queryType: "search",
            search_username: isEmpty($scope.search_username)? null : $scope.search_username,
            search_userphone: isEmpty($scope.search_userphone)? null : $scope.search_userphone
        };
        $scope.getUserListOperate.changeData($scope.userListQueryData);
        $scope.getUserListOperate.init();
    };

    //用户选择
    $scope.selectUserForPromotion=function (user) {
        $scope.selectedUser=user
    }

    //绑定任务和用户信息
    $scope.bindGroundPromoter=function () {
        console.log($scope.selectedTask)
        console.log("------------------------")
        console.log($scope.selectedUser)

        if(isEmpty($scope.selectedUser)){
            showPromptMsg_Warning("尚未选择用户");
            return false;
        }
        if(isEmpty($scope.selectedTask)){
            showPromptMsg_Warning("尚未选择任务");
            return false;
        }

        confirmSwal('绑定确认', '确认将任务《'+$scope.selectedTask.title+'》分配给"'+$scope.selectedUser.username+'"？', function (isConfirm) {
            if (isConfirm) {
                console.log("ok")
                loadingStart();

                $http({
                    url: 'task/promotion/bindGroundPromoter',
                    data: {
                        taskId:isEmpty($scope.selectedTask.id)? "" : $scope.selectedTask.id,
                        groundPromoterUserId:isEmpty($scope.selectedUser.id)? "" : $scope.selectedUser.id
                    },
                    method: 'POST'
                }).success(function (data) {
                    if (data.success) {
                        $("#groundPromoterBindModal").modal("hide")
                        showPromptMsg_Success("绑定成功");
                        $scope.promotionTaskListRefresh();
                    } else {
                        showPromptMsg_Warning(data.data);
                    }
                }).finally(function () {
                    loadingEnd();
                });
            }
        });
    };


//------------------取消任务分配----------------------------------
    $scope.cancelGroundPromoterBind=function (task) {
        confirmSwal('绑定确认', '确认取消任务《'+task.title+'》的分配？', function (isConfirm) {
            if (isConfirm) {
                loadingStart();

                $http({
                    url: 'task/promotion/cancelBindGroundPromoter',
                    data: {
                        taskId:task.id
                    },
                    method: 'POST'
                }).success(function (data) {
                    if (data.success) {
                        showPromptMsg_Success("解绑成功");
                        $scope.promotionTaskListRefresh();
                    } else {
                        showPromptMsg_Warning(data.data);
                    }
                }).finally(function () {
                    loadingEnd();
                });
            }
        });
    };


//------------------任务信息确认----------------------------------
    $scope.viewPromotionInfo=function (task) {
        loadingStart();
        $http({
            url: '/task/promotion/getPromotionInfo',
            data: {
                taskId: task.id
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.promotionInfo = data.data;
                if(!isEmpty($scope.promotionInfo.promoterLatitude)&&!isEmpty($scope.promotionInfo.promoterLongitude)){
                    var center = new qq.maps.LatLng($scope.promotionInfo.promoterLatitude, $scope.promotionInfo.promoterLongitude);

                    //微信获取的坐标是gps，需要转换成腾讯坐标
                    qq.maps.convertor.translate(center, 1, function(res){
                        latlng = res[0];
                        var map = new qq.maps.Map($("#mapContainer")[0],{
                            center: latlng,
                            zoom: 13
                        });

                        var marker = new qq.maps.Marker({
                            map : map,
                            position : latlng
                        });
                    });
                }

                $("#promotionInfoModal").modal("show")
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })

    }

    $scope.setPromotionComplete = function (task) {
        confirmSwal('确认', '确认任务《'+task.title+'》的信息填写完全？', function (isConfirm) {
            if (isConfirm) {
                loadingStart();
                $http({
                    url: 'task/promotion/setPromotionComplete',
                    data: {
                        taskId:isEmpty(task.id)? "" : task.id
                    },
                    method: 'POST'
                }).success(function (data) {
                    if (data.success) {
                        showPromptMsg_Success("确认成功");
                        $scope.promotionTaskListRefresh()
                    } else {
                        showPromptMsg_Warning(data.data);
                    }
                }).finally(function () {
                    loadingEnd();
                });
            }
        });
    }




});