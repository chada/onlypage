var mSharpApp = angular.module('mSharpApp', [
    'commonDirectives',
    'commonFilters',
    'angularFileUpload',
    'ui.router',
    'siteController',
    'consoleController',
    'strategyController',
    'articleController',
    'taskController',
    'userController',
    'wechatController',
    'frapontillo.bootstrap-switch',
    'infinite-scroll',
    'testController'
]);

mSharpApp.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.when("", "/");
        $urlRouterProvider.when("/", "/front/task/list");

        $stateProvider.state('front', {
            url: "/front",
            templateUrl: 'assets/angular/html/front/layout.html',
            controller: 'frontLayoutCtrl'
        }).state('test', {
            url: "/test",
            templateUrl: 'assets/angular/html/test/test.html',
            controller: 'testCtrl'
        })
    }]);

mSharpApp.controller('testCtrl', function ($scope, $http, $location, $rootScope) {
    $scope.test = function () {

        $http({
            url: 'manage/dataMigration',
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
            } else {
                showPromptMsg_Warning(data.data)
            }
        })

    }
})

mSharpApp.controller('frontLayoutCtrl', function ($scope, $http, $location, $rootScope) {
    $scope.pageNum = 0;
    $scope.pageSize = 10;

})


mSharpApp.directive('foot', function () {
    return {
        restrict: 'EA',
        scope: true,
        templateUrl: 'assets/angular/html/directives/foot.html'
    };
})


mSharpApp.directive('headFront', function () {
    return {
        restrict: 'EA',
        scope: true,
        templateUrl: 'assets/angular/html/directives/head/front.html',
        controller: 'directiveHeadFrontController'
    };
})

mSharpApp.controller('directiveHeadFrontController', function ($scope, $http, $location, $state, $rootScope, $timeout, $stateParams) {

    $scope.personal = function () {
        $state.go("console.userPersonal")
        // $location.path("console/sitePersonal")
    }

    $scope.logout = function () {
        $http({
            url: 'site/logout',
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.setLoginInfo()
            } else {
                showPromptMsg_Warning(data.data)
            }
        })
    }


    $scope.setLoginInfo = function () {
        $rootScope.loginInfo.login = false
        $state.go("loginAndRegist")
    }

    $scope.console = function () {
        $state.go("console.taskOwnerList")
    }
})


mSharpApp.directive('headConsole', function () {
    return {
        restrict: 'EA',
        scope: true,
        templateUrl: 'assets/angular/html/directives/head/console.html',
        controller: 'directiveHeadFrontController'
    };
})

mSharpApp.controller('directiveHeadConsoleController', function ($scope, $http, $location, $rootScope, $timeout, $stateParams) {

})


mSharpApp.directive('headArticle', function () {
    return {
        restrict: 'EA',
        scope: true,
        templateUrl: 'assets/angular/html/directives/head/article.html',
        controller: 'directiveHeadFrontController'
    };
})

mSharpApp.controller('directiveHeadArticleController', function ($scope, $http, $location, $rootScope, $timeout, $stateParams) {

})


mSharpApp.directive('selfTag', function () {
    return {
        restrict: 'EA',
        scope: true,
        templateUrl: 'assets/angular/html/directives/tag.html',
    };
})


mSharpApp.directive('loadingChangeRoute', ['$rootScope', '$http', '$state',
    function ($rootScope, $http, $state) {
        return {
            link: function (scope, element) {
                //检查路径，如果在范围内，则无需进行登录即可查看
                function checkUrl(name) {

                    var list = ["front.taskList", "articleView", "loginAndRegist", "loginAndRegistWithParam", "test"]

                    var flag = false;
                    $.each(list, function (index, ele) {
                        if (ele == name) {
                            flag = true
                        }
                    })
                    return flag
                }

                function operate(info, toState, toParams, event) {
                    if (info.login) {

                    } else {
                        //没有登录，只能通过允许路径，否则去登录注册页面
                        if (!checkUrl(toState.name)) {
                            showPromptMsg_Warning("请先登录账号")
                            $state.go('loginAndRegistWithParam', {name: toState.name, param: JSON.stringify(toParams)})
                            event.preventDefault();

                        }
                    }
                }


                $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

                    if (!$rootScope.loginInfo) {
                        $http({
                            url: 'site/getLoginInfo',
                            method: 'POST'
                        }).success(function (data) {
                            if (data.success) {
                                $rootScope.loginInfo = data.data;
                                setCurrentUserCookie($rootScope.loginInfo.username,$rootScope.loginInfo.uuid);
                                operate($rootScope.loginInfo, toState, toParams, event)
                            } else {
                                showPromptMsg_Warning(data.data)
                            }
                        })
                    } else {
                        operate($rootScope.loginInfo, toState, toParams, event)
                    }

                    loadingStart()
                })


                $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                    $(".modal-backdrop").hide()
                    loadingEnd()
                })
            }
        };
    }]
);


mSharpApp.directive('onlypageAgreement', function () {
    return {
        restrict: 'EA',
        scope: true,
        templateUrl: 'assets/angular/html/directives/agreement.html',
    };
})


mSharpApp.directive('directiveSendSms', function () {
    return {
        restrict: 'EA',
        templateUrl: 'assets/angular/html/directives/site/sendSms.html',
        controller: 'directiveSendSmsCtrl',
        scope: {
            tag: '@tag',
            phone: '=phone'
        }
    };
})

mSharpApp.controller('directiveSendSmsCtrl', function ($scope, $http, $location, $rootScope, $interval) {


    $scope.sendSMSBtnValue = "获取验证码"
    $scope.sendSms = function () {
        $scope.setSendSMSBtnDisabled()
        $http({
            url: 'sendSms',
            data: {
                phone: $scope.phone == undefined ? null : $scope.phone,
                tag: $scope.tag
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
                $scope.startTiming()
            } else {
                $scope.setSendSMSBtnEnabled()
                showPromptMsg_Warning(data.data)
            }
        })
    }


    //按钮禁止使用
    $scope.setSendSMSBtnDisabled = function () {
        $("#sendSMSBtn").attr("disabled", "disabled");
        $scope.sendSMSBtnValue = "正在发送"
    }
    //按钮可以使用
    $scope.setSendSMSBtnEnabled = function () {
        $('#sendSMSBtn').removeAttr("disabled");
        $scope.sendSMSBtnValue = "重新发送"
    }

    //计时
    $scope.startTiming = function () {
        var time = 60;
        $interval(function (count) {
            $scope.sendSMSBtnValue = time + "s";
            time--
            if (time == 0) {
                $scope.setSendSMSBtnEnabled()
                $scope.sendSMSBtnValue = "重新发送"
            }
        }, 1000, 60);
    }
})


mSharpApp.directive('directiveTooltipHtml', function () {
    return {
        restrict: 'EA',
        link: function (scope, elem, attrs) {
            $(elem).tooltip({
                trigger: "hover",
                placement: attrs.tooltipPlacement == undefined ? "top" : attrs.tooltipPlacement,
                html: true,
                template: '<div class="tooltip"><div class="tooltip-arrow"></div>' + attrs.tooltipTemplate + '</div>',
                title: 'title'
            })
        },
    };
})

