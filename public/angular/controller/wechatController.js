var wechatController = angular.module('wechatController', []);

wechatController.config(['$stateProvider','$urlRouterProvider',
    function ($stateProvider,$urlRouterProvider) {
        $stateProvider.state('console.wechatAppList', {
            url: "/wechat/list",
            templateUrl: 'assets/angular/html/wechat/accounts_list.html',
            controller: 'WeChatAppListCtrl'
        }).state('console.wechatAppInfo', {
            url: "/wechat/wechatApp/info?:uuid",
            templateUrl: 'assets/angular/html/wechat/app_info.html',
            controller: 'WeChatAppInfoCtrl'
        }).state('console.wechatAppMenu', {
            url: "/wechat/wechatApp/menu?:uuid",
            templateUrl: 'assets/angular/html/wechat/menu/list.html',
            controller: 'WeChatAppMenuCtrl'
        })
    }]);



wechatController.controller('WeChatAppListCtrl', function ($scope, $http, $location, $rootScope, $timeout,$stateParams,$state) {
    $scope.pageNum = 1;
    $scope.pageSize = 10;
    function reqdata() {
        loadingStart()
        $http({
            url: "wechat/thirdPlatform/wechatApp/list",
            data: {
                pageNum: $scope.pageNum,
                pageSize: $scope.pageSize
            },
            method: 'POST'
        }).success(function (data) {
            $scope.apps = data.data;
        }).finally(function () {
            loadingEnd();
        })
    }

    reqdata()


    $scope.hosting = function (uuid) {
        $location.path("/weixin/hosting/list/" + uuid);
    }

    $scope.appDelete = function (uuid) {
        Common.confirm({
            title: "提醒",
            message: "确定要删除此公众号吗？此账号下所有信息（包括图文素材、关键词、菜单等）均会被删除，且不可撤回！",
            operate: function (reselt) {
                if (reselt) {
                    loadingStart()
                    $http({
                        url: "weixin/delete",
                        data: {uuid: uuid},
                        method: 'POST'
                    }).success(function (data) {
                        showPromptMsg_Success("删除成功")
                        reqdata();
                    }).finally(function () {
                        loadingEnd();
                    })
                } else {

                }
            }
        })
    }

    $scope.create = function () {
        $http({
            url: 'wechat/get/app/create/link/url',
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                window.location.href = data.data;
            }else{
                showPromptMsg_Warning(data.data)
            }
        })
    }


    $scope.pass = function (id) {
        loadingStart()
        $http({
            url: "wechat/hosting/pass",
            data: {
                id: id
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
                reqdata();
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd();
        })
    }

    $scope.refuse = function (id) {
        loadingStart()
        $http({
            url: "wechat/hosting/refuse",
            data: {
                id: id
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data)
                reqdata();
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd();
        })
    }
})


//微信托管公众号信息
wechatController.controller('WeChatAppInfoCtrl', function ($scope, $http, $location, $rootScope, $timeout,$stateParams,$state) {
    $scope.tab = "baseInfo";
    $scope.showBaseInfo=function () {
        $scope.tab = "baseInfo";
    };

    $scope.showMenuInfo=function () {
        $scope.tab = "menuInfo";
    };


    var uuid = $stateParams.uuid;
    $scope.uuid = uuid;


});



//第三方托管微信公众号基本信息
taskController.directive('directiveMyWechatAppBaseInfo', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/wechat/wechatAppBaseInfo.html',
        controller: 'directiveMyWechatAppBaseInfoCtrl',
        scope: {
            uuid: '@uuid'
        }
    }
});

taskController.controller('directiveMyWechatAppBaseInfoCtrl', function ($scope, $http) {

    $scope.wechatAppBaseInfo = {};
    $scope.getWechatAppBaseInfo = function () {
        loadingStart()
        $http({
            url: '/wechat/thirdPlatform/wechatApp/getBaseInfo',
            data: {
                uuid: $scope.uuid
            },
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                $scope.wechatAppBaseInfo = data.data
            } else {
                showPromptMsg_Warning(data.data)
            }
        }).finally(function () {
            loadingEnd()
        })
    }
    $scope.getWechatAppBaseInfo();
});


//第三方托管微信公众号菜单信息
taskController.directive('directiveMyWechatAppMenuInfo', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/angular/html/directives/wechat/wechatAppMenuInfo.html',
        controller: 'directiveMyWechatAppMenuInfoCtrl',
        scope: {
            uuid: '@uuid'
        }
    }
});

wechatController.controller('directiveMyWechatAppMenuInfoCtrl', function ($scope, $http, $location, $rootScope, $timeout,$stateParams,$state) {

    $scope.AllMenus = [];

    $http({
        url: "weixin/menu/get",
        data: {
            uuid: $scope.uuid
        },
        method: 'POST'
    }).success(function (data) {
        var menus = data.data;
        for (var i = 0; i < menus.length; i++) {
            $scope.AllMenus.push({
                name: menus[i].name,
                menuId: $scope.menuId,
                subMenuId: 1,
                subMenu: [],
                dataShow: true,
                data: menus[i].data
            })
            $scope.menuId++;
            for (var j = 0; j < menus[i].subMenu.length; j++) {
                $scope.AllMenus[i].subMenu.push({
                    name: menus[i].subMenu[j].name,
                    data: menus[i].subMenu[j].data,
                    subMenuId: $scope.AllMenus[i].subMenuId
                })
                subMenuId: $scope.AllMenus[i].subMenuId++;
                $scope.AllMenus[i].dataShow = false;
            }
        }

    });


    $scope.addMainMenu = function () {
        if ($scope.AllMenus.length == 3) {
            showPromptMsg_Danger("主菜单不能超过三个")
        } else {
            $scope.AllMenus.push({
                name: "",
                data: "",
                menuId: $scope.menuId,
                subMenuId: 1,
                subMenu: [],
                dataShow: true
            })
            $scope.menuId++;
        }
    }

    $scope.addSubMenu = function (menuId) {
        for (var i = 0; i < $scope.AllMenus.length; i++) {
            if ($scope.AllMenus[i].menuId == menuId) {

                //如果主菜单下有子菜单，则主菜单只有name，没有data
                if ($scope.AllMenus[i].subMenu.length == 0) {
                    $scope.AllMenus[i].dataShow = false;
                    data:"";
                }

                if ($scope.AllMenus[i].subMenu.length == 5) {
                    showPromptMsg_Danger("子菜单不能超过五个")
                } else {
//                    var subMenuId = $scope.AllMenus[i].subMenuId;
                    $scope.AllMenus[i].subMenu.push({
                        name: "",
                        data: "",
                        subMenuId: $scope.AllMenus[i].subMenuId
                    })
                    $scope.AllMenus[i].subMenuId++;
                }
            }
        }
    }

    $scope.deleteSubMenu = function (mainMenuId, subMenuId) {
        for (var i = 0; i < $scope.AllMenus.length; i++) {
            if ($scope.AllMenus[i].menuId == mainMenuId) {

                for (var j = 0; j < $scope.AllMenus[i].subMenu.length; j++) {
                    if ($scope.AllMenus[i].subMenu[j].subMenuId == subMenuId) {
                        $scope.AllMenus[i].subMenu.splice(j, 1);
                    }
                }
                if ($scope.AllMenus[i].subMenu.length == 0) {
                    $scope.AllMenus[i].dataShow = true;
                }


            }
        }
    }


    $scope.deleteMainMenu = function (mainMenuId) {
        for (var i = 0; i < $scope.AllMenus.length; i++) {
            if ($scope.AllMenus[i].menuId == mainMenuId) {
                $scope.AllMenus.splice(i, 1);
            }
        }
    }

    $scope.save = function () {
        for (var i = 0; i < $scope.AllMenus.length; i++) {
            if ($scope.AllMenus[i].name == "") {
                showPromptMsg_Danger("主菜单名不能为空");
                return;
            }
            if ($scope.AllMenus[i].name.replace(/[^\x00-\xff]/g, 'xx').length > 8) {
                showPromptMsg_Warning("主菜单名不能多于4个汉字或8个字母");
                return;
            }
            for (var j = $scope.AllMenus[i].subMenu.length - 1; j >= 0; j--) {
                if ($scope.AllMenus[i].subMenu[j].name == "") {
                    $scope.AllMenus[i].subMenu.splice(j, 1);
                }
                if ($scope.AllMenus[i].subMenu[j].name.replace(/[^\x00-\xff]/g, 'xx').length > 16) {
                    showPromptMsg_Danger("字菜单名不能多于8个汉字或16个字母");
                    return;
                }
            }
            if ($scope.AllMenus[i].subMenu.length == 0) {
                $scope.AllMenus[i].dataShow = true;
            }

        }

        $http({
            url: "weixin/menu/create",
            data: _.extend({
                menus: JSON.stringify($scope.AllMenus),
                appUUID: $scope.appUUID
            }),
            method: 'POST'
        }).success(function (data) {
            if (data.success) {
                showPromptMsg_Success(data.data);
            }
            else {
                showPromptMsg_Warning(data.data)
            }

        });
    }


});