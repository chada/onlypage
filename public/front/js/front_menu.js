/**
 * Created by sjj on 2015/7/3.
 */

$(function () {


    $(".menuOne").on("mouseover", function () {
        $(this).css("background-color", "rgb(103, 110, 117)")
    })


    $(".menuOne").on("mouseout", function () {
        $(this).css("background-color", "#22282E")
    })

    $(".menuOne").on("click", function () {
        var target = $($(this).children(".coin"))
        if (target.hasClass("fa-caret-down")) {
            target.removeClass("fa-caret-down");
            target.addClass("fa-caret-up")
            $(this).siblings("ul").show()

        } else if (target.hasClass("fa-caret-up")) {
            target.removeClass("fa-caret-up");
            target.addClass("fa-caret-down")
            $(this).siblings("ul").hide()

        }
    })


    $(".menuTwo").on("mouseover", function () {
        $(this).css("background-color", "rgb(103, 110, 117)")
    })


    $(".menuTwo").on("mouseout", function () {
        $(this).css("background-color", "")
    })
})