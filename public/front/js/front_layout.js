/**
 * Created by sjj on 2015/7/3.
 */

$(function () {

    var resetFrontMiddleHeight = function () {
        $("#front_layout").height($(window).height());
    }
    resetFrontMiddleHeight()


    $(window).resize(function () {
        resetFrontMiddleHeight()
    });
})